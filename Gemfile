source 'https://rubygems.org'

gem 'rails', '~>3.2.12'
gem 'mongo', '~>1.11.1'
gem 'mongo_mapper', "~>0.12.0"
gem 'bson_ext', '~>1.11.1'
gem 'yajl-ruby'
gem 'em-synchrony'
gem 'em-http-request'
gem 'em-mongo'
gem 'sqlite3'
gem 'jquery-rails', '2.1.4'
gem 'httpclient', '~> 2.5.3.3'
gem 'mongo_cache_store', "~>0.2.3"
gem 'rabl', "~>0.8.4"
gem 'google-analytics-rails'
gem 'rack-oauth2', '1.0.8'
gem 'rack-openid', "~>1.3.1"
gem 'ruby-openid', "~>2.2.3"
gem 'ruby-openid-store-mongo', "~> 0.0.3"
gem 'uuid'
gem 'whenever', :require => false
gem 'omniauth', '1.2.2'
gem 'oauth2', '0.9.4'
gem 'omniauth-openid-connect', '0.1.0'
gem 'openid_connect', '0.7.3'
gem 'daemons-rails'
gem 'thread', :require => false
gem "mustache", "0.99.8"
gem "figaro"
gem 'rest-client'
# TODO: https://github.com/jashkenas/coffeescript/issues/3829
gem "coffee-script-source", "~> 1.8.0"
gem 'rack-cors', :require => 'rack/cors'
gem 'aws-sdk'
gem 'aws_cf_signer'
gem 'utf8-cleaner'
gem 'spreadsheet'

#updates for 5.4
gem 'bunny'
# gem 'net-ssh', '~> 2.9', '>= 2.9.4'
gem 'amq-protocol', '1.9.2'
# gem 'bunny', '1.7.1'

gem "sgtools_zenoss", :git => "git@bitbucket.org:mountdiablo/sgtools_zenoss.git", :branch => 'develop'
gem "service_now-dws", :git => "git@bitbucket.org:mountdiablo/sgtools_sncdws.git", :branch => 'develop'
gem "sgtools_itsm", :git => "git@bitbucket.org:mountdiablo/sgtools_itsm.git", :branch => 'develop' 
gem "pyxie", :git => "git@bitbucket.org:mountdiablo/sgtools_pyxie.git", :branch => 'feature/pyxie_cleanup'
gem "sgtools_ui", :git => "git@bitbucket.org:mountdiablo/sgtools_ui.git", :branch => 'feature/firewall_logs'
gem "sgtools_rails_base", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_base.git", :branch => 'master'
gem "sgtools_rails_rest", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_rest.git", :branch => 'v0.1.2'
gem "sgtools_rails_zenoss", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_zenoss.git", :tag => '0.2.6' 
gem "sgtools_rails_itsm", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_itsm.git", :branch => 'develop' 
gem "sgtools_rails_infoblox", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_infoblox.git", :tag => '0.2.2' 
gem "sgtools_rails_mongodb_logger", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_mongodb_logger.git", :tag => '0.1.7'
gem 'spotlight_client', :git => "git@bitbucket.org:mountdiablo/spotlight_client_ruby.git", :tag => "0.1.0"
gem "sgtools_rails_splunk", :git => "git@bitbucket.org:mountdiablo/sgtools_rails_splunk.git", :branch => "feature/PORTALOMS-3509" 
gem 'cacherank', :git => "git@bitbucket.org:mountdiablo/osstools_rails_cacherank.git", :branch => 'master'
gem 'sgtools_amqp', :git => 'git@bitbucket.org:mountdiablo/sgtools_amqp.git', :branch => 'master', :tag => '1.0.0'
gem 'sgtools_rails_hpoo', :git => 'git@bitbucket.org:mountdiablo/sgtools_rails_hpoo.git', :branch => 'feature/cr2_upgrade'

# gem "sgtools_zenoss", :path => "../sgtools_zenoss"
# gem "service_now-dws", :path => "../sgtools_sncdws"
# gem "sgtools_itsm", :path => "../sgtools_itsm"
# gem "sgtools_ui", :path => "sgtools_gems/ui"
# gem "sgtools_rails_base", :path => "sgtools_gems/rails_base"
# gem "sgtools_rails_rest", :path => "sgtools_gems/rails_rest"
# gem "sgtools_rails_zenoss", :path => "sgtools_gems/rails_zenoss"
# gem "sgtools_rails_infoblox", :path => "sgtools_gems/rails_infoblox"
# gem "sgtools_rails_itsm", :path => "sgtools_gems/rails_itsm"
# gem 'sgtools_rails_mongodb_logger', :path => '../sgtools_rails_mongodb_logger'
# gem 'spotlight_client', :path => "../spotlight_client"
# gem "sgtools_rails_splunk", :path => "sgtools_gems/sgtools_rails_splunk"
# gem "sgtools_amqp", :path => '../sgtools_amqp'
# gem 'sgtools_rails_hpoo' , :path => "sgtools_gems/sgtools_rails_hpoo"
# gem "pyxie", :path => "../sgtools_pyxie"

# For deployment
group :deployment do
  gem 'net-ssh-gateway', "~>1.2.0"
end

# sass 3.3 breaks due to new syntax requirements
group :assets do
  gem 'sass', '~> 3.2.14'
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem "therubyracer", :require => 'v8'
  gem 'uglifier', '>= 1.0.3'
  gem 'jquery-ui-rails', '3.0.0'
  gem 'requirejs-rails', :git => 'git@bitbucket.org:mountdiablo/requirejs-rails.git', :branch => 'sgtools'
end

group :development, :test do
  gem 'capistrano'
  gem 'rvm-capistrano'
  gem 'capistrano-ext'
  gem "rspec-rails", "~> 2.10.1"
  gem "factory_girl_rails", "~> 3.2.0"
  gem 'meta_request', '0.2.1'
  gem 'guard-rspec'
  gem 'guard-test'
  gem 'guard-spork'
  gem 'spork'
  gem 'spork-testunit'
  gem 'ruby-prof'
  gem 'prawn', '~>0.12.0'
end

group :test do
  gem "shoulda-matchers"
  gem "faker", "~> 1.0.1"
  gem "capybara", "~> 1.1.2"
  gem "database_cleaner", "~> 0.7.2"
  gem "launchy", "~> 2.1.0"
  gem 'simplecov', :require => false
end
