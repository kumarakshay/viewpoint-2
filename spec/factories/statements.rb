# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :statement do
    name "MyString"
    description "MyString"
    owner "MyString"
    factory :invalid_statement do
      name nil
    end
  end
end
