# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :req do
    report_id 1
    company_guid "MyString"
    user_guid "MyString"
    report "MyString"
    device_guid "MyString"
    group_guid "MyString"
    email { Faker::Internet.email }
    frequency "MyString"
    time_zone "MyString"
    delivery_time "MyString"
    subscription true
    area "MyString"
    cabinet "MyString"
    factory :invalid_req do
      report_id nil
    end
  end
end

