FactoryGirl.define do
  factory :event_subscription do
    name { Faker::Name.first_name }
    company_guid { Faker::Name.last_name }
    active true
    asset_list ["11","12"]
    email {Faker::Internet.email}
    severity {rand(0..6)}
    all_asset false
  end
end