FactoryGirl.define do
  factory :graph do
    name { Faker::Name.first_name }
    description { Faker::Name.last_name}
    metric {["MKS","CGS"].sample}
    order { rand(0..6)}
    assets ["1","2"]
    timeframe {["day","week","month"].sample}
  end
end