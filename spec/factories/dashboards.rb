FactoryGirl.define do
  factory :dashboard do
    name { Faker::Name.first_name }
    description { Faker::Name.last_name }
    user_id 1234-4557    
    default { ["true","false"].sample }    
    after_build do |f|
      graph = FactoryGirl.create :graph
      f.graphs = [graph.id.to_s]
    end
  end
end
