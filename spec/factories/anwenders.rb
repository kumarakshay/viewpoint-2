FactoryGirl.define do
  factory :anwender do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    company_guid 1234-4557
    company "Adobe"    
    email { "#{first_name}.#{last_name}@example.com".downcase }
    phone_number {Faker::PhoneNumber.phone_number}
    after_build do |f|
      role = FactoryGirl.create :role
      f.roles = [role.name]
    end
    
  end
end
