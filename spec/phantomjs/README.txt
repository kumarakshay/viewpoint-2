PhantomJS is a “headless webkit”. What does that mean? Webkit is the engine of choice that Chrome, Firefox, and some other browsers use to render the content of a webpage. The “headless” part refers to the fact that PhantomJS never displays web pages. For our purposes, we just need to understand that PhantomJS can execute Javascript like a browser, but without having to display a web page.

Installing PhantomJS on Ubuntu

 For 32-bit system use phantomjs-1.9.0-linux-i686.tar.bz2
 For 64-bit system use phantomjs-1.9.0-linux-x86_64.tar.bz2

1. Download latest phantomjs build from phantomjs site( http://phantomjs.org/download.html )
2. Extract the files to directory ( sudo tar xjf phantomjs-1.9.0-linux-i686.tar.bz2 )
3. Add phantomjs bin folder path to your .bashrc file
4. To check if completed, just type: phantomjs --version

  Done, you installed phantomJS on your machine.

Running phantom script

1. Go go script folder under spec/phantomjs 
2. phantomjs --ignore-ssl-errors=yes  your_file_name.js

