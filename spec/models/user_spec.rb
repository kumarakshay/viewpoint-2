require 'spec_helper'

describe User do
  it "has a valid factory" do
    create(:user).should be_valid
  end
  
  context "validations" do
    subject {FactoryGirl.build(:user)}
    it {should validate_presence_of(:company_guid).with_message('cannot be empty') }
    it {should validate_presence_of(:email).with_message('cannot be empty') }
  end

  describe "search by company guid" do      
    before :each do
      @smith = create(:user, email: "smith@example.com", company_guid: 'E71312DC-AAF1-4BF2-941D-B297B1D326AB')
      @jones = create(:user, email: "jones@example.com", company_guid: 'E71312DC-AAF1-4BF2-941D-B297B1D326AB')
    end
    context "specific guid" do
      it "returns the first record found" do
        u = User.where(company_guid: 'E71312DC-AAF1-4BF2-941D-B297B1D326AB').first
        u.email.should == "smith@example.com"
      end
    end
  end

  context "role_check" do 
    ['company_admin','sungard_admin','viewpoint_admin'].each do |role|
      it "#{role} should have admin role" do
        user = FactoryGirl.create(:user, roles: ["#{role}"])        
        user.admin_role?.should be_true
      end      
    end

    ['sungard_admin','viewpoint_admin'].each do |role|
      it "#{role} should have admin role" do
        user = FactoryGirl.create(:user, roles: ["#{role}"])        
        user.vp_admin_role?.should be_true
      end      
    end

    it "should have give company admin role" do
      user = FactoryGirl.create(:user)
      User.set_as_company_admin(user.email)        
      user.reload
      user.role?('company_admin').should be_true
    end
  end

  context "validate_password" do
     it "should reject password that are less than 8 character long" do
       u = FactoryGirl.create(:user)
       u.validate_password("aAAa", "aAAa")
       u.valid? == false
     end

     it "should reject password that not have at least one lowercase character" do
       u = FactoryGirl.create(:user)
       password = "A" * 8
       confirm_password = password
       u.validate_password(password, confirm_password)
       u.valid? == false
     end

     it "should reject password that not have at least one uppercase character" do
       u = FactoryGirl.create(:user)
       password = "a" * 8
       confirm_password = password
       u.validate_password(password, confirm_password)
       u.valid? == false
      end

      it "should reject password that not have at least one number" do
       u = FactoryGirl.create(:user)
       password = "AaBb!@aaaaaa"
       confirm_password = password
       u.validate_password(password, confirm_password)
       u.valid? == false
      end

      it "should accept password that have special character in it" do
         u = FactoryGirl.create(:user)
         password = "S@!23nny0#$*" 
         confirm_password = password
         u.validate_password(password, confirm_password)
         u.valid? == true
      end

      it "should accept password that have at least one number,uppercase character,lowercase character,minimum 8 character length" do
         u = FactoryGirl.create(:user)
         password = "a1A!23Jung@#" 
         confirm_password = password
         u.validate_password(password, confirm_password)
         u.valid? == true
      end
    end

end