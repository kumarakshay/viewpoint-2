require 'spec_helper'

describe Dashboard do 
  it "has a valid factory" do
    FactoryGirl.create(:dashboard).should be_valid
  end
  it "should load childs in ascending order" do
  	range = (0..5)
  	ids = range.to_a.inject([]){|ar,e| ar << FactoryGirl.create(:graph, :order => e).id.to_s}
  	dash = FactoryGirl.create(:dashboard)
  	dash.graphs = ids
  	dash.save
  	order = dash.load_graphs().inject([]){|ar,d| ar << d.order}
  	range.to_a.should == order
  end

 end