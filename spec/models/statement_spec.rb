require 'spec_helper'

describe Statement do
  it "has a valid factory" do
    create(:statement).should be_valid
  end
  it "is invalid without a name" do
     build(:statement, name: nil).should_not be_valid
  end
end