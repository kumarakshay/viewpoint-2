require 'spec_helper'

describe Graph do
  it "has a valid factory" do
  	FactoryGirl.create(:graph).should be_valid
  end

  context "validations" do
  	subject {FactoryGirl.build(:graph)}
  	it {should validate_presence_of(:name).with_message('cannot be empty') }
  	it {should validate_presence_of(:assets).with_message('cannot be empty') }   	
  	it {should validate_presence_of(:metric).with_message('cannot be empty') }
  end

  it "removes empty asset elements before save" do
  	graph = FactoryGirl.create(:graph, assets: ["1","","2"])
  	graph.assets.length.should be(2)
  end

  it "should delete itself without checking reference" do |variable|
  	graph = FactoryGirl.create(:graph)
	expect{ graph.destroy }.to change(Graph,:count)  	
  end
end