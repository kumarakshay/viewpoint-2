require 'spec_helper'

describe Company do

  before :each do
    @company = Company.create
  end

  it "should have error message on require field" do
    @company.should have(1).error_on(:snt)
    @company.should have(1).error_on(:guid)
  end
end

describe "filter by name" do
  before :each do
      @smith = create(:company, name: "Smith", guid: "12345abcd", snt: "zzz4")
      @jones = create(:company, name: "Jones", guid: "12345abce",  snt: "zzzt")
      @johnson = create(:company, name: "Johnson", guid: "12345abcf",  snt: "zzzp")
  end
  context "specific name" do
    it "returns a sorted array of results that match" do
      c = Company.where(name: "Jones").first
      c.name.should == "Jones"
    end
  end
end
