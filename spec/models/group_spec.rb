require 'spec_helper'

describe Group do 
  it "has a valid factory" do
    FactoryGirl.create(:group).should be_valid
  end
  context "validations" do
  	subject {FactoryGirl.build(:group)}
  	it {should validate_presence_of(:name).with_message('cannot be empty') }
  	it {should validate_presence_of(:asset_list).with_message('cannot be empty') }
  end

  it "removes empty asset elements before save" do
  	group = FactoryGirl.create(:group, asset_list: ["1","","2"])
  	group.asset_list.length.should be(2)
  end

  context "validation before delete" do 
  	before :each do
      @group = FactoryGirl.create(:group)  		
  	end

    it "should check references in subscription before delete" do
      Req.should_receive(:has_associated_subscription?).with(@group.id.to_s).and_return(false)  	        
      expect{ @group.destroy }.to change(Group,:count)
    end

    it "should not destroy if referenced in subscription" do    
      Req.should_receive(:has_associated_subscription?).with(@group.id.to_s).and_return(true)  	        
      expect{ @group.destroy }.to_not change(Group,:count)
    end

  end
	
end