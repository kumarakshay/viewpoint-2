require 'spec_helper'

describe DashboardsController do

  it "should use DashboardsController" do
    controller.should be_an_instance_of(DashboardsController)
  end
  
  it "should require authentication to access" do
      get 'index'
      response.should redirect_to new_oid_path
  end

  describe "GET 'index'" do
    before :each do
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      @dashboards = [mock_model(Dashboard),mock_model(Dashboard)]
      Dashboard.should_receive(:all).with(:user_id => mock_user.guid).and_return @dashboards
      get :index
    end

    it "populates all dashboards" do      
      assigns[:dashboards].should == @dashboards 
    end
    
    it "renders the :index view" do
      response.should render_template :index
    end
  end

  describe "GET 'show'" do
    before :each do  
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)    
      controller.stub!(:load_instance).and_return(mock_dashboard)
      get :show, :id => mock_dashboard.id
    end    
    it { response.should be_success}
    it { response.should render_template('show') }
  end

  describe "POST create" do
    before :each do  
      Dashboard.delete_all
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
    end
   
    context "with valid attributes" do
      it "creates a new Dashboard" do
        expect{
          post :create, dashboard: FactoryGirl.attributes_for(:dashboard)
        }.to change(Dashboard,:count).by(1)
      end

      it "redirects to the new dashboard" do
        post :create, dashboard: FactoryGirl.attributes_for(:dashboard)
        flash[:notice].should_not be_blank
        response.should redirect_to Dashboard.last
      end
    end

    context "with invalid attributes" do
      it "re-renders the new method" do
        post :create, dshboard: {}
        flash[:notice].should be_blank
        response.should render_template :new
      end
    end 
  
  end

  describe "PUT dashboard/:id" do

    before :each do 
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      Dashboard.stub!(:where).with(:user_id =>mock_user.guid).and_return(Dashboard)
      Dashboard.stub!(:first).with(:id => "#{mock_dashboard.id}", :user_id => mock_user.guid).and_return(mock_dashboard)
    end

    describe "with valid params" do
      before :each do
        mock_dashboard.stub!(:update_attributes).and_return(true)
        put :update, :dashboard => {}, :id => mock_dashboard.id
      end

      it "should redirect to show page after update" do
        response.should redirect_to(dashboard_url(mock_dashboard))
      end

      it "should have a flash notice" do
        flash[:notice].should_not be_blank
      end
    end
    
    describe "with invalid params" do
      before(:each) do
        mock_dashboard.stub!(:update_attributes).and_return(false)
      end

      it "should render the edit form" do
        put :update, :dashboard => {}, :id => mock_dashboard.id
        response.should render_template('edit')
      end
    
      it "should call update the dashboard object's attributes" do
        mock_dashboard.should_receive(:update_attributes).and_return(false)
        put :update, :dashboard => {}, :id => mock_dashboard.id
       end
    end
  end

  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end

  def mock_dashboard
    @mock_dashboard ||= mock_model(Dashboard).as_null_object
  end  

end
