require 'spec_helper'

describe Admin::WelcomeController do
	before :each do
	  controller.stub!(:authorize_as_admin).and_return(true)
      controller.stub!(:authenticated?).and_return(true)
    end

  describe "GET 'welcome'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end
end