require 'spec_helper'

describe Admin::UsersController do

	it "should use UsersController" do
    controller.should be_an_instance_of(Admin::UsersController)
  end
  
  it "should require authentication to access" do
      get 'index'
      response.should redirect_to new_oid_path
  end
  
  describe "GET 'show'" do
    before :each do  
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:authorize_as_admin).and_return(true)
      controller.stub!(:current_user).and_return(mock_user) 

      controller.stub!(:load_instance).and_return(mock_user)
      get :show, :id => mock_user.id
    end    
    it { response.should be_success}
    it { response.should render_template('show') }
  end

  describe "POST create" do
    before :each do  
      User.delete_all
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
    end
   
    context "with valid attributes" do
      pending "creates a new User" do
        expect{
          post :create, user: FactoryGirl.attributes_for(:user)
        }.to change(User,:count).by(1)
      end

     pending "redirects to the new user" do
        post :create, user: FactoryGirl.attributes_for(:user)
        flash[:notice].should_not be_blank
        response.should redirect_to User.last
      end
    end

    context "with invalid attributes" do
      pending "re-renders the new method" do
        post :create, dshboard: {}
        flash[:notice].should be_blank
        response.should render_template :new
      end
    end 
  
  end


  describe "PUT user/:id" do

    before :each do 
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:authorize_as_admin).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      User.should_receive(:find).with(mock_user.id.to_s).and_return(mock_user) 
      MyOpenid.stub!(:update_user).and_return(mock_openid)
    end

    describe "with valid param and opernid return success" do
      before :each do
        controller.stub!(:check_status).and_return("success")
        mock_user.stub!(:update_attributes).and_return(true)
        controller.stub!(:create_audit_log).with(mock_user).and_return(true)
        
        put :update, :user => {}, :id => mock_user.id
       
      end

      it "should redirect to edit page after update" do
        response.should render_template('edit')
      end

      it "should have a flash notice" do
        flash[:notice].should eql("User was successfully updated.")
      end
    end

    describe "with valid param and opernid return failure" do
      before :each do
        controller.stub!(:check_status).and_return("failure")
        mock_user.stub!(:update_attributes).and_return(true)
        put :update, :user => {}, :id => mock_user.id
      end

      it "should redirect to edit page after update" do
        response.should render_template('edit')
      end

      it "should have a flash notice" do
        flash[:notice].should eql('Error: profile not saved.')
      end
    end
    
    describe "with invalid params" do
      before(:each) do
        mock_user.stub!(:update_attributes).and_return(false)
      end

      it "should render the edit form" do
        put :update, :user => {}, :id => mock_user.id
        response.should render_template('edit')
      end
    
    end
   end

  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end

  def mock_openid
    @mock_openid = "{\"action\":\"update_user\",\"message\":\"success\",\"email\":{\"email\":\"san112@gmail.com\",\"active\":\"true\",\"company_name\":\"SUBARU OF AMERICA INC\",\"company_guid\":\"D9EFA335-F8E5-4271-9541-BB5D7590AEE5\"}}"
  end

end
