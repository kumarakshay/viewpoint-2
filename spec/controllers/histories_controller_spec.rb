require 'spec_helper'

describe HistoriesController do
	it "should use HistoriesController" do	
		controller.should be_an_instance_of(HistoriesController)
	end

	before :each  do
		controller.stub!(:authenticated?).and_return(false)
		controller.stub!(:current_user).and_return(mock_user)
	end

	describe "Get #index" do
		before :each do
			mock_user.stub!(:is_sungard?).and_return(false)
			get :index
		end

		it "response should be sucess" do	
			mock_user.is_sungard?.should eq(false)
			response.should be_success
		end	

		it "renders the index view" do
			response.should render_template :index
		end
	end



   def mock_user
    @mock_user ||= mock_model(User).as_null_object
   end

end
