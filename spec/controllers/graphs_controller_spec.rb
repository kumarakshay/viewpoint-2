require 'spec_helper'

describe GraphsController do
  
  it "should use GraphsController" do
    controller.should be_an_instance_of(GraphsController)
  end

  before :each do
      controller.stub!(:authenticated?).and_return(true)
  end
  
  describe "GET 'index'" do
    before :each do
    #  controller.stub!(:load_instance).and_return(true)
      @graphs = [mock_model(Graph),mock_model(Graph)]
      Graph.should_receive(:all).and_return(@graphs)
      get :index
    end

    it "populates all graphs" do      
      assigns[:graphs].should == @graphs
    end
    
    it "renders the :index view" do
      response.should render_template :index
    end
  end

  describe "GET 'show'" do
    before :each do  
      controller.stub!(:load_instance).and_return(mock_graph)
      get :show, :id => mock_graph.id
    end    
    it { response.should be_success}
    it { response.should render_template('show') }
  end

  describe "POST create" do
    before :each do
       Graph.delete_all
    end

    context "with valid attributes" do
      it "creates a new Graph" do

        expect{
          post :create, graph: FactoryGirl.attributes_for(:graph)
        }.to change(Graph,:count).by(1)
      end

      it "redirects to the new Graph" do
        post :create, graph: FactoryGirl.attributes_for(:graph)
        response.should redirect_to Graph.last
      end
    end

    context "with invalid attributes" do
      it "re-renders the new method" do
        post :create, graph: {}
        response.should render_template :new
      end
    end 
  end

  describe "PUT graphs/:id" do

    before :each do 
      Graph.stub!(:find).with("#{mock_graph.id}").and_return(mock_graph)
    end

    describe "with valid params" do
      before(:each) do
        mock_graph.stub!(:update_attributes).and_return(true)
      end
        
      it "should find graph and return object" do
        Graph.should_receive(:find).with("#{mock_graph.id}").and_return(mock_graph)
        put :update, :id => mock_graph.id, :dash_id => mock_dashboard.id, :graph => {}
      end

      it "should redirect to show page after update" do
        put :update, :id => mock_graph.id, :dash_id => mock_dashboard.id, :graph => {}
        response.should redirect_to(dashboard_path(mock_dashboard.id))
      end
    end
    
    describe "with invalid params" do
      before(:each) do
        mock_graph.stub!(:update_attributes).and_return(false)
      end

      it "should render the edit form" do
        put :update, :id => mock_graph.id, :graph => {}
        response.should render_template('edit')
      end

      it "should call update the graph object's attributes" do
        mock_graph.should_receive(:update_attributes).and_return(false)
        put :update, :id => mock_graph.id, :graph => {}
     end

    end
  end

  def mock_graph
    @mock_graph ||= mock_model(Graph).as_null_object
  end  

  def mock_dashboard
    @mock_dashboard ||= mock_model(Dashboard).as_null_object
  end

end
