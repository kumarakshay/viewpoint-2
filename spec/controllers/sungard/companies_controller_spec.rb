require 'spec_helper'

describe Sungard::CompaniesController do

	it "should use CompaniesController" do
    controller.should be_an_instance_of(Sungard::CompaniesController)
  end
  
  describe "DELETE 'Companies'" do
    before :each do  
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:authorize).and_return(true)
      controller.stub!(:select_branding).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      MyOpenid.stub!(:delete_company).and_return(true)
      Company.should_receive(:find_by_guid).and_return(mock_company)
      Company.should_receive(:destroy_all).and_return(true)
      User.should_receive(:where).and_return([mock_user])
      delete :destroy, :id => mock_company.guid
    end    
    it {  response.should redirect_to sungard_companies_path }

    it "should have a flash notice" do
      flash[:notice].should eql("Company was successfully updated.")
    end

  end

  def mock_company
    @mock_company||= mock_model(Company).as_null_object
  end 

  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end 

  def mock_openid
    @mock_openid =  '{"action":"delete_company","message":"failure","company_guid":"1722C526-EC8B-427A-A473-165815C12C4A"}'
  end
end