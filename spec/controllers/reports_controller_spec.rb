require 'spec_helper'

describe ReportsController do

it "should use ReportsController" do
    controller.should be_an_instance_of(ReportsController)
  end
  
  it "should require authentication to access" do
      get 'index'
      response.should redirect_to new_oid_path
  end

  before :each do
  	@group =[mock_model(Group), mock_model(Group)]
  end

  describe "GET 'index'" do
    before :each do
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user)
      Group.should_receive(:dat_for_dropdown).with(mock_user.company_guid).and_return(@group)
      Device.should_receive(:dat_for_dropdown).with(mock_user.asset_list).and_return(true)
      Device.should_receive(:dropdown_by_device_class).with(mock_user.asset_list, "SysEdge").and_return(true)
      Device.should_receive(:dropdown_by_device_class).with(mock_user.asset_list, "Server").and_return(true)
      Power.should_receive(:find_distinct_area).with(mock_user.company_guid).and_return(true)
      Power.should_receive(:find_distinct_cabinet).with(mock_user.company_guid).and_return(true)
	    get :index
    end
        
    it "renders the :index view" do
      response.should render_template :index
    end
  end

  describe "GET 'show'" do
    before :each do  
      controller.stub!(:authenticated?).and_return(true)
      controller.stub!(:current_user).and_return(mock_user) 
    end   
    
    it "should sucessfully returns file" do
      Report.should_receive(:construct_path).with(report_name="sungard", company_guid =mock_user.company_guid, report_format="txt").and_return(File)
      controller.should_receive(:send_file).and_return(:success)
      pending do
       get :show, :id => "sungard", :ext =>"application/txt", :format => "txt"
      end
    end

  
  end

   def mock_user
     @mock_user ||= mock_model(User).as_null_object
  end  

end
