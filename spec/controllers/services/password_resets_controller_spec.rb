require 'spec_helper'

describe Services::PasswordResetsController do
  
  it "should use PasswordResetsController" do
    controller.should be_an_instance_of(Services::PasswordResetsController)
  end
  
  describe "GET 'show'" do
    before :each do  
      get :show, :id => "55555555555555555555sgsgs"
    end    
    it { response.should redirect_to edit_services_password_reset_path }
  end

  describe "PUT user/:id" do

    describe "With invalid params" do
      before :each do
        User.should_receive(:find).and_return(mock_user)
        mock_user.should_receive(:validate_password).and_return(true)
      end

      it "should redirect to edit page if password empty" do
        mock_user.should_receive(:errors).and_return(["password cannot be empty"])
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.confirmation_token
        response.should render_template('edit')
      end
    end   

    describe "With valid params" do
      before :each do
        User.should_receive(:find).and_return(nil)
        mock_user.should_receive(:validate_password).and_return(true)
        User.should_receive(:new).and_return(mock_user)
        mock_user.should_receive(:errors).and_return([])
        MyOpenid.stub!(:confirm_password_reset).and_return(mock_openid)
      end

      it "should redirect welcome page if open id respose status is success" do
        controller.stub!(:check_confirm_status).and_return(["success", mock_user.email])
        User.should_receive(:first).and_return(mock_user)
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.id
        response.should redirect_to welcome_path
      end

      it "should render edit page if open id respose status is failure" do
        controller.stub!(:check_confirm_status).and_return(["failure", mock_user.email])
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.id
        response.should render_template('edit')
      end
    end
	end   

  describe "POST create" do

    describe "with open id status success" do
      before :each do
         User.should_receive(:first).and_return(mock_user)
         MyOpenid.stub!(:openid_confirm).and_return(mock_openid)
         controller.stub!(:check_status).and_return("success")
         controller.stub(:send_email).and_return(true)
         post :create, :email => mock_user.email
      end

      it "should have a flash notice" do
        flash[:notice].should_not be_blank
        flash[:notice].should eql("Email sent with password reset instructions")
      end

      it "should redirect to admin users page with email instructions" do
        response.should redirect_to admin_users_path(:notice => "Email sent with password reset instructions", 
                                     :company_id => mock_user.company_guid)
      end
    end

    describe "with open id status failure" do
      before :each do
         User.should_receive(:first).and_return(mock_user)
         MyOpenid.stub!(:openid_confirm).and_return(mock_openid)
         controller.stub!(:check_status).and_return("failure")
         post :create, :email => mock_user.email
      end

       it "should have a flash notice with Unable to reset password" do
        flash[:notice].should_not be_blank
        flash[:notice].should eql("Unable to reset password")
      end

      it "should redirect to admin users page with Unable to reset password" do
        response.should redirect_to admin_users_path(:notice => "Unable to reset password", 
                                     :company_id => mock_user.company_guid)
      end
    end
  end

  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end

  def mock_openid
    @mock_openid = '{"request_reset":"confirm","message":"success","email":"sandip.mondal@sungard.com" "user":{"admin":null,"company_guid":"D9EFA335-F8E5-4271-9541-BB5D7590AEE5","company_name":"SUBARU OF AMERICA INC","confirmation_token":null,"created_at":"2013-05-31T09:39:14Z","email":"ssss@gmail.com","id":197,"public_persona_id":195,"remember_token_expires_at":null,"reset_password_token":"at7h65hycso75NUEuvhX","unlock_token":null,"updated_at":"2013-05-31T09:44:30Z"}}'
  end

end
