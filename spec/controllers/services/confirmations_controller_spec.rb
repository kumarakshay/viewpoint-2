require 'spec_helper'

describe Services::ConfirmationsController do

  it "should use ConfirmationsController" do
    controller.should be_an_instance_of(Services::ConfirmationsController)
  end
  
  describe "GET 'show'" do
    before :each do  
      get :show, :id => "at7h65hycso75NUEuvhX"
    end    
    it { response.should redirect_to edit_services_confirmation_path }
  end

  describe "PUT user/:id" do

    describe "With invalid params" do
      before :each do
        User.should_receive(:find_by_confirmation_token).and_return(mock_user)
        mock_user.should_receive(:validate_password).and_return(true)
      end

      it "should redirect to edit page if password empty" do
        mock_user.should_receive(:errors).and_return(["password cannot be empty"])
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.confirmation_token
        response.should render_template('edit')
      end

      it "should redirect to edit page if update attribute return false" do
        mock_user.should_receive(:update_attributes).and_return(false)
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.confirmation_token
        response.should render_template('edit')
      end   

      it "should redirect to edit page if confirmation_token nil" do
        mock_user.should_receive(:confirmation_token).and_return(nil)
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => nil
        response.should render_template('edit')
      end
    end   

    describe "With valid params" do
      before :each do
        User.should_receive(:find_by_confirmation_token).and_return(mock_user)
        mock_user.should_receive(:validate_password).and_return(true)
        mock_user.should_receive(:errors).and_return([])
        mock_user.should_receive(:update_attributes).and_return(true)
        MyOpenid.stub!(:openid_confirm).and_return(mock_openid)

      end

      it "should redirect to home path if openid send success response" do
        controller.stub!(:check_status).and_return("success")
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.confirmation_token
        response.should redirect_to home_path
      end

      it "should ender edit if openid send failure response" do
        controller.stub!(:check_status).and_return("failure")
        put :update, :user => {:password => "", :password_confirmation=>""}, :id => mock_user.confirmation_token
        response.should render_template('edit')
      end  
    end 
  end


  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end

  def mock_openid
    @mock_openid = '{"action":"confirm","status":"success","confirmation_token":"Mh1sxz4wv3tQMc7xKQDn","user":{"active":true,"admin":null,"company_guid":"D9EFA335-F8E5-4271-9541-BB5D7590AEE5","company_name":"SUBARU OF AMERICA INC","confirmation_token":null,"created_at":"2013-05-31T09:39:14Z","email":"ssss@gmail.com","id":197,"public_persona_id":195,"remember_token_expires_at":null,"reset_password_token":null,"unlock_token":null,"updated_at":"2013-05-31T09:44:30Z"}}'
  end

end
