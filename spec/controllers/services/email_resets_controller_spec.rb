require 'spec_helper'

describe Services::EmailResetsController do
  
  it "should use EmailResetsController" do
    controller.should be_an_instance_of(Services::EmailResetsController)
  end
  
  describe "GET 'show'" do
    before :each do  
      get :show, :id => "55555555555555555555sgsgs"
    end    
    it { response.should redirect_to edit_services_email_reset_path }
  end

  describe "PUT user/:id" do

    describe "With invalid params" do
      before :each do
        User.should_receive(:find).and_return(mock_user)
      end

      it "should redirect to edit page if id empty" do
        put :update
        response.should render_template('edit')
      end
    end   

    describe "With valid params" do
      before :each do
        MyOpenid.stub!(:confirm_email).and_return(mock_openid)
      end

      it "should redirect welcome page if open id respose status is success" do
        controller.stub!(:check_status).and_return(["success"])
        User.should_receive(:first).and_return(mock_user)
        put :update, :id => mock_user.id
        response.should redirect_to root_path
      end

      it "should render edit page if open id respose status is failure" do
        controller.stub!(:check_status).and_return(["failure"])
        put :update, :id => mock_user.id
        response.should render_template('edit')
      end
    end
	end   

  
  def mock_user
    @mock_user ||= mock_model(User).as_null_object
  end

  def mock_openid
    @mock_openid = '{"request_reset":"change_email","status":"success","email":"sandip.mondal@sungard.com" "user":{"admin":null,"company_guid":"D9EFA335-F8E5-4271-9541-BB5D7590AEE5","company_name":"SUBARU OF AMERICA INC","confirmation_token":null,"created_at":"2013-05-31T09:39:14Z","email":"ssss@gmail.com","id":197,"public_persona_id":195,"remember_token_expires_at":null,"reset_password_token":"at7h65hycso75NUEuvhX","unlock_token":null,"updated_at":"2013-05-31T09:44:30Z"}}'
  end

end
