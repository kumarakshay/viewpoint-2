require 'spec_helper'

  describe MyOpenid do

      before :each do
         @mock_user  = mock_model(User).as_null_object
         @user  = FactoryGirl.create(:user)
         @invalid_dada = "{'user[email]' => 'incompletedata@example.com'}"
         @invalid_openid_url = "http://fakeurl.api/v1/fake"
      end

    
      describe "register user" do
        
          before :each do
             @mock_openid_sucess = "{\"action\":\"register\",\"status\":\"success\",\"email\":\"test@email.com\",\"user\":{\"admin\":null,\"company_guid\":\"<company guid>\",\"company_name\":\"<company name>\",\"confirmation_token\":\"7J1NPvsXdE7ejmY8KLqX\",\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":\"test@email.com\",\"id\":76,\"public_persona_id\":31,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":\"2013-04-04T15:42:25Z\"}}"
             @mock_openid_failure = "{\"action\":\"register\",\"status\":\"failure\",\"email\":\"test@email.com\",\"user\":{\"admin\":null,\"company_guid\":\"<company guid>\",\"company_name\":\"<company name>\",\"confirmation_token\":\"7J1NPvsXdE7ejmY8KLqX\",\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":\"test@email.com\",\"id\":76,\"public_persona_id\":31,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":\"2013-04-04T15:42:25Z\"}}"
             @open_id_api_url = "http://sgoid-dev.sgns.net/api/v1/users"
             @open_id_update_url = "http://sgoid-dev.sgns.net/api/v1/users/update"
          end

          it "should register user and updtate user" do
            MyOpenid.stub!(:access_openid_update_data).and_return(@mock_openid_failure)
            data = MyOpenid.resister_user_data({"user" => {"email" => "test@email.com","first_name" => "firstname","last_name" =>"lastname"}}, @mock_user)
            MyOpenid.stub!(:access_openid_update_data).with(@open_id_api_url,data).and_return(@mock_openid_sucess)
            MyOpenid.access_openid_update_data(@open_id_api_url, data).should == @mock_openid_sucess

            data = MyOpenid.update_user_data({"user" => {"email" => "test@email.com","first_name" => "newfirstname","last_name" =>"lastname"}}, @mock_user)
            MyOpenid.stub!(:access_openid_update_data).with(@open_id_update_url,data).and_return(@mock_openid_sucess)
            MyOpenid.access_openid_update_data(@open_id_update_url, data).should == @mock_openid_sucess
            ["http://sgoid-dev.sgns.net/api/v1/users","http://sgoid-dev.sgns.net/api/v1/users/update"].each do |url|
              MyOpenid.access_openid_update_data().should == @mock_openid_failure
              MyOpenid.access_openid_update_data(url).should == @mock_openid_failure
              MyOpenid.access_openid_update_data(data).should == @mock_openid_failure
              MyOpenid.access_openid_update_data(url,@invalid_dada).should == @mock_openid_failure
              MyOpenid.access_openid_update_data(@invalid_openid_url,@invalid_dada).should == @mock_openid_failure
            end
          end
      end

         describe "confirm user" do

            before :each do
              @mock_openid_sucess = "{\"action\":\"confirm\",\"status\":\"success\",\"confirmation_token\":\"7J1NPvsXdE7ejmY8KLqX\",\"user\":{\"admin\":null,\"company_guid\":@user.company_guid,\"company_name\":@user.company_name,\"confirmation_token\":null,\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":@user.email\",\"id\":76,\"public_persona_id\":31,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":\"2013-04-04T16:13:49Z\"}}"
              @mock_openid_failure = "{\"action\":\"confirm\",\"status\":\"failure\",\"confirmation_token\":\"7J1NPvsXdE7ejmY8KLqX\",\"user\":{\"admin\":null,\"company_guid\":@user.company_guid,\"company_name\":@user.company_name,\"confirmation_token\":null,\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":@user.email\",\"id\":76,\"public_persona_id\":31,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":\"2013-04-04T16:13:49Z\"}}"
              @open_id_confirm_url = "http://sgoid-dev.sgns.net/api/v1/users/confirmation"
            end

            it "should register user and updtate user" do
              MyOpenid.stub!(:access_openid).and_return(@mock_openid_failure)
              data = MyOpenid.set_confirmation_data(@user)
              MyOpenid.stub!(:access_openid).with(@open_id_confirm_url,data).and_return(@mock_openid_sucess)
              MyOpenid.access_openid(@open_id_confirm_url, data).should == @mock_openid_sucess
           end

        end

        describe "change email" do

            before :each do
              @mock_openid_sucess = "{\"action\":\"change_email\",\"status\":\"success\",\"confirmation_token\":\"7J1NPvsXdE7ejmY8KLqX\",\"user\":{\"admin\":null,\"company_guid\":@user.company_guid,\"company_name\":@user.company_name,\"confirmation_token\":null,\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":@user.email\",\"id\":76,\"public_persona_id\":31,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":\"2013-04-04T16:13:49Z\"}}"
              @mock_openid_failure = "{\"action\":\"change_email\",\"status\":\"failure\",\"confirmation_token\":\"7J1NPvsXdE7ejmY8KLqX\",\"user\":{\"admin\":null,\"company_guid\":@user.company_guid,\"company_name\":@user.company_name,\"confirmation_token\":null,\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":@user.email\",\"id\":76,\"public_persona_id\":31,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":\"2013-04-04T16:13:49Z\"}}"
              @open_id_change_email_url = "http://sgoid-dev.sgns.net/api/v1/users/change_email"
              @open_id_confirm_email_url = "http://sgoid-dev.sgns.net/api/v1/users/confirm_email"
            end

            it "should change user email address" do
              MyOpenid.stub!(:access_openid_update_data).and_return(@mock_openid_failure)
              data = data = {
                              'user[email]' => @user.email,
                              'user[new_email]' => @user.pending_email,
                              'auth_token' => APP_CONFIG['open_id_auth_token']
                            }
              MyOpenid.stub!(:access_openid_update_data).with(@open_id_change_email_url,data).and_return(@mock_openid_sucess)
              MyOpenid.access_openid_update_data(@open_id_change_email_url, data).should == @mock_openid_sucess
           end

           it "should confirm user new email address" do
              MyOpenid.stub!(:access_openid_update_data).and_return(@mock_openid_failure)
              data = data = {
                              'user[email]' => @user.email,
                              'user[new_email]' => @user.pending_email,
                              'user[confirmation_token]' => @user.confirmation_token,
                              'auth_token' => APP_CONFIG['open_id_auth_token']
                            }
              MyOpenid.stub!(:access_openid).with(@open_id_confirm_email_url,data).and_return(@mock_openid_sucess)
              MyOpenid.access_openid(@open_id_confirm_email_url, data).should == @mock_openid_sucess
           end

        end

        describe "password" do

            before :each do
              @mock_openid_sucess = "{\"action\":\"request_reset\",\"message\":\"success\",\"email\":@user.email,\"user\":{\"admin\":null,\"company_guid\":@user.company_guid,\"company_name\":@user.company_name,\"confirmation_token\":null,\"created_at\":\"2013-04-04T15:42:24Z\",\"email\":@user.email,\"id\":76,\"public_persona_id\":31,\"reset_password_token\":\"at7h65hycso75NUEuvhX\",\"unlock_token\":null,\"updated_at\":\"2013-04-04T16:16:39Z\"}}"
              @mock_openid_failure ="{\"action\":\"request_reset\",\"message\":\"failure\",\"email\":@user.email,\"user\":{\"admin\":null,\"company_guid\":null,\"company_name\":null,\"confirmation_token\":null,\"created_at\":null,\"email\":\"<user email>\",\"id\":null,\"public_persona_id\":null,\"reset_password_token\":null,\"unlock_token\":null,\"updated_at\":null}}"  
              @open_id_reset_url = "http://sgoid-dev.sgns.net/api/v1/users/password"
              @open_id_change_password_url = "http://sgoid-dev.sgns.net/api/v1/users"
            end

            it "should reset user password" do
              expected_data = "{'user[email]' => #{@user.email}, 'auth_token' => 'zpzRTD5ajwrPUHZYAsy2'}"
              MyOpenid.stub!(:access_openid).and_return(@mock_openid_failure)
              MyOpenid.stub!(:reset_data).with(@user.email).and_return(expected_data)
              received_data = MyOpenid.reset_data(@user.email)
              MyOpenid.stub!(:access_openid).with(@open_id_reset_url,received_data).and_return(@mock_openid_sucess)
              MyOpenid.access_openid(@open_id_reset_url, received_data).should == @mock_openid_sucess
              
              MyOpenid.access_openid().should == @mock_openid_failure
              MyOpenid.access_openid(@invalid_openid_url, received_data).should == @mock_openid_failure
              MyOpenid.access_openid(@invalid_openid_url, @invalid_data).should == @mock_openid_failure
           end

           it "should confirm reset password " do
              @user.password = "Password123"
              @user.password_confirmation = "Password123"
              expected_data = {'user[reset_password_token]' => @user.reset_password_token,'user[password]' => @user.password,'user[password_confirmation]' =>@user.password_confirmation,'auth_token' => "zpzRTD5ajwrPUHZYAsy2"}
              MyOpenid.stub!(:access_openid_update_data).and_return(@mock_openid_failure)
              received_data = MyOpenid.confirm_reset_data(@user)
              MyOpenid.stub!(:access_openid_update_data).with(@open_id_reset_url,received_data).and_return(expected_data)
              expected_data.should == received_data
              MyOpenid.stub!(:access_openid_update_data).with(@open_id_reset_url,received_data).and_return(@mock_openid_sucess)
              MyOpenid.access_openid_update_data(@open_id_reset_url, received_data).should == @mock_openid_sucess
              MyOpenid.access_openid_update_data().should == @mock_openid_failure
           end

           it "should change user password " do
              @user.password = "Password123"
              @user.password_confirmation = "Password123"
              expected_data = {'user[email]' => @user.email,'user[password]' => @user.password,'user[password_confirmation]' => @user.password_confirmation,'auth_token' => "zpzRTD5ajwrPUHZYAsy2"}
              MyOpenid.stub!(:access_openid_update_data).and_return(@mock_openid_failure)
              received_data = MyOpenid.change_password_data(@user)  
              MyOpenid.stub!(:access_openid_update_data).with(@open_id_change_password_url,received_data).and_return(expected_data)
              expected_data.should == received_data
              MyOpenid.stub!(:access_openid_update_data).with(@open_id_change_password_url,received_data).and_return(@mock_openid_sucess)
              MyOpenid.access_openid_update_data(@open_id_change_password_url, received_data).should == @mock_openid_sucess
              MyOpenid.access_openid_update_data().should == @mock_openid_failure
           end

        end

   end
