var records = {
  "records": [{
    "status": "warning",
    "time": "1",
    "message": "Update Failed"
  }, {
    "status": "high-pri",
    "time": "3",
    "message": "6 new updates available"
  }, {
    "status": "low-pri",
    "time": "5",
    "message": "System Updates Available"
  }, {
    "status": "medium-pri",
    "time": "7",
    "message": "Feature Enhancement: New Ticket Management"
  }]
};

var vp = {
  "tools": {
    "tabList": {
      "header": [{
        "title": "Applications",
        "active": true
      }, {
        "title": "Administrative",
        "active": false
      }],
      "body": {
        "Applications": [{
          "title": "Launchboard",
          "target": "#",
          "icon": "link-launchboard",
          "items": null
        }, {
          "title": "Ticketing",
          "target": "#",
          "icon": "link-ticketing",
          "items": null
        }, {
          "title": "Environment",
          "target": "#",
          "icon": "link-environment",
          "active": true,
          "items": [{
            "title": "Devices",
            "target": "viewpoint.sungardas.com",
            "icon": "link-devices",
            "items": null
          }, {
            "title": "Interfaces",
            "target": "#",
            "icon": "link-interfaces",
            "items": null
          }, {
            "title": "Events",
            "target": "#",
            "icon": "link-events",
            "items": null
          }, {
            "title": "Event Notifications",
            "target": "#",
            "icon": "link-event-sub",
            "items": null
          }, {
            "title": "Environment Health",
            "target": "#",
            "icon": "link-environment-health",
            "items": null
          }, {
            "title": "Firewalls",
            "target": "#",
            "icon": "link-firewalls",
            "items": null
          }]
        }, {
          "title": "Reports",
          "target": "#",
          "icon": "link-reports",
          "items": [{
            "title": "Reports",
            "target": "#",
            "icon": "link-reports",
            "items": null
          }, {
            "title": "Subscriptions",
            "target": "#",
            "icon": "link-subs",
            "items": null
          }]
        }, {
          "title": "Tools",
          "target": "#",
          "icon": "link-tools",
          "items": [{
            "title": "IDS",
            "target": "#",
            "icon": "link-ids",
            "items": null
          }, {
            "title": "New IDS",
            "target": "#",
            "icon": "link-new-ids",
            "items": null
          }, {
            "title": "SIEM",
            "target": "#",
            "icon": "link-siem",
            "items": null
          }, {
            "title": "US Vaulting",
            "target": "#",
            "icon": "link-us-vaulting",
            "items": null
          }, {
            "title": "UK VytalVaults Console",
            "target": "#",
            "icon": "link-uk-vv-console",
            "items": null
          }, {
            "title": "UK VytalVault Report",
            "target": "#",
            "icon": "link-uk-vv-reports",
            "items": null
          }, {
            "title": "Managed Email Archiving",
            "target": "#",
            "icon": "link-eas",
            "items": null
          }, {
            "title": "Server Replication",
            "target": "#",
            "icon": "link-server-replication",
            "items": null
          }, {
            "title": "SE Online Backup Portal",
            "target": "#",
            "icon": "link-se-portal",
            "items": null
          }]
        }],
        "Administrative": [{
          "title": "Groups",
          "target": "#",
          "icon": "link-groups",
          "items": null
        }, {
          "title": "Company Admin",
          "target": "#",
          "icon": "link-company-admin",
          "items": null
        }]
      }
    }
  }
} ;

var groupResults =  [{
  "id": "54c2980fd373ae69a300bf51",
  "name": "Server Test",
  "updated_at": "2016-05-29T05:01:15Z"
}, {
  "id": "56fcc6b5d373ae0361001982",
  "name": "Servers",
  "updated_at": "2016-05-29T05:01:34Z"
}];

var assetResults = [{
  "asset_guid": "10562608-ce82-6fbb-e050-a2a8587d3646",
  "id": "574fb785d373ae22cf001b00",
  "name": "alpucs1"
}, {
  "asset_guid": "10562608-ce83-6fbb-e050-a2a8587d3646",
  "id": "574fb78ed373ae22cf004e54",
  "name": "alpucs2"
}];

var infoConfig = {
  "ms-ehealth": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-ehealth",
    "tutorials": {
      "title": "Ehealth",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-crShow": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-crShow",
    "tutorials": {
      "title": "Run a Command",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-crFirewall": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-crFirewall",
    "tutorials": {
      "title": "Schedule a Change",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-interfaces": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-interfaces",
    "tutorials": {
      "title": "Interfaces",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-events": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-events",
    "tutorials": {
      "title": "Events",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-event-subscriptions": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-event-subscriptions",
    "tutorials": {
      "title": "Event Subscriptions",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-ticket-landing": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-ticket-landing",
    "tutorials": {
      "title": "Ticket Landing",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-reports": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-reports",
    "tutorials": {
      "title": "Reports",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-firewalls": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-firewalls",
    "tutorials": {
      "title": "Firewalls",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-devices": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-devices",
    "tutorials": {
      "title": "Devices",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-company-admin": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-company-admin",
    "tutorials": {
      "title": "Company Admin",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-groups": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-groups",
    "tutorials": {
      "title": "Groups",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
    "ms-dns": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-dns",
    "tutorials": {
      "title": "DNS",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-report-subscription": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-report-subscription",
    "tutorials": {
      "title": "Report Subscriptions",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "ms-portal-landing": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=ms-portal-landing",
    "tutorials": {
      "title": "Launchboard",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  },
  "default": {
    "pageUrl": "/api/portal/mock/get_info_html?page_id=nyi",
    "tutorials": {
      "title": "MS Video Tutorials",
      "videos": [{
        "name": "A brief tour of the MS portal",
        "id": "177252866"
      }]
    }
  }
}