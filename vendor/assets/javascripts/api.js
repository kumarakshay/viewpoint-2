define(['jquery'] , function ($) {
    return {
      id_requests: {},
      sendRequest:  function(url, method, data, options) {
      const self = this;
      const id = options && options.id;
      return new Promise(function apiPromise(resolve, reject) {
        const request = new XMLHttpRequest();
        request.open(method || 'GET', url);
        request.setRequestHeader('Content-type', "application/json");
        request.withCredentials = true;
        request.onload = function findstatus() {
        if (request.status >= 200 && request.status < 400) {
          if(request.status === 204) {
            resolve();
          } else {
            const result = 'response' in request ? request.response : request.responseText;
            try {
              resolve(JSON.parse(result));
            } catch (e) {
              // console.error(e);
              reject(e);
            }
          }
        } else {
          reject(request.response);
        }
      };
        request.onerror = function onerror() {
          reject(request.status);
        };

        if (id) {
          if (self.id_requests[id]) {
            self.id_requests[id].abort();
          }
          self.id_requests[id] = request;
        }

        if (data) {
          request.send(data);
        } else {
          request.send();
        }
      });
    }

    }
});