class ReqsController < ApplicationController
  before_filter lambda { help_menu_name('subscriptions') }
  def index
    # users subscriptions
    @reqs = Req.where(:$or => [{:user_guid => current_user.guid}, {:email => current_user.email}], subscription: true).all
    respond_to do |format|
      @body_class = "reqs index single-column"
      format.html # index.html.erb
      format.json { render json: @reqs }
    end
  end

  def show
    @body_class = "reqs show single-column"
    @req = Req.find(params[:id])
    z = Adhoc.new
    rpt = z.gen_ehealth_report(@req.id)
    prepare_file_for_download(@req.report_id, rpt)
  end

  # GET /reqs/new
  # GET /reqs/new.json
  def new
    authorize! :access_non_omi_pages, current_user, session[:is_omi]
    @form_type = "new"
    # new Req object for the form
    @req = Req.new
    # hidden form fields
    @current_user_email = current_user.email
    @current_user_company_guid = current_user.company_guid
    @current_user_guid = current_user.guid

    # @form_type = "new_record"
    respond_to do |format|
      @body_class = "reports single-column"
      format.html # new.html.erb
      format.json { render json: @req }
    end
  end

  # GET /reqs/1/edit
  def edit
    @body_class = "reports single-column"
    @req = Req.find(params[:id])
    @form_type = "edit"
    @group_dat = Group.dat_for_dropdown(current_user.company_guid)
    @device_dat = current_user.asset_list
    @current_user_email = @req.email
    @current_user_company_guid = current_user.company_guid
    @current_user_guid = current_user.guid
    @group_dat = Group.dat_for_dropdown(current_user.company_guid)
    @device_dat = Device.dat_for_dropdown(current_user.asset_list)
    @device_sysedge = Device.dropdown_by_device_class(current_user.asset_list,"SysEdge")
    @device_server = Device.dropdown_by_device_class(current_user.asset_list,"Server")
    @area_dat = Power.find_distinct_area(current_user.company_guid)
    @cabinet_dat = Power.find_distinct_cabinet(current_user.company_guid)
  end

  def create
    #Overwrite report_id based on report_type
    params[:req][:report_id] = get_actual_report_id(params[:report_name],params[:req][:report_type])
    Rails.logger.info "converted to old report id - #{params[:req][:report_id]}"
    @req = Req.new(params[:req])
    #generate a job_id for message_bus and store in reqs collection
    # statement = Statement.find_by_report_id(@req[:report_id])
    @req.generate_job_id
    @req.report = @req.report_name
    #when a sungardas.com user will masquerade as a customer capture his email address
    #where report is emailed to the logged in sungardas user and not saved in pyxie.
    if session[:masquerade] == "true"
      @req.masquerade_email = sign_in_user.email
    end
    respond_to do |format|
      @body_class = "reqs save single-column"
      if @req.save
        if APP_CONFIG['bus_report_process']
          format.json { render json: @req, status: :created, location: @req }
          format.html { redirect_to root_path}
        else
          format.html { redirect_to @req, notice: 'Req was successfully created.' }
          format.json { render json: @req, status: :created, location: @req }
        end
      else
        @body_class = "reqs create single-column"
        format.html { render action: "new" }
        format.json { render json: @req.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @req = Req.find(params[:id])
    respond_to do |format|
      if @req.update_attributes(params[:req])
        if APP_CONFIG['bus_report_process']
          flash[:notice] = 'we are processing your report and you will be notified when available.'
          format.json { head :no_content }
          format.html { redirect_to reqs_path}
        else
          @body_class = "reqs update single-column"
          format.html { redirect_to @req, notice: 'Req was successfully updated.' }
        end
        format.json { head :no_content }
      else
        @body_class = "reqs update single-column"
        format.html { render action: "edit" }
        format.json { render json: @req.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reqs/1
  # DELETE /reqs/1.json
  def destroy
    @req = Req.find(params[:id])
    @req.destroy
    respond_to do |format|
      @body_class = "reqs destroy single-column"
      format.html { redirect_to reqs_url }
      format.json { head :no_content }
    end
  end

  def group
    @group_dat = Group.dat_for_dropdown(current_user.company_guid)
    respond_to do |format|
      format.json { render json: @group_dat}
    end
  end

  def device_sysedge
    @device_sysedge =  Device.dropdown_by_device_class(current_user.asset_list,"SysEdge")
    respond_to do |format|
      format.json { render json: @device_sysedge}
    end
  end

  def device_server
    @device_server = Device.dropdown_by_device_class(current_user.asset_list,"Server")
    respond_to do |format|
      format.json { render json: @device_server}
    end
  end

  def area
    @area = Power.find_distinct_area(current_user.company_guid)
    respond_to do |format|
      format.json { render json: @area}
    end
  end

  def cabinet
    @cabinet = Power.find_distinct_cabinet(current_user.company_guid)
    respond_to do |format|
      format.json { render json: @cabinet}
    end
  end


  private 

  #This method take report_name coming from params and report type as input 
  #and return related old report_id by querying Statement collection 
  def get_actual_report_id(name,type)
    query = Statement
    if name.present?
      query = query.where(:display_name => name)
    end
    if type.present?
      query = query.where(:type => type)
    end
    query.first.report_id
  end  

  def prepare_file_for_download(report_id,rpt)  
    cookies[:fileDownload] = {
       :value => 'true',
       :path => '/'
     }
    report = Statement.where(:report_id => report_id).first    
    if report      
      send_data(rpt,  :type => report.meta_type, 
                      :filename => "#{report.file_name}#{Time.now}#{report.ext}", 
                      :disposition => 'attachment')
    end  
  end

end
