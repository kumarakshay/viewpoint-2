
class Api::V1::Portal::RequestLoggerController < SGTools::Rails::REST.api_v1_controller_class
  def index
    Rails.logger.add_metadata(params["type"] => params["request_logger"]) if Rails.logger.respond_to?(:add_metadata)
    render :nothing => true, :status => 204,:content_type => "application/json"
  end
end
