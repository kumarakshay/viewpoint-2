class Api::V1::Portal::TicketingController < SGTools::Rails::REST.api_v1_controller_class

  before_filter :authorize_call
  before_filter :get_upwurst_data, :if => proc { !APP_CONFIG['stub_webservice']}


  BASE_URL = APP_CONFIG['up_wurst_url'] + '/up/ws'

  #Method : GET
  #Fetch all tasks
  def companies_tasks
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/tasks.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  def companies_tasks_details
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/task_details.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  def companies_tasks_search
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/task_details.json")
      render :text => @records, :content_type => "application/json"
    end
  end


  #Method : GET
  #Displays the list of all services
  def cti_service
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/cti_services.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  #Method : GET
  #Displays the list of all prompts
  def cti_prompts
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/cti_prompts.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  #Method : GET
  #Displays the list of prompts according to service Id
  def cti_prompt
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/cti_prompt.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  def cti_assignment_group
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/assignment_groups.json")
      render :text => @records, :content_type => "application/json"
    end
  end


  #Method : GET
  #Displays the list of all assignment groups according to search string
  def lookup_location_search
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/locations.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  #Method : GET
  #Displays the list of all companies according to search string
  def lookup_company_search
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/company_search.json")
      render :text => @records, :content_type => "application/json"
    end
  end


  #Method : GET
  #Displays the list of all assignment groups according to search string
  def lookup_assignment_group_search
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/assignment_group_search.json")
      render :text => @records, :content_type => "application/json"
    end
  end


  #Method : GET
  #Displays the list of all assignment groups according to search string
  def lookup_ci_search
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/assets.json")
      render :text => @records, :content_type => "application/json"
    end
  end


  #Method : GET
  #Displays the list of all assignment groups according to search string
  def lookup_user_search
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/users.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  #Method : GET
  #Displays the list of all comments according to comment ID.
  def ticketing_comments
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/comment_sys_id.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  #Method : PUT
  #Adds a new comment
  def update_task
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/new_comment.json")
      render :text => @records, :content_type => "application/json"
    end
  end

  #Method : POST
  #Creates tasks
  def task
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ticketing/task.json")
      render :text => @records, :content_type => "application/json"
    end
  end


  private

  def get_upwurst_data()
    url = BASE_URL + request.fullpath
    options = {}
    options["method"] = request.method
    unless params[:ticketing].blank?
      options["query"] = params[:ticketing]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
    render :text => @records.to_json, :content_type => "application/json"
  end

  def authorize_call
    if params[:company_guid]
      if  params[:company_guid] == current_user.company_guid
        return true
      else
        render :text => "Unauthorise Access", :content_type => "application/json"
      end
    end
  end
end
