class Api::V1::Portal::MasqueradesController < SGTools::Rails::REST.api_v1_controller_class
  def index
    data = BSON::OrderedHash.new()
    tc = Time.now
    authorize! :masquerade, sign_in_user
    @records = []
    if params[:search].blank?
      @records = Company.all(:limit => 50)
    else
      data['search_string'] = params[:search]
      @records = Company.search_by_name(params[:search])
    end
    data['total_time'] = Time.now - tc
    Rails.logger.add_metadata('company_impersonate' => data) if Rails.logger.respond_to?(:add_metadata)
  end

  def show
    authorize! :masquerade, sign_in_user
    @records = []
    if params[:company_guid]
      @records = User.where(:company_guid => params[:company_guid], active: true).order("first_name, last_name")
    end
  end

  def new
    @o = @u = nil
    if (sign_in_user.can_masquerade?)
      session.delete(:company_has_childs)
      mu = MasqueradeUser.new(current_user.real_user)
      mu.masquerade_as(params[:id])
      if mu.masqueraded_is_sungard?
        #Sungard user cannot masquerade as another sungard user
        @blocked_user_email = mu.email
        render :json => { :errors => ["Can not process this request."]}, :status => 422
        # render json: [{msg: "Can not process this request."}],:status => 422
      else
        #session[:operator_guid] = mu.guid
        session[:operator_guid] = params[:id]
        # This is used in context of company masquerade, that too when Sungard employee masquerades as
        # someone who is having switch company rights.
        session[:user_mas_guid] = params[:id]
        session[:masquerade] = "true"
        @masquerade = true
        @masquerade_user = mu.user
        session[:is_omi] = @masquerade_user.is_omi?
        session[:user_snt] = @masquerade_user.company.snt
        session[:masquerade_company] = @masquerade_user.company.name
        session[:masquerade_company_id] = @masquerade_user.company_guid
        ##  set has childs if mu has child companies
        set_has_childs_to_session(@masquerade_user)
        render :nothing => true, :status => 204,:content_type => "application/json"
      end
    else
      render :json => { :errors => ["Can not process this request."]}, :status => 422
    end
  end

  def destroy
    session[:operator_guid] = session[:user_guid]
    session.delete(:company_mas_guid)
    session.delete(:user_mas_guid)
    session.delete(:company_has_childs)
    session[:masquerade] = "false"
    @masquerade = false
    session.delete(:is_omi)
    session.delete(:user_snt)
    session.delete(:masquerade_company)
    session.delete(:masquerade_company_id)
    @o = @u = nil #resetting current_user
    render :nothing => true, :status => 204,:content_type => "application/json"
  end

  private

  def set_has_childs_to_session(user)
    # # Let know if current user company has child companies.
    if user.role?('switch_company') and not session.include?(:company_has_childs)
      session[:company_has_childs] = Company.parent_child_list(user.company_guid).count > 1
    end
  end
end
