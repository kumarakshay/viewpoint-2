class Api::V1::Portal::MockController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_access, only: [:compliance_status, :capacity_status]

  def vp_tools_selector
    @records = File.read("#{Rails.root}/test/fixtures/unified/tools_selector.vp.json")
    render :text => '{"result":' + @records + '}', :content_type => "application/json"
  end

  def raas_tools_selector
    @records = File.read("#{Rails.root}/test/fixtures/unified/tools_selector.raas.json")
    render :text => '{"result":' + @records + '}', :content_type => "application/json"
  end

  def get_info_html
    if params[:page_id]
      case params[:page_id]
      when 'ms-ehealth'
        # load html for ehealth page
        html = File.read("#{Rails.root}/test/fixtures/help/ehealth.html")
      when 'ms-ticket-landing'
        # load html for ticket landing page
        html = File.read("#{Rails.root}/test/fixtures/help/ticket-landing.html")
      when 'ms-reports'
        # load html for reports page
        str = session[:is_omi] ? "obr-reports.html" : "reports.html"
        html = File.read("#{Rails.root}/test/fixtures/help/#{str}")
      when 'ms-events'
        # load html for events page
        html = File.read("#{Rails.root}/test/fixtures/help/events.html")
      when 'ms-omi-events'
        # load html for omi-events page
        html = File.read("#{Rails.root}/test/fixtures/help/omi-events.html")
      when 'ms-interfaces'
        # load html for interfaces page
        html = File.read("#{Rails.root}/test/fixtures/help/interfaces.html")
      when 'ms-omi-interfaces'
        # load html for omi-interfaces page
        html = File.read("#{Rails.root}/test/fixtures/help/omi-interfaces.html")
      when 'ms-devices'
        # load html for devices page
        html = File.read("#{Rails.root}/test/fixtures/help/devices.html")
      when 'ms-omi-devices'
        # load html for omi-devices page
        html = File.read("#{Rails.root}/test/fixtures/help/omi-devices.html")
      when 'ms-event-subscriptions'
        # load html for event-subscriptions page
        html = File.read("#{Rails.root}/test/fixtures/help/event-subscriptions.html")
      when 'ms-crShow'
        # load html for command runner show
        html = File.read("#{Rails.root}/test/fixtures/help/crShow.html")
      when 'ms-crFirewall'
        # load html for command runner schedule change
        html = File.read("#{Rails.root}/test/fixtures/help/crFirewall.html")
      when 'ms-firewalls'
        # load html for firewall page
        html = File.read("#{Rails.root}/test/fixtures/help/firewalls.html")
      when 'ms-ticket-landing'
        # load html for ticket-landing page
        html = File.read("#{Rails.root}/test/fixtures/help/ticket-landing.html")
      when 'ms-company-admin'
        # load html for company-admin page
        html = File.read("#{Rails.root}/test/fixtures/help/company-admin.html")
      when 'ms-groups'
        # load html for groups page
        html = File.read("#{Rails.root}/test/fixtures/help/groups.html")
      when 'ms-dns'
        # load html for dns page
        html = File.read("#{Rails.root}/test/fixtures/help/dns-ipam.html")
      when 'ms-report-subscription'
        # load html for report-subscription page
        url = session[:is_omi] ? "obr-subscriptions.html" : "report-subscription.html"
        html = File.read("#{Rails.root}/test/fixtures/help/#{url}")
      when 'ms-ipv4'
        # load html for dns page
        html = File.read("#{Rails.root}/test/fixtures/help/ipv4.html")
      when 'ms-ipv6'
        # load html for dns page
        html = File.read("#{Rails.root}/test/fixtures/help/ipv6.html")
      when 'ms-dns-audits'
        # load html for dns audit page
        html = File.read("#{Rails.root}/test/fixtures/help/dns-audits.html")
      when 'ms-portal-landing'
        # load html for dns audit page
        html = File.read("#{Rails.root}/test/fixtures/help/portal-landing.html")
      when 'ms-policy'
        # load html for patch policy page
        html = File.read("#{Rails.root}/test/fixtures/help/patch-policy.html")
      when 'ms-schedule'
        # load html for patch schedule page
        html = File.read("#{Rails.root}/test/fixtures/help/patch-schedule.html")
      when 'ms-servers'
        # load html for patch server page
        html = File.read("#{Rails.root}/test/fixtures/help/patch-server.html")
      else
        html = File.read("#{Rails.root}/test/fixtures/help/nyi.html")
      end
    else
      html = File.read("#{Rails.root}/test/fixtures/help/nyi.html")
    end
    render :text => html
  end

  def help_sections
    @records = HelpSection.where(:is_active => true).order("orderIndex ASC").all.group_by{ |obj| obj["type"]} rescue []
    render :text => '{"meta":null,"status": 200, "result": ' + @records.to_json + '}', :content_type => "application/json"
  end

  def compliance_status
    if params[:type].downcase == 'compute'
      @records = ComplianceCompute.get_compliance_widget
    elsif params[:type].downcase == 'storage'
      @records = []
    else
      @records = Compliance.get_compliance_widget
    end
    render :json =>  @records
  end

  def capacity_status
    @records = Hash.new
    if params && params[:type].present?
      sets = Capacity.for_device_type(params[:type]).all.group_by(&:name)
      result = sets.map do |k,obj|
        [k,obj.group_by{|s|s.location }]
      end.to_h
      result.each do |k, l_hash|
        #VUPT-5594 remove MOS_OCS_PortCapacity option from the dropdown
        unless (k == 'MOS_OCS_PortCapacity')
          @records[k] = []
          l_hash.each do |l,v|
            a = Hash.new
            a["type"]= v.map(&:type).uniq.first
            a["location"]= l
            a["total"] = v.map(&:total).sum
            a["used"] = v.map(&:used).sum
            a["available"] = v.map(&:available).sum
            a["unit"] = v.map(&:unit).uniq.first
            @records[k] << a
          end
        end
      end
    end
    render :json =>  @records
  end

  private
  def authorize_access
    unless current_user.is_sungard?
      render "api/v1/shared/unauthorize", :status => 401
      return false
    end
  end
end
