class Api::V1::Portal::ToolSelectorsController < SGTools::Rails::REST.api_v1_controller_class
  def index
    @get_sn_url = get_sn_url
    @current_user = current_user
    @homepage_url = get_home_url
    if !current_user.is_sungard? && current_user.portal_patching_access? && !APP_CONFIG['vp_cauldron']
      client = ServiceNowClient.new
      @sn_permissions = client.get_user_roles(current_user.guid) rescue {:server_change=>"0"}
    end
  end

  def quick_links
    @current_user = current_user
  end

  def get_sn_url()
    if current_user
      if (current_user.is_sungard? || @masquerade)
        url = APP_CONFIG['service_now_url'] + "/saml_redirector.do?sysparm_uri=navpage.do"
      elsif (current_user.is_allstream_user? and !(request.url.match('allstream.com').nil?))
        url = APP_CONFIG['service_now_allstream'] + "/saml_redirector.do?sysparm_uri=snc/sg/home.do"
      else
        url = APP_CONFIG['service_now_url'] + "/saml_redirector.do?sysparm_uri=sg/home.do"
      end
    else
      url = APP_CONFIG['service_now_url']
    end
    return url
  end

  def get_home_url()
    return "/my_board"
  end
end
