class Api::V1::Portal::UsersController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :load_instance, :only => [:update,:forgot_password]

  def update
    begin
      current_email = @user.email
      change_email = current_email != params[:user][:email] ? params[:user][:email] : nil
      if (sign_in_user.id == @user.id || sign_in_user.is_sungard?)
        if is_valid_with_new_params? && phone_valid?
          @user.email = current_email #restored original email
          if @user.update_user_data(change_email)
            if @user.save_sn_data!(params[:user])
              @user.reload
              render :nothing => true, :status => 204,:content_type => "application/json"
            else
              render :json => { :errors => ["Error: profile not saved"]}, :status => 422
            end
          else
            render :json => { :errors => ["That email is already being used. Profile not saved"]}, :status => 422
          end
        else
          render :json => { :errors => ["Can not process this request"]}, :status => 422
        end
      else
        render :json => { :errors => ["You are not the authorized person to update this"]}, :status => 422
      end
    rescue Exception => e
      render :json => { :errors => ["Can not process this request"]}, :status => 422
    end
  end

  def forgot_password
    begin
      if @user and @user.send_password_reset!
        render json: {result: { msg: 'Email sent with password reset instructions'}}, status: 200
      else
        render :json => { :errors => ["Unable to reset password"]}, :status => 422
      end
    rescue
      render :json => { :errors => ["Can not process this request"]}, :status => 422
    end
  end

  def country_codes
    records = CountryCode.order(:name).all
    render json: records
  end

  def timezones
    records = Timezone.order(:label).all
    render json: records
  end

  def sn_permissions
    client = ServiceNowClient.new
    record = client.get_user_roles(current_user.guid) rescue {}
    render json: record
  end


  private

  # Checks if user object is valid after considering new input parameters.
  def is_valid_with_new_params?
    @user.attributes = params[:user]
    @user.valid?
  end

  def phone_valid?
    matched_phone = /(^\d{3,13}$)|(^\d{3}[\-\s]?\d{3}[\-\s]?\d{4}$)|(^\d{2}[\-\s]?\d{3}[\-\s]?\d{3}[\-\s]?\d{4}$)|(^\d{3}[\-\s]?\d{3}[\-\s]?\d{3}[\-\s]?\d{3}$)/
    params[:user][:phone_number].match(matched_phone) && ( params[:user][:cell_phone].blank? || params[:user][:cell_phone].match(matched_phone))
  end

  def load_instance
    begin
      @user = User.find(params[:id]) || User.where(:guid => params[:user_guid]).first
      if @user.blank?
        Rails.logger.info("Unable to load user with id  #{params[:id]}")
        render :json => { :errors => ["Can not process this request."]}, :status => 422
      end
    rescue
      Rails.logger.warning("Unable to load user with id  #{params[:id]}")
      render :json => { :errors => ["Can not process this request."]}, :status => 422
    end
  end
end
