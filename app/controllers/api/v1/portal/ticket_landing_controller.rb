class Api::V1::Portal::TicketLandingController < SGTools::Rails::REST.api_v1_controller_class

  before_filter :authorize_call
  before_filter :get_up_wurst_data, :if => proc { !APP_CONFIG['stub_webservice']}

  BASE_URL = APP_CONFIG['up_wurst_url'] + '/up/ws'

  def tickets
    if APP_CONFIG['stub_webservice_ticketing']
      @mock_records = File.read("#{Rails.root}/test/fixtures/ticket_landing/tickets.json")
      render :text => @mock_records, :content_type => "application/json"
    else
      render :text => @records.to_json, :content_type => "application/json"
    end
  end

  def ticket_stats
    if APP_CONFIG['stub_webservice_ticketing']
      @mock_records = File.read("#{Rails.root}/test/fixtures/ticket_landing/ticket_stats.json")
      render :text => @mock_records, :content_type => "application/json"
    else
      render :text => @records.to_json, :content_type => "application/json"
    end
  end

  private

  def get_up_wurst_data()
    url = BASE_URL + request.fullpath
    url = url + "&is_sungard_user=true&email=" + current_user.email if current_user.is_sungard?
    options = {}
    options["method"] = request.method
    unless params[:ticket_landing].blank?
      options["query"] = params[:ticket_landing]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
  end

  def authorize_call
    if params[:company_guid]
      if  params[:company_guid] == current_user.company_guid
        return true
      else
        render :text => "Unauthorise Access", :content_type => "application/json"
      end
    end
  end
end
