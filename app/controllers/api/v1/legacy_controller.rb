class Api::V1::LegacyController < SGTools::Rails::REST.api_v1_controller_class
  def show
    if APP_CONFIG['stub_webservice']
      Rails.logger.info("stubbed data")
      guid = "stubbed subaru"
      dat = File.read("#{Rails.root}/test/fixtures/zenoss/details.json")
      render :json => dat
    else
      Rails.logger.info("live data")
      metadata = "live"
      device_name = "zencon2"
      dat = SGTools::Zenoss::Device.device_details(device_name)
    end  
    @metadata = metadata
    @dat = JSON.parse(dat)
  end
end