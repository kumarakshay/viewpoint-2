class Api::V1::InterfacesController < SGTools::Rails::REST.api_v1_controller_class

  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/interfaces.json")
    render :json => dat
  end
  
end