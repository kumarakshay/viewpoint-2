class Api::V1::ThresholdsController < SGTools::Rails::REST.api_v1_controller_class
  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/thresholds.json")
    render :json => dat
  end
end