class Api::V1::SystemsController < ToolsTeamController

  def login_count
    s = SystemInfo.new
    render json: s.login_count
  end

  def echo
    m = 'echo'
    m = params[:m] if(params[:m])
    render json: m
  end

  # @return ok when Company.acl_monitor is greater than success threshold
  # @return problem when Company.acl_monitor is less than success threshold
  def acl
    m = Company.acl_monitor
    render json: m
  end
end