class Api::V1::ReqsController < SGTools::Rails::REST.api_v1_controller_class
  include SGTools::Rails::REST::RestfulResources

  MODEL_CLASS = Req
  before_filter :set_model_class
  before_filter :load_records, :only => [:index]
  before_filter lambda { request.session_options[:skip] = true }

  def create
    params[:req][:company_guid] = current_user.company_guid
    params[:req][:user_guid] = current_user.guid
    params[:req][:email] = current_user.email
    @req = Req.new(params[:req])
    @req.generate_job_id
    @req.report = @req.report_name
    # req"=>{"report_id"=>"1", "group_guid"=>"", "device_guid"=>"b878767c-0111-abee-e040-a2a8567d3005", "start_date"=>"", "end_date"=>"", "subscription"=>"", "frequency"=>"", "delivery_time"=>"", "time_zone"=>"", "email"=>"", "company_guid"=>"", "user_guid"=>""}, "report_subject"=>"device", "format"=>"json", "action"=>"create", "controller"=>"api/v1/reqs"}
    # req"=>{"report_id"=>"1", "group_guid"=>"", "device_guid"=>"b878767c-0111-abee-e040-a2a8567d3005", "start_date"=>"2016-03-01", "end_date"=>"yyyy-mm-dd", "subscription"=>"0", "frequency"=>"", "delivery_time"=>"", "time_zone"=>"American Samoa", "email"=>"jhoch@subaru.com"}, "report_subject"=>"device"}
    if session[:masquerade] == "true"
      @req.masquerade_email = sign_in_user.email
    end
    respond_to do |format|
      if @req.save
        if APP_CONFIG['bus_report_process']
          format.json { render json: @req, status: :created, location: @req }
        else
          format.json { render json: @req, status: :created, location: @req }
        end
      else
        format.json { render json: @req.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @req = MODEL_CLASS.find(params[:id])
    unless @req.blank?
      response = delete_user_subscription(@req.report_id, @req.obr_schedule_id)
      if(!response.blank? && (response.has_key? 'id') && response["id"].to_s === @req.obr_schedule_id)
        if @req.destroy
          render nothing: true, status: 204
          return
        end
      end
    end
    render json: {msg: "Can not process this request."}, status: 422
  end

  def index
    unless @records.blank?
      @records = @records.sort(:created_at.asc)
    else
      render :text => '{"result":' + @records.count.to_s + '}', :content_type => "application/json"
    end
  end

  def subscriptions
    # users subscriptions
    @reqs = MODEL_CLASS.where(:$or => [{:user_guid => current_user.guid}, {:email => current_user.email}], subscription: true).all
    render :text => '{"result":' + @reqs.to_json + '}', :content_type => "application/json"
  end

  #Type - PUT
  #This method update ack status to true for a given job_id
  #/api/reqs/ack?job_id=xxx
  def ack
    @record = MODEL_CLASS.where(:job_id => params[:job_id]).first
    if @record and @record.update_attribute(:acknowledged,true)
      render :text => '{"meta":null,"status": 200, "result": []}', :content_type => "application/json"
    else
      render json: [{msg: "Error occurred during operation"}],status: :unprocessable_entity
    end
  end

  private

  def set_model_class
    model_class = session[:is_omi] ? ReqObr : Req
    self.class.send(:remove_const, "MODEL_CLASS") if self.class.const_defined?("MODEL_CLASS")
    self.class.const_set "MODEL_CLASS", model_class
  end

  def load_records
    params = request.query_parameters
    #the api/reqs should return last 30 days records as a deafult
    if params[:__filter].blank?
      params.merge!({"__filter"=>{"created_after"=> 10.days.ago.to_time.to_i}})
    elsif params["__filter"]["created_after"].blank?
      params["__filter"].merge!({"created_after" => 10.days.ago.to_time.to_i})
    end

    unless params[:user_guid].blank?
      params.merge!('user_guid' => params[:user_guid])
    else
      params.merge!('user_guid' => current_user.guid)
    end
    super
  end

  def delete_user_subscription(doc_id, scheduled_id)
    options = {}
    url = Figaro.env.omi_base_url + ":443/reporting/documents/#{doc_id}/schedules/#{scheduled_id}"
    header = {"Authorization" => Figaro.env.omi_obr_authorization}
    options["header"] = header
    options["method"] = request.method
    rest_client = HttpClient.new(url, options)
    response = rest_client.fetch
  end
end
