class Api::V1::ObrReporting::ReqObrController < SGTools::Rails::REST.api_v1_controller_class
  REQ_COLLECTION = ReqObr
  ADHOCH_REPORT_PATH="user_adhoc_reports"
  BASE_URL = Figaro.env.obr_reporting_base_url
  AUTHORIZATION = Figaro.env.omi_obr_authorization
  PORT = Figaro.env.omi_obr_port

	def create
		job_id = ReqObr.initialize_job_id
		obr_params =  params["req_obr"]
		company_guid = obr_params["company_guid"]
		is_masqueraded = sign_in_user.is_sungard?
		obr_statement = StatementObr.where(obr_report_id: obr_params['obr_id'], name: params['name']).last
		obr_statement_name = obr_statement.name.gsub(/[^0-9A-Za-z]/, '').camelcase
		name = "#{company_guid}-#{ADHOCH_REPORT_PATH}~#{obr_statement_name}.#{job_id}"
		req_obr = ReqObr.new(job_id: job_id, obr_schedule_id: obr_params[:obr_id], company_guid: company_guid, user_guid: current_user.guid,
												 report: obr_statement.name, report_id: obr_statement.obr_report_id, group_guid: obr_params[:group_guid],
												 start_date: obr_params[:start_date], end_date: obr_params[:end_date], email: obr_params[:email],
												 frequency: obr_params[:recurrence_type], subscription: obr_params[:recurrence], is_masqueraded: is_masqueraded,
												 time: obr_params[:time]
												 )
		file_type = obr_params[:file_type].empty? ? "pdf" : obr_params[:file_type]
		payload = default_params(name, obr_params, obr_statement, company_guid, file_type)
		if !obr_params[:recurrence]
			payload = process_adhoc_payload(payload, obr_statement, obr_params)
		else
			payload = process_subscription_payload(payload, obr_params, obr_statement)
		end
		if obr_statement.network_guid
			network_guid_parameter = process_parameter(obr_statement.network_guid_dpid, obr_statement.network_guid_id, obr_statement.network_guid_technical_name, obr_statement.network_guid_name, company_guid.downcase, "Text")
			payload["parameters"]["parameter"].push(network_guid_parameter)
		end
		if obr_statement.is_device_report
			nodes_parameter = process_node_values(obr_params, obr_statement)
			payload["parameters"]["parameter"].push(nodes_parameter)
		end
		url = BASE_URL + ":" + PORT + "/reporting/documents/" + obr_params['obr_id'] + "/schedules"
		response = post_obr_parameters(url, payload)
		respond_to do |format|
			if response["success"] && response["success"]["id"]
				req_obr.obr_schedule_id = response["success"]["id"]
				req_obr.device_guid = OmiDevice.find_by_global_id(obr_params[:device_id]).asset_guid if obr_params[:device_id].present?
				req_obr.payload = payload
				begin
					if req_obr.save
						format.json { render json: response }
					else
						format.json { render :json => {:error => { :message => "Can not process this request"}}}
					end
				rescue Exception => e
					puts "#{e.message}"
      		logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      		{ :error => { :message => e.message}}
      	end
			else
				format.json { render json: response }
			end
		end
	end

	def process_parameter(dpid, id, technical_name, name, value, type)
		{ "@dpId" =>  dpid, "@type" => "prompt", "@optional" => "true", "id" => id, "technicalName" => technical_name,
      "name" => name,
      "answer" => {
        "@type" => type,
        "@constrained" => "false",
        "values" => {
          "value" => value
          }
        }
    }
  end

	def post_obr_parameters(url, payload)
		begin
	    options = {}
	    header = {"Authorization" => AUTHORIZATION}
	    options["header"] = header
	    options["method"] = request.method
	    options["query"] = payload
	    Rails.logger.info "******url**#{url}"
	    Rails.logger.info "******options**#{options}"
	    rest_client = HttpClient.new(url, options)
	    rest_client.fetch
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      { :error => { :message => "Can not process this request"}}
    end
	end

	def default_params( name, obr_params, obr_statement, company_guid, file_type)
		{	"name"=>name,
		 	"file_type"=> file_type,
		 	"destination"=> { "type"=> "sftp","name"=> "fred"},
 		 	"recurrence"=> {
 		 		"type"=> "daily",
 		 		"retries"=> 2,
 		 		"retry_interval"=> 60,
 		 		"start_date"=> obr_params[:start_date],
 		 		"end_date"=> obr_params[:end_date]
 		 	},
 		  "parameters"=>{
 		 		"parameter"=>[
 		 			{"@dpId"=> obr_statement.company_guid_dpid,
 		 				"@type"=>"prompt",
 		 				"@optional"=>"true",
 		 				"id"=> obr_statement.comapny_guid_id,
 		 				"technicalName"=>obr_statement.company_guid_technical_name,
 		 				"name"=> obr_statement.company_guid_name,
 						"answer"=>
 							{"@type"=>"Text", "@constrained"=>"false",
 								"values"=>{"value"=>company_guid.downcase}
 							}
 					}
 				]
 			}
 		}
	end

	def process_adhoc_payload(payload, obr_statement, obr_params)
		payload.delete("recurrence")
		start_date_parameter = process_parameter(obr_statement.start_date_dpid, obr_statement.start_date_id, obr_statement.start_date_technical_name, obr_statement.start_date_name, obr_params["start_date"], "DateTime")
		payload["parameters"]["parameter"].push(start_date_parameter)
		end_date_parameter = process_parameter(obr_statement.end_date_dpid, obr_statement.end_date_id, obr_statement.end_date_technical_name, obr_statement.end_date_name, obr_params["end_date"], "DateTime")
		payload["parameters"]["parameter"].push(end_date_parameter)
		payload
	end

	def process_subscription_payload(payload, obr_params, obr_statement)
		payload['recurrence']['type'] = obr_params[:recurrence_type] || ''
		if obr_params[:recurrence_type] == "daily"
			payload['recurrence']['day_interval'] = 1
			range = 'Last One Day'
		end
		if obr_params[:recurrence_type] == "weekly"
			payload['recurrence']['monday'] = true
			range = 'Last Week'
		end
		if obr_params[:recurrence_type] == "monthly"
			payload['recurrence']['month_interval'] = 1
			range = 'Last Month'
		end
		date_range_parameter = process_parameter(obr_statement.date_range_dpid, obr_statement.date_range_id, obr_statement.date_range_technical_name, obr_statement.date_range_name, range, "Text")
		payload["parameters"]["parameter"].push(date_range_parameter)
		payload
	end

	def process_node_values(obr_params, obr_statement)
		if obr_params[:group_id].present?
			group = Group.find_by_id(obr_params[:group_id])	if obr_params[:group_id].present?
			node_values = OmiDevice.find_all_by_asset_guid(group.asset_list).collect(&:global_id).compact.reject(&:empty?)
		else
			node_values = obr_params[:device_id]
		end
		process_parameter(obr_statement.global_id_dpid, obr_statement.global_id_id, obr_statement.global_id_technical_name, obr_statement.global_id_name, node_values, "Text")
	end
end

