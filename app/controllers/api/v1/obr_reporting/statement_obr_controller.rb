class Api::V1::ObrReporting::StatementObrController < SGTools::Rails::REST.api_v1_controller_class
	STATEMENT_COLLECTION = StatementObr

	def index
		@statements = STATEMENT_COLLECTION.where(can_show: true).order(:name).all
	end
end