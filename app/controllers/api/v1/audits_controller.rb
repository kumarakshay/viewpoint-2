require 'csv'
class Api::V1::AuditsController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :validate_user_access

  def login_statistic
  	query = {:auditable_type => "User", :action =>  "login"}
  	if params[:start_date].present? && params[:end_date].present?
  		start_date = params[:start_date].to_time.beginning_of_day rescue ""
  		end_date = params[:end_date].to_time.end_of_day rescue ""
  	else
  		start_date = (Date.today - 1.month).to_time.beginning_of_day
  		end_date = (Date.today).to_time.end_of_day
  	end
  	query.merge!({:created_at.gte => start_date, :created_at.lte => end_date})
  	if params[:email].present?
  		query.merge!({:user => params[:email]})
  	end
  	if params[:company_guid].present?
  		query.merge!({:company => params[:company_guid]})
  	end
  	if params[:internal_user].present? && params[:internal_user] == "true"
  		query.merge!({:sungard_user => true})
  	end
  	if params[:external_user].present? && params[:external_user] == "true"
  		query.merge!({:sungard_user => false})
  	end
  	@audits = Audit.where(query).order("created_at ASC").all
  	@internal_user_count = @audits.select{|a| a.sungard_user}.count
  	@external_user_count = @audits.select{|a| !a.sungard_user}.count
  	@statistic = {total: @audits.count, internal_user_count: @internal_user_count, external_user_count: @external_user_count}
  	if params["download"].present?
  	csv_string = CSV.generate do |csv|
      csv << ["User", "Company Name", "Sungard User", "Login Date"]
      @audits.each do |audit|
        csv << [audit.user, audit.company_name, audit.sungard_user, audit.created_at.to_date]
      end
    end
      send_data( csv_string, :type => 'text/csv', :filename => "login_statistic_#{Time.now}.csv", :disposition => 'attachment')
  	end
  end

  private

  def validate_user_access
    if current_user.is_sungard?
      authenticate_or_request_with_http_basic do |username, password|
        username == APP_CONFIG['api_user'] && password == APP_CONFIG['api_password']
      end
    else
      if current_user.company_guid == "53DA12A8-C067-40E7-85AA-05C8DC863E09"
        params[:company_guid] = current_user.company_guid
      else
        render :json => { :errors => ["Can not process this request"]}, :status => 422
      end
    end
  end
end