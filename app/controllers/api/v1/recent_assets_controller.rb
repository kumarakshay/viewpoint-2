class Api::V1::RecentAssetsController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :restrict_action, :only => [:create, :update, :destroy]


  def index
    @records = RecentAsset.where(:company_guid => current_user.company_guid,:user_guid => current_user.guid).sort(:updated_at.desc).limit(20);
  end

  private

  def restrict_action
    render json: {msg: "Restricted action"}, status: 403
  end
end
