class Api::V1::CurrentUsersController < SGTools::Rails::REST.api_v1_controller_class
  # skip_before_filter  :authenticated?
  include "ApplicationHelper".constantize

  def index
    if APP_CONFIG['stub_webservice']
      user_hash = JSON.parse(File.read("#{Rails.root}/test/fixtures/ticket_landing/current_user.json"))
    else
      user_hash = {}
      if current_user
        #Inser some general information
        user_hash["masquerade"] = @masquerade
        user_hash["company_masquerade"] = @company_masquerade
        user_hash["branding"] = @branding
        user_hash["environment"] = Rails.env
        #Insert current_user company informations
        current_user_hash = current_user.as_json
        company = current_user.company
        current_user_hash["company_name"] = company.name
        current_user_hash["company_snt"] = company.snt
        current_user_hash["support_org"] = company.support_org
        current_user_hash["job_title"] = current_user.job_title
        current_user_hash.delete("roles")
        user_hash.merge!({"current_user" => current_user_hash})
        user_hash["current_user"]["phone_number_cc"] = current_user.phone_number_cc
        user_hash["current_user"]["phone"] = current_user.phone_number
        user_hash["current_user"]["phone_number_ext"] = current_user.phone_number_ext
        user_hash["current_user"]["business_phone_notes"] = current_user.business_phone_notes
        user_hash["current_user"]["cell_phone_cc"] = current_user.cell_phone_cc
        user_hash["current_user"]["cell_phone"] = current_user.cell_phone
        user_hash["current_user"]["cell_phone_ext"] = current_user.cell_phone_ext
        user_hash["current_user"]["cell_phone_notes"] = current_user.cell_phone_notes
        user_hash["current_user"]["is_sungard"] = current_user.is_sungard?
        user_hash["current_user"]["is_omi"] = session[:is_omi]
        user_hash["current_user"]["is_backup"] = current_user.company.is_backup_company?
        user_hash["current_user"]["access_to_cloud_portal"] = current_user.company.cloud_portal || false rescue false
        user_hash["current_user"]["company_user_count"] = company.user_count
        user_hash["current_user"]["masquerade_company"] = session[:masquerade_company] ? session[:masquerade_company] : ""
        user_hash["current_user"]["masquerade_company_id"] = session[:masquerade_company_id] ? session[:masquerade_company_id] : ""
        user_hash["current_user"]["is_apm"] = company.is_apm || false rescue false
        #Insert current user's abilities
        ability = Ability.new(current_user)
        ability_hash = {"abilities" => {}}
        ability_hash["abilities"]["can_read_DNS"] = ability.can? :read, SGTools::Rails::Infoblox::Dns::Zone
        ability_hash["abilities"]["can_masquerade"] = current_user.can_masquerade?
        ability_hash["abilities"]["can_see_ticketing"] = Access.can_access_SN(current_user.company_guid)
        user_hash["current_user"].merge!(ability_hash)
        #Inser user viewpoint roles into current user object
        user_hash["current_user"].merge!({"vp_roles" => {}})
        vp_roles = Role.all.collect(&:name)
        cu_roles = current_user.roles
        vp_roles.each{|a| user_hash["current_user"]["vp_roles"][a] = cu_roles.include?(a) ? true : false }
        #Insert current user's servicenow related roles and permission
        sn_role_hash = {}
        sn_role_hash.merge!({"sn_roles" => {"grant_access" => "0","notification" => "0","authorization" => "0", "server_change"=>"0"}})
        ## UP-1953 Removing SN permissions from current user object
        # client = ServiceNowClient.new
        # sn_permissions = client.get_user_roles(current_user.guid) rescue nil
        # unless sn_permissions.blank?
        #   sn_role_hash["sn_roles"]["grant_access"] = sn_permissions[:grant_access]
        #   sn_role_hash["sn_roles"]["notification"] = sn_permissions[:notification]
        #   sn_role_hash["sn_roles"]["authorization"] = sn_permissions[:authorization]
        #   sn_role_hash['sn_roles']["server_change"] = sn_permissions[:server_change]
        # end
        user_hash["current_user"].merge!(sn_role_hash)

        #Insert servicenow base URL into current user object
        url = current_user.is_allstream_user? ? APP_CONFIG['service_now_allstream'] : APP_CONFIG['service_now_url']
        user_hash["current_user"].merge!({"sn_URL" => url})
        ticketing_url = get_sn_url()
        user_hash["current_user"].merge!({"ticketing_url" => ticketing_url})
        user_hash["current_user"].merge!({"portal_url" => APP_CONFIG['legacy_portal']})

        #Insert sign in user details
        user_hash.merge!({"sign_in_user" => {}})
        user_hash["sign_in_user"]["first_name"] = sign_in_user.first_name
        user_hash["sign_in_user"]["last_name"] = sign_in_user.last_name
        user_hash["sign_in_user"]["email"] = sign_in_user.email
        user_hash["sign_in_user"]["guid"] = sign_in_user.guid
        user_hash["sign_in_user"]["company_name"] = sign_in_user.company_name
        user_hash["sign_in_user"]["company_guid"] = sign_in_user.company_guid
        user_hash["sign_in_user"]["is_sungard"] = sign_in_user.is_sungard?
        #Insert session details
        user_hash.merge!({"session" => {"company" => session[:user_company],"company_has_childs" => (session[:company_has_childs].blank? ? false : session[:company_has_childs])}})
      end
    end
    @record = OpenStruct.new(user_hash)
  end
end
