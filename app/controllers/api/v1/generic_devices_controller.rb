class Api::V1::GenericDevicesController < SGTools::Rails::REST.api_v1_controller_class
  MODEL_CLASS = Device
  include SGTools::Rails::REST::RestfulResources
  before_filter :set_model_class
  before_filter :load_records, :only => [:index]
  before_filter :restrict_action, :only => [:create, :update, :destroy]


  def index
    #There are devices having same asset_guid which is causing for asset count issue in the group
    #For fixing this issue below change has been added. #VUPT-3844
    @records = @records.all.uniq_by(&:asset_guid)
  end

  private

  def restrict_action
    render json: {msg: "Restricted action"}, status: 403
  end

  private

  def load_records
    params = request.query_parameters
    if params[:__filter].blank?
      params.merge!({"__filter"=>{"for_company"=>current_user.company_guid}})
    else
      params["__filter"].merge!({"for_company"=>current_user.company_guid})
    end
    super
  end

  def set_model_class
    model_class = session[:is_omi] ? OmiDevice : Device
    self.class.send(:remove_const, "MODEL_CLASS") if self.class.const_defined?("MODEL_CLASS")
    self.class.const_set "MODEL_CLASS", model_class
  end
end
