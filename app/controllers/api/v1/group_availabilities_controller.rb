module Api
  module V1
    class GroupAvailabilitiesController < ToolsTeamController
      def index
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/device_glance.json")
          @metadata = "stubbed device glance"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            DeviceGlance.new(r)
          end
        else
          DeviceReport.bulk_load("15")
          @dats = DeviceReport.where(:report_id => "15").all  
          @metadata = "Group availabilties"
        end  
      end
    end
  end
end