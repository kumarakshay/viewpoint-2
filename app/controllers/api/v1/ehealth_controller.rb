class Api::V1::EhealthController < SGTools::Rails::REST.api_v1_controller_class
  include SGTools::Rails::REST::RestfulResources

  # before_filter :fetch_healths, only: [:asset_types, :active_assets_by_type, :active_assets ]

  CACHE_EXPIRE_TIME = 60.minutes
  EH_CACHE_EXPIRE_TIME = 10.minutes
  BACKUP_MONTHS = 13

  def events
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/events.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      @records = []
      if current_user
        eh = SGTools::Zenoss::EventHistogram.new(current_user.company_guid.downcase, :z_instance => 'mom')
        if eh
          eh.load!
          eh_data = eh.timestamp_keyed_data
          mappings = {"5" => "critical", "4" => "error", "3" => "warning", "2" => "information"}
          eh_data.sort.map do |y,z|
            temp = {}
            temp.merge!({"date" => y*1000})
            temp.merge!(Hash[z.map {|k, v| [mappings[k], v] if mappings[k] }])
            @records << OpenStruct.new(temp)
          end
        end
      end
    end
  end

  def backups
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/backups.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      @records = []
      if current_user
        @records =  Ehealth::BackupReport.bulk_load(current_user.company_guid)
      end
    end
  end

  def backups_for_month
    @records = []
    if current_user && params[:filename].present?
      date_arr = params[:filename].split("-")
      @records =  Ehealth::BackupReport.where(:company_guid => current_user.company_guid, :filename => params[:filename]).all
      unless @records.blank?
        last_month_end_dt = (date_arr[2] + "-" + date_arr[3] + "-" + date_arr[4]).to_date.at_beginning_of_month - 1.day
        @records << Ehealth::BackupReport.new({:company_guid => current_user.company_guid,:filename => params[:filename],:date => last_month_end_dt,:success_rate => 0.0})
      end
    end
    @records.sort_by!{|obj|obj.date}
  end

  def backup_months
    @records = []
    if current_user
      #VUPT-4211 Provide 13 months of Backup Success Rates Reports for Customers
      # We will pick previous 4 Months for now.
      backups =  Ehealth::BackupReport.where(:company_guid => current_user.company_guid).order("date DESC").collect(&:filename).uniq[0..12]
      unless backups.blank?
        backups.each do |backup|
          temp = backup.split("-")
          str = temp[2] + temp[3] + temp[4]
          @records << OpenStruct.new({"name" => str.to_date.strftime("%B %Y").to_s,"filename" => backup })
        end
      end
    end
  end

  def environment_healths
    @records = {"result" => {}}.to_json
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/environment_healths.json")
    else
      company = Company.where(:guid =>current_user.company_guid).first
      unless company.blank?
        z_instance = company.zenoss_instance
        unless z_instance.blank?
          severity = SGTools::Zenoss::MaxSeverity.new(company.guid,{:z_instance => z_instance})
          @records = severity.max_severities
        end
      end
    end
    render :text => '{"result":' + @records + '}', :content_type => "application/json"
  end

  def groups_environment_health
    @record = {"result" => {}}.to_json
    if APP_CONFIG['stub_webservice']
      @record = File.read("#{Rails.root}/test/fixtures/ehealth/groups_environment_health.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      company = Company.where(:guid =>current_user.company_guid).first
      unless company.blank?
        @record = company.group_availability
      end
      @record
    end
  end

  def asset_types
    @records = []
    x = Ehealth::ActiveAsset
    types =  Cacherank.fetch('ehealth_active_assets', current_user.company_guid).collect(&:asset_type).compact.uniq rescue Array.new()
    types.each do |type|
      @records << OpenStruct.new({"name" => type})
    end
    @records
  end

  # /ehealth/:group_id/group_active_assets
  # require parameter  - group_id
  # return active assets for the given group
  def group_active_assets
    @records = []
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/active_assets.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      group = Group.find(params[:group_id])
      unless group.blank?
        @records = group.active_assets
      end
      @records
    end
  end

  def active_assets
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/active_assets.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      x = Ehealth::ActiveAsset
      @records = Cacherank.fetch('ehealth_active_assets', current_user.company_guid, :expires_in => EH_CACHE_EXPIRE_TIME)
      if @records.blank?
        @records =  Ehealth::ActiveAsset.bulk_load(current_user.company_guid)
        Cacherank.write('ehealth_active_assets', current_user.company_guid, @records, :expires_in => 60.minutes)
      end
      @records
    end
  end

  def active_assets_by_type
    if APP_CONFIG['stub_webservice']
      @records = []
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/active_assets.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      unless params["asset_type"].blank?
        x = Ehealth::ActiveAsset
        active_assets = Cacherank.fetch('ehealth_active_assets', current_user.company_guid, :expires_in => CACHE_EXPIRE_TIME)
        @records = active_assets.select  {|asset| asset.asset_type == params["asset_type"]}
      end
      @records
    end
  end

  def ticketings
    if APP_CONFIG['stub_webservice']
      @records = File.read("#{Rails.root}/test/fixtures/ehealth/ticketings.json")
      render :text => '{"result":' + @records + '}', :content_type => "application/json"
    else
      @records = Ehealth::Ticketing.bulk_load(current_user.company_guid)
    end
  end

  #################################### API FOR TESTING DATA ########################################
  # Below are the api created for testing data coming from end points

  def test_active_assets
    company = Company.where(:guid => params[:company_guid]).first
    z_instance = company.zenoss_instance
    unless z_instance.blank?
      start_date = (Date.today - 1.month).strftime("%m/%d/%Y")
      end_date =  (Date.today + 1).strftime("%m/%d/%Y")
      company_avail = SGTools::Zenoss::CompanyAvailability.new(
        company_guid.downcase,{:z_instance => z_instance,:start => start_date,:end=> end_date} #TODO: Moved this to conf file
      )
      results = company_avail.availability
      render :text => '{"result":' + results.to_json + '}', :content_type => "application/json"
    end
  end

  def test_active_asset_status
    unless params[:asset_guid].blank?
      active_asset = Ehealth::ActiveAsset.find_or_initialize_by_guid(params[:asset_guid])
      active_asset.calculate_active_asset_status
      render :text => '{"result":' + active_asset.status.to_json + '}', :content_type => "application/json"
    end
  end

  def test_active_asset_incidents
    unless params[:asset_guid].blank?
      active_asset = Ehealth::ActiveAsset.find_or_initialize_by_guid(params[:asset_guid])
      @record = OpenStruct.new({:incidents => active_asset.fetch_live_incidents})
      render :text => '{"result":' + @record.to_json + '}', :content_type => "application/json"
    end
  end

  def test_active_asset_req_changes
    unless params[:asset_guid].blank?
      active_asset = Ehealth::ActiveAsset.find_or_initialize_by_guid(params[:asset_guid])
      @record = OpenStruct.new({:req_changes => active_asset.fetch_live_req_changes})
      render :text => '{"result":' + @record.to_json + '}', :content_type => "application/json"
    end
  end

  def test_active_asset_mx_windows
    unless params[:asset_guid].blank?
      active_asset = Ehealth::ActiveAsset.find_or_initialize_by_guid(params[:asset_guid])
      @record = OpenStruct.new({:mx_windows => active_asset.fetch_live_mx_windows})
      render :text => '{"result":' + @record.to_json + '}', :content_type => "application/json"
    end
  end

  def test_ticketing_incidents
    ticket = Ehealth::Ticketing.find_or_initialize_by_company_guid(current_user.company_guid)
    @records = ticket.fetch_live_incidents || []
    render :text => '{"result":' + @records.to_json + '}', :content_type => "application/json"
  end

  def test_ticketing_requests
    ticket = Ehealth::Ticketing.find_or_initialize_by_company_guid(current_user.company_guid)
    @records = ticket.fetch_live_requests || []
    render :text => '{"result":' + @records.to_json + '}', :content_type => "application/json"
  end

  def test_ticketing_change_requests
    ticket = Ehealth::Ticketing.find_or_initialize_by_company_guid(current_user.company_guid)
    @records = ticket.fetch_live_change_requests || []
    render :text => '{"result":' + @records.to_json + '}', :content_type => "application/json"
  end

  def test_ticketing_maintenance
    ticket = Ehealth::Ticketing.find_or_initialize_by_company_guid(current_user.company_guid)
    @records = ticket.fetch_live_maintenance || []
    render :text => '{"result":' + @records.to_json + '}', :content_type => "application/json"
  end

  def test_events
    @records = []
    eh = SGTools::Zenoss::EventHistogram.new(current_user.company_guid.downcase, :z_instance => 'mom')
    if eh
      eh.load!
      eh_data = eh.timestamp_keyed_data
      mappings = {"5" => "critical", "4" => "error", "3" => "warning", "2" => "information"}
      eh_data.sort.map do |y,z|
        temp = {}
        temp.merge!({"date" => y*1000})
        temp.merge!(Hash[z.map {|k, v| [mappings[k], v] if mappings[k] }])
        @records << OpenStruct.new(temp)
      end
    end
    render :text => '{"result":' + @records.to_json + '}', :content_type => "application/json"
  end

  # private
  # def fetch_healths
  #     @records ||= Ehealth::ActiveAsset.bulk_load(current_user.company_guid) || []
  #     @records
  # end
end
