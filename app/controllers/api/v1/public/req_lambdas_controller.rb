module Api
  module V1
    module Public
      class ReqLambdasController < ToolsTeamController
        before_filter :setup_logger, :only => [:process_report]
        MODEL_CLASS = ReqObr

        def process_report
          filename = params["req_lambda"]["filename"]
          job_id = params["req_lambda"]["job_id"]

          @@my_logger.info "************#{Time.now}************"
          @@my_logger.info "Pyxie filename: #{filename}"
          @@my_logger.info "job_id: #{job_id}"

          if APP_CONFIG['stub_webservice']
            render :json=> {:target=>"test@email.com", :status => :ok}
          else
            if params && !params["req_lambda"]
              @@my_logger.error "Some parameters are missing!"
              render :json => { :errors => ["Can not process this request"]}, :status => 400
            elsif !filename || !job_id
              render :json => { :errors => ["Some parameter is missing Can not process this request"]}, :status => 422
            else
              obr_req = MODEL_CLASS.where(:job_id => job_id).first
              unless  obr_req.blank?
                @@my_logger.info "OBR request record found in db"
                #Update the status to "finished" & destination to filename recived from pyxie
                #This destination is used to construct the links to download report from pyxie
                obr_req.update_attributes(:job_status => "finished",:destination => "/pyxie/#{filename}")
                @@my_logger.info "OBR request status and destination updated in db"
                #Send report on the email only when user is impersonated or it's a subscription
                if (obr_req.subscription || obr_req.is_masqueraded)
                  email = obr_req.email
                  unless email.blank?
                    @@my_logger.info "Report emailed to user -  #{email}"
                    #find the pyxie report path and email address to send report over email address
                    email_report(filename, email)
                  else
                    @@my_logger.info "User email not found"
                  end
                  # audit_lambda_req(filename,job_id,email)
                end
                render :json => {:message => "Report processed successfully", :status => 200}
              else
                @@my_logger.warn "No request record found having job_id-#{job_id}"
                render :json => {:message => "No report request found", :status => 200}
              end
            end
          end
        end

        def email_report(filename, email)
          email = Rails.env == 'staging' ? "qasungardtest@gmail.com" : email
          response = {}
          if filename && email
            options = {}
            options["method"] = 'POST'
            options["query"] = {"filename" => filename, "email" => email }
            rest_client = HttpClient.new(APP_CONFIG['obr_mailer_url'], options)
            response = rest_client.fetch
          end
        end

        def audit_lambda_req(filename,job_id,email,emailed=false)
          obj = ReqLambda.new
          obj.filename = filename
          obj.job_id = job_id
          obj.email  = email
          obj.is_emailed = emailed
          obj.save!
        end

        def setup_logger
          @@my_logger ||= Logger.new("#{Rails.root}/log/req_lambda.log")
          original_formatter = Logger::Formatter.new
          @@my_logger.formatter = proc { |severity, datetime, progname, msg|
            original_formatter.call(severity, datetime, progname, msg.dump)
          }
        end
      end
    end
  end
end
