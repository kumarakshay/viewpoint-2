module Api
  module V1
    class VpUsersController < ToolsTeamController
       def delete_user
          user = User.where(email: /#{params[:email]}/i).first
          if(user and user.delete)
            render nothing: true, status: 204
          else
            render nothing: true, status: 422
          end
       end
    end
  end
end
