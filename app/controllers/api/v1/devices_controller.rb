require 'csv'

##
# The device service lets you search for SunGard asset data
# @url Assets
# @topic Devices
#
class Api::V1::DevicesController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :check_query_access, only: ["search", "core_search", "index", "companies"]

  def index
    data = Device.all
    render json: data
  end
  
  ##
  # Returns a list of SunGard device data based on search options.
  #
  # The default return columns are 'logical_name', 'location', 'type', 'istatus',
  # 'ip_address' and 'ip_host'
  # 
  # @argument [String] auth_token Authentication token for user.
  # @optional_argument [String] sort Field to use for sorting.
  # @optional_argument [String] dir Direction to sort [ASC|DESC].
  # @optional_argument [String] query JSON formatted query string. See {file:yard_includes/query_options.rdoc}.
  # @optional_argument [Integer] start Record number to use when paging data. Default 0
  # @optional_argument [Integer] limit Limit number of records returned.
  #
  # @example_response
  #   {
  #     "total":19,
  #     "data":[
  #       {
  #         "logical_name":"CISCO.SAL08218KHW",
  #         "location":"CA.SANDIEGO.010",
  #         "type":"router",
  #         "istatus":"Installed-Active",
  #         "ip_host":"sanir2.sgns.net",
  #         "ip_address":"63.102.224.139"
  #       },{
  #         "logical_name":"CISCO.SAL08218KHW",
  #         "location":"CA.SANDIEGO.010",
  #         "type":"router",
  #         "istatus":"Installed-Active",
  #         "ip_host":"car02.san01.inflow.net",
  #         "ip_address":"63.102.224.139"
  #       },
  #       ...
  #     ]
  #   }
  # 
  # @example_response
  #   {msg: "unable to find Device for somedevice.sgns.net"}
  #
  # @response_field [Integer] total Number of records returned by query.
  # @response_field [Array] data Array of device objects with fields matching select argument.
  #
  # @response_code 200 OK
  # @response_code 401 Authentication error
  # @response_code 404 Not Found
  # 
  def search
    select = Device.search_columns(['logical_name', 'location', 'type', 
                                    'istatus'])
    @body['select'] ||= select

    @join = {ip_hostname:{outer_join: true, cols: ['ip_host']},
             ip_record:{outer_join: true, cols:['ip_address']}}
    data = Device.query_with(@body,@join)
    render json: data
  end

  ##
  # Returns a list of SunGard core device data based on search options.
  # This query currently only returns Install-Active routers and switches that
  # are flagged as core devices by their description in Service Center.
  #
  # The default return columns are 'logical_name', 'location', 'type', 'istatus',
  # 'ip_address' and 'ip_host'
  # 
  # @argument [String] auth_token Authentication token for user.
  # @optional_argument [String] sort Field to use for sorting.
  # @optional_argument [String] dir Direction to sort [ASC|DESC].
  # @optional_argument [String] query JSON formatted query string. See {file:yard_includes/query_options.rdoc}.
  # @optional_argument [Integer] start Record number to use when paging data. Default 0
  # @optional_argument [Integer] limit Limit number of records returned.
  #
  # @example_response
  #   {
  #     "total":19,
  #     "data":[
  #       {
  #         "logical_name":"CISCO.SAL08218KHW",
  #         "location":"CA.SANDIEGO.010",
  #         "type":"router",
  #         "istatus":"Installed-Active",
  #         "ip_host":"sanir2.sgns.net",
  #         "ip_address":"63.102.224.139"
  #       },{
  #         "logical_name":"CISCO.SAL08218KHW",
  #         "location":"CA.SANDIEGO.010",
  #         "type":"router",
  #         "istatus":"Installed-Active",
  #         "ip_host":"car02.san01.inflow.net",
  #         "ip_address":"63.102.224.139"
  #       },
  #       ...
  #     ]
  #   }
  # 
  # @example_response
  #   {msg: "unable to find Device for somedevice.sgns.net"}
  #
  # @response_field [Integer] total Number of records returned by query.
  # @response_field [Array] data Array of device objects with fields matching select argument.
  #
  # @response_code 200 OK
  # @response_code 401 Authentication error
  # @response_code 404 Not Found
  # 
  def core_search
    query = [{'name' => "description", 'value' => "type=core", 'op' => "like"},
             {'name' => "type", 'value' => ["router", "switch"], 'op' => "in"}, 
             {'name' => "istatus", 'value' => "Installed-Active", 'op' => "eq"}]
    @body['query'] ||= []
    @body['query'] << query
    @body['query'].flatten!

    self.search
  end

  ##
  # Returns a list of SunGard comopany data for a given device based on search options.
  # The returned fields are 'customer_name', 'snt_code', 'sde', 'ae', 'oracle_id', 'interfaces'  
  # 
  # @url [GET] /api/devices/[:device_id]/companies[.format]?[arguments]
  # 
  # @argument [String] device_id Hostname of device to query for company data.
  # @argument [String] auth_token Authentication token for user.
  # @optional_argument [String] format Format to return data in. Default is json, csv is also supported.
  #
  # @example_response
  #   {
  #     "total":51,
  #     "data":[
  #       {
  #         "customer_name":"ACCURATE BACKGROUND(803307190)",
  #         "snt_code":"accb",
  #         "sde":"Hoard, Jerry",
  #         "ae":"Wright, Donald",
  #         "oracle_id":"76251",
  #         "interfaces":["3/38"]
  #       },{
  #         "customer_name":"Alliant Credit Union",
  #         "snt_code":"allf",
  #         "sde":"MS Teir 1 SD-WEST -Mid/West",
  #         "ae":"HAMMEL, MATTHEW",
  #         "oracle_id":"17372",
  #         "interfaces":["3/31"]
  #       },
  #       ...
  #     ]
  #   }
  # 
  # @example_response
  #   {msg: "unable to find Device for somedevice.sgns.net"}
  #
  # @example_response
  #    CSV will return the company details one company per line in CSV format.
  #
  # @response_field [Integer] total Number of records returned by query.
  # @response_field [Array] data Array of location objects with fields matching select argument.
  # @response_field [String] customer_name Name of customer
  # @response_field [String] snt_code SunGard SNT code
  # @response_field [String] sde Service Delivery Executive
  # @response_field [String] ae Account Executive
  # @response_field [Integer] oarcle_id SunGard Oracle ID number
  # @response_field [String] interfaces List of interfaces on the device that the customer is mapped to.
  #
  # @response_code 200 OK
  # @response_code 401 Authentication error
  # @response_code 404 Not Found
  # 
  def companies
    d = device_by
    render json: {msg: "#{d.logical_name} is not a network device."}, status: 404 unless ['Router','Switch'].include?(d.type)

    # get network names for join and start building hash of interface data
    data = []
    port_data = {}
    company_if = {'Unknown' => []}
    port_in = []
    d.ports.each do |port|
      unless port.network_name.blank?
        port_in.append(port.network_name)
        port_data[port.network_name] = port.protocol_address
      end
    end

    @options = {'limit' => 50000}
    query = [{'name' => "logical_name", 'value' => port_in, 'op' => "in"},
             {'name' => "istatus", 'value' => 'Installed-Active', 'op' => 'eq'}]

    @options['query'] = query
    @options['select'] = ['logical_name', 'company_use']
    
    rd_data = Rd.query_with(@options)
    rd_data[:data].each do |rd|
      if rd['company_use'].blank?
        company_if['Unknown'].append(port_data[rd['logical_name']])
      else
        company_if[rd['company_use']] ||= []
        company_if[rd['company_use']].append(port_data[rd['logical_name']])
      end
    end  

    # clear out rows with no interfaces defined
    #
    company_if.delete_if {|key, value| value.length < 1  or value == nil} 
    company_if.sort.each do |k,v|
      v ||= []
      v.compact!
      begin
        company = Company.find(k)
      rescue
        company = Company.new(:id => k)
      end
      details = company.sn_company_details || SnCompanyDetails.new(:name => k)
      data.append({ 'customer_name' => details.name || '',
                    'snt_code' => company.naming_prefix || '',
                    'sde' => details.care_rep || '',
                    'ae' => details.account_exec || '',
                    'oracle_id' => details.oracle_id || '',
                    'interfaces' => v.uniq.sort || []})    
    end
    if request.format.csv?  
      body = ''
      data.each do |row| 
        body << CSV.generate_line([row['customer_name'], 
                                row['snt_code'],
                                row['sde'],
                                row['ae'],
                                row['oracle_id'],
                                row['interfaces'].join(',')
                                ])
      end

      send_data( body, :type => 'text/csv', :filename => "companies_#{Time.now}.csv", :disposition => 'attachment')
    else
      render json: {:total => data.length(), :data => data}
    end
  end

   #/devices/123/components
  def components
    d = device_by_guid
    render text: Yajl::dump(d.friendly_component_list), content_type:'application/json'
  end

  #/devices/123/interfaces
  def components_by_type
    body={}
    body = JSON::parse(request.body.read) if request.body.size > 0
    d = device_by_guid
    component_type = params[:component_type]
    list = d.__send__(:"#{component_type}",body)
    render json: list
  end

  #/devices/123/interfaces/123343asasdfasdf
  def component
    d = device_by_guid
    component_type = params[:component_type]
    m = component_type.singularize
    details = d.__send__(:"#{m}_details",params[:cid].to_i)
    render text: Yajl::dump(details), content_type:'application/json'
  end

  def device_by_guid
    id = params[:id] || params[:device_id]
    d = Device.find_by_asset_guid(id)
    raise ActiveRecord::RecordNotFound.new("unable to find Device for #{id}") if d.blank?
    d
  end

  def device_by
    id = params[:id] || params[:device_id]
    iph  = IpHostname.find_by_ip_host(id)
    d = iph.device unless iph.blank?
    raise ActiveRecord::RecordNotFound.new("unable to find Device for #{id}") if d.blank?
    d
  end
end
