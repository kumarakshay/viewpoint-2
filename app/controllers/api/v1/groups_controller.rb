class Api::V1::GroupsController < SGTools::Rails::REST.api_v1_controller_class
  MODEL_CLASS = Group
  include SGTools::Rails::REST::RestfulResources
  before_filter :load_records, :only => [:index]
  before_filter :set_additional_data, :only => [:create]
  

  def index
    @records = @records.where(:active => true)    
  end

  def create
    if @record.save
      render :show, status: :ok
    else
      render :show, status: :unprocessable_entity
    end
  end

  def update
    if @record.update_attributes(params[:group])
      render :show
    else
      render :show, status: :unprocessable_entity
    end
  end

  def destroy
  	if @record.destroy
	    render nothing: true, status: 204
  	else
      render :show, status: :unprocessable_entity
    end
  end
  
  private

  def load_records
    params = request.query_parameters
    params.merge!('company_guid' => current_user.company_guid)
    super
  end

  def set_additional_data  	
  	@record.company_guid = current_user.company_guid
  end

end