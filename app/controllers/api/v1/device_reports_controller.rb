module Api
  module V1
    # API controller for Device Report subscription
    #
    # @return [String] subscription_id
    # @return [String] subscription
    # @return [String] email
    # @return [String] company_guid
    # @return [String] frequency
    # @return [String] delivery_time
    # @return [Array] asset_list
    #
    # Using controller inheritance for authentication
    # @see http://www.therailsway.com/2009/4/6/controller-inheritance
    #
    # @see DeviceReport
    # @see https://wiki.sungardas.corp/display/OSSTOOLS/Zenoss+Report+API+Hooks
    class DeviceReportsController < ToolsTeamController
      def index
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/device_report_subscription.json")
          @metadata = "stubbed device report subscription"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            DeviceReport.new(r)
          end
        else
          DeviceReport.bulk_load("1")
          @metadata = "device report subscription"
          @dats = DeviceReport.where(:report_id => "1").all
        end
      end

      def device_availability
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/device_availability_report_subscription.json")
          @metadata = "stubbed device availability report subscription"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            DeviceReport.new(r)
          end
        else
          DeviceReport.bulk_load("15")
          @metadata = "device availability report subscription"
          @dats = DeviceReport.where(:report_id => "15").all
        end
      end

      def network_interface
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/network_interface_report_subscription.json")
          @metadata = "stubbed network interface report subscription"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            DeviceReport.new(r)
          end
        else
          DeviceReport.bulk_load("17")
          @metadata = "network interface report subscription"
          @dats = DeviceReport.where(:report_id => "17").all
        end
      end
    end
  end
end
