module Api
  module V1
    class OpenidController < ApplicationController
      def show
        @dat = OpenidManager.get_user(params[:id])
        Rails.logger.info("in OpenidController: #{@dat}")
        Rails.logger.add_metadata(returned_json: @dat) if Rails.logger.respond_to?(:add_metadata)
        response.headers["Content-Type"]= "application/json"
        render text: @dat
      end
    end
  end
end
