class Api::V1::IpServicesController < SGTools::Rails::REST.api_v1_controller_class

  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/ip_services.json")
    render :json => dat
  end
  
end