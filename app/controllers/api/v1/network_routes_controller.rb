class Api::V1::NetworkRoutesController < SGTools::Rails::REST.api_v1_controller_class
  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/network_routes.json")
    render :json => dat
  end  
end