class Api::V1::SearchesController < SGTools::Rails::REST.api_v1_controller_class
  # skip_before_filter :login_required
  # global search across elastic - results returned for
  # various categories
  def global
    results = {}
    d = Device.search(:per_page => 8) do |s|
      p = params
      s.query do
        boolean do
          should { string "hostname:#{p[:query].downcase}*"} if p[:query].present?
          should { string "logical_name:#{p[:query].downcase}*" } if p[:query].present?
          should { string "ip_address:#{p[:query].downcase}*" } if p[:query].present?
          should { string "serial_number:#{p[:query].downcase}*" } if p[:query].present?
          should { string "primary_path:#{p[:query].downcase}*" } if p[:query].present?
        end
      end
      s.fields ['hostname','type','asset_guid']
      s.filter :terms, :type => ['router','switch','server','firewall','circuit', 'system']
    end
    results['assets'] = {}
    results['assets']['results'] = d
    results['assets']['total_hits'] = d.total_count

    c = Company.search(:per_page => 8) do |s|
      p = params
      s.query do
        boolean do
          should { string "company:#{p[:query].downcase}" } if p[:query].present?
          should { string "company_prefix:#{p[:query].downcase}" } if p[:query].present?
        end
      end
      s.fields ['company','company_guid']
    end
    results['companies'] = {}
    results['companies']['results'] = c
    results['companies']['total_hits'] = c.total_count

    l = Location.search(:per_page => 8) do |s|
      p = params
      s.query do
        boolean do
          should { string "location:*#{p[:query]}*" } if p[:query].present?
          should { string "location_code:*#{p[:query]}*" } if p[:query].present?
        end
      end
      s.fields ['location','location_guid']
    end
    results['locations'] = {}
    results['locations']['results'] = l
    results['locations']['total_hits'] = l.total_count

    render json: [results]
  end

  def advanced
    check_query_access

    page = params[:page] || @body[:page] || 1
    per_page = params[:per_page] || @body[:per_page] || 25
    operator = params[:operator] || @body[:operator] || "AND"

    results = Device.search(page: page, per_page: per_page) do |s|
      p = params
      b = @body
      s.query do
        query = p[:query] || b["query"] || nil
        query_fields = p[:query_fields] || b["query_fields"] || {}
        boolean do
          if !query.blank?
            should { string query, default_operator: operator }
          else
            query_fields.each do |k,v|
              should { string "#{k}:#{v.gsub(' ','%20').downcase}*" }
            end
          end
        end
      end
      s.fields ['asset_guid', 'ip_address', 'hostname', 'logical_name', 'type',
                'company.customer_id', 'istatus', 'description'
                ]
      s.filter :terms, :type => ['router','switch','server','firewall','circuit', 'system']
    end

    render json: {:total => results.total_count, :total_pages => results.total_pages,
                  :page => page, :results => results}
  end

  #This API is called from VP5 search widget
  #query(device_name) is a input parameter and it will return all the matched devices
  def device_search
    @devices =[]
    unless params[:query].blank?
      @query_param = params[:query]
      if current_user.is_sungard?
        @devices = Device.search(params[:query])
      else
        @devices = Search.search_with_params(current_user.company_guid, params[:query])
      end
    end
  end
end
