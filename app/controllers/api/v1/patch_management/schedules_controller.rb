class Api::V1::PatchManagement::SchedulesController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_mfa
  before_filter :get_data
  BASE_URL = Figaro.env.patch_management_base_url
  AUTHORIZATION = Figaro.env.patch_management_authorization

  def customer_schedules
  end

  def add_schedule
  end

  def add_to_patch
  end

  def update_schedule_servers
  end

  def add_schedule_servers
  end

  def update_schedule
  end

  def delete_schedule
  end

  def schedule_name_exist
  end

  def schedule_status
  end

  def validate_kb
  end

  def schedule_count
  end

  def details
  end

  def byChangeNumber
  end  

  private

  def get_data
    url = BASE_URL + request.fullpath
    options = {}
    header = {"Authorization" => AUTHORIZATION}
    options["header"] = header
    options["method"] = request.method
    unless params[:schedule].blank?
      options["query"] = params[:schedule]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
    render :text => @records.to_json, :content_type => "application/json"
  end

  def authorize_mfa
    if MfaEvent.for_user(sign_in_user.guid).first.blank?
      render :json=> {:message=>"user is not mfa authenticated", :oneportal_base_url=>APP_CONFIG["oneportal_url"]}, :status=>401
    end
  end
end
