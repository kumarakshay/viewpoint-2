class Api::V1::PatchManagement::ServersController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_mfa
  before_filter :get_data

  BASE_URL = Figaro.env.patch_management_base_url
  AUTHORIZATION = Figaro.env.patch_management_authorization

  def server_id_by_name
  end

  def serverCentric
  end

  private

  def get_data
    url = BASE_URL + request.fullpath
    options = {}
    header = {"Authorization" => AUTHORIZATION}
    options["header"] = header
    options["method"] = request.method
    unless params[:server].blank?
      options["query"] = params[:server]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    @records = rest_client.fetch
    render :text => @records.to_json, :content_type => "application/json"
  end
    
  def authorize_mfa
    if MfaEvent.for_user(sign_in_user.guid).first.blank?
      render :json=> {:message=>"user is not mfa authenticated", :oneportal_base_url=>APP_CONFIG["oneportal_url"]}, :status=>401
    end
  end
end