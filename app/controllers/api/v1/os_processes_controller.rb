class Api::V1::OsProcessesController < SGTools::Rails::REST.api_v1_controller_class

  def index
    dat = File.read("#{Rails.root}/test/fixtures/zenoss/os_processes.json")
    render :json => dat
  end
  
end