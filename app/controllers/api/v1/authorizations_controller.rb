class Api::V1::AuthorizationsController < SGTools::Rails::REST.api_v1_controller_class

  # when you want to display mock data
  # def show
  #   dat = File.read("#{Rails.root}/test/fixtures/authorizations.json")
  #   render :json => dat
  # end

  def show
    @metadata = "authorization data from itsm gem"
    @record_set = User.first.authorization_matrix    
  end
end