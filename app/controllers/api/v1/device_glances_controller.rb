module Api
  module V1
    # API controller for Device Glance subscription
    #
    # @return [String] subscription_id
    # @return [String] subscription
    # @return [String] email
    # @return [String] company_guid
    # @return [String] frequency
    # @return [String] delivery_time
    # @return [Array] asset_list
    #
    # Using controller inheritance for authentication
    # @see http://www.therailsway.com/2009/4/6/controller-inheritance/
    #
    # @see DeviceGlance
    # @see https://wiki.sungardas.corp/display/OSSTOOLS/Zenoss+Report+API+Hooks
    class DeviceGlancesController < ToolsTeamController
      def index
        if APP_CONFIG['stub_webservice']
          dat = File.read("#{Rails.root}/test/fixtures/subscriptions/device_glance.json")
          @metadata = "stubbed device glance"
          source_records = JSON.parse(dat)
          @dats = source_records.map do |r|
            DeviceGlance.new(r)
          end
        else
          DeviceGlance.bulk_load("2")
          @dats = DeviceGlance.where(:report_id => "2").all  
          @metadata = "device glance report subscription"
        end  
      end
    end
  end
end