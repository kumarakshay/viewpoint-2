class Api::V1::Omi::CompaniesController < ApplicationController
	COMPANY_COLLECTION = OmiCompany

	def index
		@companies = COMPANY_COLLECTION.all
		if @companies.present?
			render text: @companies.to_json, content_type: "application/json"
		else
			render text: {error: 'Companies not found.'}, content_type: "application/json"
		end
	end

	def show
		@company = COMPANY_COLLECTION.where(guid: params[:id]).first
		if @company.present?
			render text: @company.to_json, content_type: "application/json"
		else
			render text: {error: 'Company not found.'}, content_type: "application/json"
		end
	end
end