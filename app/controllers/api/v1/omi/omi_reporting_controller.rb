class Api::V1::Omi::OmiReportingController < SGTools::Rails::REST.api_v1_controller_class

  BASE_URL = Figaro.env.omi_base_url
  AUTHORIZATION = Figaro.env.omi_authorization
  STUB_MOCK_DATA = false
  MODEL = APP_CONFIG['vp_cauldron'] ? Device : OmiDevice

  def perfdata
    @records = get_omi_data
    render_response
  end

  def rosetta
    @records = get_omi_data
    render_response
  end

  def graph_metrics
    dat = File.read("#{Rails.root}/test/fixtures/device_graph/device-graph-metrics.json")
    @records = JSON.parse(dat)
    render_response
  end

  #/monitoring/perfdata/interface/{sg_nnmi_interface_uuid}?metric=packets_in
  def interface_graph
    @records = get_omi_data
    render_response
  end

  def network_graph_data
    @records = []
    if STUB_MOCK_DATA
      dat = File.read("#{Rails.root}/test/fixtures/network_device_graph/cpu_utilization.json")
      @records = JSON.parse(dat)
    else
      qry_str = ''
      host_nnm_uid = MODEL.where(:asset_guid => params[:device_guid]).first.host_nnm_uid
      if host_nnm_uid && params[:metric].present?
        url = BASE_URL + '/monitoring/perfdataNetworkDevice/' + host_nnm_uid + '?metric=' + params[:metric]
        if params[:starttime ].present?
          qry_str = qry_str + "&starttime=" + params[:starttime]
        end
        if params[:endtime ].present?
          qry_str = qry_str + "&endtime=" + params[:endtime]
        end
        if params[:frequency  ].present?
          qry_str = qry_str + "&frequency=" + params[:frequency ]
        end
        url = url + qry_str
      end
      @records = get_omi_data(url)
    end
    render_response
  end

  def graph_data
    if STUB_MOCK_DATA
      if params[:metric] == 'GBL_SWAP_SPACE_UTIL'
        dat = File.read("#{Rails.root}/test/fixtures/device_graph/GBL_SWAP_SPACE_UTIL.json")
      elsif params[:metric] == 'GBL_LOADAVG'
        dat = File.read("#{Rails.root}/test/fixtures/device_graph/GBL_LOADAVG.json")
      elsif  params[:metric] == 'GBL_MEM_PAGEIN_RATE'
        dat = File.read("#{Rails.root}/test/fixtures/device_graph/GBL_MEM_PAGEIN_RATE.json")
      end
      @records = JSON.parse(dat)
    else
      url = BASE_URL + '/cmdb/rosetta?asset_guid=' + params[:device_guid]
      rosseta_data = get_omi_data(url)
      if rosseta_data.present? && rosseta_data[0].present? && rosseta_data[0]["rtsm_cmdb_id"].present?
        rtsm_cmdb_id = rosseta_data[0]["rtsm_cmdb_id"]
        if rtsm_cmdb_id
          ds = params[:ds].present? ? '?ds=' + params[:ds] : ''
          dsclass = params[:dsclass].present? ? '&dsclass=' + params[:dsclass] : ''
          metric = params[:metric].present? ? '&metric=' + params[:metric] : ''
          interval = params[:interval].present? ? '&interval=' + params[:interval] : ''
          starttime = params[:starttime].present? ? '&starttime=' + params[:starttime] : ''
          endtime = params[:endtime].present? ? '&endtime=' + params[:endtime] : ''
          perfdata_url = BASE_URL + '/monitoring/perfdata/' + rtsm_cmdb_id + ds + dsclass + metric + interval + starttime + endtime
          @records = get_omi_data(perfdata_url)
        end
      else
        @records = [points: []]
      end
    end
    render_response
  end

  private

  def render_response
    render :text => @records.to_json, :content_type => "application/json"
  end

  def get_omi_data(given_url=nil)
    url = given_url ? given_url : BASE_URL + request.fullpath
    options = {}
    header = {"Authorization" => AUTHORIZATION}
    options["header"] = header
    options["method"] = request.method
    unless params[:omi].blank?
      options["query"] = params[:omi]
    end
    Rails.logger.info "******url**#{url}"
    Rails.logger.info "******options**#{options}"
    rest_client = HttpClient.new(url, options)
    rest_client.fetch
  end
end
