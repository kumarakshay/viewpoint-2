class Api::V1::Omi::HpcController < SGTools::Rails::REST.api_v1_controller_class

  BASE_URL = Figaro.env.omi_base_url
  AUTHORIZATION = Figaro.env.omi_authorization


  def process_workflows
    @record = get_esb_data()
    render :text => @record.to_json, :content_type => "application/json"
  end

  private

  def get_esb_data()
    begin
      url = BASE_URL + request.fullpath
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.read_timeout = 500
      http.open_timeout = 500
      http.use_ssl = true
      request = Net::HTTP::Post.new(uri.request_uri, nil)
      request["Authorization"] = AUTHORIZATION
      request["Content-Type"] = 'application/json'
      data = Hash.new()
      data["parameters"] = params["parameters"]
      request.body = data.to_json
      response = http.request(request)
      @records = JSON.parse(response.body)
    rescue Exception => e
      Rails.logger.info ">>>>>>>>>>>>>>>>>>>ERORR>>>>>>>>>>>>>>>>>>>>>>>>>>"
      Rails.logger.info e
      return {}
    end
  end
end
