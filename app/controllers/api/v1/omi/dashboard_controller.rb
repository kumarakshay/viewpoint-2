class Api::V1::Omi::DashboardController < SGTools::Rails::REST.api_v1_controller_class
  before_filter :authorize_call

  BASE_URL = Figaro.env.omi_base_url
  AUTHORIZATION = Figaro.env.omi_authorization
  BASE_URL_PROD = "https://esb.tools.sgns.net"
  AUTHORIZATION_PROD = "SGAS 6aa66ea80e28cc3652b47aa0ae312ffd:f484548e619c5e68c8452cd629faba0941feb1ea7fc5f09440572a2f8fe71583"


  def monthly_average_cpu_memory
    render_response
  end

  def monthly_used_file_system
    render_response
  end

  def max_utilization_cpu
    render_response
  end

  def max_utilization_memory
    render_response
  end

  def max_utilization_file_system
    render_response
  end

  def base_data_cpu_memory
    @records = []
    raw_records = fetch_records
    record_hash =  raw_records.group_by{ |obj| [obj["DNS_Name"],obj["OS"],obj["Vendor"],obj["globalid"]]}
    record_hash.each do |k,arr|
      temp = Hash.new()
      temp["DNS_Name"] = k[0]
      temp["OS"] = k[1]
      temp["Vendor"] = k[2]
      temp["globalid"] = k[3]
      temp["CpuMemory"] = []
      arr.each { |a|
        tmp = Hash.new()
        tmp["monthno"] = a["monthno"]
        tmp["monthval"] =  a["monthval"]
        tmp["yearval"] = a["yearval"]
        tmp["avgcpu"] = a["avgcpu"]
        tmp["avgmemory"] = a["avgmemory"]
        temp["CpuMemory"].push(tmp)
      }
      @records.push(temp)
    end
    render :text => @records.to_json, :content_type => "application/json"
  end

  private

  def render_response()
    @records = fetch_records
    render :text => @records.to_json, :content_type => "application/json"
  end

  def fetch_records()
    begin
      records = {}
      if params and (params[:os].present? or params[:app].present?)
        global_ids = fetch_gobal_ids
      else
        global_ids = ""
      end
      api_url= "#{BASE_URL}:443#{request.fullpath}"
      api_body = {
        globalId: global_ids,
        company_guid: current_user.company_guid.downcase
      }
      records = get_esb_data(api_url,AUTHORIZATION,api_body)
      return records
    rescue Exception => e
      return {}
    end
  end

  def fetch_gobal_ids()
    global_ids = []
    tql_url= "#{BASE_URL_PROD}:443/cmdb/tql"
    if params && !params[:os].blank?
      tql_body = {
        tql_name: params[:os]
      }
    elsif params && !params[:app].blank?
      tql_name = get_app_tqn_name_for_company
      tql_body = {
        tql_name: tql_name,
        tqlParams: {
          name:  params[:app]
        }
      }
    end
    results = get_esb_data(tql_url,AUTHORIZATION_PROD,tql_body)
    if results && !results["cis"].blank?
      global_ids = results["cis"].map{|x| x["properties"]["global_id"] if x["properties"]["sg_contracting_company"] == current_user.company_guid.downcase}
    end
    global_ids.map { |s| "'#{s}'" }.join(',')
  end

  def get_esb_data(url,autho_token,body_params={})
    Rails.logger.info "@@@@@@@@@@@@@@@@@@@@@@@@@"
    Rails.logger.info url
    Rails.logger.info body_params
    Rails.logger.info "@@@@@@@@@@@@@@@@@@@@@@@@@"
    begin
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Post.new(uri.request_uri, nil)
      request["Authorization"] = autho_token
      request["Content-Type"] = 'application/json'
      request.body = body_params.to_json
      response = http.request(request)
      result = JSON.parse(response.body)
    rescue Exception => e
      Rails.logger.info "@@@@@@@@@@@@@@@@@@@@@@@@@"
      Rails.logger.info e
      Rails.logger.info "@@@@@@@@@@@@@@@@@@@@@@@@@"
      return {}
    end
  end

  def get_app_tqn_name_for_company
    name = current_user.company_guid.downcase == "d811fa8b-4288-4f6d-b369-072f2c4c1d21" ? "NCL_BizApp_Param" : "BizApp_Param"
    return name
  end


  def authorize_call
    if params[:company_guid]
      if  params[:company_guid].downcase == current_user.company_guid.downcase && !current_user.is_sungard?
        return true
      else
        render "api/v1/shared/unauthorize", :status => 401
        return false
      end
    end
  end
end
