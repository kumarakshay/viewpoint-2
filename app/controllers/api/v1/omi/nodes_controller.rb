class Api::V1::Omi::NodesController < SGTools::Rails::REST.api_v1_controller_class
  NODE_COLLECTION = OmiDevice
  before_filter :authorize_access

  def index
    @nodes = NODE_COLLECTION.find_all_by_asset_guid(current_user.asset_list, :order => "name")
  end

  def show
    @node = NODE_COLLECTION.find_by_asset_guid(params[:id])
  end

  private

  def authorize_access
    if current_user.is_sungard? 
      render "api/v1/shared/unauthorize", :status => 401
      return false
    end
  end

end