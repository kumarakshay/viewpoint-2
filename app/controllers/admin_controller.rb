# This controller will be used for company administrators 
class AdminController < ApplicationController
  before_filter  :authorize_as_admin
  layout 'admin'
  
  private

  def authorize_as_admin
  	if sign_in_user.admin_role?
      return true
    end
    redirect_to welcome_path, notice: "Unauthorized Access"
  end
end