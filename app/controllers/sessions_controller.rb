class SessionsController < ApplicationController
  layout 'basic'
  skip_before_filter  :authenticated?

  # required to render the login form   
  def new_create    
    redirect_to(new_oid_path)
  end

  def failure
    render 'errors/not_found', :status => :not_found
  end

  def destroy
    Rails.cache.delete("user-#{session[:user_guid]}")
    Rails.cache.delete("roles_for_user_#{current_user.guid}") if current_user
    cookies.delete('_sgmc_app_proxy_session'.to_sym)
    #Remove user mfa active token from DB on logout 
    user_mfa_token = MfaEvent.for_user(current_user.guid).first
    unless user_mfa_token.blank?
      user_mfa_token.destroy   
    end
    @masquerade = false
    reset_session
    logout_current_user
  end

  private

  def logout_current_user
    if APP_CONFIG['open_id_version'] == 'V1'
      logout_url = APP_CONFIG['open_id_url']
    else
      if current_user and current_user.is_allstream_user? and request.url.match('allstream.com').present?
        logout_url = "#{Figaro.env.sso_v2_allstream_base_url}/service/web/Logout?goto=#{Figaro.env.allstream_base_url}"
      else
        logout_url = "#{Figaro.env.sso_v2_base_url}/service/web/Logout?goto=#{Figaro.env.vp_base_url}"
      end
    end
    redirect_to "#{logout_url}"
  end
end
