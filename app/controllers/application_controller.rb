class ApplicationController < SGTools::Rails::Base::ActionController
  include "OidAspects::#{APP_CONFIG['open_id_version']}::OidsHelper".constantize
  include SGTools::Rails::MongodbLogger::Base
  # TOKEN="a0772c51f9016dd1d3016e828e67f64d"

  before_filter :authenticated?, :select_branding, :audit_user_action
  helper_method :current_user, :sign_in_user

  def user_guide
    if !PortalMapWorker.is_child?("mtai", current_user.company_guid)
      send_file Rails.root.join('downloads', 'sg_vp_user_guide.zip'), :type=>"application/zip", :x_sendfile=>true
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def rs_user_guide
    send_file Rails.root.join('downloads', 'sg_rs_user_guide.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def user_guide_allstream
    send_file Rails.root.join('downloads', 'sg_vp_user_guide.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def user_guide_omi
    send_file Rails.root.join('downloads', 'sg_vp_user_guide_omi.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def customer_catalog
    send_file Rails.root.join('downloads/tutorials', 'self_service_customer_catalog.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def remote_intelligent_hands
    send_file Rails.root.join('downloads/tutorials', 'remote_intelligent_hands.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def secure_site_access
    send_file Rails.root.join('downloads/tutorials', 'secured_site_access.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def create_ticket
    send_file Rails.root.join('downloads/tutorials', 'how_to_create_a_ticket.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def ticket_updates
    send_file Rails.root.join('downloads/tutorials', 'looking_for_ticket_updates.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def modify_authorizations
    send_file Rails.root.join('downloads/tutorials', 'how_to_modify_authorizations.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def creating_reports
    send_file Rails.root.join('downloads/tutorials', 'creating_reports.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def notification_matrix
    send_file Rails.root.join('downloads/tutorials', 'notification_matrix.pdf'), :type=>"application/pdf", :x_sendfile=>true
  end

  def providers_list
    send_file Rails.root.join('downloads/tutorials', "service_delivery_providers_list.pdf"), :type=>"application/pdf", :x_sendfile=>true
  end

  def mx_window
    send_file Rails.root.join('downloads/tutorials', "MX_Windows.pdf"), :type=>"application/pdf", :x_sendfile=>true
  end

  def contract_management 
    send_file Rails.root.join('downloads/tutorials', "contract_management.pdf"), :type=>"application/pdf", :x_sendfile=>true
  end

  def self_service_guide
    send_file Rails.root.join('downloads/tutorials', "self_service_guide.pdf"), :type=>"application/pdf", :x_sendfile=>true
  end

  def mobile_guides
    if  params[:name].present? && params[:name].start_with?('mobile-')
      send_file Rails.root.join('downloads/tutorials', "#{params[:name]}.pdf"), :type=>"application/pdf", :x_sendfile=>true
    end
  end

  def video_guides
    if  params[:name].present?
      send_file Rails.root.join('downloads/tutorials/', "#{params[:name]}.mp4"), type: "video/mp4", disposition: "inline", range: true
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to welcome_path, notice: "Unauthorized Access"
  end

  private

  private

  def send_file(path, options = {})
    if options[:range]
      send_file_with_range(path, options)
    else
      super(path, options)
    end
  end

  def send_file_with_range(path, options = {})
    if File.exist?(path)
      size = File.size(path)
      if !request.headers["Range"]
        status_code = 200 # 200 OK
        offset = 0
        length = File.size(path)
      else
        status_code = 206 # 206 Partial Content
        bytes = Rack::Utils.byte_ranges(request.headers, size)[0]
        offset = bytes.begin
        length = bytes.end - bytes.begin
      end
      response.header["Accept-Ranges"] = "bytes"
      response.header["Content-Range"] = "bytes #{bytes.begin}-#{bytes.end}/#{size}" if bytes

      send_data IO.binread(path, length, offset), options
    else
      raise ActionController::MissingFile, "Cannot read file #{path}."
    end
  end

  def select_branding
    if current_user and (PortalMapWorker.is_child?("mtai", current_user.company_guid) || (current_user.company_guid == "1DA8A657-21A8-42EE-83A7-B4E32C8B480C"))
      @branding = "allstream"
    else
      @branding = "sungard"
    end
    Rails.logger.add_metadata(viewpoint_branding: @branding) if Rails.logger.respond_to?(:add_metadata)
    @branding
  end

  # operator is personality (if masqueraded, this is the masqueraded user)
  # user is who logged in
  def current_user
    # User
    # MasqueradeUser
    if session[:user_guid] == session[:operator_guid]
      @u ||= User.find(session[:user_guid])
      @o ||= @u
    else
      @o ||= User.find(session[:operator_guid])
      @u ||= User.find(session[:user_guid])
      # We need to check if company masquerading in done to decorate this user.
    end
    @o = decorate_proxy_user(@o)
    # TODO: the following calls failed when the user submitted password for the first time and was automatically logged in
    if (@u && @o)
      Rails.logger.add_metadata(current_user_email: @u.email) if Rails.logger.respond_to?(:add_metadata)
      Rails.logger.add_metadata(current_user_company_guid: @u.company_guid) if Rails.logger.respond_to?(:add_metadata)
      @u_company ||= Company.with_guid(@u.company_guid)
      Rails.logger.add_metadata(current_user_company_name: @u_company.name) if @u_company and Rails.logger.respond_to?(:add_metadata)
      Rails.logger.add_metadata(current_operator_email: @o.email) if Rails.logger.respond_to?(:add_metadata)
      Rails.logger.add_metadata(current_operator_company_guid: @o.company_guid) if Rails.logger.respond_to?(:add_metadata)
      if (@u.company_guid == @o.company_guid)
        @o_company = @u_company
      else
        @o_company ||= Company.with_guid(@o.company_guid)
      end
      Rails.logger.add_metadata(current_operator_company_name: @o_company.name) if @o_company and Rails.logger.respond_to?(:add_metadata)
    end
    return @o
  end

  def set_user_company
    company = Company.with_guid(sign_in_user.company_guid)
    unless company.blank?
      session[:user_company] ||= company.name
      session[:user_snt] ||= company.snt
    end
  end

  # Decorates the current user in case company masquerading is in progress.
  def decorate_proxy_user(user)
    if (!session[:company_mas_guid].blank? and !user.blank?)
      if user.roles.include?(Role::SWITCH_COMPANY)
        @masq_company = Company.find_by_guid(session[:company_mas_guid])
        if @masq_company
          user.company_guid = @masq_company.guid
          # Removing old roles and setting this role to the user on the fly
          # as we do not want him to have user management done during switch company.
          # This would be later enhanced but for time being we restrict this.
          # user.roles = ['switch_company']
        end
      else
        session.delete(:company_mas_guid)
        redirect_to home_path
        flash[:notice] ='Your switch_company access has been revoked'
      end
    end
    user
  end

  # Sign In user is the actual user and not masqueraded user
  def sign_in_user
    @sign_in_user ||= User.find(session[:user_guid])
  end

  def masquerading_settings
    @masquerade = session[:user_guid] != session[:operator_guid] ? true : false
    @masquerade_user = User.find(session[:operator_guid]) if @masquerade
    @company_masquerade = session.include?(:company_mas_guid)
    if @masquerade
      #@masq_company = Company.find_by_guid(current_user.company_guid) rescue 'none'
      if (@masquerade_user.id = current_user.id)
        @masquerade_user = current_user
      end
    end
    @masquerade
  end

  def impersonate_user_from_cookie
    @o = @u = nil
    cookie_user = JSON.parse(cookies["current_user"]) rescue nil
    Rails.logger.info ">>>>>>>>>>>>>>> COOKIE USER FOUND >>>>>>>>>>>>>>>"
    Rails.logger.info cookie_user
    if cookie_user && cookie_user["impersonated"]
      user = fetch_user(cookie_user)
      if user.present? && sign_in_user.guid.downcase != user.guid.downcase && sign_in_user.can_masquerade? 
        Rails.logger.info ">>>>>>>>>>>>>>> IMPERSONATED USER FOUND >>>>>>>>>>>>>>>"
        reset_impersonation_setting
        mu = MasqueradeUser.new(user)
        mu.masquerade_as(user.id)
        if mu.masqueraded_is_sungard?
          @blocked_user_email = mu.email
        else
          Rails.logger.info ">>>>>>>>>>>>>>> SETTING USER IMPERSONATION >>>>>>>>>>>>>>>"
          session[:operator_guid] = user.id
          session[:user_mas_guid] = user.id
          session[:masquerade] = "true"
          @masquerade = true
          @masquerade_user = mu.user
          session[:is_omi] = true
        end
         @o = @u = nil
         @body_class = "masquerades index single-column"
      else
        Rails.logger.info ">>>>>>>>>>>>>>> RESETTING USER AS USER IS NOT FOUND >>>>>>>>>>>>>>>"
        reset_impersonation_setting
      end
    else
       reset_impersonation_setting
    end
  end

  def company_impersonate_user_from_cookie
    cookie_user = JSON.parse(request.cookies['current_user']) rescue nil
    if cookie_user && cookie_user['company_guid'].present? && cookie_user['email'].present? && cookie_user['impersonated']
       company = Company.where(guid: /#{cookie_user['company_guid']}/i).first
       if (company.present? && current_user.company_guid.downcase != company.guid.downcase && PortalMapWorker.is_child?(current_user.company.snt, company.guid))
         #Setting the company impersonation
         Rails.logger.info "@@@@@@@@@@ SETTING COMPANY IMPERSONATION WITH USER #{current_user.email} AND WITH COMPANY #{company.name} @@@@@@@@@@"
         session[:company_mas_guid] = company.guid
         @masquerade = true
         @masquerade_user = current_user
         @o = @u = nil #resetting current_user
      end
    else
       Rails.logger.info "@@@@@@@@@@@@@@@@@@@@@@@ REMOVING COMPANY IMPERSONATION @@@@@@@@@@@@@@@@@@@@@@@"
       session.delete(:company_mas_guid)
       session[:is_omi] = true
       session.delete(:masquerade_company)
       session.delete(:masquerade_company_id)
       set_user_company
       @o = @u = nil #resetting current_user
    end
  end

  def fetch_user(cookie_user)
    user = User.where(email: /#{cookie_user["email"]}/i).first
    if user.blank?
      one_portal_user = OnePortalUser.where(
        email: /#{cookie_user["email"]}/i, 
        guid: cookie_user["guid"], 
        company_guid: cookie_user["company_guid"],
        userType: "Portal").first
      if one_portal_user
        user = User.create!(
         :active => true,
         :company_guid => one_portal_user.company_guid,
         :email => one_portal_user.email,
         :first_name => one_portal_user.first_name,
         :last_name => one_portal_user.last_name,
         :guid => one_portal_user.guid,
         :portal_name => one_portal_user.portal_name,
         :roles => one_portal_user.roles,
         :provider => one_portal_user.provider,
         :app_settings => one_portal_user.app_settings
         )
        Rails.logger.info ">>>>>>>>>>>>>>>>> CREATING COOKIE USER IN USER COLLECTION>>>>>>>>>>>>>>>>>>>>"
      end   
    end
    return user
  end

  def reset_impersonation_setting
    session[:operator_guid] = session[:user_guid]
    session.delete(:company_mas_guid)
    session.delete(:user_mas_guid)
    session.delete(:company_has_childs)
    session[:masquerade] = "false"
    session.delete(:masquerade_company)
    session.delete(:masquerade_company_id)
    @masquerade = false
    session[:is_omi] = true
    set_user_company
    @o = @u = nil #resetting current_user
  end

  def authenticated?
    if session[:user_guid]
      if current_user and current_user.active
        masquerading_settings
        set_user_company #Refeering user company from company collection instade user
        set_omi_flag
        set_has_childs_to_session(current_user)
        #This setting is added for one portal
        impersonate_user_from_cookie
        company_impersonate_user_from_cookie
        return true
      else
        redirect_to logout_path
        return
      end
    end
    # authenticate_with_http_token do |token, options|
    #   Rails.logger.debug("Token: #{token}")
    #   if TOKEN==token
    #     user = User.find_by_guid(request.headers['HTTP_USERGUID']) #This is used for viewpoint_ui
    #     if user.blank?
    #       user = User.find_by_email('sandip.mondal@sungardas.com') # This is hard coded user if viewpoint ui is not sending any guid
    #     end
    #     session[:user_guid] = user.id
    #     session[:operator_guid] = user.id
    #     if current_user and current_user.active
    #       masquerading_settings
    #       set_user_company #Refeering user company from company collection instade user
    #       set_has_childs_to_session(current_user)
    #       return true
    #     end
    #   end
    # end
    session[:redirect_to] = request.fullpath
    if APP_CONFIG['current_user'] && Rails.env != "production"
      redirect_to new_session_path
    else
      oid_redirect
    end
  end

  # @return 403 [Forbidden] unless current_user
  def check_user
    if(!session[:user_guid])
      authenticate_with_http_basic do|username, password|
        is_user_valid = Authenticate.validate_user(username, password)
        if is_user_valid.to_i == 200
          u = User.find_by_email(/^#{username}$/i)
          session[:user_guid] = u.guid
        end
      end
    end
    render text: 'Forbidden', status: 403 unless current_user
    return
  end

  def deny_for_sungard_user
    if current_user.is_sungard?
      render text: 'You are not allowed to use this resource, impersonate a user.', status: 403
    end
  end

  def set_omi_flag
    session[:is_omi] ||= current_user.is_omi?
  end

  protected

  def rescue_not_found
    cookies[:fileDownload] = {
      :value => 'false',
      :path => '/'
    }
    render 'errors/not_found', :status => :not_found
  end

  # Sets the menu item name to be used for displaying help
  def help_menu_name(menu_name)
    @page_name=menu_name
  end

  def set_has_childs_to_session(user)
    # # Let know if current user company has child companies.
    if user.role?('switch_company') and not session.include?(:company_has_childs)
     companies = Company.parent_child_list(user.company_guid)
      if companies
       session[:company_has_childs] = companies.count > 1
     end
    end
  end

  def audit_user_action
    begin 
      if sign_in_user.present? && current_user.present?
        if ((request.method == 'POST' || request.method == "DELETE" || request.method == "PUT") && sign_in_user.is_sungard?  && (session[:masquerade] == "true"))
          resource = params["controller"] != "welcome" ? "#{params["controller"]} : #{params["action"]}" : params["action"]
          body = params["parameters"] ? params["parameters"] : ""
          Audit.create(auditable_type: "UserActionAudits",
                      loggedin_user: sign_in_user.guid,
                      company: current_user.company_guid,
                      company_name: current_user.company_name,
                      loggedin_user_email: sign_in_user.email,
                      auditable_changes: body, 
                      action: request.method,
                      resource: resource,
                      url: request.fullpath,
                      impersonating_user_email: current_user.email,
                      impersonating_user: current_user.guid)
        end
      end
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
    end
  end
end
