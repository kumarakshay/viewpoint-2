class AuthsController < ApplicationController
  layout 'blank'

  # required to render the login form
  def index
    #This is a double check to avoid some one misusing this link.
    if params.has_key?(:u)
      if is_a_url(params[:u])
        @url = params[:u]
      else
        @url = "/"
      end
    else
      @url = "/"
    end
    # redirect_to(@url)
  end

  protected

  def is_a_url(string)
    uri = URI.parse(string)
    %w( http https ).include?(uri.scheme)
    rescue URI::BadURIError
      false
    rescue URI::InvalidURIError
      false
  end

end