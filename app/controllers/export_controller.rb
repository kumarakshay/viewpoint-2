require 'csv'

class ExportController < ApplicationController

  # CSV export based on the index grid
  def devices       
    @devices = Device.find_all_by_asset_guid(current_user.asset_list)
    csv_string = CSV.generate do |csv|
      csv << ["Name", "IP", "HW Model", "Location", "OS Model", "Priority", "Production State", "Company"]
      @devices.each do |d|
        csv << [d.name, d.display_ip, d.hw_model, d.location, d.os_model, d.priority, d.production_state, d.company_use]
      end
    end         
   send_data csv_string,
   :type => 'text/csv; charset=iso-8859-1; header=present',
   :disposition => "attachment; filename=devices.csv" 
  end

  # CSV export based on the index grid
  def live_events       
    @events = Event.where(:companyGuid => current_user.company_guid, :eventState => ["0","1"], :severity => ["5","4","3","0"]).limit(300)
    csv_string = CSV.generate do |csv|
      csv << ["Device", "EventClass", "Summary", "First Time", "Last Time" "Count","Impact","Prod State","Location", "Device Class", "Severity"]
      @events.each do |e|
        csv << [e.device, e.eventClass, e.summary, Time.at(e.first_time), Time.at(e.last_time), e.count, e.DeviceImpact, e.prodState, e.facility, e.DeviceClass, e.severity]
      end
    end         
   send_data csv_string,
   :type => 'text/csv; charset=iso-8859-1; header=present',
   :disposition => "attachment; filename=live_events.csv" 
  end

  # CSV export based on the index grid
  def events 
    start_date = (Time.now - 3.days).to_i * 1000
    end_date = Time.now.to_i * 1000    
    @events = EventHistory.where(:companyGuid => current_user.company_guid, :eventState => ["0","1"], :severity => ["5","4","3","0"],
      :lastTime => start_date..end_date).limit(300).order("device")
    csv_string = CSV.generate do |csv|
      csv << ["Device", "EventClass", "Summary", "First Time", "Last Time" "Count","Impact","Prod State","Location", "Device Class", "Severity"]
      @events.each do |e|
        csv << [e.device, e.eventClass, e.summary, Time.at(e.first_time/1000), Time.at(e.last_time/1000), e.count, e.DeviceImpact, e.prodState, e.facility, e.DeviceClass, e.severity]
      end
    end         
   send_data csv_string,
   :type => 'text/csv; charset=iso-8859-1; header=present',
   :disposition => "attachment; filename=events.csv" 
  end

  # CSV export based on the index grid
  def interfaces       
    guid = current_user.company_guid
    @interfaces = Interface.where(:$or => [{:company_guid =>  guid},{contracting_guid: guid},{owner_guid: guid}]).sort(:device_name)
    csv_string = CSV.generate do |csv|
      csv << ["Device", "Interface", "Description", "Internet", "Network name", "Company"]
      @interfaces.each do |i|
        csv << [i.device_name, i.ip_interface, i.description, i.internet, i.network_name, i.company_use]
      end
    end         
   send_data csv_string,
   :type => 'text/csv; charset=iso-8859-1; header=present',
   :disposition => "attachment; filename=interfaces.csv" 
  end 
end