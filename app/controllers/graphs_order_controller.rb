class GraphsOrderController < ApplicationController 
  before_filter :authenticated?

  def create
	  dashboard = Dashboard.find(params[:dash_id])
    respond_to do |format|
      if dashboard and  dashboard.order_graph_child(params[:graph_id], params[:movement])
        format.html { redirect_to dashboard_path(params[:dash_id]), notice: 'Graph was successfully updated.' }
      else
        format.html { redirect_to dashboard_path(params[:dash_id]), 
        	  notice: 'unable to update Graph .' }
      end
    end
  end
end