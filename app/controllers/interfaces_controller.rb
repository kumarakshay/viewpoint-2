require 'sgtools/zenoss/interface'

class InterfacesController < ApplicationController
  before_filter lambda { help_menu_name('interfaces') }
  before_filter :omi_user_redirect, only: :index
 
  def index
    guid = current_user.company_guid
    @interfaces = Interface.where(:$or => [{:company_guid =>  guid},{contracting_guid: guid},{owner_guid: guid}]).sort(:device_name)
    @body_class = "interfaces single-column"
  end

  def show
  end

  def graph
    width = params[:width] || '500'
    drange = params[:drange].to_i > 0 ? params[:drange].to_i : 86400
    start_time = params[:start].nil? ? drange : params[:start].to_i
    start_string = "now-#{start_time}s"
    end_time = start_time - drange
    end_string = "now-#{end_time}s"
    # Rails.logger.info("width #{width}")
    # Rails.logger.info("drange #{drange}")
    # Rails.logger.info("*** start_time #{start_time}")
    # Rails.logger.info("start_string #{start_string}")
    # Rails.logger.info("*** end_time #{end_time}")
    # Rails.logger.info("end_string #{end_string}")
    comment = start_string.to_s + " to " + end_string.to_s
    g = SGTools::Zenoss::InterfaceGraph.new(params[:token], :z_instance => 'net', :width => width, :end => end_string, :start => start_string, :drange => drange, :comment => comment)
    #g = SGTools::Zenoss::InterfaceGraph.new(params[:token], :z_instance => 'net', :width => width, :end => end_string, :start => start_string, :drange => drange)    

    render text: g.png, content_type: 'image/png'
  end

  private

  def omi_user_redirect
    redirect_to "/ui/index.html#/interfaces" if session[:is_omi]
  end
end