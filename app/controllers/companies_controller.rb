class CompaniesController < ApplicationController
  respond_to :json

  # id = '5120c6d7e1b72746da0229c2'
  # checked = true
  def update
    id = params[:id]
    checked = params[:checked]
    ret = Company.set_active_flag(id, checked)
    respond_to do |format|
      format.json { render :json => ret }
    end
  end  
end