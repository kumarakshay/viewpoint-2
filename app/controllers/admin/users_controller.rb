class Admin::UsersController < AdminController
  before_filter :load_instance, :only => [:show, :edit, :update, :destroy, :change_password, :update_password, :toggle_unlock, :resend_confirmation,:change_email,:update_email,:change_email_success,:convert_non_portal,:toggle_active]
  before_filter :load_sso_data, :only => [:show]
  skip_before_filter :authorize_as_admin, :only =>[:edit, :update,:change_email, :update_email, :change_email_success]
  helper_method :user_company_guid
  after_filter :add_custom_metadata

  def index
    if (current_user.is_sungard_admin?) || (current_user.is_viewpoint_admin?)
      temp_company = Company.where(:guid => params[:company_id]).first
      session[:temp_company_guid] = temp_company.guid
      session[:temp_company_name] = temp_company.name
      @users = User.where(:company_guid => params[:company_id]).sort(:email)
    else
      @users = User.where(:company_guid => current_user.company_guid).sort(:email)
    end
    @toolname = 'portal users'
    @user = User.new()
  end

  # this will display both vp and sso data.  the page is calling the
  # the page makes an ajax call via assets/javascripts/admin/users_page.js to get
  # the data from openid
  def show
    authorize! :see_user_details, current_user
    respond_to do |format|
      @toolname = ': portal user'
      format.html # show.html.erb
    end
  end

  def new
    @user = User.new()
    if params[:company_id]
      @user.company_guid = params[:company_id]
    end
    respond_to do |format|
      @toolname = ': new portal user'
      format.html # new.html.erb
    end
  end

  # This method find non-portal user using guid as a parameter and prepopulate this informat in user
  # form to create portal user
  def convert
    np_user = get_sn_user(params[:id])
    unless (np_user.blank? or np_user.u_guid.blank?)
      @user = User.new
      @user.first_name = np_user.first_name
      @user.last_name = np_user.last_name
      @user.email =  np_user.email
      @user.old_contact_guid = np_user.u_guid
      #If user is converted from non-portal to portal we are logging data
      if Rails.logger.respond_to?(:add_metadata)
        Rails.logger.add_metadata(converted_user_email: @user.email)
        Rails.logger.add_metadata(converted_user_guid: @user.old_contact_guid)
      end
      respond_to do |format|
        @toolname = ': convert to portal user'
        format.html # new.html.erb
      end
    else
      Rails.logger.add_metadata(user_with_guid_not_present_in_sn: params[:id]) if Rails.logger.respond_to?(:add_metadata)
      flash[:notice] = 'There is some problem with converting this user. Please contact administrator'
      redirect_to root_path
    end
  end

  # Anybody can see the profile. but only user having higher role than other user can edit other users profile
  def edit
    authorize! :edit_user_details, current_user, @user
    @toolname = ': view/edit profile'
    # @edit_permission = current_user.has_higher_role_than_me?(@user)
    if @user.is_sungard?
      @sungard_user = true
    else
      @sungard_user = false
    end
    if not current_user.has_higher_role_than_me?(@user)
      render :file => 'shared/unauthorized', :layout => 'basic'
    end
  end

  # create method for the user - using the UUID gem to generate the guid
  def create
    begin
      @user = User.new(params[:user]).tap do |user|
        @toolname = ': new user'
        if ((current_user.is_sungard_admin?) || (current_user.is_viewpoint_admin?))
          user.company_guid = session[:temp_company_guid]
        else
          user.company_guid = current_user.company_guid
        end
        user.created_by = current_user.id.to_s

        if user.old_contact_guid.blank? # for new user
          user.set_guid if APP_CONFIG["open_id_version"] == 'V1'
        else
          #If user is converted from non-portal to portal we will validate and keep same guid
          # Checking if old_contact guid received from user form is same as the service now user
          sn_user = get_sn_user(user.old_contact_guid)
          if sn_user.blank?
            flash[:notice] = 'There is some problem with converting this user. Please contact administrator'
            redirect_to root_path
            return
          end
          user.set_guid(user.old_contact_guid)
        end
      end
      if @user.register! && @user.save_sn_data!(params[:user])
        send_email @user
        flash[:notice] = 'User was successfully created'
        redirect_to [:edit_admin,@user]
      else
        unless @user.sso_errors.blank?
          flash[:notice] =  @user.sso_errors[:message]
        else
         flash[:notice] = "Unable to create new User"
        end
        log_message('create', false, @user)
        render action: "new"
      end
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page
    end
  end

  def update
    begin
      @user.tap do |user|
        @toolname = ': edit portal user'
      end
      if current_user.admin_role?
        params[:user][:roles] ||= []
      else
        params[:user][:roles] = @user.roles
      end
      current_email = @user.email
      change_email = current_email != params[:user][:email] ? params[:user][:email] : nil
      if ((current_user.admin_role?) || ( sign_in_user.id == @user.id))
        if is_valid_with_new_params?
          @user.email = current_email #restored original email
          if @user.update!(change_email)
            if @user.save_sn_data!(params[:user])
              Rails.cache.delete("roles_for_user_#{@user.guid}")
              @user.reload
              if change_email.present? and @user.pending_email == change_email
                flash[:notice] = 'The user information was successfully submitted. <br/>You will receive an email to confirm your new address and complete the change.'
                notify_email_changed(@user)
              else
                flash[:notice] = 'The user information was successfully updated.'
              end
              redirect_to [:edit_admin,@user]
            else
              flash.now[:notice] = 'Error: profile not saved'
              render :edit
            end
          else
            flash.now[:notice] = 'That email is already being used. Profile not saved.'
            render :edit
          end
        else
          render_edit
        end
      else
        flash[:notice] = 'You are not the authorized person to update this'
        log_message('update', false, @user)
        render :edit
      end
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page_SSO
    end
  end

  # POST /admin/resend_confirmation
  # This will request for a new confirmation token from openid.
  def resend_confirmation
    begin
      if @user.resend_confirmation!
        send_email(@user) unless APP_CONFIG["open_id_version"] == 'V2'
        flash[:notice] = "A confirmation email was sent out to #{@user.email}"
      else
        flash[:notice] = "Unable to send confirmation email"
        log_message('resend_confirmation', false, @user)
      end
      redirect_to(admin_users_path(:company_id => params[:company_id] ))
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page
    end
  end

  def destroy
    authorize! :delete_user, current_user, @user
    begin
      if @user.delete_user!
        flash[:notice] = "User #{@user.email} deleted successfully"
      else
        flash[:notice] = @user.sso_errors[:message]
        log_message('destroy', false, @user)
      end
      unless params["search"].blank?
        redirect_to sungard_root_path(:search => params["search"] )
      else
        redirect_to admin_users_path(:company_id => @user.company_guid)
      end
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page
    end
  end


  # POST /admin/unlock
  # This will send an unlock request to openid for unlocking user.
  def toggle_unlock
    begin
      if @user.toggle_unlock!
        flash[:notice] = "User #{@user.email} was unlocked successfully"
      else
        flash[:notice] = "Unable to unlock user #{@user.email}"
        log_message('toggle_unlock', false, @user)
      end
      redirect_to(admin_users_path(:company_id => params[:company_id] ))
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page
    end
  end

  # This will send a request to openid for enabling/disabling user.
  # This API specific to SSO V2
  def toggle_active
    begin
      if @user.toggle_active!
        action = @user.active? ? 'enabled' : 'disabled'
        flash[:notice] = "User #{@user.email} was #{action} successfully"
      else
        flash[:notice] = "Unable to process request"
        log_message('toggle_active_for_SSO_v2', false, @user)
      end
      redirect_to(admin_users_path(:company_id => params[:company_id] ))
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page
    end
  end

  def convert_non_portal
    @sys_user = SGTools::ITSM::SystemUser.get_records(:u_guid => @user.guid).first
    unless @sys_user.blank?
      @company_id = params[:company_id]
      @edit_user_id = @user.guid
      @body_class = "contact_user"
      #If user is converted from portal to non-portal we are logging data
      Rails.logger.info @sys_user.inspect
      if Rails.logger.respond_to?(:add_metadata)
        Rails.logger.add_metadata(converted_user_email: @sys_user[:email])
        Rails.logger.add_metadata(converted_user_guid: @sys_user[:u_guid])
      end
    else
      Rails.logger.add_metadata(user_with_guid_not_present_in_sn: @user.guid) if Rails.logger.respond_to?(:add_metadata)
      flash[:notice] = 'There is some problem with converting this user. Please contact administrator'
      redirect_to root_path
    end
  end

  def cleanup
    @user = User.find_by_guid(params[:id])
    authorize! :delete_user, current_user, @user
    begin
      respond_to do |format|
        if @user.delete_user!
          format.js{ render :nothing => true }
        else
          log_message('destroy', false, @user)
          format.js{ render :nothing => true}
        end
      end
    rescue Exception => e
      logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      render_home_page
    end
  end

  private

  #Return user object from service now
  def get_sn_user(guid)
    SGTools::ITSM::SystemUser.get_users(:u_guid => guid).first
  end

  #Return company_guid for user which is further used for navigating to non-portal users.
  def user_company_guid
    if params && params[:company_id].present?
      params[:company_id]
    elsif @user && @user.company_guid
      @user.company_guid
    else
      current_user.company_guid
    end
  end

  def render_new
    flash[:notice] = "Unable to create new User"
    render action: "new"
  end

  def render_edit(msg = 'Error: profile not saved')
    flash.now[:notice] = msg
    render action: "edit"
  end

  def send_email(user)
    UserMailer.registration_confirmation(user).deliver unless APP_CONFIG["open_id_version"] == 'V2'
  end

  def notify_email_changed(user)
    if APP_CONFIG["open_id_version"] != 'V2'
      UserMailer.reset_email_confirmation(user).deliver #sending confirmation email on users new email address
      UserMailer.reset_email(user).deliver #sending notification email on users old email address
    end
  end

  def load_sso_data
    begin
      if @user
        result = @user.get_user!
        @sso_user = OpenStruct.new(result)
      else
        @sso_user = OpenStruct.new({"roles"=>[],
                                    "applications"=>[],
                                    "timezone"=>"",
                                    "email"=>"",
                                    "groups"=>[],
                                    "company_guid"=>"",
                                    "first_name"=>"",
                                    "last_name"=>"",
                                    "guid"=>""
                                    })
      end
    rescue
      Rails.logger.warning("Unable to load user data from sso id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

  def load_instance
    begin
      @user = User.find(params[:id])
      if @user.blank?
        Rails.logger.info("Unable to load user with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.warning("Unable to load user with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

  def check_status(res, res_type)
    check_status_with_attribute(res,res_type,nil)
  end

  def check_status_with_attribute(res, res_type, attribute=nil)
    status = "failure"
    token = nil
    if (res and res.kind_of? Net::HTTPSuccess)
      json_res = JSON.parse(res.body)
      status = json_res[res_type]
      token = json_res["user"][attribute] unless attribute.blank?
    end
    attribute.blank? ? status : [status, token]
  end

  def create_audit_log(user, action)
    audit = {}
    audit["user_email"] = user.email
    audit["action"] = action
    audit["auditable_type"] = 'User'
    Audit.create(audit)
  end

  # Checks if user object is valid after considering new input parameters.
  def is_valid_with_new_params?
    @user.attributes = params[:user]
    @user.valid?
  end

  def log_message(action, res_status, user)
    logger.error("------------- Please check following data in #{action} -------------
      \n-------- Received response status ------------- \n \t #{res_status}
      \n-------- User errors ------------- \n  \t #{user.errors.full_messages} \n ")
  end

  def render_home_page(msg="There is some problem with updating data. Please contact administrator")
    flash[:notice] = msg
    redirect_to home_path
  end
  def render_home_page_SSO(msg="Cannot update user. User is not in SSO. Please contact an administrator.")
    flash[:notice] = msg
    redirect_to [:edit_admin,@user]
  end

  # Logging for custom metadata for auditing purpose.
  def add_custom_metadata
    data = BSON::OrderedHash.new()
    # Fresh zone so we have the updated serial number
    if @user
      data['user_name'] = "#{@user.first_name} #{@user.last_name}" if (@user.first_name and @user.last_name)
      data['user_guid'] = @user.guid if @user.guid
      data['user_email'] = @user.email if @user.email
      data['user_company_guid'] = @user.company_guid if @user.company_guid
    end
    Rails.logger.add_metadata('user_profile_data' => data)
  end
end
