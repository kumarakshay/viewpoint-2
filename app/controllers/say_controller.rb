class SayController < ApplicationController
  layout 'application'
  before_filter :set_body_class


  def privacy
  end  

  def terms
  end
  
  def safe_harbor
  end

  def dashboards
  end

  def set_body_class
  	@body_class = 'single-column'
  end

end
