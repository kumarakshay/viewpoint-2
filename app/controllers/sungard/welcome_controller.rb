class Sungard::WelcomeController < VpAdminController

  def index
    unless params[:search].blank?
      @users = User.search_by_email(params[:search]) 
      # @openid_user = MyOpenid.get_user(params[:search])
    end
    respond_to do |format|
      @body_class = "single-column"
      format.html # index.html.erb
    end
  end
end
