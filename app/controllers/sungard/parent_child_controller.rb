# Controls the display of single level of parent child relations based on 
# the parent guid.  

class Sungard::ParentChildController < ToolsTeamController
  layout 'blank'
  
  def index
  end

  def show
    company_guid = params[:id]
    company = Company.where(guid: company_guid).first
    @dats = PortalMapWorker.where(parent_company_snt: company.snt).all
    respond_to do |format|
      format.html
    end
  end
end