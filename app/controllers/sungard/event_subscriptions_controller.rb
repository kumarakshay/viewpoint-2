class Sungard::EventSubscriptionsController <  VpAdminController
  def index
    @event_subscriptions = EventSubscription.all_events    
  end 
  
  def show
    @event_subscription = EventSubscription.load_event_subscription(params[:id])
    respond_to do |format|      
      format.html
    end
  end
end