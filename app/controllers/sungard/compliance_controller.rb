class Sungard::ComplianceController <  VpAdminController
  # load_and_authorize_resource
  # skip_before_filter :authorize
  before_filter :load_instance, :only => [:update,:destroy]

  def index
    @compliance_groups = Compliance.sort(:name.asc).all
    @toolname = 'Compliance Network Management'
    @body_class = "single-column"
  end

  def new
    @compliance = Compliance.new()
    @toolname = 'New Compliance Group'
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def create
    @toolname  = 'New Compliance Group'
    @compliance = Compliance.new(params[:compliance])
    respond_to do |format|
      if @compliance.save
        format.html { redirect_to '/sungard/compliance', notice: 'compliance group was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @toolname = 'Generate Compliance Report'
    begin
      respond_to do |format|
        if @compliance.update_attributes(params[:compliance])
          hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => "executions", :method => "POST" , :query => compliance_params })
          response = hpClient.fetch
          executionId = response["executionId"]
          if executionId
            pathname = "executions/" + executionId +  "/execution-log"
            get_hpClient = SGTools::Rails::Hpoo::HpooClient.new({:pathname => pathname, :method => "GET" , :query => nil})
            get_response = get_hpClient.execute_long_running
            if get_response["flowOutput"]
              @compliance.compliant_count = get_response["flowOutput"]["compliantCount"]
              @compliance.non_compliant_count = get_response["flowOutput"]["nonCompliantCount"]
              if @compliance.save
                insert_compliance_devices(get_response["flowOutput"],@compliance)
              end
            end
            notice = params[:commit] == "Check Compliance" ? "Compliance report check has been completed successfully. Please check the result for more details" : "Compliance report request has been submitted successfully(#{executionId}). Please <a href='/ui/index.html#/reports'>click here </a> to download the report"
            format.html { redirect_to '/sungard/compliance', notice: notice }
          else
            format.html { redirect_to '/sungard/compliance', notice: 'There is an issue with running HPOO flow' }
          end
        else
          format.html { redirect_to '/sungard/compliance', notice: "There is an issue with updating compliance version: #{@compliance.errors.full_messages}" }
        end
      end
    rescue Exception =>e
      Rails.logger.info "*******Compliance Reporting Exception*********************"
      Rails.logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
      format.html { redirect_to '/sungard/compliance', notice: e.message }
    end
  end

  def destroy
    @toolname = ': delete help'
    @compliance.destroy
    respond_to do |format|
      format.html { redirect_to "/sungard/compliance" }
    end
  end

  private

  def insert_compliance_devices(dataString,obj)
    unless dataString["DeviceComplianceData"].blank?
      existing_records = ComplianceDevice.where(:parent => obj.name,:group => obj.group_name,:company_guid => current_user.company_guid.downcase).all
      existing_records.map(&:delete)
      version = obj.version.blank? ? "" : obj.version
      batch = prepare_record(dataString["DeviceComplianceData"], obj.name, obj.group_name, version)
      ComplianceDevice.collection.insert(batch) unless batch.empty?
    end
  end

  def prepare_record(recordStr,parent,group,version)
    batch = []
    strArray = recordStr.split("||")
    strArray.each do |element|
      deviceObj = {}
      objArray = element.split("|")
      unless objArray.blank?
        compliant = objArray[1].match(version).present?
        deviceObj["name"] = objArray[0]
        deviceObj["version"] = objArray[1]
        deviceObj["compliant_version"] = version
        deviceObj["type"] = "Network"
        deviceObj["parent"] = parent
        deviceObj["group"] = group
        deviceObj["company_guid"] = current_user.company_guid
        deviceObj["is_compliant"] = (objArray[2] == "YES")
        deviceObj["created_at"] =  DateTime.now.strftime('%Y-%m-%dT%H:%M:%S%z')
        deviceObj["updated_at"] =  DateTime.now.strftime('%Y-%m-%dT%H:%M:%S%z')
        batch.push(deviceObj)
      end
    end
    return batch
  end

  def load_instance
    begin
      @compliance = Compliance.find(params[:id])
      if @compliance.blank?
        Rails.logger.info("Unable to load compliance with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.info("Unable to load compliance with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

  def compliance_params
    default_params = {
      "uuid" => "316dea09-9f9f-42da-90f3-62c6f7842238",
      "runName"=> "ViewPoint - Compliance Report"
    }
    input = {"GroupName" => @compliance.group_name, "CompliantVersion" => @compliance.version}
    input.merge!("pyxieRequired" => params[:commit] && params[:commit] != "Check Compliance" ? "Yes" : "No")
    default_params.merge!("inputs" => input)
    default_params
  end
end
