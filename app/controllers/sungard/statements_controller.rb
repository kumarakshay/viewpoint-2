class Sungard::StatementsController <  VpAdminController

  # GET /statements
  def index
    @toolname = ': Statements'
    # @statements = Statement.all
    @statements = Statement.order("position")
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /statements/1
  def show
    @toolname = ': Statements'
    @statement = Statement.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /statements/new
  def new
    @toolname = ': new statement'
    @statement = Statement.new
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /statements/1/edit
  def edit
    @toolname = ': edit statements'
    @statement = Statement.find(params[:id])
  end

  # POST /statements
  def create
    @statement = Statement.new(params[:statement])
    respond_to do |format|
      if @statement.save
        format.html { redirect_to [:sungard, @statement], notice: 'Statement was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /statements/1
  def update
    @statement = Statement.find(params[:id])
    respond_to do |format|
      if @statement.update_attributes(params[:statement])
        format.html { redirect_to [:sungard, @statement], notice: 'Statement was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /statements/1
  def destroy
    @statement = Statement.find(params[:id])
    @statement.destroy
    respond_to do |format|
      format.html { redirect_to sungard_statements_url }
    end
  end
end