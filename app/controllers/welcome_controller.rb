class WelcomeController < ApplicationController

  before_filter :authorize_mfa, only: [:patching_schedule, :patching_policy, :patching_servers ]

  def index
    @body_class = "dash single-column"
  end 

  def landing_page
    ###Common Portal Landing page internal and external users
    redirect_to "/ui/index.html#"
  end

  #Proxy routes to VP5 tutorials page
  def tutorials_page
    redirect_to "/ui/index.html#/tutorial"
  end

  #Proxy routes to VP5 dns page
  def dns_page
    redirect_to "/ui/index.html#/dns"
  end

  #Proxy routes to VP5 reports page
  def reports_page
    redirect_to "/ui/index.html#/reports"
  end

  #Proxy routes to VP5 restricted_contracts page
  def restricted_contracts
    redirect_to "/ui/index.html#/restricted_contracts"
  end

  #Proxy routes to VP5 restricted_invoices page
  def restricted_invoices
    redirect_to "/ui/index.html#/restricted_invoices"
  end

  #Proxy routes to VP5 ipam page
  def ipam
    redirect_to "/ui/index.html#/ipam"
  end

  #Proxy routes to VP5 patching-schedule page
  def patching_schedule
    url = "/ui/index.html#/patching-schedule"
    redirect_to url
  end

  #Proxy routes to VP5 patching-policy page
  def patching_policy
    url = "/ui/index.html#/patching-policy"
    redirect_to url
  end

  #Proxy routes to VP5 patching-servers page
  def patching_servers
    url = "/ui/index.html#/patching-servers"
    redirect_to url
  end

  #Proxy routes to VP5 cloud portal page
  def sg_cloud_portal
    url = "/ui/index.html#/sg-cloud-portal"
    redirect_to url
  end

  #Proxy routes to firewalls
  def vp_firewalls
    redirect_to firewalls_path
  end

  #Proxy routes to VP5 ipv4 page
  def ipv4
    redirect_to "/ui/index.html#/ipv4"
  end

  #Proxy routes to VP5 ipv6 page
  def ipv6
    redirect_to "/ui/index.html#/ipv6"
  end

  #Proxy routes to VP5 dns_audits page
  def dns_audits
    redirect_to "/ui/index.html#/dns_audits"
  end

  #Proxy routes to HPC admin page
  def hpc_admin
    url = "/ui/index.html#/hpc_admin"
    set_2fa_cookie(url)
    redirect_to url
  end

  #Proxy routes to VP5 Command Runner Show Command page
  def command_runner
    url =  "/ui/2f/index.html#/cr-show/#{params[:asset_guid]}"
    set_2fa_cookie(url)
    redirect_to url
  end

  def pyxie_reports
    pyxie_url = "/ui/index.html#/reports"
    if params[:folder].present? && params[:filename].present?
      pyxie_url =  "/pyxie/#{params[:folder]}/#{params[:filename]}"
    end
    redirect_to pyxie_url
  end

  #Proxy routes to VP5 dashboard for NCL and Menzies
  def osappdashboard
    redirect_to "/ui/index.html#/os-app-dashboard"
  end

  def set_2fa_cookie(value)
    cookies[:stored_url] = {
      :value => value,
      :expires => 2.minutes.from_now
    }
  end

  private

  def authorize_mfa
    if MfaEvent.for_user(sign_in_user.guid).first.blank?
     redirect_to "#{APP_CONFIG["oneportal_url"]}mfa?goto=#{request.original_url}"
    end
  end
end
