class Services::PasswordResetsController < Admin::UserPasswordResetsController
  layout 'reset'
	skip_before_filter :authenticated?, :select_branding
  before_filter :load_user, :only => [:edit, :update]
  
	def new
	end

  def show
     redirect_to :edit_services_password_reset
   end

  def create
    begin
      @user = User.first(:email => { :$regex => /^#{params[:email]}$/i }, :active => true)
      if @user and @user.send_password_reset!
        send_email @user unless APP_CONFIG["open_id_version"] == 'V2'
        redirect_to_index('Email sent with password reset instructions', @user.company_guid)  
      else
        redirect_to_index("Unable to reset password for #{params[:email]}", nil)             
      end 
    rescue Exception => e 
       logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
       redirect_to_index("Unable to reset password for #{params[:email]}", nil)
    end
  end

  def edit
    @user = User.find_by_reset_password_token(params[:id])
    if @user.blank?      
      @user = User.new(:reset_password_token => params[:id])
    end
    @user
  end

  def update
    begin
      @user.attributes = params[:user]
      if @user.confirm_password_reset! 
          redirect_to welcome_path
          return
      else
        if @user.sso_errors.blank?
          render "edit"
        else
         render_edit('Invalid reset token')
        end
      end  
    rescue Exception => e 
       logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
       flash[:notice]= "Invalid reset token, Please contact your administrator"
       redirect_to home_path 
    end
  end

  private

  def redirect_to_index(message, company_guid)
    respond_to do |format|
      format.html { 
        flash[:notice] = message
        if company_guid.blank?
          redirect_to welcome_path
        else
        redirect_to(admin_users_path(:notice => message, 
                                     :company_id => company_guid))
        end
      }
    end
  end

  def load_user
    @user = User.find_by_reset_password_token(params[:id])
    if @user.blank?      
      @user = User.new(params[:user])
    end
  end  

  def render_edit(message)
    flash[:notice] = message 
    render action: "edit"
  end
end
