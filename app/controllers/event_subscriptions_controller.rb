class EventSubscriptionsController < ApplicationController
  before_filter :load_instance, :only => [:show, :edit, :update, :destroy]
  before_filter lambda { help_menu_name('event_subscription') }

  # display subscriptions by user
  def index
    authorize! :access_non_omi_pages, current_user, session[:is_omi]
    if current_user.is_sungard?
      @body_class = "event_subscriptions index single-column"
      render :file => 'shared/unauthorized_masq', :layout => 'application'
      return
    else
      if params[:company_guid]
        authorize! :event_subscription, current_user
        @event_subscriptions =  EventSubscription.load_event_subscriptions_for_company(params[:company_guid])
      else
        @event_subscriptions = EventSubscription.load_event_subscriptions_by_user_guid(current_user.guid)
      end
      @body_class = "event_subscriptions index single-column"
    end
  end 
  
  def show
    respond_to do |format|
      @body_class = "event_subscriptions show single-column"
      format.html # show.html.erb
    end
  end

  def new
    @event_subscription = EventSubscription.new(:asset_list => [""]) 
    #default email set to current_user's email address 
    @event_subscription.email = current_user.email   
    respond_to do |format|
      @body_class = "event_subscriptions new single-column"
      format.html # new.html.erb
    end
  end

  def edit
   # @group = Group.find(params[:id])
    @body_class = "event_subscriptions edit single-column"
  end  

  def create 
    @event_subscription = prepare_event_subscription (params) 
    respond_to do |format|
      @body_class = "event_subscriptions create single-column"
      if @event_subscription.save
        format.html { redirect_to event_subscriptions_path, notice: 'Event notification was successfully created.' }
        format.json { render json: @event_subscription, status: :created, event_subscription: @event_subscription}
      else
        format.html { render action: "new" }
        format.json { render json: @event_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @body_class = "event_subscriptions update single-column"
      if @event_subscription.update_attributes(params[:event_subscription])
        if params[:company_guid].blank?
          format.html { redirect_to event_subscriptions_path, notice: 'Event notification was successfully updated.' }
        else
          format.html { redirect_to event_subscriptions_path(:company_guid => params[:company_guid]), notice: 'Event notification was successfully updated.' }
        end
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @event_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      @body_class = "event_subscriptions destroy single-column"
      if @event_subscription.destroy
        format.html { redirect_to event_subscriptions_path(:company_guid => params[:company_guid]) }
      else
        format.html { redirect_to event_subscriptions_url(:company_guid => params[:company_guid]), notice: 'Unable to delete event notification as it may be referenced in subscriptions' }
      end
    end    
  end

  private 

  def load_instance
    begin
      @event_subscription = EventSubscription.load_event_subscription(params[:id]) 
      if @event_subscription.blank?
        Rails.logger.info("Unable to load event notification with id  #{params[:id]}")
        render 'errors/not_found',  :status => :not_found
      end
    rescue
      Rails.logger.info("Unable to load event notification with id  #{params[:id]}")
      render 'errors/not_found',  :status => :not_found
    end
  end

  def prepare_event_subscription(params)
    event_subscription = EventSubscription.new(params[:event_subscription])
    event_subscription.company_guid = current_user.company_guid
    event_subscription.user_guid = current_user.guid
    event_subscription
  end
end
