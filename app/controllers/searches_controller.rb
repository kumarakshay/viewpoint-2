class SearchesController < ApplicationController
  def new
    @search = Search.new
  end

  def create
    @search = Search.create!(params[:search])
    redirect_to @search
  end

  def show
    @dat =[]
    unless params[:query].blank?
      if current_user.is_sungard?
        @dat = Device.search(params[:query])
      else
        @dat = Search.search_with_params(current_user.company_guid, params[:query])
      end
    end
    @query_param = params[:query]
    respond_to do |format|
      format.html
      format.js
    end
  end
end
