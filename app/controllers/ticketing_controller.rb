class TicketingController < ApplicationController
  before_filter  :authenticated?
  before_filter  :set_body_class

  def index
  end
  
  private
  def set_body_class
    @body_class = 'dashboard single-column'
  end
  
end