class CompanyMasqueradesController < ApplicationController

  before_filter lambda { help_menu_name('masquerade') }  
  before_filter :load_parent_child_companies, :only =>[:index, :show, :new]
  
  def index    
    authorize! :company_masquerade, @real_user
    @body_class = "masquerades index single-column"
    @errors = []
    @companies = []
    # TODO why is there a limit of 50 here?
    if params[:search].blank?        
      # @companies = Company.all(:limit => 50, :guid => {:$in => @guids})
      @companies = Company.where(:guid => {:$in => @guids}).sort(:name.asc)
    else
      @companies = Company.where(name: /#{params[:search]}/i, :guid => {:$in => @guids}).sort(:name.asc)
    end
  end

  def new
    authorize! :company_masquerade, @real_user
    #proxy = User.find_by_guid(PROXY_USR_GUID)
    @company = Company.find_by_guid(params[:id])
    if @company
      session[:company_mas_guid] = params[:id]
      session[:operator_guid] = current_user.id
      @masquerade = true
      @masquerade_user = current_user
      session[:is_omi] = @masquerade_user.is_omi?
      @o = @u = nil #resetting current_user
      #session.delete(:company_has_childs)
    else
      flash[:notice] ='Unable to use switch to other company'
    end
    conditional_redirect
    @body_class = "masquerades index single-column"
  end

  def destroy
    # Check if previously masqueraded    
    if session.include?(:user_mas_guid)
      session[:operator_guid] = session[:user_mas_guid]
    else
      session[:operator_guid] = session[:user_guid]
      session[:masquerade] = "false"
      @masquerade = false
    end
    session.delete(:company_mas_guid)
    session.delete(:company_has_childs)
    session.delete(:is_omi)
    #session.delete(:user_mas_guid)
    @o = @u = nil #resetting current_user
    conditional_redirect
  end

  def conditional_redirect
    # if Access.can_access_SN(current_user.company_guid)
    #  redirect_to "/managedservice/#ticket_landing"
    # else
    #   redirect_to home_path
    # end
    redirect_to my_board_path
  end

  private 

  # Load all the parent child companies
  # -- if sungard user is masquerading then we need to hold the child companies from the 
  #       masquerading user
  # -- else if a normal user with masquerading permission logs in then we load the parent child
  #       company from his parent child relationship.
  #    
  def load_parent_child_companies
    @real_user = current_user
    if session.include?(:user_mas_guid)
      mu = User.find(session[:user_mas_guid])
      unless mu.blank?
        @real_user = mu
      end
    else
     current_user.reload
    end
    @guids = Company.parent_child_list(@real_user.company_guid)
    @guids.delete(@real_user.company_guid)
  end
end
