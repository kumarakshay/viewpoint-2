class HelpsController < ApplicationController

  before_filter  :set_body_class

  def index
    @helps = Help.all
  end

  def show
    if params[:menu].blank?    
      @help = Help.where(id: params[:id]).first
    else
      @help = help_for_menu
    end  
    
    respond_to do |format|
      format.html { render 'show', :layout => false }
      format.js do
        render :action => 'menu_help'
      end
    end
  end

  private 
  #Fetches help associated with particular Menu item.
  def help_for_menu    
    @help = nil
    menu = Menu.where(:title => params[:menu]).first
    if menu      
      @help = Help.where(:menu_id => menu.id.to_s).fields(:body).first
    end   
    @help
  end

  def set_body_class
    @body_class = 'dashboard single-column'
  end

end