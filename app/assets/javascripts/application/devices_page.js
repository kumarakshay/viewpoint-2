define([
    'sg/jquery',
    'sg/ajax',
    'sg/util',
    'backbone.marionette',
    '../livetable/main',
    './mockup_json/device_records_generator',
    './mockup_json/device_columns_generator',
    'livetable/builder/column_builder'
    ], function(
        $,
        ajax,
        util,
        Marionette,
        LiveTable,
        sampleLogs,
        sampleColumns,
        columnBuilder
    ) {

        return({

            test_value: "true",

            render_logs: function(columns, logs){
              var self = this;
              var LogList = new Marionette.Application();
              LogList.addRegions ({
               page: "#section"
              });
              LogList.module("FirewallLogs", {
                moduleClass: LiveTable,
                columnObj: columns,
                dataObj: logs,
                trCallbacks : {
                  rowClick: function(args){
                    window.location = '/devices/' + args.asset_guid;
                  }
                }
              });
             LogList.page.show(LogList.FirewallLogs.layout);
             LogList.start();
            },

            init: function(stub_webservice) {
              var self = this;
              stub_webservice = stub_webservice || "false";
              $('#section').css({'display':'block', 'top': '110px'});
              if(stub_webservice === "true") {
                var columns = sampleColumns();
                var logs = sampleLogs(100);
                self.render_logs(columns,logs);
              }
              else{
                
                var request = ajax.call('/api/vp_devices');
                request.done(function($data){
                  if($data.status != 200){
                    alert('invalid req');
                  }
                  else{
                    var columns = columnBuilder.dummy_json_logs([{"modelAttribute" : 'name', 'title': "Name", "columnType":"text"},
                                                                 {"modelAttribute" : 'ip_address_formatted', 'title': "IP Address", "columnType":"text"},
                                                                 {"modelAttribute" : 'hw_model', 'title': "HW Model", "columnType":"text"},
                                                                 {"modelAttribute" : 'location', 'title': "Location", "columnType":"text"},
                                                                 {"modelAttribute" : 'os_model', 'title': "OS Model", "columnType":"text"},
                                                                 {"modelAttribute" : 'priority', 'title': "Priority", "columnType":"text"},
                                                                 {"modelAttribute" : 'production_state', 'title': "Production State", "columnType":"text"},
                                                                 {"modelAttribute" : 'company_use', 'title': "Company", "columnType":"text"},
                                                                 ]);
                    var logs = $data;
                    self.render_logs(columns,logs);
                  }
                });
              }
            }
        });
});
