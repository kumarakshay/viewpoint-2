//TODO: Remove this file after Adding shim file for fix HTML5 placeholder issue less than IE10 Browser version support
/*********
* This code is used to solve placeholder issue fix for browsers less than IE10
*********/
define([
  'backbone.marionette'
], function(Marionette) {
  "use strict";
  var PlaceHolderBehavior = Backbone.Marionette.Behavior.extend({
    onShow: function() {         
      /****Start : Placeholder hack for lessthan IE10 version *************/
          jQuery(function() {
            jQuery.support.placeholder = false;
            var inputEle = document.createElement('input');
            if('placeholder' in inputEle) jQuery.support.placeholder = true;
          });
          jQuery(function() {
          // Placeholder text is not supported.
              var saveBtnHandler = function(event) {
                  event.preventDefault();
                  $('#group_manager').find('[placeholder]').each(function() {
                  var input = $(this);
                  if (input.val() == input.attr('placeholder')) {
                    input.val('');
                  }
                  })
                };
              if(!$.support.placeholder) {
                $("input[type='button']").click(saveBtnHandler);
                $('[placeholder]').focus(function() {
                  var input = $(this);
                  if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                  }
                }).blur(function() {
                  var input = $(this);
                  if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                  }
                }).blur();                
              }
            });
          /*****************END : Placeholder hack for lessthan IE10 version**************/
          }
        });
  return PlaceHolderBehavior;
});
