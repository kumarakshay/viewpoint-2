define([
       'underscore',
       'backbone.marionette',
       'hbs!../templates/options_list'
], function(_,Marionette,optionListTmpl) {

  var OptionView = Marionette.ItemView.extend({
    tagName: "option",
    initialize: function(options) {
      this.optName = options.optName;
      this.optValue = options.optValue;
    },
    attributes: function() {
      return {
        value: this.optValue || this.model.get('id')
      };
    },
    template: optionListTmpl,
    serializeData: function() {
      if (this.optName) {
        return {
          name: this.optName
        };
      }
      return this.model.toJSON({withAllRelations: true});
    }
  });
  return OptionView;
});
