define([
       'underscore',
       'backbone.marionette',
       '../collections/groups',
       '../collections/devices',
       '../models/group',
       '../views/option_view',
       'hbs!../templates/group_device_list_form',
       'hbs!../templates/result_row'
], function(
  _,
  Marionette,
  GroupCollection,
  DeviceCollection,
  GroupModel,
  OptionListView,
  GroupDeviceTmpl,
  resultRowTmpl
) {


  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: resultRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center;">No Results Found</td>')
  });

  var newGroupView =  Marionette.CompositeView.extend({
    template: GroupDeviceTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.group_results_table tbody.group_results',
    
    initialize: function(options){
      var self = this;
      this.app = options.app;
      self.sort_dir = "ASC";
      data = this.collection.fetch().done(function(){
        self._clonedAndRemoveDevices();
      });
      this.groups = new GroupCollection();
      var group_id = self.model.get("id");
      this.groups.fetch({
        reset: true,
        data: {
         '__filter[without]': [group_id]
        },

      });
      self.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        } else {
          // $(window).off("resize")
          //   .on("resize",_.bind(self.syncColumns,self,sc_options))
          //   .resize();
        }
      });
      self.collection.listenTo(self.app.vent,'group:device:removed',function(m, deleted_model) {
        self.model = m;
      //This is used for if device removed from left form, 
      //then we are checking this device belogs to displayed group then 
      //we wiil add device to right for
        var match_model = self.cloned.get(deleted_model.get('id'));
        if(match_model !== undefined)
        {
          self.collection.add(deleted_model);
        }
      });

      sc_options = {
        header_row: self.ui.header_row,
        results_first_row: self.ui.results_first_row
      };
     
      self.FilteredCollection.bind("change",function(){
      });

    },

    ui: {
     add: '.add_selected',
     device_detail: '.group_results',
     devices: 'input[name="device"]',
     check_all: 'input[name="select_all"]',
     add_all: '.add_all',
     groups: 'select[name="groups"]',
     name: 'input[name="device_name"]',
     type: 'input[name="type"]',
     header_row: '.sync_columns_head tr:first-child th',
     results_first_row: '.sync_columns_results tr:first-child td',
     location: 'input[name="location"]',
     ip_address_formatted: 'input[name="ip_address_formatted"]',
     sort: '.sort',
     clearable: '.onX'
    },

    events: {
      "click @ui.add": "AddDeviceToGroup",
      'click @ui.check_all': 'checked_devices',
      'click @ui.add_all': 'addAllDevices',
      'change @ui.groups': 'populateDevices',
      'keyup @ui.name' : 'applyFilter',
      'keyup @ui.type' : 'applyFilter',
      'keyup @ui.location' : 'applyFilter',
      'keyup @ui.ip_address_formatted' : 'applyFilter',
      'click @ui.sort': 'doSort',
      'click @ui.clearable': 'clearFilter'
    },
    clearFilter: function(e) {
      var self = this;
      $(e.currentTarget).val('');//clear only current input Field
      self.applyFilter(e);
    },
    clearSearchFields: function(e){
     this.ui.name.val("");
     this.ui.type.val("");
     this.ui.ip_address_formatted.val("");
     this.ui.location.val("");
    },

    applyFilter: function(e){
      var self = this;
      var name = this.ui.name.val();
      var type = this.ui.type.val();
      var location = this.ui.location.val();
      var ip_address_formatted = this.ui.ip_address_formatted.val();
      
      var filterCollection = self.filteredData.where({name: name, type: type,
        ip_address_formatted: ip_address_formatted, location: location });
      
      self.collection.reset(filterCollection.models);
      self.removeExistingDevice();
      if (filterCollection.models.length === 0) {
        self.emptyView = NoResultsView;
        self.collection.reset();
      }
    },

    FilteredCollection: function(original){
      var self = this;
      var filtered = new original.constructor();
      filtered.add(original.models);
      // allow this object to have it's own events
      filtered._callbacks = {};

      // call 'where' on the original function so that
      // filtering will happen from the complete collection
      filtered.where = function(criteria){
        // var items;
        // call 'where' if we have criteria
        // or just get all the models if we don't
        criteria = _(criteria).reduce(function(a,v,k){
               if(v){ a[k]=v; }
               return a;
          },{});

        if(criteria && !_.isEmpty(criteria)){
          self.items = _.filter(original.models, function(mod){
            var match = false;
            for(var prop in criteria){
              var mod_prop = mod.get(prop) ? mod.get(prop) : '';
              match = (mod_prop.toLowerCase().indexOf(criteria[prop].toLowerCase()) > -1);
              if (!match){
                break;
              }
            }
            if(match){
              return mod;
            }
          });
        }else {
          self.items = original.models;
        }
        filtered._currentCriteria = criteria;
        // reset the filtered collection with the new items
        filtered.reset(self.items);
        return filtered;
      };
    
      // when the original collection is reset,
      // the filtered collection will re-filter itself
      // and end up with the new filtered result set
      original.on("reset", function(){
          filtered.where(filtered._currentCriteria);
      });
      return filtered;
    },

    //Add selected devices to left form
    AddDeviceToGroup: function(){
      var self = this;
      this.ui.device_detail.find('input:checked').each(function(index, elm){
        var model = self.collection.get(elm.value);
        self.update_asset(model);
        self.collection.remove(model);
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        }
      });
    },

    //Checked all the devices from right form
    checked_devices: function(){
      var self = this;
      var chkState = this.ui.check_all.is(":checked") ;
      self.ui.device_detail.find('input:checkbox').attr("checked",chkState);
     },

    //Add all the devices to left form
    addAllDevices: function(){
      var self = this;
      self.collection.each(function(model){
        self.update_asset(model);
      });
      self.emptyView = NoResultsView;
      self.collection.reset();
    },


    update_asset: function(model){
      var self = this;
      var asset_lists = self.model.get('asset_list') || [];
      asset_lists.push(model.get('asset_guid'));
      self.model.set("asset_list",asset_lists ); //update asset list to model
      self.app.vent.trigger('group:device:added', model);
    },

    // Populating group related device to list
    populateDevices: function(){
      var self = this;
      var id = this.ui.groups.val();
      self.clearSearchFields();
      if(id === 'Load existing group...')
      {
        self.collection.fetch().done(function(){
          self._clonedAndRemoveDevices();
        });
      }
      else{
        var model = GroupModel.findOrCreate({id: id});
        model.fetch({
          success: function(m) {
            self.collection.fetch({
              reset: true,
              data: {
                '__filter[for_group]': model.get("id")
              }
            }).done(function(){
              self._clonedAndRemoveDevices();
            });
          }
        });
      }
    },

    syncColumns: function(options){
      var self = this;
      // take away any existing forced width for recalc
      if (typeof options.header_row !== 'object') {
        options.header_row = $(options.header_row);
        options.results_first_row = $(options.results_first_row);
      }

      options.header_row
        .add(options.results_first_row.find('.sync_width'))
        .removeAttr('style')
        .closest('table')
        .each(function(){
          $(this).css('table-layout','fixed');
        });
      options.results_first_row.each(function(i){
        var width = $(this)
          .find('div.sync_width')
          .width();
        if (width > 0){
          options.header_row.eq(i)
            // .find('div.sync_width') not sure why this works commented
            .width(width);
          $(this).find('div.sync_width')
            .width(width);
          options.header_row.add(options.results_first_row).closest('table').each(function(){
            $(this).css('table-layout','fixed');
          });
        }
      });

      // Todo...
      // check to see if either table is larger than it's parent and remove cols
      // or add hidden ones if there's room
    },

    //If devices added to left form then it should 
    //removed from right form
    removeExistingDevice: function()
    {
      var self = this;
      var asset_lists = self.model.get('asset_list');
      _.each(asset_lists, function(elm){
        var model = self.collection.where({asset_guid: elm});
        self.collection.remove(model);
      });
      if (self.collection.models.length === 0) {
        self.emptyView = NoResultsView;
        self.collection.reset();
      }
    },

    _clonedAndRemoveDevices: function(){
      var self = this;
      self.cloned = self.collection.clone();
      self.removeExistingDevice();
      self.filteredData = new self.FilteredCollection(self.cloned);
    },

    doSort: function(e) {
      var self = this;
      var modelAttribute = $(e.currentTarget).data('sortby');
      (self.sort_dir === "ASC") ? self.sort_dir = "DESC" : self.sort_dir = "ASC";
      self.collection.setSortField(modelAttribute, self.sort_dir);
      self.collection.sort();
      self.collection.trigger('reset');
      $(e.currentTarget).siblings().removeClass('asc desc');
      $(e.currentTarget).removeClass('asc desc').addClass(self.sort_dir.toLowerCase());
    },

    onRender: function(){
      var self = this;
      var groupOptionsView = new Marionette.CollectionView({
        el: this.ui.groups,
        itemView: OptionListView,
        collection: this.groups
      });
    },

    onShow: function(){
      var self = this;
    }
    
  });

  return newGroupView;
});