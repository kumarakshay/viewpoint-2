define([
  'underscore',
  'backbone.marionette',
  'hbs!../templates/index',
  'hbs!../templates/index_row',
  '../collections/groups'
], function(
  _,
  Marionette,
  indexTmpl,
  indexRowTmpl,
  GroupCollection
) {
  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: indexRowTmpl,
    initialize: function() {
     var self = this;
     this.app = require('application');
    },
    events: {
      "click a.deleteBtn": "DeleteBtnClick"
    },
    DeleteBtnClick: function () {
    var self = this;
    var ok = confirm("Are you sure you want to delete this group?");
    if (ok)
      {
        self.model.destroy({
          success: function (model, response) {
            self.app.vent.trigger('app:notification',{
              message: 'Group was deleted successfully'
            });
            self.app.vent.trigger('app:group_delete');
          },
          error: function (model, response) {
            self.app.vent.trigger('app:notification',{
              message: 'Cannot delete this group as it is associated to a subscription'
            });
          },
          wait: true
        });
      }
    }
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="4" style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="4" style="text-align: center;">No Results</td>')
  });

  return Marionette.CompositeView.extend({
    template:  indexTmpl,
    emptyView: LoadingView,
    itemView: RowView,
    itemViewContainer: '#group_index tbody#group_results',
    initialize: function(options) {
      var self = this;
      this.collection = new GroupCollection();
      self.sort_dir = "ASC";
      this.app = options.app;
      self.collection.fetch({reset: true}).done(function(){
        self.copycollection = self.collection.clone();
        self.filteredData = new self.FilteredCollection(self.copycollection);
      });

      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        }
      });
      self.collection.listenTo(self.app.vent,'app:group_delete',function() {
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        }
      });

    },

    ui: {
      group: 'input[name="group_name"]',
      sort: 'th.sort'
    },

    events: {
      'click @ui.sort': 'doSort',
      'keyup @ui.group' : "applyFilter"
    },

    FilteredCollection: function(original){
      var self = this;
      var filtered = new original.constructor();
      filtered.add(original.models);
      // allow this object to have it's own events
      filtered._callbacks = {};
      // call 'where' on the original function so that
      // filtering will happen from the complete collection
      filtered.where = function(criteria){
        // var items;
        // call 'where' if we have criteria
        // or just get all the models if we don't
        criteria = _(criteria).reduce(function(a,v,k){
               if(v){ a[k]=v; }
               return a;
          },{});

        if(criteria && !_.isEmpty(criteria)){
          self.items = _.filter(original.models, function(mod){
            var match = false;
            for(var prop in criteria){
              var mod_prop = mod.get(prop) ? mod.get(prop) : '';
              match = (mod_prop.toLowerCase().indexOf(criteria[prop].toLowerCase()) > -1);
              if (!match){
                break;
              }
            }
            if(match){
              return mod;
            }
          });
        }else {
          self.items = original.models;
        }
        filtered._currentCriteria = criteria;
        // reset the filtered collection with the new items
        filtered.reset(self.items);
        return filtered;
      };
    
      // when the original collection is reset,
      // the filtered collection will re-filter itself
      // and end up with the new filtered result set
      original.on("reset", function(){
          filtered.where(filtered._currentCriteria);
      });
      return filtered;
    },

    applyFilter: function(e){
      var self = this;
      var name = this.ui.group.val();
      
      var filterCollection = self.filteredData.where({name: name});
      
      self.collection.reset(filterCollection.models);
      if (filterCollection.models.length === 0) {
        self.emptyView = NoResultsView;
        self.collection.reset();
      }
    },

    // searchGroups:  function(e){
    //   var self = this;
    //   var searchTerm = this.ui.group.val().toLowerCase();
    //   var items;
    //   if (searchTerm) {
    //     items = _.filter(self.copycollection.models, function(mod){
    //       if((mod.get('name').toLowerCase()).indexOf(searchTerm) > -1){
    //        return mod;
    //       }
    //     });
    //   }
    //   else {
    //     items = self.copycollection.models; 
    //   }
    //   if (items.length === 0) {
    //     self.emptyView = NoResultsView;
    //   }
    //   self.collection.reset(items);
    // },

    doSort: function(e) {
      var self = this;
      var modelAttribute = $(e.currentTarget).data('sortby');
      (self.sort_dir === "ASC") ? self.sort_dir = "DESC" : self.sort_dir = "ASC";
      self.collection.setSortField(modelAttribute, self.sort_dir);
      self.collection.sort();
      self.collection.trigger('reset');
      $(e.currentTarget).siblings().removeClass('asc desc');
      $(e.currentTarget).removeClass('asc desc').addClass(self.sort_dir.toLowerCase());
    },
  });
});

