define([
    'underscore',
    'backbone.marionette'
    ],
      function(
       _,
      marionette
    ) {
  return {
    validation_error: function(model, response){
    var self = this;
    if (response.status >= 400 && response.status < 500)
    {
      var statusMessage = JSON.parse(response.responseText);
      var va_errors = statusMessage.result.errors;
      var message;
      $.each(va_errors, function(key, value){
        if (key == 'msg')
        {
          message = value;
        }
        else{
          message = key + " "+ value;
        }
        model.errors.push({name:key , message: message });
      });
      self.showErrors(model.errors);
    }
  },

  showErrors: function(errors) {
  var self = this;
  self.hideErrors();
  _.each(errors, function (error) {
      var error_block = $('.notice');
      error_block.append('<p>' + error.message + '</p>');
      error_block.slideDown('fast');
    }, this);
  },

  hideErrors: function() {
    $('.notice').text('').slideUp('fast');
  },
  };
});