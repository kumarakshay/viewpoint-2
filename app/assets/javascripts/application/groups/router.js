define([
       'backbone.marionette'
], function(
  Marionette,
  AppController
) {
  return Marionette.AppRouter.extend({
    
    appRoutes: {
      "groups/new": "new_group",
      "groups/:id/edit": "edit_group",
      "groups/:id": "show_group",      
      "groups": "index"
    },
  });
});
