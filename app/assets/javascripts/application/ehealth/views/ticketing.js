define([
  'backbone',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/ticketing',
  'hbs!../templates/ticketing_row'
], function(
  Backbone,
  _,
  Marionette,
  TicketingTmpl,
  TicketingRowTmpl
) {
  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: TicketingRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td colspan="100" style="text-align: center;">No Results Found</td>')
  });

  var TicketingView =  Marionette.CompositeView.extend({
    template: TicketingTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.ticketing_table tbody',

    initialize: function(){
      var self = this;
      this.collection.fetch().done(function(){
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        }
      });
      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        }
      });
    },

    onShow: function(){
    },

  });

  return TicketingView;
});