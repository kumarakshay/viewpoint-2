define([
	 'sgtools/rails/ui/models/base'
	],
	function(BaseModel){
		var BackupModel = BaseModel.extend({
			urlRoot: "/api/ehealth/backups",
		})
		return BackupModel
	})