define([
   'sgtools/rails/ui/models/base'
  ],
  function(BaseModel){
    var TicketingModel = BaseModel.extend({
      urlRoot: "/api/ticketings",
      toJSON: function(){
        // get the standard json for the object
        var data = Backbone.Model.prototype.toJSON.apply(this, arguments);

        // check for specific types and change to --
        if (data.type === 'incidents' || data.type ==='requests') {
          data.future = '--';
        }

        if (data.type === 'Maintenance') {
          data = null;
        }
        return data;
      }

    });
    return TicketingModel;
  });