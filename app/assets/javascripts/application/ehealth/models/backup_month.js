define([
  'sgtools/rails/ui/models/base'
], function(BaseModel){
  var BackupMonthModel = BaseModel.extend({
   urlRoot: "/api/ehealth/backup_months",
  });
  // Return the model for the module
  return BackupMonthModel;
});

