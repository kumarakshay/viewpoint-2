define([
  'sgtools/rails/ui/models/base'
  ],
  function(BaseModel){
    var ActiveAssetModel = BaseModel.extend({
      urlRoot: "/api/active_assets",

      calculate_percentage: function(p){
        var percentage = parseFloat(p.replace('%',''),10) * 100;
        if (percentage < 100) {
          percentage = percentage.toFixed(6);
        } else {
          percentage = 100;
        }
        return percentage;
      },

      map_status_number: function(s){
        var status_numbers = ["Healthy","Warning","Error","Critical"],value;
        value = status_numbers.indexOf(s);
        return value >= 0? value : 100;
      },
      
      parse: function(data){
        data.status_number = this.map_status_number(data.status);
        data.avail = this.calculate_percentage(data.avail);
        return data;
      },

    
    });
    return ActiveAssetModel;
  });