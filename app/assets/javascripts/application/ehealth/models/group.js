define([
  'sgtools/rails/ui/models/base'
], function(BaseModel){
  var GroupModel = BaseModel.extend({
    parse: function(data) {
      data.api = "group_active_assets";
      return data;
    }
  });
  // Return the model for the module
  return GroupModel;
});

