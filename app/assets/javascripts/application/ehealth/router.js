define([
       'backbone.marionette'
], function(
  Marionette,
  AppController
) {
  return Marionette.AppRouter.extend({
    
    appRoutes: {
      "ehealth/:id": "show",
      "ehealth": "index"
    },
  });
});