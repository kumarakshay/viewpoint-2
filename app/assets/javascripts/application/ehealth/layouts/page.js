define([
       'backbone.marionette',
       'hbs!../templates/main_page'
], function(Marionette, mainTemplate) {
  var MainLayout =  Marionette.Layout.extend({
    template: mainTemplate,

    regions: {
      environment_health: "#env_dash_health",
      events: '#env_dash_events',
      ticketing: "#env_dash_ticketing",
      backups: "#env_dash_backups",
      assets: "#env_dash_assets .assets_view_contatiner",
      env_dash : '#env_dash',
      grid_col_1 : '.grid_col_1',
      grid_col_2 : '.grid_col_2'
    },

    resize_modules: function(options){
      var self = this;

      if (typeof options !== 'object') {
        if (typeof options === 'function') {
          options = {
            callback : options
          };
        }
        options.regions = self.regions;
      } else {
        options.regions = options.data;
      }

      var size = {
        minimum_column_width : 265,
        margin_width : 20,
        border_width : 2,
        window_width : $(window).width(),
        available_width : $(options.regions.env_dash).width()-3,
        grid_col_width : '',
        grid_col_2_width : '',
        events_width : '',
        half_module_width : '',
        env_dash_width: $(options.regions.env_dash).width(),
        env_dash_margin_width : 50
      };

      if (size.window_width > 1052) { //full mode
        size.grid_col_2_width = size.available_width - size.margin_width - size.minimum_column_width;
        // if width including margins is more than window width, remove another margin from grid_col_2.
        if ((size.env_dash_width + size.env_dash_margin_width) >= size.window_width) {
          size.grid_col_2_width = size.grid_col_2_width - size.margin_width;
        }
        size.events_width = size.grid_col_2_width - size.margin_width - size.minimum_column_width - size.border_width - 3;

        $(options.regions.grid_col_1)
          .width(size.minimum_column_width);
        $(options.regions.grid_col_2)
          .width(size.grid_col_2_width);

        $(options.regions.assets)
          .width(size.grid_col_2_width-size.border_width);
        $(options.regions.events)
          .width(size.events_width);
        $(options.regions.environment_health)
          .add($(options.regions.backups))
          .add($(options.regions.ticketing))
            .width(size.minimum_column_width-size.border_width);
      
      } else if (size.window_width > 680){ //tablet mode

        size.grid_col_width = size.available_width;
        size.half_module_width = Math.floor( (size.available_width - (size.border_width*2) - size.margin_width) / 2);

        $(options.regions.grid_col_1).width(size.grid_col_width);
        $(options.regions.grid_col_2).width(size.grid_col_width);

        $(options.regions.assets)
          .add($(options.regions.events))
          .add($(options.regions.ticketing))
            .width(size.grid_col_width-size.border_width);

        $(options.regions.backups)
          .add($(options.regions.environment_health))
            .width(size.half_module_width);

      } else { //mobile mode

        size.grid_col_width = size.available_width >= size.minimum_column_width ? size.available_width : size.minimum_column_width; //minimum width

        $(options.regions.grid_col_1).width(size.grid_col_width);
        $(options.regions.grid_col_2).width(size.grid_col_width);

        $(options.regions.environment_health)
          .add($(options.regions.backups))
          .add($(options.regions.ticketing))
          .add($(options.regions.events))
          .add($(options.regions.assets))
            .width(size.grid_col_width - size.border_width);
      }

      if (typeof options.callback === 'function'){
        options.callback();
      }
    },

    onShow : function(view) {
      var self = this;

      var f = function(){
        $(window).on("resize.ehealth", self.regions, self.resize_modules);
      };
      self.resize_modules(f);
      setTimeout(function(){
        self.resize_modules(f);
      },100);
    },

    onClose : function(view) {
      $(window).off(".ehealth");
    }

  });

  return MainLayout;
});