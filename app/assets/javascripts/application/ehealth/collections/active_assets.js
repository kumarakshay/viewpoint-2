define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/active_asset'
], function(_, BaseCollection, ActiveAssetModel){
  var ActiveAssetCollection = BaseCollection.extend({
    model: ActiveAssetModel,
    url: '/api/ehealth/active_assets'
  });
  // You don't usually return a collection instantiated
  return ActiveAssetCollection;
});
