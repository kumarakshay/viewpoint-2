define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/ticketing'
], function(_, BaseCollection, TicketingModel){
  var TicketingCollection = BaseCollection.extend({
    model: TicketingModel,
    url: '/api/ehealth/ticketings'
  });
  // You don't usually return a collection instantiated
  return TicketingCollection;
});
