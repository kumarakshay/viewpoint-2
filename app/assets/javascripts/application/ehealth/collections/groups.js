define([
  'sgtools/rails/ui/collections/base',
  '../models/group'
], function(
  BaseCollection,
  GroupModel
){

  var GroupCollection = BaseCollection.extend({
    model: GroupModel,
    url: '/api/groups',

    initialize: function () {
      // Default sort field and direction
      this.sortField = "name";
      this.sortDirection = "ASC";
    }

  });
  // You don't usually return a collection instantiated
  return GroupCollection;
});
