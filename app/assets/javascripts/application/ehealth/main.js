define([
  'domReady',
  'sg/jquery',
  'sg/util',
  'backbone.marionette',
  './controllers/ehealth',
  './router',
  './layouts/page',
  './views/tools',
  // './views/unauthorized',
  // './models/current_user',
  'hbs!./templates/layout',
  'application/page',
  'jquery.flot',
  'jquery.flot.pie',
  'jquery.flot.time'
],

function(
  domReady,
  $,
  util,
  Marionette,
  AppController,
  AppRouter,
  PageLayout,
  ToolsView,
  // UnauthorizedView,
  // CurrentUserModel,
  layoutTemplate,
  vp_page
) {

  var layout = new Marionette.Layout({
    template: layoutTemplate,
  });
  layout.addRegions({
    ehealth: "#ehealth",
  });

  toolsView = new ToolsView({});
  // unauthorizedView = new UnauthorizedView({})
  // var current_user = new CurrentUserModel();
  return Marionette.Module.extend({

    initialize: function(options, moduleName, app) {
      var self = this;

      var pageLayout = new PageLayout();
      var router = new AppRouter({
        controller: new AppController({
          app: app,
          module: this,
          router: router,
          moduleLayout: layout,
          pageLayout: pageLayout
        })
      });

      router.on('route', function() {
        if (layout._firstRender || layout.isClosed) {
          //Set the selected page as ticket_landing
          window.C.page = "ms-ehealth";
          app.getRegion('subtools').show(toolsView);
          // current_user = current_user.fetch({'async':false}).done(function(data){
          //   is_sungard =  data.result.current_user.is_sungard
            // if(is_sungard == true)
            // {
            //   app.getRegion('main').show(unauthorizedView)
            // }
            // else
            // {
             app.getRegion('main').show(layout); 
            // }
          // })


          $('#ehealth_help').click(function(){
            vp_page.helps_ajax('ehealth');
           });

          // EnvDashboard.init();
          // layout.ehealthDetail.show(pageLayout);
        }
      });
    },



    onShow: function(options) {
    },

  });

});