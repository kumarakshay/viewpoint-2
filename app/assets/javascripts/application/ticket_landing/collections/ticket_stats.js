define([
  'underscore',
  'sgtools/rails/ui/collections/base',
  '../models/ticket_stat'
], function(_, BaseCollection, TicketingModel){
  var TicketStatesCollection = BaseCollection.extend({
    model: TicketingModel,
    url: '/api/ticket_landing/ticket_stats'
  });
  // You don't usually return a collection instantiated
  return TicketStatesCollection;
});
