define([
    'domReady',
    'sg/jquery',
    'sg/util',
    'backbone.marionette',
    './controllers/ticket_landing',
    './router',
    './layouts/page',
    'hbs!./templates/layout',
    'jquery.flot',
    'jquery.flot.pie',
    'jquery.flot.time'
  ],
  function(
    domReady,
    $,
    util,
    Marionette,
    AppController,
    AppRouter,
    PageLayout,
    layoutTemplate
  ) {

    var layout = new Marionette.Layout({
      template: layoutTemplate,
    });
    layout.addRegions({
      ticket_landing: "#ticket_landing",
    });

    return Marionette.Module.extend({

      initialize: function(options, moduleName, app) {
        var self = this;

        var pageLayout = new PageLayout();
        var router = new AppRouter({
          controller: new AppController({
            app: app,
            module: this,
            router: router,
            moduleLayout: layout,
            pageLayout: pageLayout
          })
        });

        router.on('route', function() {
          //Set the selected page as ticket_landing
          window.C.page = "ms-ticket-landing";

          if (layout._firstRender || layout.isClosed) {
            app.getRegion('main').show(layout);
          }
        });
      },
      onShow: function(options) {},

    });

  });