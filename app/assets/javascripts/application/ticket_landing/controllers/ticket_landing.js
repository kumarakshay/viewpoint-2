define([
       'underscore',
       'underscore.string',
       'backbone',
       'backbone.marionette',
        '../views/quick_link',
        '../views/sn_link',
        '../views/ticket_stats',
        '../views/recent_asset',
        '../views/tickets',
        '../views/asset_health',
        '../collections/tickets',
        '../collections/ticket_stats',
        '../collections/environment_healths',
        '../collections/recent_assets',
        '../models/current_user',
        '../views/tools',
        '../views/top_buttons'
], function(
  _,
  _s,
  Backbone,
  Marionette,
  QuickLinkView,
  SnLinkView,
  TicketStatsView,
  RecentAssetView,
  TicketsView,
  AssetHealthView,
  TicketCollection,
  TicketStatesCollection,
  EnvironmentHealthCollection,
  RecentAssetCollection,
  CurrentUserModel,
  ToolsView,
  TopButtons
) {
  return Marionette.Controller.extend({

    initialize: function(options) {
      var self = this;
      this.app = options.app;
      this.moduleLayout = options.moduleLayout;
      this.pageLayout = options.pageLayout;
    },

    my_board: function() {
      window.location = '/my_board'; //redirect to new landing page
    },

    index: function() {
      var self = this;
      this.moduleLayout.on('show', function () {
        var top_buttons, toolsView, quick_link;
        var current_user = new CurrentUserModel();
        var omi_user;
        var environment_health_collection = new EnvironmentHealthCollection();
        var ticket_stats_collection = new TicketStatesCollection();
        var ticket_collection = new TicketCollection();
        var recent_asset_collection = new RecentAssetCollection()

        var ticket_stats = new TicketStatsView({
          collection: ticket_stats_collection
        }); 

        var tickets_view;
        
        var asset_health_view = new AssetHealthView({
          collection: environment_health_collection
        });

        var recent_asset = new RecentAssetView({
          collection: recent_asset_collection
        });

        var LoadingView = Backbone.Marionette.ItemView.extend({
          tagName: "tr",
          template: _.template('<td colspan="100" style="text-align: center;">Loading...</td>')
        });

        current_user = current_user.fetch({'async':false}).done(function(data){

          omi_user =  data.result.current_user.is_omi ;
              toolsView = new ToolsView({'optName': data.result.current_user.first_name || data.result.current_user.email});
            self.app.getRegion('subtools').show(toolsView);
            sn_link = new SnLinkView({
              'grant_access': data.result.current_user.sn_roles.grant_access,
              'sn_URL': data.result.current_user.sn_URL,
            }); 

           top_buttons = new TopButtons({
              'notification': data.result.current_user.sn_roles.notification,
              'authorization': data.result.current_user.sn_roles.authorization,
              'sn_URL': data.result.current_user.sn_URL,
              'grant_access': data.result.current_user.sn_roles.grant_access,
              'company_guid': data.result.current_user.company_guid
            });

            tickets_view = new TicketsView({
              collection: ticket_collection,
              'sn_URL': data.result.current_user.sn_URL
            });

            quick_link = new QuickLinkView({
              'dns_link': data.result.current_user.abilities && data.result.current_user.abilities.can_read_DNS,
              'user_guide_link': data.result.branding  === 'allstream' ?  '/user_guide_allstream' : '/user_guide'
            });

            self.moduleLayout.ticket_landing.show(self.pageLayout);
            self.pageLayout.top_buttons.show(top_buttons);
            // self.pageLayout.sn_link.show(sn_link);
            self.pageLayout.quick_link.show(quick_link);
            self.pageLayout.ticket_stats.show(ticket_stats);
            self.pageLayout.asset_health.show(asset_health_view);
            self.pageLayout.recent_assets.show(recent_asset);
            self.pageLayout.tickets.show(tickets_view);

            tickets_view.listenTo(ticket_stats, "radioChange", function(type){
              tickets_view.emptyView = LoadingView;
              ticket_collection.url = "/api/ticket_landing/tickets?type=" + type;
              ticket_collection.reset();
              ticket_collection.fetch();
            });

            environment_health_collection.listenTo(asset_health_view, "choiceChange", function(){
              asset_health_view.emptyView = LoadingView;
              environment_health_collection.url = asset_health_view.urlRoot;
              environment_health_collection.reset();
              environment_health_collection.fetch();
            });
        });

      })
    }
  });

});