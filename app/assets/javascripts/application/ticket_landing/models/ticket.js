define([
  'sgtools/rails/ui/models/base'
  ],
  function(BaseModel){
    var TicketModel = BaseModel.extend({
      urlRoot: "/api/ticket_landing/tickets",
    });
    return TicketModel;
  });
