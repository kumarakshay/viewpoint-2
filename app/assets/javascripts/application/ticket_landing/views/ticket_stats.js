define([
  'backbone',
  'jquery',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/ticket_stats',
  'hbs!../templates/ticket_stats_row'
], function(
  Backbone,
  $,
  _,
  Marionette,
  TicketingTmpl,
  TicketingRowTmpl
) {
  var RowView = Marionette.ItemView.extend({
    tagName: 'li',
    template: TicketingRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "li",
    template: _.template('<div style="text-align: center;">Loading...</div>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "li",
    className: "no_results",
    template: _.template('<div style="text-align: center;">No Results Found</div>')
  });

  var TicketStatsView =  Marionette.CompositeView.extend({
    template: TicketingTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'ul.radio-list',
    ui: {
      radio: 'input[name=ticket-stats-radio]'
    },

    onShow: function () {
      var self = this;
      self.collection.fetch().done(function(){
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        } else {
          var selected_radio = $('input[name=ticket-stats-radio]');
          if(selected_radio[0]) {
            selected_radio[0].checked = true;
            self.plot_chart();
          }
        }
      });
    },

    initialize: function(){
      var self = this;
      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        }
      });
    },

    events: {
      'change @ui.radio': 'triggerTicketChange'
    },

    triggerTicketChange: function () {
      var selected_radio = $('input[name=ticket-stats-radio]:checked');
      var selectedItem = selected_radio.attr('data-name').split(' ')[0].toLowerCase();
      this.trigger('radioChange', selectedItem);
      this.plot_chart();
    },

    plot_chart: function() {
      var ticket_stats_chart;
      var selected_radio = $('input[name=ticket-stats-radio]:checked');
      var selected_label = selected_radio.parent().children('label');
      var number_open = parseFloat(selected_label.find('.open').text());
      var number_closed = parseFloat(selected_label.find('.closed').text());
      var number_total = parseFloat(selected_label.find('.total').text());
      var data = [
          { data: [[1,number_open]], color: '#5291AE'},
          { data: [[1,number_closed]], color: '#379E5F'}
      ];
      ticket_stats_chart = $.plot('#ticket-stats-chart',  data, {
          series: {
              pie: {
                  innerRadius: 0.82,
                  show: true,
                  stroke: {width: 1}
              }
          }
      }).draw();

      var info_p = $('.ticket-stats .chart-container p');
      info_p.children('span:first-child').text(number_total);
      info_p.children('span:last-child').text('Total ' + selected_radio.attr('data-name'));
    },
  });
  return TicketStatsView;
});