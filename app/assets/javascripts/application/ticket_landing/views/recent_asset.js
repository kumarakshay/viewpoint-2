define([
  'backbone',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/recent_asset',
  'hbs!../templates/recent_asset_row'
], function(
  Backbone,
  _,
  Marionette,
  RecentAssetTmpl,
  RecentAssetRowTmpl
) {

  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: RecentAssetRowTmpl,
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center; font-weight: normal;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td colspan="100" style="text-align: center; font-weight: normal;">No Results Found</td>')
  });

  var RecentAssetView =  Marionette.CompositeView.extend({
    template:  RecentAssetTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.ticketing_table tbody',

     initialize: function(){
      var self = this;
      this.collection.fetch().done(function(){
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        } else {

            // Homepage mobile controlled show
          $('.recent-assets').find('.controlled-show .inner tbody').children('tr').addClass('hidden');

          for (i = 1; i < 9; i++) {
              $('.recent-assets').find('.controlled-show .inner tbody').children('tr:nth-child(' + i + ')').removeClass('hidden');
          }
          $('.show-buttons').find('button').click(function () {
              if ($(this).closest('.recent-assets').length) {
                  var more = $(this).hasClass('more');
                  var less = $(this).hasClass('less');

                  var table = $(this).closest('.box').find('.controlled-show .inner tbody');
                  var tr_count;
                  var tr_hidden_count;
                  if (more) {
                      tr_count = table.children('tr').not('.hidden').length;
                      if (tr_count <= 8) {
                        $(this).hide().closest('.box').find('.less').hide();
                      } else {
                        table.find('.hidden').slice(0, 9).removeClass('hidden');
                        $(this).addClass('half').closest('.box').find('.less').show();
                        tr_hidden_count = table.children('tr.hidden').length;
                        if (tr_hidden_count == 0) {
                            $(this).hide().closest('.box').find('.less').removeClass('half');
                        }
                      }
                  }
                  else if (less) {
                      tr_count = table.children('tr').not('.hidden').length;

                      if (tr_count > 8) {
                          if (tr_count > 16) {
                              table.find('tr').not('.hidden').slice(-9).addClass('hidden');
                          }
                          else {
                              table.find('tr').not('.hidden').slice(-(tr_count - 8)).addClass('hidden');
                          }
                      }

                      tr_hidden_count = table.children('tr.hidden').length;
                      if (tr_hidden_count != 0) {
                          $(this).addClass('half').closest('.box').find('.more').show().addClass('half');
                      }
                      tr_count = table.children('tr').not('.hidden').length;
                      if (tr_count <= 8) {
                          $(this).hide().closest('.box').find('.more').removeClass('half');
                      }
                  }
              }
          }).click();
        }
      });
    },

    onShow: function(){
    },
  });

  return RecentAssetView;
});