define([
  'backbone',
  'jquery',
  'underscore',
  'backbone.marionette',
  'hbs!../templates/tickets',
  'hbs!../templates/tickets_row'
], function(
  Backbone,
  $,
  _,
  Marionette,
  TicketsTmpl,
  TicketsRowTmpl
) {

  var RowView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: TicketsRowTmpl,
    serializeData: function() {
      var data = Backbone.Marionette.ItemView.prototype.serializeData.apply(this, arguments);
      data.sn_URL = this.options.sn_URL;
      return data;
    }
  });

  var LoadingView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    template: _.template('<td colspan="100" style="text-align: center;">Loading...</td>')
  });

  var NoResultsView = Backbone.Marionette.ItemView.extend({
    tagName: "tr",
    className: "no_results",
    template: _.template('<td colspan="100" style="text-align: center;">No Results Found</td>')
  });

  var TicketsView =  Marionette.CompositeView.extend({
    template: TicketsTmpl,
    itemView: RowView,
    emptyView: LoadingView,
    itemViewContainer: 'table.active_asset_tables tbody',
    itemViewOptions: function(model, index) {
      return {
        sn_URL: this.sn_URL
      }
    },

    initialize: function(options){
      var self = this;
      self.sn_URL = options.sn_URL;
      this.collection.fetch().done(function(){
        if (self.collection.length === 0) {
          self.emptyView = NoResultsView;
          self.collection.reset();
        } else {
          self.cloned = self.collection.clone();
          self.filteredData = self.cloned.RefineCollection();
        }
      });
      this.listenTo(this.collection,'sync', function(c) {
        if (c.length === 0) {
          self.emptyView = NoResultsView;
          c.reset();
        } else {
          self.cloned = self.collection.clone();
          self.filteredData = self.cloned.RefineCollection();
        } 
      });
    },

    ui: {
      ticket_number: 'input[name="ticket_number"]',
      state: 'input[name="state"]',
      summary: 'input[name="summary"]',
      created: 'input[name="created"]',
      updated: 'input[name="updated"]',
      text_search_filter: '.text_search_filter',
      sort: '.sort',
      group: 'select[name="group_choices"]',
      group_optgroup: 'optgroup.groups',
      type_optgroup: 'optgroup.types'
    },

    events: {
      'keyup @ui.text_search_filter': 'applyFilter',
      'click @ui.sort': 'doSort',
    },

    applyFilter: function(e){
      var self = this;
      var ticket_number = this.ui.ticket_number.val();
      var state = this.ui.state.val();
      var summary = this.ui.summary.val();
      var created = this.ui.created.val();
      var updated = this.ui.updated.val();
      var filterCollection = self.filteredData.where({number: ticket_number, state: state,
        summary: summary, created_at: created, updated_at: updated });
      self.collection.reset(filterCollection.models);
      if (filterCollection.models.length === 0) {
        self.emptyView = NoResultsView;
        self.collection.reset();
      }
      
    },

    doSort: function(e) {
      var self = this;
      var modelAttribute = $(e.currentTarget).data('sortby');
      (self.sort_dir === "ASC") ? self.sort_dir = "DESC" : self.sort_dir = "ASC";
      self.collection.setSortField(modelAttribute, self.sort_dir);
      self.collection.sort();
      self.collection.trigger('reset');
      $(e.currentTarget).siblings().removeClass('asc desc');
      $(e.currentTarget).removeClass('asc desc').addClass(self.sort_dir.toLowerCase());
    },
  });

  return TicketsView;
});
  
