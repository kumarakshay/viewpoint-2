define([
       'backbone.marionette'
], function(
  Marionette,
  AppController
) {
  return Marionette.AppRouter.extend({
    appRoutes: {
     "ticket_landing": "my_board"
    },
  });
});