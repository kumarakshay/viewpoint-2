define([
    'backbone.marionette',
    'sgtools/rails/splunk/search'
], function (
    Marionette,
    page
) {
    return Marionette.Module.extend({
      startWithParent: false,
      onStart: function(options) {
        page.init(options);
      }
    });
});
