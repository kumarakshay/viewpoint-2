define([
     'backbone.marionette',
     '../reports_page'
], function (
      Marionette,
      page
  ) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    },
    onStart: function(options) {
      if(options && options.bus_process){
        page.init(options.bus_process);
      }
      else{
        page.init("false");
      }
    }
  });
});
