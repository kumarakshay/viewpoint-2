define([
  'sg/jquery',
  'backbone.marionette',
  '../interfaces_page'
], function (
  $,
  Marionette,
  page
) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
      page.init();
    },
  });
});