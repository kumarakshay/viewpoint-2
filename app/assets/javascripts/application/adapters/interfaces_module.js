define([
  'sg/jquery',
  'backbone.marionette',
  '../beta/interfaces_page'
], function (
  $,
  Marionette,
  page
) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    },
    onStart: function(options) {
      page.init();
    }
  });
});