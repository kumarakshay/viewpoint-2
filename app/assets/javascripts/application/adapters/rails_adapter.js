// this file is used for any view that only uses rails server
// generate and serve data and only needs the basic page js
// if any custom js is created:
// - copy this file
// - refer to it and give it a name in application.js
// - add any init js into the view's return require function

define([
  'backbone.marionette',
  'jquery',
  'sg/util'
], function (
  Marionette,
  $,
  sg_util
) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    //This function gets called when user clicks on Command Runner icon
    //We are setting the stored_url in the cookie for rediecting properly after 2f authentication 
      $('.cmd-modal-link').click(function(){
          var device_id = $(this).attr('rel');
          document.cookie="stored_url=index.html#\/cr-show\/" + device_id;
          window.location = 'ui/2f/index.html#/cr-show/' + device_id; 
       })
    },

    onStart: function(options) {
      $('document').ready(function(){
        sg_util.apply_datatable($('table.delay_apply_datatable').fadeIn('slow'));
      });
    }
  });
});