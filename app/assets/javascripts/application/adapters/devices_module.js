define([
       'backbone.marionette',
       '../beta/devices_page'
], function (Marionette, page) {
  return Marionette.Module.extend({
    startWithParent: false,
    initialize: function(options) {
    },
    onStart: function(options) {
      $('div.tools').append('<h2>devices</h2>');
      page.init("<%=APP_CONFIG['stub_webservice']%>");
    }
  });
});