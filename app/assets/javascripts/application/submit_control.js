 define(['domReady', 'sg/jquery'], function(domReady, $) {

  return {

    ack_check: function(){

      domReady(function() {
        var $ack =  $('td.acknoweledge input'),
        $controlled = $('input.controlled');

        $ack.eq(0).prop('checked',false);
        // $ack.eq(1).prop('checked',false);

        $ack.each(function(){
          $(this).click(function(){
            // if ($ack.eq(0).prop('checked') && $ack.eq(1).prop('checked')) {
            if ( $ack.eq(0).prop('checked') ) {
              $controlled.removeAttr('disabled');
            } else {
              $controlled.attr('disabled','disabled');
            }
          });
        });

        // $controlled.hover(
        //   function() {
        //     $ack.css('background','red');
        //   }, function() {
        //     $ack.css('background','none');
        //   });

      });
    }

  };

});