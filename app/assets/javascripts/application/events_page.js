define([
  'sg/jquery',
  'domReady',
  'sg/util',
  'zenoss_events'
], function(
  $,
  domReady,
  sg_util,
  sg_events
) {

  return {

    init: function() {

      var $event_ajax_request;

      domReady(function() {
        //Set the selected page as events
        window.C.page = "ms-events";

        $('#first_seen_list li').each(function(){
          $(this).click(function(){
            sg_events.set_event_date($('#first_start_date_div'),$('#first_end_date_div'),$(this));
          });
        });

        $('#last_seen_list li').each(function(){
          $(this).click(function(){
            sg_events.set_event_date($('#last_start_date_div'),$('#last_end_date_div'),$(this));
          });
        });

        // event table scroll indicator
        $('#events .findWidth').scroll(function(){
          sg_events.event_scroll_indicator($(this));
        });

        // event page buttons bottom
        $('#events b.bottom').css({'bottom':sg_util.scrollbarWidth()+'px'});


        $('body.device #event_types').fadeOut(0);

        // event type buttons
        $('#event_types').find('a').each(function(){
          $(this).click(function(){
            sg_events.event_types($(this));
          });
        });

        // hide dropdowns on scroll
        $('.findWidth').scroll(function(){
          $("#prod_state,#event_severity,#event_state").multiselect("close");
        });

        // datepicker function
        $('#first_seen,#last_seen').each(function(){
          $(this).siblings('div.date_drop_down').hide();
          sg_events.event_date($(this));
        });

        $('#events_form input[type="submit"]').click(function(){
          $('#events input.page').val('1');
          $('div.date_drop_down').hide();
          $('div.overlayClear').remove();
        });

        $('#first_seen_list li[rel="null,null"], #last_seen_list li[rel="null,null"]').each(function(){
          $(this).click(function(){
            // remove active from date pickers
            // $('a.ui-state-active').removeClass('ui-state-active'); // datepicker overrides this
            // remove values from form inputs
            $('.first_seen, .first_seen_append, .last_seen, .last_seen_append, #first_time, #last_time').val('');
            // goto page 1
            $('#events input.page').val('1');
            $('div.date_drop_down').hide();
            $('div.overlayClear').remove();
          });
        });

        // Datepicker
        $(function() {
          $('#first_start_date_div').datepicker({
            // altFormat: "mm/dd/yy",
            altField: ".first_seen",
            changeMonth: true,
            dateFormat:"@",
            maxDate: '+0d',
            defaultDate: null,
          });
          $('#first_end_date_div').datepicker({
            altField: ".first_seen_append",
            changeMonth: true,
            dateFormat:"@",
            maxDate: '+0d',
            defaultDate: null,
          });
          $('#last_start_date_div').datepicker({
            // altFormat: "mm/dd/yy",
            altField: ".last_seen",
            changeMonth: true,
            dateFormat:"@",
            maxDate: '+0d',
            defaultDate: null,
          });
          $('#last_end_date_div').datepicker({
            altField: ".last_seen_append",
            changeMonth: true,
            dateFormat:"@",
            maxDate: '+0d',
            defaultDate: null,
          });
        });

        // Quick-fix: remove #ui-datepickder-div
        $('#ui-datepicker-div').remove();

        // Multi Selects
        $('#prod_state, #event_severity, #event_state').each(function(){
          $(this).multiselect({
            header:false,
            noneSelectedText:'--',
            minWidth:($(this).attr('size')*12),
            classes:$(this).attr('id'),
            height:'auto',
            selectedText: '#'
          });
        });


        // events submit
        $('#events_form').submit(function() {
          $class = $(this).attr('class');
          sg_events.events_submit($(this).removeClass('active').attr('class'));
          $(this).attr($class);
          return false;
        });

        // hide event types
        $('#device_detail_select li, h2.position a').not('.events_link').click(function(){
          $('#event_types').fadeOut();
        });

        // events input enter was pressed
        $('#events input, #events select, #events textarea').each(function(){
          // resetting all filter submits
          var code = null;
          $(this).keyup(function(e){
            e.preventDefault();
            code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
              $('#events input.page').val('1');
              $(this).closest('form').submit();
            }
          });
        });

        // company events
        $('#events_form.events_company').each(function(){
          sg_events.event_types($('#event_types a.events'));
        });
        // company events history
        $('#events_form.event_histories_company').each(function(){
          sg_events.event_types($('#event_types a.event_histories'));
        });

        // events tab clicked
        $('.events_link').click(function(){
          // clear form
          sg_events.event_form_clear();
          // show history toggle
          $('#event_types').fadeIn();
          // submit ajax
          $('#events_form').submit();
        });

        sg_events.event_type_click();
        
      });
    }
  };
});
