define([
    'domReady',
    'sg/jquery',
    'sg/page',
    'sg/util',
    './reports_page',
    './asset_select_deselect'
    ],
      function(
      domReady,
      $,
      sg_page,
      sg_util,
      vp_rep_page,
      vp_asset_select_deselect
    ) {
  return {
    init: function() {
      var self = this;
      vp_rep_page.init();
      vp_asset_select_deselect.init();

      $('#event_subscription_severity').val($('.subscription_severity').val());
      $('#event_subscription_severity').change(function(){
        $('.subscription_severity').val($('#event_subscription_severity').val());
      });

      $('.edit_event_subscription input[name=commit]').val('update event notification');
      $('.new_event_subscription input[name=commit]').val('create event notification');
      //Set the selected page as event subscriptions
      window.C.page = "ms-event-subscriptions";
    }
  };
});
