define([
  'underscore',
  '../models/app_notification',
  'sgtools/rails/ui/collections/base'
], function(
  _,
  AppNotification,
  BaseCollection
){
  NotificationCollection =  BaseCollection.extend({
    model: AppNotification
  });
  return NotificationCollection;
});
