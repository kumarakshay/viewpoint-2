define([
  'underscore',
  'backbone.marionette'
], function(
  _,
  Marionette
) {

  var itemView = Marionette.ItemView.extend({
    template: _.template('<%=message%>'),
    tagName: 'p',
    events: {
      'click': function(e) {
        this.close();
      }
    },
    onRender: function() {
      var self = this;
      self.$el
      .addClass(self
        .model
        .get('level')
        .get('name'));
      if (self.model.get('level').id > 5) {
        _.delay(_.bind(self.close,self),7000);
      }
    }
  });

  return Marionette.CompositeView.extend({
    template: _.template('<div class="messages"></div>'),
    itemViewContainer: 'div.messages',
    itemView: itemView,
    onRender: function() {
    },
    itemEvents: {
      render: function() {
        var outOfView = this.$el.height() + 50;
        if (!this.$el.hasClass('active')) {
          this.$el.css({
            top: -outOfView,
            display: 'block'
          })
          .addClass('active')
          .animate({top: 0},500);
        }
      },
      close: function(eName,view) {
        var self = this;
        var outOfView = this.$el.height() + 50;
        this.collection.remove(view.model);
        if (this.collection.length === 0) {
          this.$el
          .animate({top: -outOfView},500, function(){
            $(this).hide().removeClass('active');
          });
        }
      }
    }
  });
});
