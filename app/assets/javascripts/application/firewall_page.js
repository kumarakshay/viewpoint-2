define([
  'domReady',
  'sg/jquery',
  'sg/ajax',
  'sg/util',
  'backbone.marionette',
  '../livetable/main',
  '../livetable/builder/column_builder',
  '../livetable/mockup_json/firewall_columns_generator',
  '../livetable/mockup_json/firewall_logs_generator',
  './page'
  ], function(
      domReady,
      $,
      ajax,
      util,
      Marionette,
      LiveTable,
      columnBuilder,
      sampleColumns,
      sampleLogs,
      vp_page
  ) {

  return {

    render_logs: function(columns, logs, firewall_role){
      var self = this;
      var LogList = new Marionette.Application();
      LogList.addRegions ({
       page: "#section"
      });

      options = {
      moduleClass: LiveTable,
      columnObj: columns,
      dataObj: logs,
      rowCallbacks: {
        clickMe: function(args){alert(args);}}
      };

      row_link = {
        trCallbacks : {
          rowClick: function(args){
            window.location = '/2f/firewall_logs/' + args.asset_guid;
          }
        },
      };

      if(firewall_role == 'true'){
        options = $.extend(true, {}, row_link, options);
      }

      LogList.module("FirewallLogs", options);
      LogList.page.show(LogList.FirewallLogs.layout);
      LogList.start();
      LogList.FirewallLogs.configModel.set('limit', 100);
      $('#numResults option[value=100]').attr('selected','selected');
    },

    init: function(firewall_role) {
       var self = this;
       $('#section').css({'display':'block', 'top': '110px'});
       var request = ajax.call('/api/vp/devices?atype=Firewall');
       request.done(function($data){
        if($data.status != 200){
          alert('invalid req');
        } else {
          var logs, columns;
          if ($data.result.length > 0) {
            // var columns = sampleColumns();
            // var logs = sampleLogs(25);
               logs = $data;
               columns = columnBuilder.dummy_json_logs(
                [
                 {"modelAttribute" : 'name', 'title': "Name", "columnType":"text"},
                 {"modelAttribute" : 'ip_address_formatted', 'title': "IP Address", "columnType":"text"},
                 {"modelAttribute" : 'hw_model', 'title': "HW Model", "columnType":"text"},
                 {"modelAttribute" : 'location', 'title': "Location", "columnType":"text"},
                 {"modelAttribute" : 'os_model', 'title': "OS Model", "columnType":"text"},
                 {"modelAttribute" : 'production_state', 'title': "Production State", "columnType":"text"},
                ]
              );
            self.render_logs(columns,logs, firewall_role);
          } else {
            columns = columnBuilder.dummy_json_logs(
                [
                 {"modelAttribute" : 'name', 'title': "Name", "columnType":"text"},
                 {"modelAttribute" : 'ip_address_formatted', 'title': "IP Address", "columnType":"text"},
                 {"modelAttribute" : 'hw_model', 'title': "HW Model", "columnType":"text"},
                 {"modelAttribute" : 'location', 'title': "Location", "columnType":"text"},
                 {"modelAttribute" : 'os_model', 'title': "OS Model", "columnType":"text"},
                 {"modelAttribute" : 'production_state', 'title': "Production State", "columnType":"text"},
                ]
              );
             self.render_logs(columns,"No records found", firewall_role);
            // $('#section').html('<p> No records found</p>');
          }
        }
       });
       //Set the selected page as firewall 
       window.C.page = "ms-firewalls";
    }
    // END init

  };
});