define([
  'domReady',
  'sg/jquery',
  './asset_select_deselect'
], function(
  domReady,
  $,
  vp_asset_select_deselect
) {

  return {

    init: function() {

      var self = this;
      vp_asset_select_deselect.init();

      domReady(function() {
        $('#add_group').click(function(){
          window.location = '/groups/new.html';
        });
      });
    }
    // END init

  };
});
