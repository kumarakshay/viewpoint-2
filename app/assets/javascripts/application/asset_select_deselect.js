define(['domReady', 'sg/jquery', './page'], function(domReady, $, vp_page) {
  return {

    // Define two arrays for use in the module    
    src_selected: [],
    dest_selected: [],

    // Find the array index position of a matching search    
    index_of: function (ary, search) {
      for(var i=0; i < ary.length; i++) {
        if(ary[i] == search) return i;
      }
      return -1;
    },
        
    radio_function: function () {
      if (this.value == 'true') {
        $("#table_dest").css('display', 'none');
      } else{
        $("#table_dest").css('display', '');
      }
    },

    init: function() {
      var self = this;

    //  vp_page.init();

      // New functions added to JavaScript Array object. 
      // This would add the item to an array if not present and vice-a-versa
      Array.prototype.add_remove = function(item) {
        var indx = self.index_of(this, item);
        if (indx == -1){
          this.push(item);
        }else{
          this.splice(indx, 1);
        }
      };

      // Clears the array to a zero length
      Array.prototype.clear = function() {
        this.splice(0, this.length);
      };

      $(document).ready(function() {

        var sib_height = $('#table_dest_wrapper label').outerHeight();

        var right_list_resize = function(){
          $('#table_dest_wrapper > div').height($('#table_dest_wrapper').closest('.right_list').height()-sib_height).fadeIn(200);
        };

        $("#cl_right").click(function(){
          //alert(src_selected);
          if (self.src_selected.length > 0) {
            for (var i = 0; i < self.src_selected.length; i++) {
              var element = self.src_selected[i];
              var tr = $(document.getElementById(element)).remove();
             // $(tr).click(this.del_click);
              $("#table_dest").append(tr);
            }
            $("#table_dest tr").removeClass('active');
            self.src_selected.clear();
          }
          right_list_resize();
        });

        $("#cl_left").click(function(){
          //alert(dest_selected);
          if (self.dest_selected.length > 0) {
            for (var i = 0; i < self.dest_selected.length; i++) {
              var element = self.dest_selected[i];
              var tr = $(document.getElementById(element)).remove();
            //  $(tr).click(this.sel_click);
              $("#table_source").append(tr);
            }
            $("#table_source tr").removeClass('active');
            self.dest_selected.clear();
            // reorder and reset alternate
          }
          right_list_resize();
        });

        $("#table_source").on("click", "tr", function (event) {
          //var self = this;
          if (event.ctrlKey) {
            $(this).closest("tr").toggleClass('active');
            self.src_selected.add_remove(this.id);
          }
          else {
            self.src_selected.clear();
            self.src_selected.push(this.id);
            $(this).closest("tr").addClass('active')
            .siblings().add($('#table_dest tr')).removeClass('active');
          }
          $("#cl_left").fadeOut(200,function(){
            $("#cl_right").fadeIn(200);
          });
        });

        $("#table_dest").on("click", "tr", function(event) {
         // var self = this;
          if (event.ctrlKey) {
            $(this).closest("tr").toggleClass('active');
            self.dest_selected.add_remove(this.id);
          } else {
            self.dest_selected.clear();
            self.dest_selected.push(this.id);
          $(this).closest("tr").addClass('active')
            .siblings().add($('#table_source tr')).removeClass('active');
          }
          $("#cl_right").fadeOut(200, function(){
            $("#cl_left").fadeIn(200);
          });
         
        });

        $("#event_subscription_all_asset_true").click(this.radio_function);
        $("#event_subscription_all_asset_false").click(this.radio_function);
        $('#groups_form > div').height($('#page > #section').height());

        $('#table_dest_wrapper').siblings().show().each(function(){
          sib_height += $(this).show().outerHeight(true) - (parseInt($(this).css('margin-bottom'),10)/2);
        });

        var all_or_selective = function(){
          if ($('#event_subscription_all_asset_true').is(':checked')) {
            $('.left_list, #table_dest_wrapper > div, .middle_buttons').append('<div class="en_overlay"/>');
          } else {
            $('.left_list, #table_dest_wrapper > div, .middle_buttons').find('.en_overlay').remove();
          }
        };

        $('#event_subscription_all_asset_true, #event_subscription_all_asset_false').change(all_or_selective).change();

        $(window).resize(function(){
          right_list_resize();
        });

        right_list_resize();

      });
    } // END init
  };
});