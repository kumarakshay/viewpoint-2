 define(['domReady', 'sg/jquery', 'underscore', './page'], function(domReady, $, _, vp_page) {

  return {

    //function resizes the assets and events modules
    resize_modules: function(){
      var minimum_column_width = 265; //min width of a column
      var margin_width = 20; //margin between modules
      var border_width = 2; //width of border for modules - 1px each side

      var window_width = $(window).width();
      var available_width = $('#tick_dash').width()-3;

      if(window_width > 680){ //full mode
        var grid_col_2_width = available_width - margin_width - minimum_column_width;

        $('#tick_dash .grid_col_1').width(minimum_column_width);
        $('#tick_dash .grid_col_2').width(grid_col_2_width);

        $('#tick_dash_help, #tick_dash_my_tickets, #tick_dash_reports, #tick_dash_administration').width(263);


        var button_width = Math.floor( (grid_col_2_width - border_width - 28 - 28 - 2 - 20 - 2 ) / 2 );
        $('#tick_dash .tick_dash_button').css("width", button_width+'px');

        $('#tick_dash .grid_col_2').insertAfter('#tick_dash .grid_col_1');
      }
      else{ //mobile mode
        var grid_col_width = available_width >= minimum_column_width ? available_width : minimum_column_width; //minimum width

        $('#tick_dash .grid_col_1').width(grid_col_width);
        $('#tick_dash .grid_col_2').width(grid_col_width);
        $('#tick_dash_help, #tick_dash_my_tickets, #tick_dash_reports, #tick_dash_administration').width(grid_col_width - border_width);

        var button_width = grid_col_width - border_width - 28 - 28 - 2;
        $('#tick_dash .tick_dash_button').css("width", button_width+'px');

        $('#tick_dash .grid_col_2').insertBefore('#tick_dash .grid_col_1');
      }
    },

    init: function(){
      var that = this;

      domReady(function() {
        //init page
        vp_page.init();

        //resize modules
        that.resize_modules();

        $('#tick_dash .module_collapsible .module_header').on("click", function(){
          $(this).parent().toggleClass('module_collapsed');
        });

        //bind browser resize event, debounce with underscore
        that.resize_modules_debounced = _.debounce(that.resize_modules, 5);
        $(window).on("resize", function(){
          that.resize_modules();
        });

      });
    }

  };

});