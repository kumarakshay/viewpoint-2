define(['sg/jquery', 'domReady', 'sg/util', 'sg/ajax','d3.v2.min'], function($, domReady, sg_util, sg_ajax) {

  return {
    // $ele - dom element where graph goes
    // $api - array of json url's array of two-column arrays: datetime (from epoch), metric value

    apply: function($ele,$api,$i) {

      sg_util.loading_on($ele);

      var $this = this, $dataset = [];
      $api = $api.split(',');

      $api.forEach(function(val,i){
        sg_ajax.call(val, {
          dataType: "json"
        })
        .done(function($data){

          // Data sample
          //    "meta": {}
          //    "result": {
          //        "uid": "SOA-104.subaru1.com/os/filesystems/siedbprd02/usedBlocks_usedBlocks.rrd",
          //        "asset_name": "SOA-104.subaru1.com",
          //        "scope": "os",
          //        "component_type": "filesystems",
          //        "component": "siedbprd02",
          //        "display_name": "Used Blocks",
          //        "metric_name": "usedBlocks",
          //        "metric_alias": "usedBlocks",
          //        "series": []


          // modify json
          var $device = {};
          $metric = $data.result.display_name;
          $device.name = $data.result.asset_name;
          // metric may need some work ***********
          $device.metric = $data.result.uid;
          $device.result = [];
          $.each($data.result.series,function(i,item){
            if (item.value !== null){
              item.timestamp *= 1000;
              $device.result.push(item);
            }
          });

          $dataset.push($device);

          if ($api.length == (i+1)) {
            // done getting all of the data now go make the graph
            $x = [];
            $.each($device.result,function(){
              $x.push($(this)[0].timestamp);
            });
            $this.create_graph($metric,$dataset,$x,$i);
          }
          sg_util.loading_off($ele);
        })
        .fail(function(xhr, status){
          // alert("Unable to retrive data for "+val+': '+status);
          $ele.html('<div class="widget_error"> </div>');
          sg_util.loading_off($ele);
        });

      });


      $this.create_graph = function($metric,$data,$x,$i){
        var margin = {top: 20, right: 80, bottom: 30, left: 50},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

        var parseDate = d3.time.format("%Y%m%d").parse;

        var x = d3.time.scale()
        .range([0, width]);

        var y = d3.scale.linear()
        .range([height, 0]);

        var color = d3.scale.category10();

        var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(12);

        var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

        var line = d3.svg.line()
        .interpolate("basis")
        .x(function(d) { return x(+d.timestamp); })
        .y(function(d) { return y(+d.value); });

        var svg = [];
        svg[$i] = d3.selectAll('.graph-'+$i).append("svg")
        //   .attr("width", "100%")
        .attr("viewBox", "0 0 960 500")
        .attr("id", "svg"+$i)
        //   .attr("height", "100%")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        // [obj,obj,obj]
        // obj = meta:,result:{timestamp:,value:}

        // make headers
        var lines = color.domain($data);

        x.domain(d3.extent($x));


        y.domain([
                 d3.min($data, function(c) { return d3.min(c.result, function(v) { return v.value; }); }),
                 d3.max($data, function(c) { return d3.max(c.result, function(v) { return v.value; }); })
        ]);

        svg[$i].append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

        svg[$i].append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        // label needs to come dynamically
        .text($metric);

        var device_line = svg[$i].selectAll(".device")
        .data($data)
        .enter().append("g")
        .attr("class", "device");

        device_line.append("path")
        .attr("class", "device_line")
        .attr("fill-opacity", 0)
        .attr("d", function(d) { return line(d.result); })
        .style("stroke", function(d) { return color(d.name); });

        device_line.append("text")
        .datum(function(d) { return {name: d.name, value: d.result[d.result.length - 1]}; })
        .attr("transform", function(d) { return "translate(" + x(d.value.timestamp) + "," + y(d.value.value) + ")"; })
        .attr("x", 3)
        .attr("dy", ".35em")
        .text(function(d) { return d.name; });

      }; // end create_graph

    } // end sg_line_graph

  };

});
