define([
    'jquery',
    'jquery.validity'
    ], function(
        jQuery
    ) {

// Install the tooltip output.
(function($) {
    $.validity.outputs.tooltip_custom = {
        tooltipClass:"validity-tooltip-right",
    
        start:function() {
            $("." + $.validity.outputs.tooltipR.tooltipClass)
                .remove();
        },
        
        end:function(results) {
            return;
            // If not valid and scrollTo is enabled, scroll the page to the first error.
        },

        raise:function($obj, msg) {
            var pos = $obj.offset();
            var height_adjust = (30 - $obj.outerHeight()) / 2;
            // if (msg.indexOf('format') > -1) {
            //     msg = 'Invalid'; // Override the default. Allows fixed arrow size.
            // } else {
            //     msg = 'Required'; // Override the default. Allows fixed arrow size.
            // }
            pos.top = ($obj.offset().top - $('#admin_form').offset().top) - height_adjust;
            
            $e = $(
                "<div class=\"validity-tooltip-right\">" +
                    msg +
                    "<div class=\"validity-tooltip-outer\">" +
                        "<div class=\"validity-tooltip-inner\"></div>" +
                    "</div>" +
                "</div>"
            )
                .click(function() {
                    $obj.focus();
                    $(this).fadeOut();
                })
                .hide()
                .appendTo("#admin_form")
                .fadeIn();

            pos.left += ($e.width() + 35) * -1;
            $e.css(pos);

            $('#section').scrollTop(-pos.top);
        },


        /*  The default library behavior is to only show the last required field
            after the user has made changes the first time. In order to show 
            all remaining required fields on every attempt, the raiseAggregate
            function has to be a duplicate of the raise function above.
        */
        raiseAggregate:function($obj, msg) {
             var pos = $obj.offset();
            var height_adjust = (30 - $obj.outerHeight()) / 2;
            // if (msg.indexOf('format') > -1) {
            //     msg = 'Invalid'; // Override the default. Allows fixed arrow size.
            // } else {
            //     msg = 'Required'; // Override the default. Allows fixed arrow size.
            // }
            pos.top = ($obj.offset().top - $('#admin_form').offset().top) - height_adjust;
            
            $e = $(
                "<div class=\"validity-tooltip-right\">" +
                    msg +
                    "<div class=\"validity-tooltip-outer\">" +
                        "<div class=\"validity-tooltip-inner\"></div>" +
                    "</div>" +
                "</div>"
            )
                .click(function() {
                    $obj.focus();
                    $(this).fadeOut();
                })
                .hide()
                .appendTo("#admin_form")
                .fadeIn();

            pos.left += ($e.width() + 35) * -1;
            $e.css(pos);
        },
    };
    })(jQuery);
});