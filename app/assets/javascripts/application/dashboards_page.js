define([
  'domReady',
  'sg/jquery',
  'sg/util',
  'sg/ajax',
  './multiline_graph'
], function(
  domReady,
  $,
  sg_util,
  sg_ajax,
  ml_graph
) {


  return({

    update_metrics: function () {
      var self = this;
      // make this
      self.loading_on_small($('#graph_metric').siblings('label'));

      $('#graph_metric').addClass('processed');
      sg_ajax.call('/api/devices/B1B540A2-C25D-5EC6-E040-A2A8567D4375/metrics')
      .done(function($data){
        var $html = '<option value="">Select one...</option>';
        $.each($data.result,function(i,value){
          if (value.uid !== null && value.display_name !== null ) {
            $html += '<option value="' + value.uid + '">' + value.display_name + '</option>';
          }
        });
        $('#graph_metric').removeAttr('disabled').html($html);

        self.loading_off_small($('#graph_metric').siblings('label'));
      })
      .fail(function(xhr, status){
        alert("Unable to update metrics dropdown: "+status);
      });
    },

    update_devices: function($metric,$company_guid) {
      var self = this;

      self.loading_on_small($('#graph_assets').siblings('label'));
      // url is stubbed, need to insert metric to real url once it is ready
      // line 34 may need to be changed depending on json coming back.
      $url = '/api/companies/'+$company_guid+'/metrics?metric_name='+$metric;

      sg_ajax.call($url)
      .done(function($data){
        var $html = '';
        $.each($data.result,function(i,value){
          if (value.uid !== null && value.display_name !== null ) {
            $html += '<option value="' + value.c_asset_guid + '" rel="'+ value.uid +'">' + value.asset_name + '</option>';
          }
        });
        $('#graph_assets_list').removeAttr('disabled').html($html);

        self.loading_off_small($('#graph_assets').siblings('label'));


        $('#graph_assets_list').val($('#graph_assets').val());

        $("#graph_assets_list").multiselect({
          selectedText: "# of # selected",
          noneSelectedText: "Select Assets..."
        });
        // $value = JSON.parse($("#graph_assets").val());

        var $value;

        if ($("#graph_assets").val()) {
          $value = JSON.parse($("#graph_assets").val())[0].split(',');
        } else {
          $value = [];
        }

        $("#graph_assets_list").multiselect("widget").find("input").each(function(){
          if ($(this).attr('aria-selected-')!='true'){
            if($value.indexOf($(this).val()) != -1){
              $(this).click();
            }
          } else {
            if($value.indexOf($(this).val()) == -1){
              $(this).click();
            }
          }
        });
        self.metric_disable();
      })
      .fail(function(xhr, status){
        alert("Unable to update device dropdown: "+status);
        self.loading_off_small($('#graph_assets').siblings('label'));
      });
    },

    loading_on_small: function($ele) {
      // create element with loading graphic
      $ele.append('<div class="loading_small"><div class="image" /></div>').find('div.image').css({'opacity':0});

      // slide down and fade in
      $ele.find('div.loading_small').slideDown(300,function(){
        $(this).find('div.image').animate({opacity : 1});
      });
    },

    loading_off_small: function($ele){
      // slide and fade in
      $ele.find('div.image').animate({opacity:0},function(){
        $(this).closest('div.loading_small').slideUp('slow');
        // remove element
        $(this).closest('div.loading_small').remove();
      });
    },

    resize_add_graph: function() {
      $('.graph_container .add_graph').height(200);
      $('.graph_container .dash_graph').each(function(){
        $('.graph_container .add_graph').height($('.graph_container .dash_graph').height()+$('.graph_container h3').height());
      });
      // chrome
      if($.browser.chrome){
        $('.dash_graph svg').each(function(){ $(this).height(0.50*$(this).width()); });
      } 
    },

    change_order: function($ele,$order,$max) {
      $ele.click(function(){
        var offset = $(this).offset();
        left = e.clientX - offset.left;
        right = e.clientY - offset.top;
        var $query = '';
        if (left+right < 17){
          if ($order > 1){
            $query= '/'+$order+'/'+$order-1;
          }
        }
        if (left+right > 17){
          if ($order < $max){
            $query= '/'+$order+'/'+$order+1;
          }
        }
        if ($query !== ''){
          // do ajax now
          sg_ajax.call('/graphs/order'+$query)
          .done(function($data){
            // $data should be true or false
            // if true switch the elements in the view instead of reloading the page
            change_order_complete();
          });
        }
      });
    },

    metric_disable: function() {
      if ($('#graph_assets').val() !== ''){
        $('#graph_metric_list').attr('disabled','disabled');
      } else {
        $('#graph_metric_list').removeAttr('disabled');
      }
    },

    init: function() {
      var self = this;

      // keeps the add_graph the same size as the others in case of window resizing
      $(window).resize(function(){
        self.resize_add_graph();
      });

      domReady(function() {
        $('form#new_graph').find('#graph_assets, #graph_assets_list, #graph_metric, #graph_metric_list').val('');

        self.resize_add_graph();

        var $graph_array = [];
        $('.graph_container div.dash_graph').each(function($i){
          $(this).addClass('graph-'+$i);
          if ($.browser.msie && parseInt($.browser.version,10) < 9) {
            $('.graph_container div.dash_graph.graph-'+$i)
            .html('<div style="text-align:center; padding-top: 2em;">IE 9+, FireFox, Chrome, Safair and Opera required for this feature.</div>');
          } else {
            $graph_array[$i] = ml_graph.apply($('.graph_container div.dash_graph.graph-'+$i),$(this).attr('rel'),$i);
            self.resize_add_graph();
          }

        });

        // big_box the graphs
        $('.graph_container a.expand').each(function(){
          $(this).click(function(){
            $h = (($(window).width()*0.75)*500)/960;
            sg_util.big_box('<div class="graph_container" style="height:'+$h+'px">'+$(this).closest('.graph_container').html()+'</div>');
          });
        });
        // order link
        // ...

        // populate metric list : currently done inline

        // b/c the metric is static for now we need to manually sync values
        $('body.graphs #graph_metric_list').val($('#graph_metric').val());

        // metric ajax : currently not used
        $('#graph_metric_list').not('.processed').each(function(){
          self.update_metrics();
        });

        // add graph buttons
        $('body.dashboard .add_graph').each(function(){
          $(this).click(function(){
            //  $('form#new_graph').slideToggle();
          });
        });

        // update assets on metric change
        $('#graph_metric_list').change(function(){
          // syncing values again
          $('#graph_metric').val($('#graph_metric_list').val());
          $company_guid = $(this).closest('form').find('input.company_guid').val();
          if ($(this).val() !== ''){
            self.update_devices($(this).val(),$company_guid);
          }
        });

        // update asset value on list change
        $('body.graphs form').submit(function(){
          // syncing values again
          var $value = [];
          var $uids = [];
          $("#graph_assets_list").multiselect("widget").find(":checked").each(function(){
            $value.push($(this).val());
            $uids.push($('#graph_assets_list option[value="'+ $(this).val() +'"]').attr('rel'));
          });
          $('#graph_assets').val($value);
          $('#graph_uid').val($uids);
        });

        // update asset value on list change
        $("#graph_assets_list").bind("multiselectclick", function(event, ui){
          var $value = [];
          var $uids = [];
          $("#graph_assets_list").multiselect("widget").find(":checked").each(function(){
            $value.push($(this).val());
            $uids.push($('#graph_assets_list option[value="'+ $(this).val() +'"]').attr('rel'));
          });
          $('#graph_assets').val($value);
          $('#graph_uid').val($uids);

        });

        // update assets if metric has a value
        $('#graph_metric_list').each(function(){
          if ($('#graph_metric_list').val() !== '') {  
            $company_guid = $('#graph_metric_list').closest('form').find('input.company_guid').val();
            self.update_devices($('#graph_metric_list').val(),$company_guid);
          }
        });

        // disable metric if device has value
        $('#graph_assets').each(function(){
          self.metric_disable();
        });
        $('#graph_assets').change(function(){
          self.metric_disable();
        });

        // find position
        $('#dashboard_select_list li').each(function($i){
          $here = $(this).closest('#dashboard_select_list').find('.here').text();
          if ($(this).text()==$here){
            sg_util.device_position($i);      
          }
        });
      });

    }

  });
});




