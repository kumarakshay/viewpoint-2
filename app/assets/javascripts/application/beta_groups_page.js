define([
  'domReady', 
  'sg/jquery', 
  'sg/ajax',    
  'sg/util',   
  'backbone.marionette', 
  '../../../../assets/livetable/main.js',   
  './page',
  './asset_select_deselect',
  './mockup_json/group_records_generator',
  './mockup_json/group_columns_generator'
  ], function(
      domReady, 
      $,
      ajax,
      util,
      Marionette,   
      LiveTable,
      vp_page,
      vp_asset_select_deselect,
      sampleGroupLogs,
      sampleGroupColumns
  ) {

  return {

    render_logs: function(columns, logs){
      var self = this;
      var LogList = new Marionette.Application();
      LogList.addRegions ({
       page: "#section"
      });
      LogList.module("FirewallLogs", {
        moduleClass: LiveTable,
        columnObj: columns,
        dataObj: logs,
        rowCallbacks: {clickMe: function(args){alert(args)}}
      });
     LogList.page.show(LogList.FirewallLogs.layout);
     LogList.start();
    },

    init: function() {

      var self = this;
       $('#section').css({'display':'block', 'top': '110px'});
      var columns = sampleGroupColumns();
      var logs = sampleGroupLogs(100);
      self.render_logs(columns,logs);
      domReady(function() {
        $('#add_group').click(function(){
          window.location = '/groups/new.html';
        });
      });
    }
    // END init

  };
});
