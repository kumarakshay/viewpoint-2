define([
    'module',
    'jquery',
    'sg/util',
    'domReady',
    'backbone.marionette',
    './application/page',
    './application/router',
    './application/notification/collections/app_notifications',
    './application/notification/views/app_notifications',
    'dns-module',
    './application/adapters/device_module',
    './application/adapters/dashboards_module',
    './application/adapters/groups_module',
    './application/adapters/interfaces',
    './application/adapters/reports_module',
    './application/adapters/events_module',
    './application/adapters/event_subscriptions_module',
    './application/adapters/admin_users_module',
    './application/adapters/firewall_module',
    './application/adapters/rails_adapter',
    './application/adapters/sungard_module',
    './application/adapters/devices_module',
    './application/adapters/fws',
    './application/adapters/interfaces_module',
    'groups-module',
    'ehealth-module',
    'ticket-landing-module',
    'datatables'
  ],

  function(
    module,
    $,
    sg_util,
    domReady,
    Marionette,
    ViewpointPage,
    AppRouter,
    AppNotificationsCollection,
    AppNotificationsView,
    DnsModule,
    DeviceModule,
    DashboardsModule,
    GroupsModule,
    InterfacesModule,
    ReportsModule,
    EventsModule,
    EventSubscriptionsModule,
    AdminUsersModule,
    FirewallModule,
    RailsAdapter,
    SungardModule,
    DevicesModule,
    SplunkModule,
    BetaInterfacesModule,
    GroupsNewModule,
    EhealthModule,
    TicketLandingModule
  ) {

    // The App
    var App = new Marionette.Application();

    //// Set up colleciton for notifications
    var appNotifications = new AppNotificationsCollection();
    // Setup layout and regions
    App.addRegions({
      main: '#section',
      sidebar: '#aside',
      tools: '#header div.tools',
      subtools: '#subtools',
      header: '#header',
      footer: '#footer',
      body: 'body'
    });

    new AppNotificationsView({
      el: '#notifications',
      collection: appNotifications
    }).render();

   
    App.getRegion('sidebar').on('show',function() {
      this.$el.show();
    });

    App.getRegion('sidebar').on('close',function() {
      this.$el.hide();
    });


    App.getRegion('tools').on('show close dom:refresh', sg_util.resize_middle);
    App.getRegion('sidebar').on('show close dom:refresh', sg_util.resize_middle);


    // Listen for events
    appNotifications.listenTo(App.vent,'app:notification',function(m) {
      appNotifications.add(m);
    });

    if (module.config().disableRouter) {
      // Pages generated and served by Rails
      // do not need the router to be active.
    }
    else {
      new AppRouter({
        app: App
      });
    }

    App.on("initialize:after", function(options){
      domReady(function() {
        ViewpointPage.init();
      });
      Backbone.history.start();
    });

    App.module('dashboards', DashboardsModule);
    App.module('device', DeviceModule);
    App.module('events', EventsModule);
    App.module('event_subscriptions', EventSubscriptionsModule);
    App.module('groups', GroupsModule);
    App.module('interfaces', InterfacesModule);
    App.module('rails', RailsAdapter);
    App.module('reports', ReportsModule);
    App.module('users', AdminUsersModule);
    App.module('dns', DnsModule);
    App.module('firewall', FirewallModule);
    App.module('betagroups', GroupsNewModule);
    App.module('ehealth', EhealthModule);
    App.module('sungard', SungardModule);
    App.module('devices', DevicesModule);
    App.module('beta_interfaces_module', BetaInterfacesModule);
    App.module('splunk_module', SplunkModule);
    App.module('tickt_landing', TicketLandingModule)

    App.module('dashboards').startWithParent = false;
    App.module('device').startWithParent = false;
    App.module('events').startWithParent = false;
    App.module('event_subscriptions').startWithParent = false;
    App.module('groups').startWithParent = false;
    App.module('interfaces').startWithParent = false;
    App.module('reports').startWithParent = false;
    App.module('users').startWithParent = false;
    App.module('firewall').startWithParent = false;
    App.module('ehealth').startWithParent = false;
    App.module('devices').startWithParent = false;
    App.module('beta_interfaces_module').startWithParent = false;
    App.module('splunk_module').startWithParent = false;
    App.module('tickt_landing').startWithParent = false;

    // Ability.done(function() {
      App.start();
    // });

    return App;
  }
);
