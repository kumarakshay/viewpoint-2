class Report

  def self.all_reports_list(company_guid)   
    report_dir = Viewpoint::Application::USAGE_REPORTS + company_guid
    report_dir = report_dir.downcase
    Rails.logger.info("the report dir: #{report_dir}")
    if File.directory?(report_dir)
      Dir.chdir(report_dir) 
      arr = Array.new
      i = 0
      farr = Dir.glob('*.*')
      farr.each do |file| 
        # ignore the fo type files
        if File.extname(file) != '.fo'
          if File.extname(file) == '.pdf'
            e = "pdf"
          end
          if File.extname(file) == '.txt'
            e = "txt"
          end
          if File.extname(file) == '.csv'
            e = "csv"
          end
          arr[i] = Hash.new
          arr[i] = {name: File.basename(file), type: get_report_type(file), date: File.mtime(file), ext: e}
          i += 1
        end  
      end
      arr
    else
      # report directory does not exist for this company
      # returning an empty array so the page renders, grid will be empty
      arr = []
    end
  end

  def self.construct_path(file_name, company_guid, file_extension_type)
    report_dir = Viewpoint::Application::USAGE_REPORTS + company_guid
    report_dir = report_dir.downcase
    f = report_dir + "/" + file_name + "." + file_extension_type
  end 

  def self.get_report_type(f)
    if (f =~ /power(.*)/ )
      r = "powerbar"
    end
    if (f =~ /usage(.*)/ )
      r = "usage"
    end
    if (f =~ /vpls(.*)/ )
      r = "vpls"
    end
    if (f =~ /internet(.*)/ )
      r = "internet"
    end
    r
  end

end