class Company
  include MongoMapper::Document
  key :snt, String
  key :guid
  key :oracle_id
  key :sg_id
  key :site_url
  key :sfdc_product_code
  key :name
  key :support_org
  key :active, Boolean, default: true
  key :asset_list, Array
  key :deleted_at, Time
  key :zenoss_presence, Boolean, default: false
  key :user_count, Integer, default: 0
  key :us_only, Boolean, default: false
  key :is_tdr, Boolean, default: false
  key :is_apm, Boolean, default: false
  key :sys_id
  timestamps!

  validates :snt, :guid, :name, :presence => {:message => 'cannot be empty'}
  validates :guid, :uniqueness => true
  validates :snt, :uniqueness => true


  #This method return the asset_list array in lowercase associated with company.
  def lowercase_asset_list
    original = self[:asset_list]
    if original
      original.reject!(&:nil?)
      original.map!(&:downcase)
    end
    original
  end

  alias :asset_list :lowercase_asset_list

  def self.search_by_name(name)
    where(name: /#{Regexp.escape(name)}/i, deleted_at: nil).limit(100)
          end

          def self.by_letter(letter)
            where("name LIKE ?", "#{letter}%").order(:name)
          end

          def self.parent_child_list(guid)
            begin
              snt = Company.where(guid: guid).first.snt
              dat = PortalMapWorker.where(parent_company_snt: snt).all
              guids_to_report_on = []
              guids_to_report_on << guid
              dat.each do |p|
                guids_to_report_on << p['id_company_id']
              end
              # VUPT-1348 Removal of sungard-AS and sungard-AS UK from list of childs.
              cguids = guids_to_report_on - ['76C6F530-40C1-446D-A5D5-A66E78605149','E71312DC-AAF1-4BF2-941D-B297B1D326AB']
              #Just make sure parent guid should always be first
              unless cguids.index(guid).blank?
                cguids.insert(0,cguids.delete_at(cguids.index(guid)))
              end
            rescue Exception => e
              logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
              return []
            end
          end

          #This is a new method which will find children to N level down.
          def self.recurring_parent_child_list(guid)
            c = Company.where(guid: guid).first
            guids_to_report_on = []
            guids_to_report_on << guid
            guids_to_report_on += c.all_children
            # VUPT-1348 Removal of sungard-AS and sungard-AS UK from list of childs.
            cguids = guids_to_report_on - ['76C6F530-40C1-446D-A5D5-A66E78605149','E71312DC-AAF1-4BF2-941D-B297B1D326AB']
            #Just make sure parent guid should always be first
            unless cguids.index(guid).blank?
              cguids.insert(0,cguids.delete_at(cguids.index(guid)))
            end
            cguids.flatten.uniq
          end

          def all_children(children_array = [])
            children = PortalMapWorker.where(parent_company_snt: self.snt).all
            children.each do |child|
              c = Company.where(guid: child['id_company_id']).first
              unless children_array.include?(c.guid)
                children_array << c.guid
                c.all_children(children_array)
              end
            end
            children_array.flatten.uniq
          end

          def self.with_guid(guid)
            Company.where(guid: guid).first
          end

          # def user_count
          #   User.where(company_guid: guid).count
          # end

          def self.set_active_flag(id, val)
            @company = Company.find(id)
            @company.active = val
            @company.save
          end

          # utility method to verify that the asset_list has been created
          def self.acl_monitor
            z = Company.where(guid: "D9EFA335-F8E5-4271-9541-BB5D7590AEE5").first.asset_list.count
            if z > 100
              return "ok"
            else
              return "problem"
            end
          end

          def zenoss_instance
            z_instances = Device.where(asset_guid: asset_list).collect(&:zenoss_instance)
            freq =  z_instances.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
            z_instances.max_by { |v| freq[v] }
          end

          #This method will find the group availability for a company
          #which would be used in ehealth and ticket-landing page
          def group_availability
            ct = Time.now
            Rails.logger.info "********** Starting Groups Environment Health test for the company:- #{self.name} **********"
            record = OpenStruct.new()
            record.result = {"msg" => "getSeveritiesByDevice","total" => 0,"data" => {}}
            groups = Group.where(:active => true).where(:company_guid =>self.guid)
            begin
              record.result["total"] = groups.count
              groups.each do |group|
                response = group.device_severities(zenoss_instance)
                info = clear = warning = critical = error = debug  = 0
                Rails.logger.info "*" * 100
                Rails.logger.info response["result"]
                Rails.logger.info "*" * 100
                if response["result"]["success"] && response["result"]["data"].present?
                  response["result"]["data"].each do |k,v|
                    if v["severity_counts"] ["critical"].to_i > 0
                      critical += 1
                    elsif v["severity_counts"]["error"].to_i > 0
                      error += 1
                    elsif v["severity_counts"] ["warning"].to_i > 0
                      warning += 1
                    else
                      info +=1
                    end
                  end
                  total = response["result"]["total"].to_i
                  record.result["data"].merge!(
                    group.name => {
                      "total_devices" => total,
                      "sev_counts" => {
                        "info" => info,
                        "warning" => warning,
                        "critical" => critical,
                        "error" => error
                      }
                    }
                  )
                end
              end
              Rails.logger.info "********** Total time taken for Groups Environment Health:- #{Time.now-ct}."
              record
            rescue Exception => e
              record
              logger.error("ERROR: #{e.message}\n" + e.backtrace.join("\n"))
            end
          end

          #Check company has access to OMI menu
          def is_omi?
            found = false;
            file = "#{Rails.root}/db/data/omi/omi_companies.csv"
            found = true if APP_CONFIG['vp_cauldron']
            if !APP_CONFIG['vp_cauldron'] && File.exist?(file)
              config = CSV.read(file)
              config.shift
              config.flatten!
              found = config.include?(self.guid) ? true : false
            end
            found
          end

          #Check company is opted for backup_report service
          def is_backup_company?
            begin
              found = false
              file = "#{Rails.root}/db/data/import/backup_report_companies.csv"
              if self.guid != "76C6F530-40C1-446D-A5D5-A66E78605149" && !APP_CONFIG['vp_cauldron'] && File.exist?(file)
                config = CSV.read(file)
                config.shift
                processed = config.map(&:first)
                found = processed.include?(self.guid) ? true : false
              end
              found
            rescue Exception => e
              return false
            end
          end

          # Check if company have access to Portal Patching
          def portal_patching_access?
            found = false;
            file = "#{Rails.root}/db/data/portal_patching/portal_patching.csv"
            if File.exist?(file)
              config = CSV.read(file)
              if !config.empty?
                config.shift
                config.flatten!
                found = config.include?(self.guid)
              else
                found = false
              end
            else
              found = false
            end
            found
          end

          #This is soft delete, we are updating deleted_at with current time
          def delete
            self.deleted_at = Time.now
            self.active = false
            User.destroy_all(:company_guid => self.guid)
            self.save :validate => false
          end

          alias :destroy :delete

          private

          def set_active_flag(id, val)
            company = Company.find(id)
            company.active = val
            company.save
            return "200"
          end
          end
