module Filterable

  def translate_column_name(col)
    col
  end

  def translate_bogus_column_name(col)
    col
  end

  def exclude_columns
    ['uid','asset_guid','zenoss_instance'] 
  end

  def public_columns
    (self.column_names - exclude_columns)
  end

  def public_columns_for_select
    s = public_columns.map do |c|
      "#{self.quoted_table_name}.#{connection.quote_column_name(c)} #{translate_column_name(c)}"
    end.join(',')
    s + extra_columns if(extra_columns)
  end

  def extra_columns
  end

  def arel_joins(joins={})
    # rails and it's everlasting hatred of multiple dbs.
    at = Arel::Table.new(self.table_name, self.connection.pool)
    join_at = Arel::Table.new(self.table_name, self.connection.pool)
    joins.each do |join,values|
      logger.debug(join)
      r = self.reflections[join]
      klass = r.klass
      # join table
      jt = r.klass.arel_table
      if(values[:outer_join])
        join_at = join_at.join(jt,Arel::Nodes::OuterJoin).on(jt[r.foreign_key].eq(at[r.active_record_primary_key]))  
      else
        join_at = join_at.join(jt,Arel::Nodes::InnerJoin).on(jt[r.foreign_key].eq(at[r.active_record_primary_key]))
      end
    end
    join_at
  end

  def select_from(select_fields=[],joins={})
    if select_fields.blank?
      select = self.public_columns_for_select
    elsif self.respond_to?(:translate_select_fields)
      select = self.translate_select_fields(select_fields)
    else
       select = select_fields.join(',')
    end
    process_joins(select,joins)
  end
  
  def process_joins(select,joins)
    joins.each do |join,values|
      r = self.reflections[join]
      klass = r.klass
      # use joins that has field list in it instead
      cols = values[:cols] || []
      p_cols = if klass.respond_to?(:translate_column_name)
        # dealing with issues of name collision
        cols.map do |c|
          "#{klass.quoted_table_name}.#{connection.quote_column_name(c)} #{klass.translate_column_name(c)}"
        end
      else
        cols.map do |c|
          "#{klass.quoted_table_name}.#{connection.quote_column_name(c)}"
        end
      end
      unless p_cols.empty?
        select << ',' + p_cols.join(',')
      end
    end
    select
  end

  def select_join_cache(select_fields=[],joins={})
    joins_json = JSON::dump(joins)
    sum = Digest::MD5.base64digest(joins_json)
    # Rails.cache.fetch("#{self.table_name}_#{sum}", expires_in: 1.hour) do      
      select_sql = select_from(select_fields,joins)
      Rails.logger.debug(select_sql)
      {count_sql: arel_joins(joins).project(self.arel_table[self.primary_key].count).to_sql, select_sql: arel_joins(joins).project(select_sql).to_sql}
    # end
  end

  def count_sql(sql, where_sql)
    sql + ' ' + where_sql
  end

  def export_sql(body={},joins={})
    raw_sort = body['sort']
    sort_dir = body['dir'] || 'asc'
    select_fields = body['select']
    if(raw_sort.respond_to?(:keys))
      sort = raw_sort.map do |k,v|
        "#{self.translate_bogus_column_name(k)} #{v}"
      end.join(',')
      sort_dir = nil
    else
      sort  = self.translate_bogus_column_name(raw_sort) || self.primary_key
    end
    where_sql = self.last_filters(body,joins)
    where_sql = "WHERE #{where_sql}" unless where_sql.blank?
    count_sql, select_sql = self.select_join_cache(select_fields,joins).values
    "#{select_sql} #{where_sql} order by #{sort} #{sort_dir}"
  end

  def query_with(body={},joins={})
    limit = body['limit'] || 100
    if limit.to_i > 50000
      limit = 50000
    end
    start = body['start'] || 0
    raw_sort = body['sort']
    sort_dir = body['dir'] || 'asc'
    select_fields = body['select']
    if(raw_sort.respond_to?(:keys))
      sort = raw_sort.map do |k,v|
        "#{self.translate_bogus_column_name(k)} #{v}"
      end.join(',')
      sort_dir = nil
    else
      sort  = self.translate_bogus_column_name(raw_sort) || self.primary_key
    end
    where_sql = self.last_filters(body,joins)
    where_sql = "WHERE #{where_sql}" unless where_sql.blank?
    count_sql, select_sql = self.select_join_cache(select_fields,joins).values
    total = self.connection.select_value(count_sql(count_sql, where_sql))
    Rails.logger.debug(" #{select_sql} #{where_sql} order by #{sort} #{sort_dir} limit #{start}, #{limit}")
    rows = self.connection.select_all(" #{select_sql} #{where_sql} order by #{sort} #{sort_dir} limit #{start}, #{limit}")
    {total: total, data: rows}
  end

  # get query, turn query into json, base64 for unique id and put in cache
  def last_filters(body,joins={})
    query = body['query'] || []
    query_json = JSON::dump(query)
    sum = Digest::MD5.base64digest(query_json)
    Rails.cache.fetch("#{self.table_name}_#{sum}", expires_in: 1.hour) do
      self.filter_by(body,joins).to_sql self.connection.pool
    end
  end

  def column_node_from_join(col_name, joins={})
    joins.each do |join,values|
      cols = values[:cols]
      r=self.reflections[join]
      klass = r.klass
      jt = klass.arel_table
      if cols.include?(col_name)
        c  = (klass.respond_to?(:translate_bogus_column_name)) ? klass.translate_bogus_column_name(col_name) : col_name
        return jt[c]
      end
    end
    c = self.translate_bogus_column_name(col_name)
    at = Arel::Table.new(self.table_name, self.connection.pool)
    at[c]
  end

  # sets up the filter conditions
  def filter_by(body={},joins={})
    adv_filter = body['adv_query'] || false
    query = body['query'] || []
    filter = []
    filter << Arel::SqlLiteral.new("(#{adv_filter})") unless adv_filter.blank?
    query.each do |q|
      if q['name'] then
        col_node= self.column_node_from_join(q['name'],joins)
        case q['op']
          when "eq", "="
            filter << col_node.eq(q['value'])
          when "gt", ">"
            filter << col_node.gt(q['value'])
          when "lt", "<"
            filter << col_node.lt(q['value']) 
          when "like"
            filter << col_node.matches("%#{q['value']}%")
          when "in"
            filter << col_node.in(q['value'])
          when "between"
            filter << col_node.in(Range.new(*q['value']))            
          else
            filter << col_node.eq(q['value'])
        end
      end
    end
    pred = Arel::Nodes::And.new(filter)
  rescue=> e
    logger.error(e.message)
  end
end