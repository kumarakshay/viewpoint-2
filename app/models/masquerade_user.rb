class MasqueradeUser

  attr_accessor :user, :real_user
  what = User.column_names.map{|a| a.to_sym}
  delegate *what, :id, :groups, :asset_list,:update_app_widget_settings, :update_attribute, :remove_app_widget_settings, :save, :app_settings, :is_sungard?, :sungard, to: :user
  
  def initialize(user=nil)
    self.real_user = user
  end

  def can_masquerade?
    true
  end
  
  def is_masquerading?
    true
  end

  def masquerade_as(guid)
    self.user = User.find(guid)
  end

  def reload
    self.user.reload
    self
  end
  
  # for consistent API responses
  def as_json(options=nil)
    if(self.user)
      h = self.user.as_json(options)
      h[:is_masquerading?] = true
      h[:can_masquerade?] = true
      h
    else
      self.real_user.as_json(options)
    end
  end

  def masqueraded_is_sungard?
    self.user.is_sungard?
  end
end