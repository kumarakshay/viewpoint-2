class OmiDevice 
  include MongoMapper::Document
  key :asset_guid  
  key :company_use
  key :company_own
  key :name
  key :ip_address_formatted
  key :collector
  key :ip_address
  key :location
  key :location_uid
  key :ping_flag
  key :priority
  key :production_state
  key :serial_number
  key :snmp_flag
  key :tag_number
  key :zenoss_instance
  key :clear
  key :critical, Integer, default: 0
  key :debug, Integer, default: 0
  key :error, Integer, default: 0
  key :information, Integer, default: 0
  key :ordered, Integer, default: 0
  key :warning, Integer, default: 0
  key :atype
  key :management_ip
  key :cma_name
  key :snmp_sys_name
  key :customer_production_ip
  key :command_ids, Array
  key :dev_objects, Array
  key :dev_objectgroups, Array
  key :dev_groups, Array
  key :dev_services, Array
  key :na_DeviceID
  key :global_id
  key :root_class
  key :ci_type
  key :host_nnm_uid
  timestamps!
  
  scope :devices_in,
    # lambda { |str| where(:asset_guid => { :$in => str.split(',') } )}
    lambda { |str| where(:asset_guid => { :$in => str } )}

  scope :for_company,
    lambda { |str|
       asset_list = Company.where(guid: str).first.asset_list
       where(:asset_guid => asset_list)
    }

  scope :name_starts_with,
    lambda { |str| where :name => { :$regex => /#{str}/i } }

  # search devices based on group id.
  scope :for_group,
    lambda { |group_id|
      group = Group.find(group_id)
      assets = group.blank? ? [] : group.asset_list
      where(asset_guid: { :$in => assets })
    }


  EXCEPTONACOMMANDS = (["19901","20001"] + APP_CONFIG['cr_change_command_ids']).uniq

  def commands
    white_list_commands = command_ids - EXCEPTONACOMMANDS
    OmiCommand.where(:command_id => white_list_commands,:is_show_command => true).all.uniq
  end

  def support_show_command?
    !command_ids.blank?
  end

  def support_firewall_change_command?
    (command_ids & APP_CONFIG['cr_change_command_ids']).any?
  end

  def self.search(id)
    # if(/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/.match(id.to_s))
    #   dat = Device.where(:ip_address_formatted => Regexp.new(id))
    # else
    #   dat = Device.where(:name => Regexp.new(id, Regexp::IGNORECASE))
    # end
    if (id =~ /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/)
      id = id
    else
      id = Regexp.escape(id)
    end
     OmiDevice.where(:$or => [
      {:name => Regexp.new(id, Regexp::IGNORECASE)}, 
      {:ip_address_formatted => Regexp.new(id)},
      {:customer_production_ip => Regexp.new(id)}
      ]) 
  end

  def self.portal_search(*args)
    if args.length == 1
      search(args[0]).all
    else
      obj = search(args[0]).all
      unless obj.empty?
        all_asset =Company.where(guid: args[1]).first.asset_list
        obj.delete_if {|dev| !(all_asset.include?(dev.asset_guid)) }
      end
      obj
    end
  end
end