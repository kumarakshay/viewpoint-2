class EventSubscription
  include MongoMapper::Document
  key :name, String
  key :company_guid, String
  key :active, Boolean, default: true  
  key :asset_list, Array
  key :email, String
  key :rawid, String
  key :severity, Integer, default: 6
  key :all_asset, Boolean, default: false
  key :user_guid, String
  timestamps!

  validates :name, :email, :presence => {:message => 'cannot be empty'}
  validates :email, 
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :message => 'invalid format' }
  validate :asset_list_included
  validates :severity, :inclusion => {:in => 0..6 }
  before_validation :strip_empty_assets
  before_save :check_all_assets

  # down case required for zenoss upgrade
  def company_guid
    read_attribute(:company_guid).downcase  # No test for nil?
  end
  
  def lowercase_asset_list
    original = self[:asset_list]
    original.map(&:downcase)
  end
  
  alias :asset_list :lowercase_asset_list

  def self.all_events
    EventSubscription.all.inject([]) do |ar, event|
      ar << load_all_assets_if_required(event)
    end
  end

  def self.load_event_subscription(id)
    event = EventSubscription.find(id)
    load_all_assets_if_required(event) 
  end

  def self.load_event_subscriptions_for_company(id)
    EventSubscription.where(company_guid: id).all.inject([]) do |ar, event|
      ar << load_all_assets_if_required(event)
    end
  end

  def self.load_event_subscriptions_by_email(id)
    EventSubscription.where(email: id).all.inject([]) do |ar, event|
      ar << load_all_assets_if_required(event)
    end
  end

  def self.load_event_subscriptions_by_user_guid(id)
    EventSubscription.where(user_guid: id).all.inject([]) do |ar, event|
      ar << load_all_assets_if_required(event)
    end
  end

  private

  def self.load_all_assets_if_required(event)
    if event.all_asset?
      event.asset_list.clear
      company = Company.with_guid(event.company_guid)
      event.asset_list = company.asset_list unless company.blank?      
    end
    event
  end

  def check_all_assets
    if all_asset?
      self.asset_list.clear
    end
  end

  def strip_empty_assets
    self.asset_list = self.asset_list.reject(&:empty?) if self.asset_list
  end

  def asset_list_included
    self.errors[:asset_list] << 'cannot be empty' if !self.all_asset and self.asset_list.empty?
  end
end
