# this class is used just as lookup table for population of adhoc report screen.
class StatementObr 
  include MongoMapper::Document

  key :name, String
  key :obr_report_id, String
  key :description, String
  key :device_params, String
  key :format_params, String
  key :show_graph, String
  key :network_guid, Boolean
  key :can_show, Boolean
  key :is_device_report, Boolean
  key :network_guid_dpid, String
  key :network_guid_id, Integer
  key :network_guid_technical_name, String
  key :network_guid_name, String
  key :company_guid_dpid, String
  key :comapny_guid_id, Integer
  key :company_guid_technical_name, String
  key :company_guid_name, String
  key :start_date_dpid, String
  key :start_date_id, Integer
  key :start_date_technical_name, String
  key :start_date_name, String
  key :end_date_dpid, String
  key :end_date_id, Integer
  key :end_date_technical_name, String
  key :end_date_name, String
  key :global_id_dpid, String
  key :global_id_id, Integer
  key :global_id_techinal_name, String
  key :global_id_name, String

  timestamps!

  scope :available_in_obr
  where(can_show: true)
 end

