require 'time_conversion'

class DeviceSubscription
  include MongoMapper::Document
  key :report_id, String
  key :company_guid, String
  key :device_guid, String
  key :group_guid, String
  key :email, String
  key :frequency, String
  key :time_zone, String
  key :delivery_time, String
  key :delivery_time_gmt, String
  key :subscription, Boolean
  key :area, String
  key :cabinet, String
  key :report_type, String
  key :report_type_name, String
  key :zenoss_instance, String
  key :report_file_name, String
  key :job_id, String
  key :start_date, String
  key :end_date, String

  # down case required for zenoss upgrade
  # def company_guid
  #   read_attribute(:company_guid).downcase  # No test for nil?
  # end

  def array_of_company_guids
    unless company_guid.blank?
      Company.parent_child_list(company_guid)
    end
  end

  def self.bulk_load(report_id, frequency)
    DeviceSubscription.delete_all
    dat = Req.where(subscription: true, :report_id => report_id, :frequency => frequency).all
    dat.each do |d|
      start_date,end_date = d.report_date_interval_for_bus
      self.walk_the_table(
        d.report_id,
        d.group_guid,
        d.company_guid,
        d.email,
        d.device_guid,
        d.frequency,
        d.delivery_time,
        d.time_zone,
        d.area,
        d.cabinet,
        d.id,
        d.report_type_for_bus,
        d.zenoss_instance_for_bus,
        d.report_filename_for_bus,
        d.job_id,
        start_date,
        end_date
      )
    end
    DeviceSubscription.destroy_all(:asset_list => "na")
  end

  def self.walk_the_table(report_id,group_id,company_guid,email,device_guid,frequency,delivery_time,time_zone,
                          area,cabinet,id,report_type,zenoss_instance,report_filename,job_id,start_date,end_date)
    if (device_guid != "")
      assets_by_name =  Array(device_guid)
    elsif (group_id != "")
      assets_by_name =  Group.where(id: group_id).first.asset_list
    end

    dat = self.create(
      :subscription_id => id,
      :report_id => report_id,
      :email => email,
      :company_guid => company_guid,
      :frequency => frequency,
      :area => area,
      :cabinet => cabinet,
      :report_type => report_type,
      :report_type_name => Req.report_type_name[report_id],
      :zenoss_instance => zenoss_instance,
      :report_file_name => report_filename,
      :job_id => job_id,
      :start_date => start_date,
      :end_date => end_date,
      :delivery_time => TimeConversion.convert_to_utc(delivery_time, time_zone),
      :asset_list => assets_by_name
    )
    dat.save
  end
end
