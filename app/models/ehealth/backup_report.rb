require 'open-uri'
class Ehealth::BackupReport
  include MongoMapper::Document

  key :company_guid
  key :filename
  key :date, Time
  key :success_rate

  #The backdated month for which we are pulling backup data.
  BACKUP_MONTHS = (0..12).map { |i| (Date.today - i.month).end_of_month }

  def date_string
    date.to_i*1000
  end

  def self.latest_backup_filename(company_guid)
    backups =  Ehealth::BackupReport.where(:company_guid => company_guid).order("date DESC").collect(&:filename).uniq.first rescue ""
  end

  def self.fetch_remote_data(site_url)
    uri = URI.parse(site_url)
    data = {'token' => "ccd5995f61224fae48f78fcb5a17a82d"}
    req = Net::HTTP::Get.new(uri.path)
    req.set_form_data(data)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme =='https'
      pem = File.read("#{Rails.root}/config/ca-certificates.crt")
      http.use_ssl = true
      http.cert = OpenSSL::X509::Certificate.new(pem)
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end
    response = http.start do |httpvar|
      httpvar.request(req)
    end
    if response.kind_of?(Net::HTTPRedirection)
      headers = {'Content-Type' => "application/json", 'Accept' => "application/json" }
      uri = URI.parse(response['location'])
      response = open(uri).read
      return response
    end
    return response.body
  end

  def self.get_backup_files(company_guid, for_all_months)
    backup_bucket_path = "/pyxie/#{company_guid}-backup_reports"
    backup_files = []
    begin
      site_url = APP_CONFIG['backup_url'] + backup_bucket_path
      response = self.fetch_remote_data(site_url)
      unless response.blank?
        parse_data =  JSON.parse(response)
        unless parse_data["result"].blank?
          parse_data["result"].each do |data|
            if(data["content_type"] == "application/json")
              file_meta = data["meta"]
              if(data["filename"].include?(".json") && data["filename"].include?("SuccessRate"))
                file_date = Date.parse("#{file_meta['report_day']}/#{file_meta['report_month']}/#{file_meta['report_year']}")
                if (for_all_months && BACKUP_MONTHS.include?(file_date))
                  backup_files << data["filename"]
                else
                  if (file_meta["report_day"] == Date.today.end_of_month.strftime("%d") && file_meta["report_month"] == Date.today.strftime("%m") && file_meta["report_year"] == Date.today.strftime("%Y"))
                    backup_files << data["filename"]
                  end
                end
              end
            end
          end
        end
      end
      backup_files
    rescue Exception=> e
      Rails.logger.error(e)
      puts e
      return backup_files
    end
  end

  def self.load_backup_data(company_guid, for_all_months = false)
    begin
      backup_files = self.get_backup_files(company_guid, for_all_months)
      backup_files.each do |filename|
        path = "/pyxie/#{company_guid}-backup_reports/#{filename}"
        site_url = APP_CONFIG['backup_url'] + path
        response = self.fetch_remote_data(site_url)
        unless response.blank?
          parse_data =  JSON.parse(response)
          unless parse_data["daily_data"].blank?
            backup_hash = parse_data["daily_data"].select{ |item| item["servername"] == "All Servers" }
            backups = backup_hash[0]["backups"]
            unless backups.blank?
              backups.each do |k,v|
                success_rate = v["success_rate"]
                backup_date = Time.zone.parse(k).utc
                if (backup_date <= Date.today + 1.day)
                  record = Ehealth::BackupReport.find_or_initialize_by_company_guid_and_date(company_guid,backup_date)
                  record.success_rate = (success_rate == "n/a" || success_rate.blank?) ? nil : success_rate * 100
                  record.filename = filename
                  record.save
                end
              end
            end
          end
        end
      end
    rescue Exception=> e
      puts "*****************#{e.message}"
      Rails.logger.error(e)
    end
  end

  #This method performing following task
  # * Provide the success_rate for the current month from local mongo collection
  def self.bulk_load(company_guid)
    filename = Ehealth::BackupReport.latest_backup_filename(company_guid)
    unless filename.blank?
      @records = Ehealth::BackupReport.where(:filename => filename).all
      # This is the hack for showing backup graph from 1st day of the month.
      # Earlier it was skipping the first day of the month
      unless  @records.blank?
        last_month_end_dt = @records.first.date.at_beginning_of_month - 1.day
        @records << Ehealth::BackupReport.new({:company_guid => company_guid,:date => last_month_end_dt,:success_rate => 0.0})
      end
      @records.sort_by!{|obj|obj.date}
    end
  end

  #This method create a mock entries for testing backup report
  # * Provide the success_rate from (Date.current-32) to (Date.current-2) from local mongo collection
  # * For missing success_rate in given range it return mock values
  def self.dummy_bulk_load(company_guid)
    # We are always showing 30 days data...
    @records= []
    date_arr = (Date.current-32...Date.current-2).to_a
    date_arr.each do |dt|
      @records << Ehealth::BackupReport.new({:company_guid => company_guid,:date => dt,:success_rate => [100,99.99,98,99.95,97,95,55,60,0,70].sample})
    end
    @records.sort_by{|obj|obj.date}
  end
end
