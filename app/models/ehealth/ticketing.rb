module Ehealth
  class Ticketing
    include MongoMapper::Document

    key :company_guid
    many :incidents, class_name: 'Ehealth::Ticketing::Incident'
    many :requests, class_name: 'Ehealth::Ticketing::Request'
    many :change_requests, class_name: 'Ehealth::Ticketing::ChangeRequest'
    many :maintenance, class_name: 'Ehealth::Ticketing::Maintenance'

    validates :company_guid,
      :presence => {:message => 'cannot be empty'},
      :uniqueness => {:case_sensitive => false}

    def self.bulk_load(company_guid)
      begin
        snc = ServiceNowClient.new
        response = snc.company_ticket_summary(company_guid)
        @records = []
        mapping = {
          'Incidents' =>[:incident_count,:incident_active_count,:incident_future_count],
          'Requests' => [:request_count,:request_active_count,:request_future_count],
          'Changes' => [:change_count,:change_active_count,:change_future_count],
          'Maintenance' => [:maint_count,:maint_active_count,:maint_future_count]
        }
        mapping.each do |k,v|
          past = response[v[0]].blank? ? nil : response[v[0]].to_i
          active = response[v[1]].blank? ? nil : response[v[1]].to_i
          future = response[v[2]].blank? ? nil : response[v[2]].to_i
          @records << OpenStruct.new(:type => k,
                                     :past => past,
                                     :active => active,
                                     :future => future)
        end
        @records
      rescue Exception=> e
        Rails.logger.error(e)
        return []
      end
    end

    class Status
      include ::MongoMapper::EmbeddedDocument
      key :state, String
      key :count, Integer
      embedded_in :ticketing
    end

    class Incident < Ehealth::Ticketing::Status
    end

    class Request < Ehealth::Ticketing::Status
    end

    class Maintenance < Ehealth::Ticketing::Status
    end

    class ChangeRequest < Ehealth::Ticketing::Status
    end
  end
end
