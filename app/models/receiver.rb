class Receiver
  include MongoMapper::Document
  key :email
  timestamps!
end