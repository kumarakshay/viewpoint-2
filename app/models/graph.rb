class Graph
  include MongoMapper::Document  
  key :name, String
  key :metric, String
  key :description, String
  key :order, Integer
  key :timeframe, String, :default => 'day'
  key :assets, Array
  key :metric_name, String
  key :uid, Array


  validates :name, :timeframe, :metric ,:assets ,:presence => {:message => 'cannot be empty'}
  
  before_validation :strip_empty_assets

  def lowercase_assets
    original = self[:assets]
    if original
      original.reject!(&:nil?)
      original.map!(&:downcase)
    end
    original
  end  
  
   alias :assets :lowercase_assets

  private 
  #TODO.. this need to go in lib for utility
  def strip_empty_assets
    self.assets = self.assets.reject(&:empty?) if self.assets
  end
            
end