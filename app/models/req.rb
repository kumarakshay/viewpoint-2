class Req
  include MongoMapper::Document

  #
  # attr_accessible :report, :device, :start_date, :end_date, :group, :device_guid
  # validates :report, presence: true
  #
  before_create :initialize_message_id,:set_report_destination
  after_create :publish_req_created_message
  attr_accessor :masquerade_email, :custom_report_name

  key :report_id, String
  key :company_guid, String
  key :user_guid, String
  key :report, String
  key :device_guid, String
  key :device, String
  key :group, String
  key :group_guid, String
  key :start_date, String
  key :end_date, String
  key :email, String
  key :frequency, String
  key :time_zone, String
  key :delivery_time, String
  key :delivery_time_gmt, String
  key :subscription, Boolean
  key :data_center, String
  key :area, String
  key :cabinet, String
  key :rd, String
  key :zone, String
  key :report_type, String
  key :message_id, String
  key :job_status, String,:default => 'pending'
  key :job_id, String
  key :destination, String
  key :acknowledged, Boolean, default: false
  key :email_me, Boolean, default: false
  key :show_graphs, Boolean,default: true
  timestamps!

  scope :created_after,
  lambda { |val|
    where(:created_at => {:$gte =>  Time.at(val.to_i).utc + 1.second})
  }
  scope :subscriptions
  where(subscription: true)


  #This method will have a mapping hash for report type name with report_id
  #The report_type_name is passed to zenoss for displaying proper report title
  #in email notifications
  def self.report_type_name(report_id)
    statement = Statement.find_by_report_id(report_id)
    statement.report_type_name
  end

  def self.new_report_id()
    Hash[Statement.order("position ASC").all.collect(&:display_name).uniq.each_with_index.map{|cu,i| [i+1, cu]}]
  end

  def report_name
    statement.report_name
  end

  def statement
    Statement.find_by_report_id(report_id)
  end

  #Return the report_type from report_metadata hash
  #report_id is used as key for getting type
  def report_type_for_bus
    statement.report_type_for_bus
  end

  #Return the report_filename from report_metadata hash
  #report_id is used as key for getting filename
  def report_filename_for_bus
    name = statement.report_filename_for_bus
    #report_name is prefix with job_id along with sample keyword if subscription is true
    report_file_name = self.subscription ? "sample-#{self.job_id}-#{name}" : "#{self.job_id}-#{name}"
    self.custom_report_name.present? ? self.custom_report_name : report_file_name
  end

  #Return the zenoss_instance from either report_metadata hash or calculating from device/guid
  #report_id is used as key for getting zenoss_instance from report_metadata
  def zenoss_instance_for_bus
    APP_CONFIG["#{statement.zenoss_instance}"]
  end

  #Return the pyxie destination URL for sending to bus
  #destination stored in VP collection and passed to bus both are different
  def pyxie_report_destination_for_bus
    if self.email_me
      self.destination
    else
      self.destination.gsub("/pyxie/","pyxie://")
    end
  end

  #Return the email destination for bus
  #email destination for masqueraded user is a sungard user email address
  def email_report_destination_for_bus
    if self.masquerade_email.blank?
      Rails.env == 'production' ? self.email : "view.point365@gmail.com"
    else
      self.masquerade_email
    end
  end

  #Return the start/end date for report formatted as integer Ex.20150101
  #If subscription is true then the start/end date range to be the first of the month
  #until the 15th if the current day of month is greater than 15,
  #else run the report for the previous month.
  def report_date_interval_for_bus
    if self.subscription
      if Date.today > (Date.today.at_beginning_of_month + 14)
        s = (Date.today.at_beginning_of_month).strftime("%Y%m%d").to_i
        e = (Date.today.at_beginning_of_month + 14).strftime("%Y%m%d").to_i
      else
        s = (Date.today.at_beginning_of_month - 1.month).strftime("%Y%m%d").to_i
        e = (Date.today.at_beginning_of_month - 1.day).strftime("%Y%m%d").to_i
      end
    else
      s = self.start_date.gsub('-','').to_i
      e = self.end_date.gsub('-','').to_i
    end
    return s,e
  end

  # Generates a job_id along with passed prefix and assign it to req object
  def generate_job_id(prefix='')
    begin
      self.job_id =  prefix + SecureRandom.hex(5)
    end
  end

  def self.has_associated_subscription?(group_guid)
    Req.where(group_guid: group_guid).present?
  end

  # return asset_list for given group
  def devices_for_group_guid()
    Group.find_by_id(self.group_guid).asset_list
  end

  # This is a callback method for bus listener
  # It is called when status response received from bus
  # The status response is passed as parameter for this method
  # Based on job_id the job_status is updated for req record
  def self.update_req_status(response)
    if response && !response.status.blank?
      req = Req.where(:job_id => response.job_id).first
      unless req.blank?
        #Update the status to "finished" only when report is physically exist in pyxie
        if is_report_exist?(req.company_guid,req.report_filename_for_bus)
          req.update_attribute(:job_status, response.status)
        end
      end
    end
  end

  #This method return true/false if given report is exist in pyxie for a company.
  def self.is_report_exist?(company_guid,filename)
    begin
      unless filename.blank?
        path = "/pyxie/#{company_guid}-user_reports"
        site_url = APP_CONFIG['vp_base_url'] + path
        uri = URI.parse(site_url)
        data = {'token' => "ccd5995f61224fae48f78fcb5a17a82d"}
        req = Net::HTTP::Get.new(uri.path)
        req.set_form_data(data)
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme =='https'
          pem = File.read("#{Rails.root}/config/ca-certificates.crt")
          http.use_ssl = true
          http.cert = OpenSSL::X509::Certificate.new(pem)
          http.verify_mode = OpenSSL::SSL::VERIFY_PEER
        end
        res = http.start do |httpvar|
          httpvar.request(req)
        end
        unless res.body.blank?
          parse_data =  JSON.parse(res.body)
          unless parse_data["result"].blank?
            reports = []
            parse_data["result"].each do |data|
              reports << data["filename"]
            end
          end
          return reports.include?(filename)
        end
      end
    rescue Exception=> e
      return false
      Rails.logger.error(e)
    end
  end

  # Publishes a message to the exchange when a new user account is created
  def publish_req_created_message
    if APP_CONFIG['bus_report_process']
      zenoss_instance = self.zenoss_instance_for_bus

      #VUPT-4650 
      if (self.company_guid == "B05B9527-CFDC-44BD-A9CC-0F85BC5C7E52" && ["6","7"].include?(self.report_id))
        zenoss_instance = "uk2"
      end
      # Get start and end date for report
      s,e = self.report_date_interval_for_bus
      #Setting proper report title in notification email #VUPT-2734
      report_request = {:type => self.report_type_for_bus, :start => s, :end => e,
                        :force => true, :report_type_name =>  Req.report_type_name(self.report_id),
                        :job_id => job_id, :report_filename => self.report_filename_for_bus
                       }
      report_request, zenoss_instance = send("report_request_#{self.report_id}".to_sym, report_request, zenoss_instance)
      parent_child_guids(report_request) unless ["1", "2", "17", "4", "15"].include?(self.report_id)

      routing_key = "report.job.create.#{zenoss_instance}"

      # Send report copy on user email address
      # removed the prompt from the form
      if self.subscription?
        report_request.merge!({:destination => "mailto:#{self.email_report_destination_for_bus}"})
      else
        report_request.merge!({:notification_email => self.email_report_destination_for_bus})
        report_request.merge!({:destination => self.pyxie_report_destination_for_bus})
      end
      #initialize the producer
      pbroker = Amqp::MessageProducer.new
      pbroker.produce(report_request, routing_key, job_id, message_id)
    end
  end

  def report_request_1(report_request, zenoss_instance)
    report_request_guid_and_instance(report_request, zenoss_instance)
  end

  def report_request_2(report_request, zenoss_instance)
    report_request_guid_and_instance(report_request, zenoss_instance)
  end

  def report_request_4(report_request, zenoss_instance)
    # powerbar report
    if (!self.cabinet.blank?)
      report_request.merge!({:cabinet => self.cabinet,:company_guid => Array(self.company_guid)})
    elsif (!self.area.blank?)
      report_request.merge!({:area => self.area,:company_guid => Array(self.company_guid)})
    else
      parent_child_guids(report_request)
    end
    return report_request, zenoss_instance
  end

  def report_request_7(report_request, zenoss_instance)
    return csv_specific_reports(report_request), zenoss_instance
  end

  def report_request_9(report_request, zenoss_instance)
    return csv_specific_reports(report_request), zenoss_instance
  end

  def report_request_11(report_request, zenoss_instance)
    return csv_specific_reports(report_request), zenoss_instance
  end

  def report_request_13(report_request, zenoss_instance)
    return csv_specific_reports(report_request), zenoss_instance
  end

  def report_request_14(report_request, zenoss_instance)
    report_request.merge!({:no_graphs => true}) unless show_graphs
    return report_request, zenoss_instance
  end

  def report_request_15(report_request, zenoss_instance)
    #group availability report
    if self.group_guid != ""
      gname = Group.where(id: self.group_guid).first.name
      report_request.merge!({:asset_guid => devices_for_group_guid})
      report_request.merge!({:extra_data  => {:group_name => gname}})
    end
    return report_request, zenoss_instance
  end

  def report_request_16(report_request, zenoss_instance)
    csv_specific_reports(report_request)
    report_request.merge!({:no_graphs => true}) unless show_graphs
    return report_request, zenoss_instance
  end

  def report_request_17(report_request, zenoss_instance)
    report_request, zenoss_instance = report_request_guid_and_instance(report_request, zenoss_instance)
    report_request.merge!(
          {:extra_data  =>
           {:componentSearch => {:meta_type => ["IpInterface", "EthernetInterface"]},
            :template =>  "network_interfaces_all.rst",
            :infoProperties => ["description"]
           }
          }
        )
    return report_request, zenoss_instance
  end

  def csv_specific_reports(report_request)
    report_request.merge!({:csv => true})
  end

  def report_request_guid_and_instance(report_request, zenoss_instance)
    if self.device_guid != ""
      report_request.merge!({:asset_guid => Array(self.device_guid)})
      z_instance = Device.where(asset_guid: self.device_guid).first.zenoss_instance
      zenoss_instance = (Rails.env == 'production') ? z_instance : (z_instance == 'uk2' ? APP_CONFIG['bus_zenoss_uk'] : APP_CONFIG['bus_zenoss_cust'])
    end
    if self.group_guid != ""
      report_request.merge!({:asset_guid => devices_for_group_guid})
      z_instance = Device.get_max_zenoss_instance(self.group_guid)
      zenoss_instance = (Rails.env == 'production') ? z_instance : (z_instance == 'uk2' ? APP_CONFIG['bus_zenoss_uk'] : APP_CONFIG['bus_zenoss_cust'])
    end
    return report_request, zenoss_instance
  end

  def method_missing(method_name, *arguments, &block)
    if method_name.to_s.include? "report_request"
      arguments
    else
      super
    end
  end

  def parent_child_guids_original(report_request)
    if APP_CONFIG['extended_parent_child_relation']
      guids = Company.recurring_parent_child_list(self.company_guid)
    else
      guids = Company.parent_child_list(self.company_guid)
    end
    company_guids = Company.where({"$and" => [:guid => guids, :zenoss_presence => true ]}).collect(&:guid)
    report_request.merge!({:company_guid => company_guids})
    report_request
  end

  # this is a refactor of the original method
  # Damien failed to tell us that the zenoss_presence switch should not be used when
  # network utilization reports are being requested.
  def parent_child_guids(report_request)
    guids = Company.parent_child_list(self.company_guid)
    report_request.merge!({:company_guid => guids})
    report_request
  end

  private

  # Before creating a req object it will generates a message_id and assign it to req object
  def initialize_message_id
    self.message_id = SecureRandom.hex
  end

  # The pyxie destination created using users company_id, report filename and job_id
  # This destination is used to stoare report in pyxie
  # If sungard user masquerading with any customer then the destination is set to sungard
  # user's email address and all the report UI notification skipped.(VUPT-2709)
  def set_report_destination
    begin
      if !self.subscription? and self.masquerade_email.blank?
        self.destination = "/pyxie/#{self.company_guid}-user_reports/#{self.report_filename_for_bus}"
      else
        self.email_me = true #Use for filtering pyxie and email reports
        self.destination = "mailto:#{self.email_report_destination_for_bus}"
      end
    rescue Exception=> e
      Rails.logger.error(e)
      return nil
    end
  end
end

