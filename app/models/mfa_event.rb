class MfaEvent
  include MongoMapper::Document
  
  key :user_guid
  key :createdAt, Time
  key :logMessage, String

  scope :for_user,
  lambda { |guid|
    where(:user_guid => guid.downcase)
  }
end
