class OnePortalUser

  include UserAspects::UserRoles
  include MongoMapper::Document
  
  key :guid
  key :email
  key :company_guid
  key :portal_name
  key :app_settings, Hash
  key :active, Boolean, default: true
  key :time_zone, String
  key :vp_admin, Boolean
  key :old_contact_guid
  key :roles, Array
  key :first_name, String
  key :last_name, String
  key :job_title, String
  key :created_by, String
  key :last_login_at, Time
  key :confirmation_token, String
  key :reset_password_token, String
  key :unlock_token, String
  key :public_persona_id
  key :service_now_guid, String 
  key :deleted_at, Time
  key :pending_email
  key :masquerade, Boolean
  key :provider, String
  key :oauth_token, String
  key :oauth_expires_at

  timestamps!

end
