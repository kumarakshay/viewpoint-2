# this class is used just as lookup table for population of adhoc report screen.
class Statement 
  include MongoMapper::Document
  # attr_accessible :description, :name, :owner
  validates :name, presence: true
  key :report_id, String
  key :name, String
  key :description, String
  key :display_name, String
  key :type, String
  key :meta_type, String
  key :file_name, String
  key :ext, String
  key :owner
  key :position
  key :bus_report_type
  key :report_type_for_bus
  key :report_filename_for_bus
  key :zenoss_instance
  one :property

  timestamps!

  def report_name
    type.blank? ? display_name : display_name + type.upcase
  end

  def report_type_name
    display_name + " " + "Report"
  end
end

class Property
  include MongoMapper::EmbeddedDocument
  embedded_in :Statement

  key :is_report_for_group, Boolean, default: false
  key :is_report_for_device, Boolean, default: false
  key :is_cabinet_and_area_report, Boolean, default: false
  key :pdf_and_csv_report, Boolean, default: false
  key :show_graph, Boolean, default: false
  key :pdf_and_csv_report, Boolean, default: false
end
