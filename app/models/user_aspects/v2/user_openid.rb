module UserAspects
	module V2
		module UserOpenid

      module ClassMethods

        def create_with_omniauth(auth)  
          Rails.logger.info("LOOKIE")
          Rails.logger.info(auth.to_yaml)
          #For unified-sso first phase 
          #We are not considering any roles getting from sso while creating/updating user in VP
          # roles = eval(auth[:extra][:raw_info][:roles])
          if auth["info"]["email"] =~ /@sungardas.com/i
            portal_name = "sungard"
          else
            portal_name = nil
          end 
          Rails.logger.info "portal_name:-#{portal_name}"
          User.create!(
            :first_name       => auth["info"]["first_name"],
            :last_name        => auth["info"]["last_name"],
            :email            => auth["info"]["email"],
            :guid             => auth["extra"]["raw_info"]['userGuid'],
            :company_guid     => auth["extra"]["raw_info"]['company_guid'],
            :time_zone        => Timezone.vp_specific_value(auth["extra"]["raw_info"]["zoneinfo"]),
            :provider         => auth["provider"],
            :oauth_token      => auth["credentials"]["token"],
            :oauth_expires_at => auth["credentials"]["expires_at"], 
            :last_login_at    => Time.now,
            :portal_name      => portal_name,
            :active => true)
        end     
      end

			module InstanceMethods
	    	# This method returns array of sso roles mapped to vp roles
	    	def sso_roles
	    		roles = Role.get_sso_roles_for(self.roles)
	    		roles
	    	end
	    	
        def update_omniauth_token(auth)
          self.oauth_token = auth["credentials"]["token"]
          self.oauth_expires_at = auth["credentials"]["expires_at"]
          self.active = true
          self.last_login_at = Time.now
          # Email change users should see an update in their emails.
          if self.email != auth["info"]['email']
            Rails.logger.info("Changing user address to #{auth['info']['email']} from #{self.email}")
            self.email = auth["info"]['email']
            self.pending_email = nil
          end
          self.save
        end

	  	  def register!
	  	  	user_exists = sso_user_exists
	  	  	if user_exists
	  	  		sso_user = get_user!
	  	  		if (self.company_guid.upcase == sso_user["company_guid"].upcase)
	  	  			success = sso_update_application
	  	  			success = sso_enable_user if success
	  	  			return success
	  	  		else
	  	  			self.sso_errors= {:message => "This user exists in SSO under a different company."}
	  	  			return false
	  	  		end
	  	  	else
	  	  		success = register_user_data
	  	  		success = update_role_data if success
	  	  		success = update_application if success
	  	  		success
	  	  	end
	  	  end

	  	  def sso_user_exists
	  	  	path, verb =  APP_CONFIG["open_id_v2_user_exists_url"]
	  	  	if self.is_allstream_user?
	  	  		url = Figaro.env.sso_v2_allstream_base_url + path
	  	  	else
	  	  		url = Figaro.env.sso_v2_base_url + path
	  	  	end
	  	  	url.gsub!(/\$email/, email) unless email.blank?
	  	  	response = fire_request(:get, url, {})
	  	  	if !response.body.blank? and [200,201].include?(response.status)
	  	  		result = JSON.parse(response.body) rescue {}
	  	  		unless result.blank?
	  	  			if (result["exists"] == "true" && !result['guid'].blank?)
	  	  				self.guid =  result['guid'] 
	  	  				return true
	  	  			else
	  	  				return false
	  	  			end
	  	  		end
	  	  	end
	  	  end

	  	  def sso_update_application
	  	  	response = sso_action('update_application')
	  	  	if !response.body.blank? and [200,201].include?(response.status)
	  	  		result = JSON.parse(response.body) rescue {}
	  	  		unless result.blank?
	  	  			self.active = true
	  	  			self.guid =  result['guid']
	  	  			self.user_sso_as_hash = result		        
	  	  			return self.save
	  	  		end
	  	  	end
	  	  	self.sso_errors= {:message => "Unable to register user #{self.email}"}
	  	  end

	  	  def sso_enable_user
	  	  	url, verb	= sso_request_url('enable', self.guid)
	  	  	response = fire_request(verb, url, {})
	  	  	[200,201,204].include?(response.status)
  	  	end

	  	  def register_user_data
	  	  	if valid?		
	  	  		response = sso_action('create') do |rh|
	  	  			unless guid.blank?
	  	  				rh.merge!('guid' => guid)
	  	  			end
	  	  			rh
	  	  		end
	  	  		if !response.body.blank? and [200,201].include?(response.status)
	  	  			result = JSON.parse(response.body) rescue {}
	  	  			unless result.blank?
	  	  				self.active = false
	  	  				self.guid =  result['guid']
	  	  				self.user_sso_as_hash = result		        
	  	  				self.save!
	  	  				return true        
	  	  			end
	  	  		elsif response.status == 409
	  	  			self.sso_errors= {:message => "User already exists"}
	  	  			return false
	  	  		elsif response.status == 412
	  	  			self.sso_errors= {:message => "Could not create user because user company is not in SSO. Please contact an administrator."}
	  	  			return false
	  	  		elsif response.status == 500
	  	  			self.sso_errors= {:message => "Internal error"}
	  	  			return false
	  	  		end			      
	  	  		self.sso_errors= {:message => "Unable to register user #{self.email}"}
	  	  	end
	  	  	false	  			
	  	  end

	  	  def update!(new_email=nil)
	  	  	success = update_user_data(new_email)
	  	  	success = update_role_data if success
	  			#success = self.active ? self.enable! : self.disable!  if success	  			
	  			success
	  		end
        # This method should be removed once SSO is able to update everything at one go. 
 	  		# This methods removes existing roles from an user and adds new if any.
 	  		def update_role_data
 	  			url, verb =  sso_request_url('update_role', self.guid)
 	  			remove_roles = {'roles' => Role.mapping.values.compact}
 	  			response = fire_request(:delete, url, remove_roles)
 	  			if response and (response.status == 204 || response.status == 200)
 	  				return true  if self.roles.blank?
 	  				return self.update_roles 
 	  			end
 	  			false
 	  		end

        # This method should be removed once SSO is able to update everything at one go. 
        def update_user_data(new_email=nil)
        	if valid?
        		options = {"new_email" => new_email} unless new_email.blank?
        		response = sso_action('update',options={}) do |hash|
        			unless new_email.blank?
        				hash.merge!('email' => new_email)
        			end
        			hash
        		end
        		result = JSON.parse(response.body)
        		if response and response.status == 200
        			if new_email.present? && self.email != new_email 
        				self.pending_email = new_email
        			end		
        			return self.save 
        		end
        		self.sso_errors={:message => "Unable to update user #{self.email}"}
        	end
        	false
        end

        {'toggle_unlock!' => 'unlock',
        	'resend_confirmation!' => 'resend_confirmation',
        	'send_password_reset!' => 'forgot_password',
        	'update_roles' => 'update_role',
        	'update_application' => 'update_application',
        	'disable!' => 'disable',		     
        	'enable!' => 'enable'
        	}.each do |key,value|		    	
        		define_method(key){			      
        			user_action(value)			    
        		}
        	end

        	def toggle_active!
        		action = self.active? ? 'disable' : 'enable'
        		response = sso_action(action)	
        		Rails.logger.debug response 
        		if response and response.status == 204
        			self.active = !self.active
        			return self.save
        		end		
        		self.sso_errors={:message => "Unable to #{action} user #{self.email}"}
        		false    
        	end

        	def delete_user!
        		response = sso_action('delete')
        		if response and response.status == 204
        			return self.destroy 
        		end
        		self.sso_errors={:message => "Unable to delete user #{self.email}"}
        		false
        	end

        	def get_user!
        		response = sso_action('get')
        		result = {}
        		if response and response.status == 200
        			result = JSON.parse(response.body) rescue {}
        		end
        		return result
        	end

        	def user_action(action)
        		response = sso_action(action) 
        		if response and (response.status == 204 || response.status == 200)
        			return self.save 
        		end
        		self.sso_errors={:message => "Unable to carry out '#{action}' for user #{self.email}"}
        		false
        	end

	  	  ##************user create****************
	  	  def fire_request(verb, url, body_hash={}) 
	  	  	Rails.logger.info("---Fired request of type #{verb}\n\t url - #{url}\n\t params #{body_hash}")
	  	  	response = oauth_api_token.request(verb,url) do |request|
	  	  		request.headers['Content-Type']  = 'application/json'
	  	  		if self.is_allstream_user?
	  	  			request.headers['Authorization'] = "Bearer #{Figaro.env.allstream_auth_token}"
	  	  		else
	  	  			request.headers['Authorization'] = "Bearer #{Figaro.env.vp_auth_token}"
	  	  		end
	  	  		request.body= body_hash.to_json unless body_hash.empty?
	  	  	end
	  	  	Rails.logger.info("---Received response #{response.inspect}")
	  	  	Rails.logger.add_metadata(request_action: "#{verb}") if Rails.logger.respond_to?(:add_metadata)
	  	  	Rails.logger.add_metadata(request_url: "#{url}") if Rails.logger.respond_to?(:add_metadata)
	  	  	Rails.logger.add_metadata(request_data: "#{body_hash}") if Rails.logger.respond_to?(:add_metadata)
	  	  	Rails.logger.add_metadata(request_response_body: "#{response.inspect}") if Rails.logger.respond_to?(:add_metadata)
	  	  	response
	  	  end

	  	  def sso_action(action, options={}, &block)	  	  	
	  	  	begin
	  	  		options ={}
	  	  		url, verb =  sso_request_url(action, self.guid)
	  	  		if action == 'create'
	  	  			options = registration_hash
	  	  			options = yield(options) if block_given?
	  	  		elsif action == 'update'
	  	  			options = update_hash
	  	  			options = yield(options) if block_given?
	  	  		elsif action == 'update_role'
	  	  			options = role_hash
	  	  			options = yield(options) if block_given?
	  	  		elsif action == 'update_application'
	  	  			options = application_hash
	  	  			options = yield(options) if block_given?
	  	  		end
	  	  		return fire_request(verb, url, options)
	  	  	rescue Exception => e	  	  		
	  	  		Rails.logger.error "Error during SSO operation: #{$!}
	  	  		\nBacktrace:\n\t#{e.backtrace.join("\n\t")}"	  
	  	  		false	  		
	  	  	end	  	  	
	  	  end

	  	  def registration_hash
	  	  	create_hash("email","first_name","last_name","company_guid","timezone")
	  	  end

	  	  def update_hash
	  	  	create_hash("email","first_name","last_name","timezone")
	  	  end

	  	  def role_hash
	  	  	options = create_hash("sso_roles")
	  	  	options['roles'] = options.delete('sso_roles')
	  	  	return options
	  	  end

	  	  def application_hash
	  	  	{
	  	  		"applications" => ["Service Now","Viewpoint"]
	  	  	} 
	  	  end	

	  	  def sso_request_url(action, guid)
	  	  	path, verb =  APP_CONFIG["open_id_v2_#{action}_url"]
	  	  	if self.is_allstream_user?
	  	  		url = Figaro.env.sso_v2_allstream_base_url + path
	  	  	else
	  	  		url = Figaro.env.sso_v2_base_url + path
	  	  	end
	  	  	url.gsub!(/\$guid/, guid) unless guid.blank?
	  	  	return url, verb.to_sym
	  	  end

	  	  def create_hash(*args)
	  	  	data_hash = {}
	  	  	args.each do |key|
	  	  		data_hash.merge!( key => self.send(key))
	  	  	end
	  	  	data_hash
	  	  end

	  	  def oauth_api_token
	  	  	if @api_token.blank?
	  	  		ssl = {:verify => true, :ca_file => "#{Rails.root}/config/sso-2.crt"}		    	  
	  	  		if self.is_allstream_user?
	  	  			api_key = Figaro.env.sso_v2_allstream_api_key
	  	  			api_secrete = Figaro.env.sso_v2_allstream_api_secret
	  	  		else
	  	  			api_key = Figaro.env.sso_v2_viewpoint_api_key
	  	  			api_secrete = Figaro.env.sso_v2_viewpoint_api_secret
	  	  		end
	  	  		client = OAuth2::Client.new( "#{api_key}",
	  	  			"#{api_secrete}",
	  	  			{:ssl => ssl, :raise_errors => false})
	  	  		@api_token = OAuth2::AccessToken.new(client,"#{Figaro.env.sso_v2_viewpoint_api_token}")
	  	  	end
	  	  	@api_token
	  	  end



	  	end

	  	def self.included(receiver)
	  		receiver.extend         ClassMethods
	  		receiver.send :include, InstanceMethods
	  		receiver.key :time_zone_iana
	  	end

		###### These methods would be used when SSO  wouls accept everthing in one request ##########

		# def update_org!(new_email=nil)
	  #   if valid?
	  #   	options = {"new_email" => new_email} unless new_email.blank?
	  #     response = sso_action('update',options) do |hash|
	  #     	unless new_email.blank?
	  #     		hash.merge!('email' => new_email)
	  #     	end
	  #     	hash
	  #     end
	  #     if response and response.status == 204
   	#    	  if new_email.present? && self.email != new_email 
   	#          self.pending_email = new_email
   	#        end		          
	  #       return self.save 
	  #     end 
	  #     self.sso_errors={:message => "Unable to update user #{self.email}"}
	  #   end
   	# 		false
   	#  end		

	  # def register!
		#   if valid?		
			#     response = sso_action('create') do |rh|
		  #     	unless guid.blank?
			# 			rh.merge!('guid' => guid)
		  #     	end
			#      rh
		  #     end
		  #     if response and [200,201].include?(response.status)
		  #     	result = response.parsed
			# 		unless result.blank?
			# 			self.active = false
			#        self.guid =  result['user']['guid']
			#        self.user_sso_as_hash = result['user'].to_hash				        
			#        self.save!
		  #         return true        
		  #       end
		  #     end			      
		  #     self.sso_errors= {:message => "Unable to register user #{self.email}"}
		  #   end
    	# 	  false
  		# end     
   		###### ------------------END ------------------------------------- ##########  		
 		end
	end	
end