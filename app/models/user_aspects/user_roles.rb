module UserAspects
	module UserRoles
		def self.included(base)
			active_roles =  Role.all_active_roles.inject([]){|ar,r| ar << r.name}
			active_roles.each do |role_name|
				base.class_eval do
					define_method :"is_#{role_name}?" do
						begin
							role? "#{role_name}"
						rescue
							false
						end
					end
				end
			end
		end		
	end
end