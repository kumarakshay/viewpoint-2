module UserAspects
  module V1
    module SnWebApi
      #Retrieves system user from service now.

      def sys_user
        begin
          # sys_user = SGTools::ITSM::SystemUser.where(u_guid: guid).first || SGTools::ITSM::SystemUser.new
          if not self.new_record?
            SGTools::ITSM::SystemUser.where(u_guid: guid).first 
          elsif self.old_contact_guid.present? 
            SGTools::ITSM::SystemUser.where(u_guid: old_contact_guid).first
          else
            SGTools::ITSM::SystemUser.new
          end
        rescue
          nil
        end
      end  

      # Registers new user with openid. 
      # In case these values are cleared out default values are added to service now to blank out these fields.  
      # '0' is used in case of normal text fields for email field 'null@sungard.com' is used.
      def save_sn_data!(user)
        return true if self.new_record? && self.phone_1.blank? && self.phone_2.blank? && self.secondary_email.blank?
        return true if self.is_sungard? # VUPT-1536
        sn_user = prepare_user
        sn_user.mobile_phone = user["cell_phone"].blank? ? "0" : user["cell_phone"]
        sn_user.u_alternate_phone_1 = user["phone_3"].blank? ? "0" : user["phone_3"]
        sn_user.u_alternate_phone_2 = user["phone_4"].blank? ? "0" : user["phone_4"]
        sn_user.u_alternate_email = user["secondary_email"].blank? ? 'null@sungard.com' : user["secondary_email"]
        sn_user.u_alternate_phone_1_ext = user["phone_3_ext"].blank? ? "0" : user["phone_3_ext"]
        sn_user.u_alternate_phone_2_ext = user["phone_4_ext"].blank? ? "0" : user["phone_4_ext"]
        sn_user.u_business_phone_ext = user["phone_number_ext"].blank? ? "0" : user["phone_number_ext"]
        sn_user.u_mobile_phone_ext = user["cell_phone_ext"].blank? ? "0" : user["cell_phone_ext"] 
        sn_user.u_alternate_phone_1_notes = user["phone_3_notes"]
        sn_user.u_alternate_phone_2_notes = user["phone_4_notes"]
        sn_user.u_mobile_phone_notes = user["cell_phone_notes"]
        sn_user.u_business_phone_notes = user["business_phone_notes"]
        sn_user.phone = user["phone_number"]
        #print_data(sn_user)
        sn_user.save!
      end

      def prepare_user
        sn_user = SGTools::ITSM::SystemUserWriter.new 
        sn_user.email = self.email
        sn_user.active = true
        sn_user.first_name = self.first_name
        sn_user.last_name = self.last_name
        sn_user.company = self.company_guid
        sn_user.u_legacy_contact_id = self.guid
        #attribute locked_out set to false for differentiating contact_user and deployed_user in service now
        sn_user.locked_out = false
        sn_user
      end


      def print_data(sn_user)
        Rails.logger.info "Portal user------before saving------------------"
        Rails.logger.info "Portal user active:- #{sn_user.active}"
        Rails.logger.info "Portal user locked_out:- #{sn_user.locked_out}"
        Rails.logger.info "Portal user company:- #{sn_user.company}"
        Rails.logger.info "Portal user u_legacy_contact_id:- #{sn_user.u_legacy_contact_id}"
        Rails.logger.info "Portal user email:- #{sn_user.email}"
        Rails.logger.info "Portal user first_name:- #{sn_user.first_name}"
        Rails.logger.info "Portal user last_name:- #{sn_user.last_name}"
        Rails.logger.info "Portal user u_alternate_phone_1:- #{sn_user.u_alternate_phone_1}"
        Rails.logger.info "Portal user u_alternate_phone_2:- #{sn_user.u_alternate_phone_2}"
        Rails.logger.info "Portal user u_alternate_email:- #{sn_user.u_alternate_email}"
        Rails.logger.info "Portal user phone:- #{sn_user.phone}"
        
        Rails.logger.info "Portal user mobile_phone:- #{sn_user.mobile_phone}"
        Rails.logger.info "Portal user cell phone ext :- #{sn_user.u_mobile_phone_ext}"
        Rails.logger.info "Portal user phone 1 ext :- #{sn_user.u_alternate_phone_1_ext}"
        Rails.logger.info "Portal user phone 2 ext :- #{sn_user.u_alternate_phone_2_ext}"
        Rails.logger.info "Portal user business phone ext :- #{sn_user.u_business_phone_ext}"

        Rails.logger.info "Portal user phone 1 notes :- #{sn_user.u_alternate_phone_1_notes}"
        Rails.logger.info "Portal user phone 2 notes :- #{sn_user.u_alternate_phone_2_notes}"
        Rails.logger.info "Portal user mobile phone notes :- #{sn_user.u_mobile_phone_notes}"
        Rails.logger.info "Portal user business phone notes :- #{sn_user.u_business_phone_notes}"
        Rails.logger.info "------**************** ------------------"
      end
    end   
  end
end
