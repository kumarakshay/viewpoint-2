class DnsAudit
  include MongoMapper::Document
  key :user
  key :action
  key :record
  key :zone
  key :user_guid
  key :company_guid
  key :customer_name
  key :type
  key :record_type
  timestamps!

  # Scope to search records for given customer guid.
  scope :for_customer,
    lambda {|customer_guid|
    where({ "company_guid" => { :$in => [customer_guid.downcase, customer_guid.upcase, customer_guid] } })
  }
end