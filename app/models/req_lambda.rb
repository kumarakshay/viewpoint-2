class ReqLambda
  include MongoMapper::Document

  key :job_id, String
  key :filename, String
  key :email, String
  key :is_emailed, Boolean, default: false
  timestamps!
end
