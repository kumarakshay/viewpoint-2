module AdhocAspects
	module ZenossReports
	 # generates a report based on id and incoming params from req
        def gen_ehealth_report(id)
	        req = Req.find(id)
		    if req.subscription == true
		      s = "20120801"
		      e = "20120807"
		    else
		      s = req.start_date
		      s = s.gsub(/[^0-9]/, '')
		      e = req.end_date
		      e = e.gsub(/[^0-9]/, '')
		    end
	        company_guid = req.company_guid  
		    case req.report_id.to_s

	   		  # health report
			  # should run for device or group of devices
			  # zenoss: based on devices
		      when "1"
		      	zenoss_instance = APP_CONFIG['ad_hoc_zenoss']
		        if req.device_guid != ""  
		          report_on = Device.where(asset_guid: req.device_guid).first.asset_guid
		          zenoss_instance = Device.where(asset_guid: req.device_guid).first.zenoss_instance
		        end
		        if req.group_guid != ""
		          zenoss_instance = Device.get_max_zenoss_instance(req.group_guid)
		          report_on = Device.group_guid_with_zenoss_instance(req.group_guid)
		          #Replacing %23 with #
		          report_on = report_on.map {|item| item.gsub(/%23/,'#')}
		          report_on = report_on.join(",")
		        end
		        report= SGTools::Zenoss::Report.new('device_pdf_report',
		         {:name => report_on, :start => s , :end=> e, :z_instance => zenoss_instance })

		      # raw performance csv report
			  # should run for device or group of devices
			  # zenoss: based on devices
		      when "2"    
		        zenoss_instance = APP_CONFIG['ad_hoc_zenoss']
		        if req.device_guid != "" 
		          report_on = Device.where(asset_guid: req.device_guid).first.asset_guid
		          zenoss_instance = Device.where(asset_guid: req.device_guid).first.zenoss_instance
		        end
		        if req.group_guid != ""
		          zenoss_instance = Device.get_max_zenoss_instance(req.group_guid)
		          report_on = Device.group_guid_with_zenoss_instance(req.group_guid)
		          #Replacing %23 with #
		          report_on = report_on.map {|item| item.gsub(/%23/,'#')}
		          report_on = report_on.join(",")
		        end
		        report= SGTools::Zenoss::Report.new('device_csv_glance',
		         { :guid => report_on, :start => s  , :end=> e, :z_instance => zenoss_instance})

		      # network usage
			  # should run for multiple company guids (parent child)
			  # zenoss: net 
		      when "3" 
		        zenoss_instance = APP_CONFIG['ad_hoc_zenoss_network_report'] 
		        report_on = company_child_list(company_guid)
		        report= SGTools::Zenoss::Report.new('internet_usage_pdf_report',
		         { :company_guid => report_on, :start => s , :end=> e, :z_instance => zenoss_instance})

		      # powerbar report
			  # should run for device or group of devices
			  # zenoss: uk 
		      when "4" 
		      	zenoss_instance = APP_CONFIG['ad_hoc_zenoss_power_report']
			    if (req.area != "" || req.cabinet != "")
			       report_on = company_guid
			       if (req.cabinet != "") #if cabinet exists 
			       	 report= SGTools::Zenoss::Report.new('powerbar_pdf_report_cabinet',
			       	  {:company_guid => report_on, :start => s , :end=> e, :cabinet => req.cabinet, :z_instance => zenoss_instance})  
			       else
			         report= SGTools::Zenoss::Report.new('powerbar_pdf_report_area',
			          {:company_guid => report_on, :start => s , :end=> e, :area => req.area, :z_instance => zenoss_instance})  
			       end
			        #"https://#{zenoss_instance}.zen.sgns.net/zport/dmd/powerbar_pdf_report?company_guid=#{report_on}&start=#{s}&end=#{e}" + location
			    else  
			       report_on = company_child_list(company_guid)
			       report= SGTools::Zenoss::Report.new('powerbar_pdf_report',
			       	{:company_guid => report_on, :start => s , :end=> e, :z_instance => zenoss_instance})  
			    end 
		             
		      when "5"  
		        
		      # company device availability pdf
			  # should run for multiple company guids (parent child)
			  # zenoss: uk
		      when "6" 
		      	zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_device_availability_report_pdf', 
		         {:company_guids => report_on, :start => s , :end=> e, :z_instance => zenoss_instance})

		      # company device availability csv
			  # should run for multiple company guids (parent child)
			  # zenoss: uk
		      when "7"  
		        zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_device_availability_report',
		         {:company_guids => report_on, :start => s , :end=> e, :z_instance => zenoss_instance})
		      
		      # company network availability pdf
			  # should run for multiple company guids (parent child)
			  # zenoss: net
		      when "8"
		      	zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_network_availability_report_pdf',
		         {:company_guids => report_on, :start => s, :end=> e, :z_instance => zenoss_instance })

		      # company network availability csv
			  # should run for multiple company guids (parent child)
			  # zenoss: net  
		      when "9" 
		        zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_network_availability_report',
		         {:company_guids => report_on, :start => s, :end=> e, :z_instance => zenoss_instance })

		      # company power availability pdf
			  # should run for multiple company guids (parent child)
			  # zenoss: uk
		      when "10"
		        zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_power_availability_report_pdf', 
		         {:company_guids => report_on, :start => s, :end=> e, :z_instance => zenoss_instance })

		      # company power availability csv
			  # should run for multiple company guids (parent child)
			  # zenoss: uk
		      when "11"
		      	zenoss_instance = APP_CONFIG['ad_hoc_cust']
		      	report_on = company_child_list(company_guid)
        		report = SGTools::Zenoss::Report.new('company_power_availability_report_csv', 
        		 {:company_guids => report_on, :start => s, :end=> e, :z_instance => zenoss_instance })
        		
		      # company availability pdf
			  # should run for multiple company guids (parent child)
			  # zenoss: uk
		      when "12"
		        zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_availability_report_pdf',
		         {:company_guids => report_on, :start => s , :end=> e, :z_instance => zenoss_instance})
		     
		      # company availability csv
			  # should run for multiple company guids (parent child)
			  # zenoss: uk
		      when "13"            
		        zenoss_instance = APP_CONFIG['ad_hoc_cust']
		        report_on = company_child_list(company_guid)
		        report = SGTools::Zenoss::Report.new('company_availability_report_csv', 
		         {:company_guids => report_on, :start => s , :end=> e, :z_instance => zenoss_instance})
		    else
		      raise "Unable to create report for #{req.report_id}"
		    end

		    Rails.logger.info("*************************")
		    Rails.logger.info("   inspect report: #{report.inspect}")
		    Rails.logger.info("request report id: #{req.report_id.to_s}")
		    Rails.logger.info("        report_on: #{report_on}")
		    Rails.logger.info("      device guid: #{req.device_guid}")
		    Rails.logger.info("       group guid: #{req.group_guid}")
		    Rails.logger.info("     company guid: #{req.company_guid}")
		    Rails.logger.info("         end date: #{e}")
		    Rails.logger.info("       start date: #{s}")
		    Rails.logger.info("*************************")
		    return report.render
     	end

     	def company_child_list(company_guid)
     	  report_on = Company.parent_child_list(company_guid)
		  report_on = report_on.join(",")
		  report_on
     	end
	end
end
