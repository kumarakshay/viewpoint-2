# require 'yajl'

# class Device < DatabaseConnections::ServiceCenter
#   extend OracleFilterable
#   extend ElasticServiceCenter
#   include ComponentMongo

#   self.table_name='devicem1'
#   self.primary_key='logical_name'

#   belongs_to  :company, :foreign_key => 'company_use'
#   belongs_to  :company_own, :class_name => 'Company', :foreign_key => 'company_own'
#   belongs_to  :company_use, :class_name => 'Company', :foreign_key => 'company_use'
#   belongs_to  :contracting_company, :class_name => 'Company', :foreign_key => 'contracting_company'
#   has_one     :ip_record, :foreign_key => 'logical_name'
#   has_one     :ip_hostname,  :foreign_key => 'logical_name'
#   has_one     :zenoss_instance, :foreign_key => 'guid', :primary_key => 'asset_guid'
#   has_many    :device_support_contracts, :foreign_key => 'logical_name', :primary_key => 'logical_name'
#   has_many    :support_contracts, :through => :device_support_contracts, :foreign_key => 'logical_name', :primary_key => 'logical_name'

#   PRIORITIES = {
#     0 => 'Managed',
#     1 => 'Non-Managed',
#     2 => 'Stock',
#     3 => 'Monitored',
#     4 => 'Unknown'
#   }

#   # for elastic
#   settings :number_of_shards => 5,
#     :number_of_replicas => 1,
#     :analysis => {
#       :analyzer => {
#         :keyword_lowercase => {
#           "tokenizer" => "keyword",
#           "filter"    => ["lowercase"],
#           "type"      => "custom"
#         }
#       } 
#     } do 
#       mapping do
#         indexes :id,               type: 'string', analyzer: 'keyword'
#         indexes :type,             type: 'string', analyzer: 'keyword'
#         indexes :asset_guid,       type: 'string', analyzer: 'keyword_lowercase'
#         indexes :ip_address
#         indexes :ip_host,          type: 'string', analyzer: 'keyword_lowercase'
#         indexes :hostname,         type: 'string', analyzer: 'keyword_lowercase', boost: 5
#         indexes :logical_name,     type: 'string', analyzer: 'keyword_lowercase'
#         indexes :network_name,     type: 'string', analyzer: 'keyword_lowercase'
#         indexes :protocol_address, type: 'string', analyzer: 'keyword_lowercase'
#         indexes :company do
#           indexes :company,        analyzer: 'keyword'
#           indexes :company_code,   type: 'string', analyzer: 'keyword_lowercase'
#           indexes :company_guid,   :index => :not_analyzed
#           indexes :company_prefix, type: 'string', analyzer: 'keyword_lowercase'
#           indexes :customer_id,    type: 'string', analyzer: 'keyword_lowercase'
#           indexes :naming_prefix,  type: 'string', analyzer: 'keyword_lowercase'
#         end
#         indexes :description,      type: 'string', analyzer: 'keyword_lowercase'
#         indexes :istatus,          type: 'string', analyzer: 'keyword_lowercase'
#         indexes :zenoss_instance,  type: 'string', analyzer: 'keyword_lowercase'
#         indexes :serial_number,  type: 'string', analyzer: 'keyword_lowercase'
#         indexes :primary_path,  type: 'string', analyzer: 'keyword_lowercase'
#       end
#     end

#   def to_indexed_json
#     type_us = ''
#     zen_inst = ''
#     unless type.nil?
#       type_us = type.underscore
#     end

#     unless zenoss_instance.nil?
#       zen_inst = zenoss_instance.instance_name
#     end
    
#     {
#       :id               => id,
#       :type             => type_us,
#       :asset_guid       => asset_guid,
#       :ip_address       => ip_address,
#       :ip_host          => ip_host,
#       :hostname         => hostname,
#       :logical_name     => logical_name,
#       :network_name     => network_name,
#       :protocol_address => protocol_address,
#       :company          => company,
#       :description      => description,
#       :istatus          => istatus,
#       :zenoss_instance  => zen_inst,
#       :serial_number    => serial_number,
#       :primary_path     => primary_path
#     }.to_json
#   end

#   # used by core_device?
#   def network_device?
#     self.class.ancestors.include?(NetworkDevice)
#   end

#   # is device a core network device?
#   def core_device?
#     if self.network_device?
#       return self.description.include?('type=core')
#     else
#       return false
#     end
#   end

#   # return list of companies based on whether or not it is a 
#   # core device
#   def companies
#     key = Base64.urlsafe_encode64(self.asset_guid + "_companies")
#     companies = Rails.cache.fetch(key, expires_in: 1.minute) do
#       comps = []

#       unless self.company_use.blank?
#         use_details = self.company_use.sn_company_details || SnCompanyDetails.new(:name => self.company_use.customer_id)
#         comps << { 'company' => { 'customer_name' => self.company_use.company || '',
#                     'snt_code' => self.company_use.naming_prefix || '',
#                     'sde' => use_details.care_rep || '',
#                     'ae' => use_details.account_exec || '',
#                     'oracle_id' => use_details.oracle_id || '',
#                     'interfaces' => []},
#                   'type' => 'user' }
#       end

#       unless self.company_own.blank?
#         own_details = self.company_own.sn_company_details || SnCompanyDetails.new(:name => self.company_own.customer_id)
#         comps << { 'company' => { 'customer_name' => self.company_own.company || '',
#                       'snt_code' => self.company_own.naming_prefix || '',
#                       'sde' => own_details.care_rep || '',
#                       'ae' => own_details.account_exec || '',
#                       'oracle_id' => own_details.oracle_id || '',
#                       'interfaces' => []},
#                   'type' => 'owner' }     
#       end 

#       unless self.contracting_company.blank?
#         con_details = self.contracting_company.sn_company_details || SnCompanyDetails.new(:name => self.contracting_company.customer_id)
#         comps << { 'company' => { 'customer_name' => self.contracting_company.company || '',
#                       'snt_code' => self.contracting_company.naming_prefix || '',
#                       'sde' => con_details.care_rep || '',
#                       'ae' => con_details.account_exec || '',
#                       'oracle_id' => con_details.oracle_id || '',
#                       'interfaces' => []},
#                   'type' => 'contractor' }                                
#       end

#       if self.core_device?
#         self.affected_companies.each do |c|
#           comps << {'company' => c, 'type' => 'customer'}
#         end
#       end
#       comps
#     end

#     return companies
#   end

#   def priority
#     PRIORITIES[self.device_priority.to_i] || 'Unknown'
#   end

#   def ip_address
#     return self.ip_record.ip_address
#   rescue
#     return ''
#   end

#   def ip_host
#     return self.ip_hostname.ip_host
#   rescue
#     return ''
#   end

#   def name 
#     hostname.downcase
#   end

#   def hostname
#     return self.ip_host.split('.').first.upcase
#   rescue
#     return ''
#   end

#   def self.find_by_hostname(hostname)
#     IpHostname.find_by_ip_host(hostname).device
#   end

#   # overrides for STI that fixes the broken mapping with the
#   # devicem1 table in service center. values in the type column
#   # must be converted to proper class name and then back to lowercase
#   def self.find_sti_class(type_name)
#     unless type_name.nil?
#       type_name = 'display' if type_name.eql?('monitor')
#       type_name.capitalize!
#     end
#     super
#   end

#   def self.type_condition(table = arel_table)
#     sti_column = table[inheritance_column.to_sym]
#     sti_names  = ([self] + descendants).map { |model| model.sti_name.downcase }

#     sti_column.in(sti_names)
#   end

#   def self.search_columns(cols)
#     cols.map do |c|
#       "#{self.quoted_table_name}.#{connection.quote_column_name(c)}"
#     end
#   end


#   def device_health
#     health = 'ok'
#     event_totals = self.event_totals
#     unless event_totals.blank?
#       if event_totals[0][:total] > 0
#         health = 'critical'
#       elsif event_totals[1][:total] > 0
#         health = 'error'
#       elsif event_totals[2][:total] > 0
#         health = 'warning'
#       end
#     end

#     health
#   end

#   #
#   # service now methods
#   #

#   def incident_count
#     Rails.cache.fetch("device_model_incident_count_#{self.asset_guid}", :expires_in => 1.minutes) do
#       Rails.logger.debug "Begin SGTools::ITSM Incident pull for #{self.asset_guid}"
#       total = SGTools::ITSM::Incident.find_all_by_device_id(self.asset_guid,active: true).total
#       Rails.logger.debug "End SGTools::ITSM Incident pull"
#       total
#     end
#   end

#   def change_count
#     Rails.cache.fetch("device_model_change_count_#{self.asset_guid}", :expires_in => 1.minutes) do
#       Rails.logger.debug "Begin SGTools::ITSM Change Request pull for #{self.asset_guid}"
#       total = SGTools::ITSM::ChangeRequest.find_all_by_device_id(self.asset_guid,active: true).total
#       Rails.logger.debug "End SGTools::ITSM Change Request pull"
#       total 
#     end
#   end

#   def recent_changes

#     Rails.cache.fetch("device_model_recent_changes_count_#{self.asset_guid}", :expires_in => 1.minutes) do
#       return SGTools::ITSM::ChangeRequest.find_all_by_device_id(
#         self.asset_guid,
#         opened_at: Date.today-2.weeks..Date.today+2.weeks
#       )
#     end
#   end


#   def active_mx_window_count
#     Rails.cache.fetch("device_model_active_mx_count_#{self.asset_guid}", :expires_in => 1.minutes) do  
#       SGTools::ITSM::ScheduleSpan.find_all_by_device_id(
#         self.asset_guid,
#         active: true
#       ).count
#     end
#   end
  
#   #
#   # zenoss methods
#   #

#   def events(limit=nil,order=nil)
#     Event.where(assetGuid: self.asset_guid).limit(limit).order(order)
#   end

#   def event_totals
#     Event.totals([self.asset_guid])
#   end

#   def graph_for_token(token,opts={})
#     width = opts['width'] || 'default'
#     key = Base64.urlsafe_encode64("graphs-#{self.id}")
#     Rails.logger.debug(key)
#     graphs_meta unless Rails.cache.exist?(key) 
#     h = Rails.cache.read(key)
#     z = Zenoss.new
#     key = Base64.urlsafe_encode64("graph-#{self.id}--token-#{token}-width-#{width}")
#     g = Rails.cache.fetch(key, expires_in: 5.minutes) do
#       i = z.device_graph_image(self.zen_instance,h[token][:graph_path],opts)
#     end
#     g
#   end

#   def graphs_meta(opts={})
#     return {} unless self.zen_instance?

#     key = Base64.urlsafe_encode64("graphs-#{self.id}")
#     Rails.cache.fetch(key, expires_in: 15.minutes) do
#       @zenoss ||=Zenoss.new
#       graphs = @zenoss.graphs_for_device(self.zen_instance,self.asset_guid)
#       i=0
#       ret = graphs.inject({}) do |h,g|
#         i+=1
#         key = i.to_s
#         url = URI.parse(g['url'])
#         url.query = URI.decode_www_form(url.query).select{|i| i[0] == 'gopts'}.map{|w| w.join('=')}.join('&')
#         h[key] = {title: g['title'], zenoss_instance: self.zen_instance, graph_path: url.to_s}
#         h
#       end
#       #Rails.logger.debug(ret)
#       ret
#     end
#   rescue Zenoss::NetworkNotFoundError => e
#     logger.error("graphs_meta: #{e.message}")
#     []
#   end

#   def friendly_graphs_meta(opts={})
#     h = graphs_meta(opts)
#     h.map do |k,v|
#       {token: k, title: v[:title]}
#     end
#   end

#   def zen_instance?
#     ! self.zenoss_instance.blank?
#   end

#   def zen_instance
#     if self.zen_instance?
#       self.zenoss_instance.instance_name
#     else
#       nil
#     end
#   end

#   def zenoss_link
#     unless self.asset_guid.blank? || !self.zen_instance?
#       "https://#{self.zen_instance}.zen.sgns.net/zport/dmd/guid-redirect?assetguid=#{self.asset_guid}"
#     else
#       nil
#     end
#   end

#   def zenoss_pdf_report_link
#     link = nil
#     unless self.asset_guid.blank? || !self.zen_instance?
#       if self.zen_instance && !self.zenoss_details.blank?
#         link = "https://#{self.zen_instance}.zen.sgns.net/zport/dmd/device_pdf_report?name=#{self.zenoss_details['name']}"
#       end
#     end

#     link
#   end

#   def zenoss_agg_traffic_report_link
#     link = nil
#     unless self.asset_guid.blank? || !self.zen_instance?
#       if self.zen_instance && !self.zenoss_details.blank?
#         link = "https://#{self.zen_instance}.zen.sgns.net/zport/RenderServer/plugin?name=aggregateDeviceTraffic&arg=-E&device=#{self.zenoss_details['name']}&start=-36h&width=600&height=140"
#       end
#     end

#     link
#   end

#   def zenoss_details
#     d = self.details_from_zenoss('name',true)
#     unless d.blank?
#       d = d[0] if(d.class == Array)
#       d['systems'] = d['systems'].first['id'] if d['systems']
#       d['groups'] = d['groups'].map{|g| g['name']}
#       d['location'] = d['location']['name'] if d['location']
#     end
#     d
#   end

#   # details are cached for 10 minutes
#   def details_from_zenoss(m,parse_json)
#     return nil unless self.zen_instance?
#     key = Base64.urlsafe_encode64("{id: #{self.id}, m: #{m}, title: \"details\"}")
#     data = Rails.cache.fetch(key,{expires_in: 4.hours}) do 
#       z = Zenoss.new
#       data = z.__send__(:"device_details_by_#{m}",self.__send__(m),self.zen_instance)
#     end
#     return Yajl.load(data) if parse_json
#     return data
#   rescue Zenoss::DeviceNotFoundError=>e
#     return nil    
#   end

#   def friendly_component_list
#     component_types = component_list
#     component_types.each do |ct|
#       ct.delete('type')
#     end
#   end

#   def component_list
#     return [] unless self.zen_instance?
#     key = Base64.urlsafe_encode64("{id: #{self.id}, m: \"component_list\"}")
#     data = Rails.cache.fetch(key,{expires_in: 4.hours}) do 
#       z = Zenoss.new
#       data = z.device_component_list(self.zen_instance,self.asset_guid)
#       filtered = nil
#       if data
#         comps = Yajl.load(data)
#         filtered = []
#         comps.each do |c|
#           ComponentMongo::zenoss_component_api.each do |k,v|
#             if v['component_type'] == c['type']
#               c['component_type'] = k 
#               filtered << c
#             end
#           end
#         end
#         filtered
#       end
#       filtered
#     end
#     return data
#   rescue Zenoss::DeviceNotFoundError=>e
#     return []
#   end

#   def thresholds
#     return nil unless self.zen_instance?
#     key = Base64.urlsafe_encode64("{id: #{self.id}, m: \"thresholds\"}")
#     data = Rails.cache.fetch(key,{expires_in: 4.hours}) do 
#       z = Zenoss.new
#       data = z.device_thresholds(self.zen_instance,self.asset_guid)
#       Yajl.load(data)
#     end
#   rescue Zenoss::DeviceNotFoundError=>e
#     return nil
#   end


#   # end zenoss
# end
