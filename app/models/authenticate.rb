require 'net/http'
require "uri"
# Calls SgmcApp::Application::SERVICE_CENTER_API_CONFIG.url /servicenow/users/auth 
#
# The requirements for implementing Authenticate are:
#
# * <tt>access to servicenow/users/auth</tt>
# * <tt>require 'net/http'</tt>
# * <tt>require 'uri'</tt>
#
# Expects: 
# 
# * <tt>email</tt>
# * <tt>password</tt>
#
# Returns HTTP status code:
#
# * <tt>200 OK</tt>
# * <tt>401 Unauthorized</tt>
class Authenticate
  def self.validate_user(email, password)
    if APP_CONFIG['current_user']
      return 200
    else
      pw = CGI::escape(password)
      em = CGI::escape(email)
      uri = URI.parse(APP_CONFIG['service_center_url'] + "/servicenow/users/auth?user_id=#{em}&passwd=#{pw}")
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 30
      http.read_timeout = 30
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      request['Authorization'] = 'Basic ' + APP_CONFIG['service_center_auth']
      response = http.request(request)
      response.code
    end  
  end
end