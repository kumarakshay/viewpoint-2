require 'time_conversion'

class MsaSubscription
  include MongoMapper::Document
  key :report_id, String
  key :company_guid, String
  key :email, String
  key :frequency, String
  key :time_zone, String
  key :delivery_time, String
  key :delivery_time_gmt, String

  # down case required for zenoss upgrade
  def company_guid
    read_attribute(:company_guid).downcase  # No test for nil?
  end

  def self.bulk_load(report_id)
    MsaSubscription.delete_all
    if ["10","11"].include?(report_id)
      report_type = (report_id == "10") ? "PDF" : "XLS"
      dat = ReqObr.where(:subscription => true, :isCancelled => false, :is_idpu => true, :report_id => "2",:report_type => report_type).all
    else
      dat = Req.where(subscription: true, :report_id => report_id).all
    end
    dat.each do |d|
      if d.report_id == "2" && report_type =="PDF"
        new_report_id = "10"
      elsif d.report_id == "2" && report_type =="XLS"
        new_report_id = "11"
      else
        new_report_id = d.report_id
      end
      self.walk_the_table(new_report_id, d.company_guid.upcase, d.email, d.frequency, d.delivery_time, d.time_zone, d.id)
    end
  end

  def self.walk_the_table(report_id, company_guid, email, frequency, delivery_time, time_zone, id)
    report_on = company_guid == "76C6F530-40C1-446D-A5D5-A66E78605149" ? [company_guid] : Company.parent_child_list(company_guid)
    if report_on.present?
      report_on = report_on.join(",")
      report_on = report_on.downcase
      dat = self.create(
        :report_id => report_id,
        :subscription_id => id,
        :email => email,
        :company_guid => report_on,
        :frequency => frequency,
        :delivery_time => TimeConversion.convert_to_utc(delivery_time, time_zone)
      )
      dat.save
    end
  end
end
