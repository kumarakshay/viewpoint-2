class ComplianceDevice
  include MongoMapper::Document
  key :name, String
  key :version, String
  key :compliant_version, String
  key :build, String
  key :type, String
  key :parent, String
  key :group, String
  key :company_guid,String 
  key :is_compliant, Boolean, default: false
  timestamps!
end
