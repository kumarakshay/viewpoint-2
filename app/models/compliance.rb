class Compliance
  include MongoMapper::Document
  key :name, String
  key :group_name, String
  key :version, String
  key :compliant_count, Integer, default: 0
  key :non_compliant_count, Integer, default: 0
  timestamps!

  validates :name, :group_name, :presence => {:message => 'cannot be empty'}
  validates :version, presence: true, on: :update

  def total_count
    compliant_count + non_compliant_count rescue 0
  end

  def self.get_compliance_widget
    records = []
    sets = Compliance.order(:name).all
    sets.each do |obj|
      a = Hash.new
      a["product"] = obj.name + "-" + obj.group_name
      a["compliant"] = obj.compliant_count
      a["noncompliant"] = obj.non_compliant_count
      records  << a
    end
    return records
  end
end
