class Group
  include MongoMapper::Document
  key :name, String
  key :company_guid, String
  key :active, Boolean, default: true
  key :description, String
  key :asset_list, Array
  key :critical, Integer
  key :debug, Integer
  key :error, Integer
  key :information, Integer
  key :warning, Integer
  key :deleted_at, Time
  timestamps!

  validates :name, :presence => {:message => 'cannot be empty'}

  validates :asset_list, :presence => {:message => 'cannot be empty'},:on => :create


  scope :updated_after,
  lambda {|date_str|
    str = date_str.to_time rescue Time.now
    where(:updated_at.gt => str)
  }

  scope :active, -> { where(active: true) }

  #This method return the asset_list array in lowercase associated with Group.
  def lowercase_asset_list
    original = self[:asset_list]
    original.compact.map(&:downcase)
  end

  alias :asset_list :lowercase_asset_list

  before_validation :strip_empty_assets

  before_destroy :validate_dependancies

  scope :without,
    lambda { |str| where(:id => { :$nin => str }, :active => true )}

  #This method returns the group devices which are existing in company asset_list
  def valid_assets
    comp_assets = Company.where(guid: company_guid).first.asset_list
    @valid_assets ||= comp_assets & asset_list
  end

  #This will return device count for the group based on device availability.
  def asset_count
    company = Company.find_by_guid(self.company_guid)
    model = company.is_omi? && !APP_CONFIG['vp_cauldron'] ? OmiDevice : Device
    @count||= model.find_all_by_asset_guid(valid_assets).uniq_by(&:asset_guid).count
  end

  def active_assets
    assets = Ehealth::ActiveAsset.where(:guid => {:$in => self.asset_list}).all
  end

  def assets_snap
    assets = Device.find_all_by_asset_guid(valid_assets).collect(&:name)
    if assets.count < 4
      assets.join(",")
    else
      assets.slice(0,3).join(", ") + "..."
    end
  end

  def device_severities(z_instance)
    begin
      severity = SGTools::Zenoss::DeviceSeverity.new(self.asset_list,{:z_instance => z_instance})
      response = JSON.parse(severity.device_severities)
    rescue Exception=> e
      Rails.logger.error(e)
      response = {}
    end
  end

  def self.dat_for_dropdown(company_guid)
    group = Group.where(company_guid: company_guid, :active => true).all
  end

  def strip_empty_assets
    self.asset_list = self.asset_list.reject(&:empty?) if self.asset_list
  end

  def validate_dependancies
    if Req.has_associated_subscription?(self.id.to_s)
      self.errors.add :base, 'Cannot delete this group as it is associated to a subscription'
      return false   
    end
  end

  def self.portal_search(*args)
    if args.length == 1
      obj = Group.where(:name => Regexp.new(args[0], Regexp::IGNORECASE), :active => true).all
    else
      obj = Group.where(:$and => [
        {:name => Regexp.new(args[0], Regexp::IGNORECASE)}, 
        {:company_guid => args[1]},
        {:active => true}
      ]).all
    end
    obj
  end


  scope :name_starts_with,
    lambda { |str| where :name => { :$regex => /#{str}/i } }

  scope :sort_by_name,
    lambda { | str| 
      order = (str == 'desc') ? -1 : 1
      sort(:name => order) 
    }

  def delete
    self.deleted_at = Time.now
    self.active = false
    self.save 
  end
  alias :destroy :delete  

end