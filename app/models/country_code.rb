class CountryCode
  include MongoMapper::Document
  key :name, String
  key :dial_code, String
  key :code, String

  def display_name
    "#{name} (+#{dial_code})"
  end
end
