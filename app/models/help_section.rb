class HelpSection
  include MongoMapper::Document
  key :title, String  
  key :type, String  
  key :url, String  
  key :is_active, Boolean, default: true
  timestamps!
end