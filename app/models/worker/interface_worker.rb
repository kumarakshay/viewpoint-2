require 'digest/md5'

# curl --insecure --user user:password http://sgs-api-dev.sgns.net/servicecenter/ports?stream=yes
# example data coming from web service
#
# {"LOGICAL_NAME": "VORIR6.TUNNEL11532", "COMPANY_USE": "BRANCH BANKING", "PRODUCTION_STATE": "Installed-Active", 
#  "DEVICE_GUID": "8B30F676-B7CE-E3EC-E040-A2A8567D7367", "CURRENT_RECORD": 26495, "COMPANY_GUID": null, 
#  "PORT_GUID": "A8ED8DD1-D782-38A9-E040-A2A8567D5C1C", "CONTRACTING_GUID": "8EF14CB4-7F67-4F0F-88A9-99DC470B1CB9", 
#  "LAST_MODIFIED": "2011-08-03 15:14:57", "LOCATION": "NJ.VOORHEES", "INTERNET": "no", "TOTAL_RECORDS": 26495, 
#  "NETWORK_NAME": "RD.7381:1153", "OWNER_GUID": "8EF14CB4-7F67-4F0F-88A9-99DC470B1CB9", 
#  "DESCRIPTION": "VNET -see comments" }
module Worker
class InterfaceWorker

  include MongoMapper::Document
  extend WebServiceLoad

  key :asset_guid
  key :company_guid
  key :contracting_guid
  key :company_use
  key :component_id
  key :description
  key :device_name
  key :device_guid
  key :internet
  key :interface_id
  key :ip_interface
  key :logical_name
  key :network_name
  key :owner_guid
  key :parent_guid
  key :billing
  key :subtype
  key :billing_port
  key :standard_config
  timestamps!

  self.web_service_class = ServiceCenter
  self.load_service_method = 'interface_feeds'
  BATCH_SIZE = 5000
  RE = /^([\w-]+)\.(.+)/
  
  def initialize(h={})
    super(h)
    process_extra
  end

  def process_extra
    if m = RE.match(self.logical_name)
      self.device_name = m[1].downcase
      self.ip_interface = m[2].downcase
    end
  end

  def self.populate(options)
    self.load_from_webservice{|o|yield self.ws_translation(o)}
  end

  # Anytime that you'd like to add new values to the collection, do so here.
  def self.ws_translation(o={})
    o['DEVICE_GUID'] = o['DEVICE_GUID'].downcase unless o['DEVICE_GUID'].blank?
    h = {
      logical_name: o['LOGICAL_NAME'], 
      company_guid: o['COMPANY_GUID'], 
      asset_guid: o['ASSET_GUID'], 
      network_name: o['NETWORK_NAME'], 
      internet: o['INTERNET'], 
      description: o['DESCRIPTION'], 
      company_use:o['COMPANY_USE'],
      owner_guid: o['OWNER_GUID'], 
      contracting_guid: o['CONTRACTING_GUID'], 
      device_guid: o['DEVICE_GUID'],
      billing: o['BILLING'],
      subtype: o['SUBTYPE'],
      billing_port: o['BILLING_PORT'],
      standard_config: o['STANDARD_CONFIG']
    }
    if m = RE.match(h[:logical_name])
      h[:device_name] = m[1].downcase
      h[:ip_interface] = m[2].downcase
      z = Digest::MD5.digest(h[:logical_name])
      y = Base64.urlsafe_encode64(z)
      h[:interface_id] = y
    end
    h
  end

  def self.build_new_collection
    before_count = Interface.count
    start_time = Time.now
    self.delete_all
    self.ensure_index([[:logical_name, 1]], unqiue: true)
    big_bacon = []
    populate({}) do|row|
      big_bacon << row
      if big_bacon.size > BATCH_SIZE
        self.collection.insert(big_bacon)
        big_bacon=[]
      end
    end
    unless big_bacon.blank?
      self.collection.insert(big_bacon)
      big_bacon = nil
    end

    # bulk update created_at and updated_at
    self.collection.update({}, {:$set => {created_at: start_time, updated_at: start_time}}, {multi: true})
    self.ensure_index(:company_guid)
    self.ensure_index(:owner_guid)
    self.ensure_index(:contracting_guid)
    self.ensure_index(:device_guid)
    self.ensure_index(:interface_id)
    self.ensure_index(:billing)
    self.ensure_index(:internet)

    unless mailed_and_verify_record?(Interface)
      # add the parent child relationship to the collection
      loop_portal_map

      # drop the production collection and rename worker to production
      Interface.collection.drop
      InterfaceWorker.collection.rename('interfaces')
      duration = Time.now - start_time
      after_count = Interface.count
      print_log(Interface, before_count, after_count, duration )
    else
      Rails.logger.info "InterfaceWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
      p "InterfaceWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
    end
  end

  def self.loop_portal_map
    dat = PortalMapWorker.all(:parent_company_snt.ne => "sung")
    i = 0
    dat.each do |d|
      i +=1
      z = Company.where(snt: d.parent_company_snt).first
      if z
#      puts "parent: #{d.parent_company_snt} #{z.guid} child: #{d.id_company_id} count: #{i}"
        add_parent_to_interface(z.guid, d.id_company_id)
      end
    end
    return true
  end

  def self.add_parent_to_interface(parent_guid, child_guid)
    self.set({:company_guid => child_guid}, :parent_guid => parent_guid)
  end
end
end