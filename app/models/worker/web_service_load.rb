module Worker
module WebServiceLoad

  def self.extended(klass)
    klass.instance_eval do
      cattr_accessor :web_service_class, :load_service_method
    end
  end

  def load_from_webservice(nuke_all=false)
    # self.connection.execute("truncate table #{self.table_name}") if nuke_all  && self.respond_to?(:table_name)
    f=nil
    ws = self.web_service_class.new
    if block_given? 
      ws.__send__(:"#{self.load_service_method}",&Proc.new)
    else
      f = File.open("/tmp/bad_#{self.table_name}",'w')
      self.transaction do
        ws.__send__(:"#{self.load_service_method}") do |o|
          f.write("#{o}\n") unless new_from_load(o)
        end
      end
    end
  rescue => e
   p "#{Time.now}:- Error occurrred while fetching #{self.load_service_method} upstream API" 
   Rails.logger.error(e)
   raise e
  ensure
   f.close unless f.blank?
  end

  def new_from_load(h={})
    i = self.new(self.load_translation(h))
    i.save
  rescue => e
    logger.error(e.message)
    false
  end

  def load_translation(h)
    raise 'must implement'
  end

  def mailed_and_verify_record?(model)
    if APP_CONFIG['bulk_load_sanity_check']
      model_count = model.count
      worker_count = self.count
      total_deference = worker_count - model_count
      if total_deference < -APP_CONFIG["bulk_load_allowed_difference"]
        update_bulkload_tab(self, false)
        count = 0
        record = Worker::BulkLoad.where(:name => self.to_s, :run_at => {:$gt => Time.now.beginning_of_day}).first

        count = record.count unless record.blank?
        if(count == 3)
          BulkLoadMailer.send_bulk_load_mail(model, model_count, worker_count, total_deference).deliver
          p "IMPORTANT MUST READ:- After three attempts #{model.to_s} bulk load faild"
          p "Before bulk load #{model.to_s} records count: #{model_count}"
          p "After bulk load #{model.to_s} records received via web service: #{worker_count}" 
        end
        return true
      else
        update_bulkload_tab(self, true)
        return false
      end
    else
      return false
    end
  end

  def update_bulkload_tab(model, success)
    record = Worker::BulkLoad.where(:name => model.to_s, :run_at => {:$gt => Time.now.beginning_of_day}).first
    if record
      count = record.count
      record.success = success
      record.count = count + 1
    else
      record = Worker::BulkLoad.new({:name => model.to_s, :success => success, :run_at => Time.now, :count => 1})
    end
     record.save
  end

  def print_log(model,before_count,after_count,duration)
    Rails.logger.info("#{Time.now}-#{model} bulk load executed successfully")
    Rails.logger.info("Before bulk load #{model} Count: #{before_count}")
    Rails.logger.info("After bulk load #{model} Count: #{after_count}")
    p "#{Time.now}:- #{model} bulk load executed successfully"
    p "Before bulk load #{model} Count: #{before_count}"
    p "After bulk load #{model} Count: #{after_count}"
    p "Time taken to complete #{model} bulk load: #{duration}"
    Rails.logger.info "Time taken to complete #{model} bulk load: #{duration}"
  end

end
end
