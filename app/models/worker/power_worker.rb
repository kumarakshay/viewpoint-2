module Worker
class PowerWorker

  #
  # {"CONTRACTING_GUID": null, "COMPANY_GUID": "0C25D95C-978E-4EE0-BE39-2C94D2177914", "NAME": "IPDUSASUKLTCA5CK29D", 
  #  "CURRENT_RECORD": 1, "DC": "LTC", "PRODUCTION_STATE": "Installed-Active", "TAG_NUMBER": "IPDUSASUKLTCA5CK29D", 
  #  "OWNER_GUID": "E71312DC-AAF1-4BF2-941D-B297B1D326AB", "PRIORITY": "0", "AREA": "A5", "LAST_MODIFIED": null, "LOCATION": 
  #  "GB:HOUNSLOW.001", "ASSET_GUID": "002FC4E0-0CE4-495B-B926-200F3A4F004B", "TOTAL_RECORDS": 519, "SERIAL_NUMBER": "ZA0927016434", 
  #  "CABINET": "CK29", "IP_ADDRESS": "10.132.205.16", "ASSET_TYPE": "pdu", "UNIT": "D"}
  #

  include MongoMapper::Document
  extend WebServiceLoad

  key :contracting_guid
  key :company_guid
  key :name
  key :current_record
  key :dc
  key :production_state
  key :tag_number
  key :owner_guid
  key :priority
  key :area
  key :last_modified
  key :location
  key :asset_guid
  key :total_records
  key :serial_number
  key :cabinet
  key :ip_address
  key :asset_type
  key :unit
  timestamps!

  self.web_service_class = ServiceCenter
  self.load_service_method = 'power'
  BATCH_SIZE = 5000

  def self.load_from_service_center
    bulk_load(ignore_keys: true)
  end

  def self.populate(options)
    self.load_from_webservice(true){|o|yield self.load_translation(o)}
  end

  def self.build_new_collection
    before_count = Power.count
    start_time = Time.now
    PowerWorker.bulk_load
    unless mailed_and_verify_record?(Power)
      Power.collection.drop
      PowerWorker.collection.rename('powers')
      duration = Time.now - start_time
      after_count = Power.count
      print_log(Power, before_count, after_count, duration )
    else
      Rails.logger.info "PowerWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
      p "PowerWorker bulk load aborted as record difference was more than #{APP_CONFIG['bulk_load_allowed_difference']}"
    end
  end

  def self.load_translation(h={})
    tn = Time.now 
     h['ASSET_GUID'] =  h['ASSET_GUID'].downcase unless  h['ASSET_GUID'].blank?
    { contracting_guid: h['CONTRACTING_GUID'],
      company_guid: h['COMPANY_GUID'],
      name: h['NAME'],
      current_record: h['CURRENT_RECORD'],
      dc: h['DC'],
      production_state: h['PRODUCTION_STATE'],
      tag_number: h['TAG_NUMBER'],
      owner_guid: h['OWNER_GUID'],
      priority: h['PRIORITY'],
      area: h['AREA'],
      last_modified: h['LAST_MODIFIED'],
      location: h['LOCATION'],
      asset_guid: h['ASSET_GUID'],
      total_records: h['TOTAL_RECORDS'],
      serial_number: h['SERIAL_NUMBER'],
      cabinet: h['CABINET'],
      ip_address: h['IP_ADDRESS'],
      asset_type: h['ASSET_GUID'],
      unit: h['UNIT'],
      created_at: tn, 
      updated_at: tn }
  end

  def self.bulk_load
    start_time = Time.now
    logger.debug('starting')
    self.delete_all
    big_bacon = []
    populate({}) do|row|
      big_bacon << row
      if big_bacon.size > BATCH_SIZE
        self.collection.insert(big_bacon)
        big_bacon=[]
      end
    end
    unless big_bacon.blank?
      self.collection.insert(big_bacon)
      big_bacon = nil
    end
    logger.debug("ending: #{Time.now - start_time}")
  end
end
end