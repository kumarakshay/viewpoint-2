require 'em-synchrony'
require 'em-synchrony/em-http'
require 'yajl'
require 'cgi'

# Viewpoint retrieves data from other SunGard systems on a regular basis via web services.  
#
# == SGS-API environmental settings
#
#   # staging
#   url: http://sgs-api-dev.sgns.net
#  
#   # development
#   url: http://sgs-api-dev.sgns.net
#
#   # production
#   url: https://sgs-api.sgns.net
#
# == Creation
#
# A daily cron executed from {https://cust.zen.sgns.net/zport/dmd/Devices/Server/Linux/devices/zencon1 zencon1}
# at 7am EST.  This will initiate the process to repopulate the Viewpoint with the appropriate data. 
# 
#   /home/sgmc/bin/sgmc_rake.sh
#
# *Note*: The rake task can be run at anytime throughout the day.  Temporary tables are created and the production collections 
# are dropped and renamed.  The user update is made using an upsert, so that the user defined settings 
# are maintained.
#
# @see CompanyWorker
# @see DeviceWorker
# @see UserWorker
# @see InterfaceWorker
# @see Power
module Worker
class ServiceCenter

  # Defines content type and basic authorization string.
	def common_header
    {
      'Content-type'=> 'application/json',
      'Authorization'=> 'Basic ' + Viewpoint::Application::SERVICE_CENTER_API_CONFIG.auth
    }
  end

  # Defines the SGS-API url to retrieve customers.
  #   production url: https://sgs-api.sgns.net/servicenow/customers?stream=yes
  def company_list
  	list_for_path('/servicenow/customers?stream=yes', &Proc.new) if block_given?
  end

  # Defines the SGS-API url to retrieve ports.
  #   production url: https://sgs-api.sgns.net/servicecenter/ports?stream=yes
  def interface_feeds
    list_for_path('/servicecenter/ports?stream=yes', &Proc.new) if block_given?
  end

  # Defines the SGS-API url to retrieve monitored devices.
  #   production url: https://sgs-api.sgns.net/servicecenter/devices/monitored?stream=yes
  def company_monitored_devices
  	list_for_path('/servicecenter/devices/monitored?stream=yes', &Proc.new) if block_given?
  end

  # Defines the SGS-API url to retrieve users.
  #   production url: https://sgs-api.sgns.net/servicenow/users?stream=yes
  def user_feeds
    list_for_path("/servicenow/users?stream=yes", &Proc.new) if block_given?
  end  

  # Defines the SGS-API url retrieve the portal map.
  #   production url: https://sgs-api.sgns.net/servicenow/portalmap?stream=yes
  def portal_map
    list_for_path("/servicenow/portalmap?stream=yes", &Proc.new) if block_given?
  end 

  # Defines the SGS-API url to retrieve power bars.
  #   production url: https://sgs-api.sgns.net/servicecenter/power/uk?stream=yes
  def power
    list_for_path("/servicecenter/power/uk?stream=yes", &Proc.new) if block_given?
  end 

  # Connects to API
  def list_for_path(api_path, read_timeout=200)
    EventMachine.synchrony do 
      Rails.logger.info(api_path)
      http_response= EventMachine::HttpRequest.new(Viewpoint::Application::SERVICE_CENTER_API_CONFIG.url, {verify_peer: false, inactivity_timeout: read_timeout}).aget path: api_path, head: common_header
      http_response.errback do
        Rails.logger.error('error callback trigger')
        Rails.logger.error(http_response.response_header.status)
        EM.stop
      end
      http_response.headers do |h|
        if http_response.response_header.status.to_i != 200
          EM.stop
          raise "invalid response, unable to load data"
        end
      end
      parser = Yajl::Parser.new
      parser.on_parse_complete = lambda{|o|yield o}
      http_response.callback do
        Rails.logger.info(http_response.response_header.status)
      end
      http_response.stream do |chunkie|
        begin
          parser << chunkie
        rescue => e
          Rails.logger.error(e)
        end
      end
      http_response.callback{EM.stop}
    end
  end
end
end