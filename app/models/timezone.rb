# ServiceNow provided timezone list
# 
class Timezone 
  include MongoMapper::Document	
  key :label
  key :value
  key :iana_value
  
  # Return IANA specific value of tiemzone.
  def self.vp_specific_value(iana_value)
  	tz = Timezone.where('iana_value' => iana_value).first
  	if tz
  		tz.label
    else
  	  "(GMT - 05:00) Eastern Time (US & Canada)" # Defaulting to Us eastern time.
  	end
  end
end