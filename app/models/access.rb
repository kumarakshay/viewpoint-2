class Access
  include MongoMapper::Document
  key :company_name
  key :company_guid
  key :access_code, Array
  timestamps!

  #This method check the given company has access to SN links VUPT-3761
  def self.can_access_SN(company_guid)
    can_access = true
    access = Access.where(:company_guid => company_guid).first
    if access && access.access_code.include?("VUPT-3761")
      can_access =false
    end
    can_access
  end
end
