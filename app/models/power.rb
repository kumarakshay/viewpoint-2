class Power
  include MongoMapper::Document
  key :contracting_guid
  key :company_guid
  key :name
  key :current_record
  key :dc
  key :production_state
  key :tag_number
  key :owner_guid
  key :priority
  key :area
  key :last_modified
  key :location
  key :asset_guid
  key :total_records
  key :serial_number
  key :cabinet
  key :ip_address
  key :asset_type
  key :unit
  timestamps!

  def self.find_distinct_area(company_guid)
    dat = Power.collection.distinct("area", {:company_guid => company_guid}).compact.map(&:upcase).uniq
    dat = dat.sort
  end

  def self.find_distinct_cabinet(company_guid)
    dat = Power.collection.distinct("cabinet", {:company_guid => company_guid}).compact.map(&:upcase).uniq
    dat = dat.sort
  end
end
