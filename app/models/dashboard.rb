class Dashboard
  include MongoMapper::Document  
  key :name, String
  key :user_id, String
  key :description, String
  key :default, Boolean, :default => false
  key :graphs, Array
            
#  def delete
#  end

#  alias :destroy :delete 
  validates :name, :user_id, :presence => {:message => 'cannot be empty'}

  def load_graphs(sort_by='asc')
    graphs = []
    self.graphs.each do |graph_id|
      begin
        graph = Graph.find(graph_id)
        graphs << graph unless graph.blank?
      rescue Exception => e
        Rails.logger.error "Unable to find graph with id #{graph_id}" 
      end 
    end
    graphs.sort!{|a,b| a.order <=> b.order}
    graphs.reverse! if sort_by =='desc'
    graphs
  end
  
  # Appends the given graph and return the array count.
  def self.add_graph(dash_id,graph_id)
    dashboard = Dashboard.find(dash_id)
    if dashboard.blank?
      Rails.logger.error "Unable to find dashboard with id #{dash_id}" 
      return nil
    else
      if dashboard.graphs.nil?
        dashboard.graphs =[graph_id]
      else
        dashboard.graphs << graph_id
      end
      dashboard.save      
    end
    dashboard.graphs.count
  end 

  # Sets the dashboard with given id as default:true while setting the previous dashboard to default:false
  def self.set_as_default(dashboard_id)
    old_def_dashboard = Dashboard.where(:default => true).first
    new_def_dashboard = Dashboard.find(dashboard_id).first
    if new_def_dashboard
      unless old_def_dashboard.blank?
        old_def_dashboard.update_attribute(:default, false)
      end
      new_def_dashboard.update_attribute(:default, true)
    end
  end

  # Run  through the children and set the index as its order.
  def update_graph_childs
    graphs = self.graphs
    graphs.each_with_index do |id, index|
      graph = Graph.find(id)
      if graph.order != (index + 1)        
        graph.update_attribute(:order, index)
      end
    end
    self.save
  end

  def order_graph_child(id,direction)
    childs = self.graphs
    cur_index = childs.index(id)
    if cur_index
      dest_index=0
      if 'next' == direction and childs[cur_index +1]
        dest_index = cur_index + 1
      elsif 'prev' == direction and childs[cur_index -1]
        dest_index = cur_index - 1
      else
        Rails.logger.error "Unable to order as direction is unknown"
        return false
      end
      childs[cur_index], childs[dest_index]  = childs[dest_index], childs[cur_index]
      return update_graph_childs
    end
    false
  end


  private

 
  #TODO.. this need to go in lib for utility
  def strip_empty_assets
    self.assets = self.assets.reject(&:empty?) if self.assets
  end
end