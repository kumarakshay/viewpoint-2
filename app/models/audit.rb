# 
# * <tt>user_email</tt> since users can be masqueraded, current_user.real_user.email should be captured
# * <tt>audited_changes</tt> incoming data
# * <tt>auditable_type</tt> the model/collection 
# * <tt>action</tt> CRUD action being called
#
# usage:
#   def create
#     audit = {}
#     audit["user_email"] = current_user.real_user.email
#     audit["action"] = "create"
#     audit["auditable_type"] = 'Group' 
#     group = if request.body.size > 0
#       body = JSON::parse(request.body.read) 
#       audit["audited_changes"] = body
#       Audit.create(audit)   
#       create_from_body(body)
#     else
#       audit["audited_changes"] = params
#       Audit.create(audit)   
#       create_from_params(params)
#     end
#     respond_to do |format|
#       if group.save
#         format.json { render json: group, status: :created }
#       else
#         format.json { render json: group.errors, status: :unprocessable_entity }
#       end
#     end
#   end
class Audit
  include MongoMapper::Document
  key :user
  key :action
  key :auditable_changes
  key :auditable_type
  key :company
  key :company_name
  key :loggedin_user
  key :loggedin_user_email
  key :resource
  key :url
  key :impersonating_user
  key :impersonating_user_email
  key :sungard_user, Boolean, default: false
  timestamps!
end