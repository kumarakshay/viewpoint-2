class Role
  include MongoMapper::Document
  key :name, String
  key :description, String 
  key :display_name, String
  key :sso_mapped_name, String  
  # position is the pointer to keep the priority of user.
  # lower the position higher is the role importance.
  key :position, Integer
  key :active, Boolean, default: true
  timestamps!

  validates :name, :presence => {:message => 'cannot be empty'}
  validates :description, :presence => {:message => 'cannot be empty'}
  validates :name, 
            :uniqueness => {:case_sensitive => false, :message => 'Should be unique'},
            :format => {:with => /^[\S]*$/i, :message => 'No spaces allowed'},
            :if         => lambda {|s| s.errors['name'].empty? } 

  validates :position, 
            :uniqueness => true,
            :if           => lambda {|s| s.new_record? } 
            
  validates :position, 
            :numericality => { :only_integer => true,
                               :greater_than_or_equal_to  => 0,
                               :message => 'must be an integer'
            },
            :if           => lambda {|s| !s.new_record? } 

  validates :position, 
            :uniqueness => true,
            :if           => lambda {|s| !s.new_record? && s.errors['position'].empty? } 


  before_create :initialize_position
  after_save :refresh_roles
  after_destroy :refresh_roles

  COMPANY_ADMIN = "company_admin"
  VIEWPOINT_ADMIN = "viewpoint_admin"
  SUNGARD_ADMIN= "sungard_admin"
  SWITCH_COMPANY= "switch_company"
  
  scope :sorted_roles,
    where(:order => :active.desc)
  # Actives roles that are allowed to be seen.
  # So if an user is viewpoint_admin he will see all the other roles.
  # if an user is [sungard_admin, company_admin] he would see all the roles other than viewpoint_admin 
  scope :active_roles,
    lambda{|my_role =[] |
      begin
        lowest_role = Role.all(:active => true,  :order => :position.asc, :name => {"$in" => my_role }).first
        if lowest_role.blank?
          # This would return an empty array... hence position is set to high number.
          where(:active => true,  :order => :position.asc,:position => {:$gte => 100000000 })      
        else
          where(:active => true,  :order => :position.asc,:position => {:$gte => lowest_role.position })      
        end
      rescue
        []
      end
    }

  scope :all_active_roles,
      where(:active => true,  :order => :position.asc) 

  scope :all_customer_roles,
      where(:active => true, :name => {"$nin" => ['viewpoint_admin','sungard_admin','hpc_network_admin'] }, :order => :position.asc)

  # def delete
  #   self.active = false
  #   self.name = self.name + Time.now.utc.to_s
  #   self.save :validate => false
  # end

  #After adding,updating,deleting roles the mapping cache needs to be refreshed
  def refresh_roles
    Rails.cache.delete("roles_mapping")
  end

  #Cached lookup hash
  def self.mapping
     Rails.cache.fetch("roles_mapping") do
       Hash[Role.all.map{|role| [role.name, role.sso_mapped_name]}]
     end 
  end

  #This method return array of sso roles for mapped vp roles
   def self.get_sso_roles_for(vp_roles=[])
      arr = vp_roles.inject([]) do |ar, role|
        ar << Role.mapping[role]
      end
      arr.compact
   end

  #This method return array of vp roles for mapped sso roles
  def self.get_vp_roles_for(sso_roles=[])
   arr = sso_roles.inject([]) do |ar, role|
     ar << Role.mapping.index(role) 
   end
   arr.compact
  end

  # alias :destroy :delete  
  private
  def initialize_position
    return if self.position
    self.position = Role.sort(:position.desc).first.position + 1 rescue 1
  end  

end
