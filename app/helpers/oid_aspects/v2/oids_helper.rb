module OidAspects
  module V2
    module OidsHelper
      def create
	  	begin
		  	auth = request.env["omniauth.auth"]  
		  	log_auth(auth)
		  	# We are not allowing user to login to viewpoint if he doesn't have application "Viewpoint"
		  	# in the response received from sso after user authentication.
		  	unless auth['extra']['raw_info']['applications'].include?("Viewpoint")
		  		Rails.logger.info("User #{auth["info"]["email"]} is not allowed to login to viewpoint")
		  		redirect_to "/up/portalboard"
		  		return
		  	end
		    user = User.first(:guid => { :$regex => /^#{auth['extra']['raw_info']['userGuid']}$/i })
		    if user.blank?
		     	user = User.create_with_omniauth(auth) 
			    Rails.logger.info("create ---> #{user.inspect}")
			  else
		    	user.update_omniauth_token(auth)
		    end
		    # operator_guid is used by pyxie for auth
		    session[:user_guid] = user.id
		    session[:operator_guid] = user.id
		    Rails.logger.info("authorizing access for #{user.email}")
		    create_audit_log(user)
            if request.session["redirect_to"].blank? || ["/","/managedservice","/home","/welcome"].include?(request.session["redirect_to"])
              app_redirect
            else
              redirect_to request.session["redirect_to"]
            end
		    return	    
	    rescue Exception => e
            logger.error("Error occured while creating/updating or fetching user details -  #{e.inspect}")
            render :file => 'shared/sso' # , :layout => 'application'
        end
	  end


	 	 def app_redirect
	  	   if request.url.match('allstream.com').blank?
	      		redirect_to APP_CONFIG["oneportal_url"]
	    	else
	    		redirect_to  APP_CONFIG["oneportal_allstream_url"]
	    	end
	  	 end
 	

      def oid_redirect
        if request.url.match('allstream.com').blank?
          redirect_to "/auth/viewpoint", status: 302
        else
          redirect_to "/auth/allstream", status: 302
        end
      end

      def log_auth(auth)
          Rails.logger.info "Received auth token -> #{auth}\n \
	  	  Received auth provider -> #{auth["provider"].inspect}\n \
	  	  Received auth applications -> #{auth['extra']['raw_info']['applications']}\n \
	  	  Received auth info -> #{auth["info"].inspect}\n \
	  	  Received auth credentials -> #{auth["credentials"].inspect}\n \
	  	  Received auth guid -> #{auth['extra']['raw_info']['userGuid']}\n \
	  	  Received auth email -> #{auth["info"]["email"]}\n \
	  	  Received auth family_name -> #{auth['extra']['raw_info']['family_name']}\n \
	  	  Received auth given_name -> #{auth['extra']['raw_info']['given_name']}\n \
	  	  Received auth zoneinfo -> #{auth['extra']['raw_info']['zoneinfo']}\n \
	  	  Received auth company_guid -> #{auth['extra']['raw_info']['company_guid']}\n \
	  	  Received auth token -> #{auth["credentials"]["token"]}\n \
	  	  Received auth expires_at -> #{auth["credentials"]["expires_at"]}"		  	
      end

      def create_audit_log(user)
        audit = {}
        audit["user"] = user.email
        audit["action"] = "login"
        audit["auditable_type"] = 'User'
        audit["company"] = user.company_guid
        audit["sungard_user"] = user.is_sungard?
        audit["company_name"] = user.company.name
        Audit.create(audit)
      end
    end
  end
end
