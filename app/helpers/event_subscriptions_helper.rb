module EventSubscriptionsHelper

  def event_table_for_assets(event_subscription)
    devices = Device.dat_for_dropdown(event_subscription.asset_list)
    event_table_for_collection devices 
  end
 
  def event_table_head_row()
    columns=['name','IpAdress','location']
    thead = content_tag :thead do
      content_tag :tr do 
        columns.collect{|col| 
          concat content_tag(:th, col) }.join().html_safe
      end
    end
    thead
  end

  def event_table_for_collection(devices)   
    thead = event_table_head_row()
    tbody = event_table_body(devices)
    content_tag( :table,thead.concat(tbody),:class =>"grid", :id => "table_source")
  end

  def event_table_for_company_assets(company_guid, a_list)
    company = Company.with_guid(company_guid)
    asset_list =  company.asset_list.reject{ |elem| a_list.include? elem }
    devices = Device.dat_for_dropdown(asset_list)
    event_table_for_collection devices 
  end

  def event_table_body_asset_list(a_list)
    devices =Device.dat_for_dropdown(a_list)
    event_table_body(devices)
  end

  def event_table_body(devices)
    tbody = content_tag :tbody do
      devices.collect {|elem|
          content_tag :tr, :id => elem.asset_guid do 
             concat content_tag(:td, elem.name)
             concat content_tag(:td, elem.ip_address_formatted)
             concat content_tag(:td, elem.location)
             concat hidden_field_tag("event_subscription[asset_list][]", elem.asset_guid, {:id => "event_subscription_asset_list_"})
          end
      }.join().html_safe
    end
    tbody
  end
end