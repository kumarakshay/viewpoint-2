module GroupsHelper

 def table_for_assets(group)
    devices =Device.dat_for_dropdown(group.asset_list)
    table_for_collection devices 
 end
 
 def table_head_row()
   columns=['name','IpAdress','location']
   thead = content_tag :thead do
     content_tag :tr do 
       columns.collect{|col| 
         concat content_tag(:th, col) }.join().html_safe
     end
   end
   thead
 end

 def table_for_collection(devices)   
   thead = table_head_row()
   tbody = table_body(devices)
   content_tag( :table,thead.concat(tbody),:class =>"grid", :id => "table_source")
 end

  def table_for_company_assets(company_guid, a_list)
    company = Company.with_guid(company_guid)
    asset_list =  company.asset_list.reject{ |elem| a_list.include? elem }
    devices = Device.dat_for_dropdown(asset_list)
    table_for_collection devices 
  end

  def table_body_asset_list(a_list)
    devices =Device.dat_for_dropdown(a_list)
    table_body(devices)
  end

  def table_body(devices)
    tbody = content_tag :tbody do
      devices.collect {|elem|
          content_tag :tr, :id => elem.asset_guid do 
             concat content_tag(:td, elem.name)
             concat content_tag(:td, elem.ip_address_formatted)
             concat content_tag(:td, elem.location)
             concat hidden_field_tag("group[asset_list][]", elem.asset_guid, {:id => "group_asset_list_"})
          end
      }.join().html_safe
    end
    tbody
  end

end
