module ReqsHelper
  def report_type
  	report_type = { 'pdf' => 'PDF', 'csv' => 'CSV'}
  end

  def frequency_range
	frequency = { '1' => 'Daily',
		          '2' => 'Weekly',
		          '3' => 'Monthly'
      			}
  end

  def time_range
  	time = Hash[("1:00".."24:00").step(100).to_a.map {|array| [array,array]}]
  end
end
