collection @devices, :object_root => false
attributes :name,:asset_guid
node(:ip_address) { |device| (device.customer_production_ip.present? and device.customer_production_ip.match(@query_param)) ? device.customer_production_ip : device.ip_address_formatted  }
