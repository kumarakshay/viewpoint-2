object false
node :statistics do
  @statistic
end

child(@audits => :data) do
  attributes :user,:company_name
  node(:last_login_date) { |audit| audit.created_at.to_date}
end