collection @record_set
node do |record|
  json_record = record.get_elements(:init_by_service_now).inject({}) do |r,e|
    r[e.name] = e.value
    r["dv_#{e.name}"] = e.display_value
    r
  end
  json_record[:service_now_url] = record.get_service_now_url
  json_record
end