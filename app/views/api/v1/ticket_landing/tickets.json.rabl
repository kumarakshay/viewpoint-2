collection @records, :object_root => false
attributes :number, :state, :active, :sys_id
node(:type) {|record| record.sys_class_name}
node(:summary) {|record| record.short_description}
node(:created_at) {|record| format_ticketing_date(record.sys_created_on)}
node(:updated_at) {|record| format_ticketing_date(record.sys_updated_on)}