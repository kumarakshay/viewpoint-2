collection @records, :object_root => false
attributes :asset_guid, :host, :updated_at
node(:last_view) {|record| distance_of_time_in_words(record.updated_at.to_time,Time.now) + " ago"}