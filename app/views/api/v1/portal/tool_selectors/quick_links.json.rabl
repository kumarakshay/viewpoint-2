devices_target = session[:is_omi] ? "/ui/index.html#/devices" : "/devices"
interfaces_target = session[:is_omi] ? "/ui/index.html#/interfaces" : "/interfaces"
events_target = session[:is_omi] ? "/ui/index.html#/events" : "/events"

application_links = []
interface_link = {
  "classname"=> "interfaces-link",
  "title"=> "Interfaces",
  "target"=> "_self",
  "href"=> interfaces_target
}

vp_admin_disable =  @current_user.is_sungard? && current_user.is_viewpoint_admin? ? false : true
vp_admin = {
    "classname"=> "company-admin-link",
    "title"=> "Viewpoint Admin",
    "target"=> "_self",
    "href"=> "/sungard",
    "disabled" => vp_admin_disable
}

if @current_user.is_sungard? 
  application_links.push(interface_link)
  application_links.push(vp_admin)
else
  application_links.push({
    "classname"=> "devices-link",
    "title"=> "Devices",
    "target"=> "_self",
    "href"=> devices_target
  }, {
    "classname"=> "events-link",
    "title"=> "Events",
    "target"=> "_self",
    "href"=> events_target
  })
  application_links.push(interface_link)
end

collection application_links.flatten
node do |s|
  s
end

