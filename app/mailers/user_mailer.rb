require 'development_mail_interceptor'

class UserMailer < ActionMailer::Base
  default :from => APP_CONFIG['email_from']

  def registration_confirmation(user)
    @user = user
    mail(:to => "#{user.first_name} #{user.last_name} <#{user.email}>", :subject => "Your Viewpoint Account was Created")
  end

  def reset_password(user)
  	@user = user
    mail(:to => "#{user.first_name} #{user.last_name} <#{user.email}>", :subject => "Password Reset")
  end 

  def reset_email(user)
    @user = user
    mail(:to => "#{user.first_name} #{user.last_name} <#{user.email}>", :subject => "You have Requested to Change Your Email Address")
  end  

  def reset_email_confirmation(user)
  	@user = user
    mail(:to => "#{user.first_name} #{user.last_name} <#{user.pending_email}>", :subject => "Email Reset")
  end	

  def send_dashboard_report_notification(user)
    mail(:to => "#{user.first_name} #{user.last_name} <#{user.email}>", :subject => "Your Dashboard Report is ready", body: "Please login to viewpoint portal to download your report.")
  end
  
end
# This intercepter needs to be modified.
ActionMailer::Base.register_interceptor(DevelopmentMailInterceptor) if (Rails.env.development? || Rails.env.staging? || Rails.env.qa?)
