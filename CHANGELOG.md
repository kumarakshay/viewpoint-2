# Change Log

## [4.04.00](https://bitbucket.org/mountdiablo/viewpoint/src/75cf607b1e0434c3f08e10f2ec5011649e9613c0/?at=feature%2Fpurport) (2015-04-29)

[Full Changelog](https://mountdiablo.jira.com/jira/secure/ReleaseNote.jspa?projectId=11930&version=25300)

** Improvement **  
- Added all component threshold/severity settings for each device. [\#2269](https://mountdiablo.jira.com/browse/VUPT-2269)  
- Fix grammar on non-portal to portal user conversions. [\#2974](https://mountdiablo.jira.com/browse/VUPT-2974)  
- Fix SungardAS users with transitioned email in Event Subscriptions. [\#2893](https://mountdiablo.jira.com/browse/VUPT-2893)  
- Remove client side validation on business phone extension in user managment. [\#2979](https://mountdiablo.jira.com/browse/VUPT-2979)  
- Remove white space being propagated during DNS zone add.  [\#1716](https://mountdiablo.jira.com/browse/VUPT-1716)  
- Additional device component templates. [\#2426](https://mountdiablo.jira.com/browse/VUPT-2426)  

** Bug **  
- Fixed create user with already used email fields to clear. [\#2736](https://mountdiablo.jira.com/browse/VUPT-2736)  
