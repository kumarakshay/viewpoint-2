role :web, "sgmc.annlab.sgns.net"
role :app, "sgmc.annlab.sgns.net"
role :db,  "sgmc.annlab.sgns.net", :primary => true # This is where Rails migrations will run

set :deploy_to,"/home/sgmc/#{application}"
set :user, 'sgmc'
set :use_sudo, false
set :rails_env, "test"
