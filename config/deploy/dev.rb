require 'rvm/capistrano'


role :app, "phlnu415v"
set :deploy_to,"/home/sgmc/#{application}"
set :user, 'sgmc'
set :use_sudo, false
set :rails_env, "development"

default_environment["TERM"] = 'xterm'
