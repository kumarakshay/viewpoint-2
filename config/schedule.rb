# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
#env :PATH, ENV['PATH']

set :environment, ENV['RAILS_ENV']
set :output, {:error => 'log/error.log', :standard => 'log/cron.log'}
set :path,  ENV['PWD']
job_type :rake, "cd :path && :environment_variable=:environment RACK_ENV=:environment bundle exec rake :task --silent :output"
job_type :command, "cd :path && NODE_ENV=:environment command :task --silent :output"

every :day, :at => '12:30am', :roles => [:worker] do
 rake "load:portal_map"
end

every :day, :at => '01:00am', :roles => [:worker] do
  rake 'load:company'
  # rake 'load:device'
  # rake 'load:interface'
  # rake 'load:power'
end

#All bulk load for OMI
# VUPT-5511
# Removing OMI bulk load from schedule file as we have move the codebase to om_portal
# every :day, :at => '01:00am' do
#  command "/usr/bin/node #{path}/packages/omi-worker/src/bulk_load_all.js"
# end

every :day, :at => '04:00am' do
  rake 'db:clean_groups'
end

every :day, :at => '01.00pm' do
  rake 'db:load_backup_report'
end

every 15.minutes do
  rake 'hpoo:change_request'
end

every :day, :at => '02:00am, 03.00am' do
  rake 'load:bulk_load'
end

every '0 2 01 * *' do
  rake 'db:create_network_availability_report_pdf'
  rake 'db:create_network_availability_report_csv'
end

#This system task will bulk load the capacity data from pyxie to show widget on dahboard.
every 2.days do
  runner "PyxieClient.load_capacity_data"
end
