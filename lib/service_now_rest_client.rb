require 'base64'
require 'rest-client'

class ServiceNowRestClient

  @@setup = false

  def initialize
    self.setup()
    #Custom logging for reports
    @@my_logger ||= Logger.new("#{Rails.root}/log/service_now_calls.log")
    original_formatter = Logger::Formatter.new
    @@my_logger.formatter = proc { |severity, datetime, progname, msg|
      original_formatter.call(severity, datetime, progname, msg.dump)
    }
  end

  def setup(env = Rails.env)
    if !self.setup?
      @@config = self.load_config()
      @@env_config = @@config[env]
      raise StandardError, "Env #{env} not found in config" if @@env_config.nil?
      # symbolize the keys, which Bunny expects
      @@env_config.keys.each {|key| @@env_config[(key.to_sym rescue key) || key] = @@env_config.delete(key) }
      # raise StandardError, "'exchange' key not found in config" if !@@env_config.has_key?(:exchange)
      @@setup = true
    end
  end

  # Load configuration from config/amqp.yml
  def load_config
    if defined?(::Rails)
      config_file = ::Rails.root.join('config/sgtools_itsm.yml')
      if config_file.file?
        YAML.load(ERB.new(config_file.read).result)
      else
        nil
      end
    end
  end

  # def test
  #   p @@env_config
  # end

  # Is the connection set up?
  def setup?
    @@setup
  end

  def support_org_for_the_company(company_guid)
    support_org = nil
    query_str ="sysparm_query=u_portal_id=#{company_guid}&sysparm_fields=u_support_organization"
    begin
      response = RestClient.get(URI.encode("#{@@env_config[:client_url]}/api/now/table/core_company?#{query_str}"),
                                {:authorization => "Basic #{Base64.strict_encode64("#{@@env_config[:client_basic_auth_user]}:#{@@env_config[:client_basic_auth_password]}")}",
                                 :accept => 'application/json'})
      data =  JSON.parse(response)
      unless data["result"].blank?
        support_org = data["result"].first["u_support_organization"]
      end
      support_org
    rescue Exception => e
      Rails.logger.info "--Unable to Load Support Org for the given company - #{e}"
      support_org
    end
  end

  def support_org_companies
    results = []
    query_str ="sysparm_query=u_portal_id!=NULL&active=true&nameISNOTEMPTY&sysparm_fields=u_portal_id,u_support_organization"
    begin
      response = RestClient.get(URI.encode("#{@@env_config[:client_url]}/api/now/table/core_company?#{query_str}"),
                                {:authorization => "Basic #{Base64.strict_encode64("#{@@env_config[:client_basic_auth_user]}:#{@@env_config[:client_basic_auth_password]}")}",
                                 :accept => 'application/json'})
      data =  JSON.parse(response)
      results = data["result"]
    rescue Exception => e
      Rails.logger.info "--Unable to Load ServiceNow US based Companies - #{e}"
      results
    end
  end

  def us_based_companies
    results = []
    query_str ="sysparm_query=u_us_based=true"
    begin
      response = RestClient.get(URI.encode("#{@@env_config[:client_url]}/api/now/table/core_company?#{query_str}"),
                                {:authorization => "Basic #{Base64.strict_encode64("#{@@env_config[:client_basic_auth_user]}:#{@@env_config[:client_basic_auth_password]}")}",
                                 :accept => 'application/json'})
      data =  JSON.parse(response)
      results = data["result"]
    rescue Exception => e
      Rails.logger.info "--Unable to Load ServiceNow US based Companies - #{e}"
      results
    end
  end

  def company_tickets(company_id,start_date,end_date,type)
    results = []
    fixed_query_str = "&sysparm_display_value=true&sysparm_fields=sys_class_name,number,opened_at,sys_created_on,sys_updated_on,sys_id,short_description,state,active&sysparm_display_value=true"
    begin
      unless company_id.blank?
        if type == 'incidents'
          type_query_str = "^sys_class_name=incident"
        elsif type == 'changes'
          type_query_str = "^sys_class_name=change_request"
        elsif type == 'requests'
          type_query_str = "^sys_class_name=sc_req_item"
        else
          type_query_str = "^sys_class_name=incident^ORsys_class_name=change_request^ORsys_class_name=sc_req_item"
        end
        query_str = "sysparm_query=company.u_portal_id=#{company_id}#{type_query_str}^ORDERBYDESCsys_updated_on^active=true^NQcompany.u_portal_id=#{company_id}^active=false^closed_atONLast 30 days@javascript:gs.daysAgoStart(30)@javascript:gs.daysAgoEnd(0)#{type_query_str}"+ fixed_query_str
        ct = Time.now
        response = RestClient.get(URI.encode("#{@@env_config[:client_url]}/api/now/table/task?#{query_str}"),
                                  {:authorization => "Basic #{Base64.strict_encode64("#{@@env_config[:client_basic_auth_user]}:#{@@env_config[:client_basic_auth_password]}")}",
                                   :accept => 'application/json'})
        data =  JSON.parse(response)
        results = data["result"]
      end
      results
    rescue Exception => e
      Rails.logger.info "--Unable to Load ServiceNow Company Tickets - #{e}"
      results
    end
  end
end
