class HttpClient
  attr_accessor :http_method, :query, :url, :header
  def initialize(url,options)
    @url = url
    @http_method = options["method"] || 'GET'
    @query = options["query"] || nil
    @header = options["header"] || {}
  end

  def common_header
    {'Content-type'=> 'application/json'}.merge!(header)
  end

  def fetch
    begin
      httpclient = HTTPClient.new
      httpclient.ssl_config.verify_mode = OpenSSL::SSL::VERIFY_NONE
      if http_method == 'GET'
        response = httpclient.get(url, :header => common_header)
      elsif http_method == 'POST'
        response = httpclient.post(url, :body => Yajl.dump(query), :header => common_header)
      elsif http_method == 'PUT'
        response = httpclient.put(url, :body => Yajl.dump(query), :header => common_header)
      elsif http_method == 'DELETE'
        response = httpclient.delete(url, :header => common_header)
      end
    rescue  Timeout::Error
      retry
    end
    return JSON.parse(response.body) unless response.body.blank? rescue {}
  end
end
