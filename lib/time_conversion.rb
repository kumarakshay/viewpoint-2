class TimeConversion
  def self.convert_to_utc(deliver_time,time_zone)
    @zone = ActiveSupport::TimeZone[time_zone]        
    delivery_time_string = "09/04/12 " + deliver_time
    delivery_time = DateTime.strptime(delivery_time_string, "%m/%d/%y %H:%M") 
    utc_time = Time.parse(delivery_time.to_s).utc
    new_utc_time = utc_time - @zone.utc_offset
    deliver_at = new_utc_time.strftime("%H:%M")
  end
end