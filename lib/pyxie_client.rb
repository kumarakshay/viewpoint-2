require 'open-uri'
require 'spreadsheet'

class PyxieClient

  def self.load_capacity_data(company_guid='76C6F530-40C1-446D-A5D5-A66E78605149')
    capacity_files = self.get_capacity_files
    unless capacity_files.blank?
      #Remove all old records from the collection
      Capacity.destroy_all
      capacity_files.each do |filename|
        path = "/pyxie/#{company_guid}-capacity_reports/#{filename}"
        site_url = APP_CONFIG['backup_url'] + path
        response = self.fetch_capacity_data(site_url)
        begin
          book = Spreadsheet.open(response)
          worksheets = book.worksheets # can use an index or worksheet name
          worksheets.each do |worksheet|
            worksheet.each 1 do |row|
              unless row.blank?
                c = Capacity.new
                c.name = filename.split("-")[1]
                c.device_type = filename.split("-")[0].downcase rescue ""
                c.type = worksheet.name
                c.location = row[5]
                c.unit = row[8]
                c.total = row[10]
                c.used = row[11]
                c.available = row[12]
                c.save!
              end
            end
          end
        rescue Exception => e
        end
      end
    end
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts "Total Capacity Records Imported: #{ Capacity.all.count}"
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  end

  def self.get_capacity_files(company_guid='76C6F530-40C1-446D-A5D5-A66E78605149')
    capacity_bucket_path = "/pyxie/#{company_guid}-capacity_reports"
    capacity_files =  []
    latest_files = []
    begin
      site_url = APP_CONFIG['backup_url'] + capacity_bucket_path
      response = self.fetch_remote_data(site_url)
      unless response.blank?
        parse_data =  JSON.parse(response)
        unless parse_data["result"].blank?
          parse_data["result"].each do |data|
            if(data["content_type"] == "application/vnd.ms-excel")
              file_meta = data["meta"]
              if(data["filename"].include?(".xls") && data["filename"].start_with?( 'Network-', 'Compute-', 'Storage-'))
                file_date = Date.parse("#{file_meta['report_day']}/#{file_meta['report_month']}/#{file_meta['report_year']}")
                if (Date.today.beginning_of_month..Date.today.end_of_month).include_with_range?  file_date
                  capacity_files << data["filename"]
                end
              end
            end
          end
          # puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
          # puts capacity_files
          # puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
          capacity_files_by_device_type =  capacity_files.group_by { |filestr| filestr.split('-')[0] }
          capacity_files_by_device_type.each do |k,v|
            capacity_files_by_name = v.group_by {|filename| filename.split("-")[1]}
            capacity_files_by_name.each do |x,y|
              latest_files << y.sort.last
            end
          end
        end
      else
        puts "NO Response Received From Pyxie"
      end
      latest_files
    rescue Exception=> e
      Rails.logger.error(e)
      puts e
      return latest_files
    end
  end

  def self.fetch_remote_data(site_url)
    p site_url
    uri = URI.parse(site_url)
    data = {'token' => "ccd5995f61224fae48f78fcb5a17a82d"}
    req = Net::HTTP::Get.new(uri.path)
    req.set_form_data(data)
    http = Net::HTTP.new(uri.host, uri.port)
    if uri.scheme =='https'
      pem = File.read("#{Rails.root}/config/ca-certificates.crt")
      http.use_ssl = true
      http.cert = OpenSSL::X509::Certificate.new(pem)
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end
    response = http.start do |httpvar|
      httpvar.request(req)
    end
    if response.kind_of?(Net::HTTPRedirection)
      uri = URI.parse(response['location'])
      response = open(uri).read
      return response
    end
    return response.body
  end

  def self.fetch_capacity_data(url)
    response = []
    begin
      uri = URI.parse(url)
      data = {'token' => "ccd5995f61224fae48f78fcb5a17a82d"}
      req = Net::HTTP::Get.new(uri.path)
      req.set_form_data(data)
      http = Net::HTTP.new(uri.host, uri.port)
      if uri.scheme =='https'
        pem = File.read("#{Rails.root}/config/ca-certificates.crt")
        http.use_ssl = true
        http.cert = OpenSSL::X509::Certificate.new(pem)
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end
      response = http.start do |httpvar|
        httpvar.request(req)
      end
      if response.kind_of?(Net::HTTPRedirection)
        uri = URI.parse(response['location'])
        response = open(uri)
      end
      return response
    rescue Exception => e
      puts "------------------------Error --------------"
      puts url
    end
  end
end
