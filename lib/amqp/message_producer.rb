require 'bunny'
require 'json'

module Amqp
  class MessageProducer

    @@setup = false
    @@connected = false

    def initialize
      self.setup()
      self.connect()

      #Custom logging for reports
      @@my_logger ||= Logger.new("#{Rails.root}/log/bus_report.log")
      original_formatter = Logger::Formatter.new
      @@my_logger.formatter = proc { |severity, datetime, progname, msg|
        original_formatter.call(severity, datetime, progname, msg.dump)
      }
    end

    # Load configuration from config/amqp.yml
    def load_config
      if defined?(::Rails)
        config_file = ::Rails.root.join('config/amqp.yml')
        if config_file.file?
          YAML.load(ERB.new(config_file.read).result)
        else
          nil
        end
      end
    end

    # Is the connection set up?
    def setup?
      @@setup
    end

    # Is the connection established?
    def connected?
      @@connected
    end

    # Create the connection to the report.job.topic exchange
    def connect
      raise StandardError, "AMQP not setup. Call setup before calling connect" if !self.setup?
      @@bunny.start
      ch = @@bunny.create_channel
      @@exchange = ch.topic('report.job.topic', :durable => true)
      @@connected = true
    end

    # Disconnect from the job.topic.exchange
    def disconnect
      begin
        @@bunny.stop
      rescue
        # if this is being called because the underlying connection went bad
        # calling stop will raise an error. that's ok....
      ensure
        @@connected = false
      end
    end

    def setup(env = Rails.env)
      if !self.setup?
        @@config = self.load_config()
        @@env_config = @@config[env]
        raise StandardError, "Env #{env} not found in config" if @@env_config.nil?
        # symbolize the keys, which Bunny expects
        @@env_config.keys.each {|key| @@env_config[(key.to_sym rescue key) || key] = @@env_config.delete(key) }
        # raise StandardError, "'exchange' key not found in config" if !@@env_config.has_key?(:exchange)
        @@bunny = Bunny.new(@@env_config)
        @@setup = true
      end
    end

    def produce(message, routing_key, job_id, message_id)
      @@my_logger.info "job id: #{job_id}"
      @@my_logger.info "routing key: #{routing_key}"
      @@my_logger.info "message id: #{message_id}"
      @@my_logger.info "message properties: #{message}"

      if !self.setup? || !self.connected?
        @@my_logger.error "AMQP not setup or connected. Call setup and connect before calling produce"
      else
        begin
          @@exchange.publish(message.to_json,
            :routing_key => routing_key,
            :type => "report.job.create",
            :app_id => 'vpdev',
            :message_id => message_id,
            :delivery_mode => 1,
            :timestamp => Time.now.to_i)
        rescue => err
          @@my_logger.error "Unexpected error producing AMQP messsage: (#{message.to_json})"
          @@my_logger.error "#{err.message}"
          @@my_logger.error err.backtrace.join("\n")
          self.disconnect
        end
      end
      self.disconnect
    end
  end
end
