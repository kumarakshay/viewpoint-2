#!/usr/bin/env ruby

# You might want to change this
ENV["RAILS_ENV"] ||= "development"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)

require File.join(root, "config", "environment")
require 'thread/every'
require 'cacherank'

# setup custom log file for daemon
@log ||= Logger.new("#{Rails.root}/log/ehealth_prefetch.log", 'daily')
original_formatter = Logger::Formatter.new
@log.formatter = proc { |severity, datetime, progname, msg|
  original_formatter.call(severity, datetime, progname, msg.dump)
}

# cycle & cache expiration times
@CYCLE_TIME = 5.minutes
@EXPIRE_TIME = 60.minutes
@UPDATE_TIME = 60.minutes

# range of days to find cache values
@CACHE_RANGE = 10

# cache keys to prefetch
@cache_keys = {'ehealth_ticketing' => Ehealth::Ticketing,
               'ehealth_active_assets' => Ehealth::ActiveAsset,
               'ehealth_backup_reports' => Ehealth::BackupReport}

#companies having backup reports
@backup_companies =  Ehealth::Company.backup_companies.all.collect(&:company_guid)

@timers = {}
# setup jobs
def job_setup
  # load in the guids of companies that should be force loaded
  force_load_companies = []
  load_companies = []
  Ehealth::Company.where({force_prefetch: true}).each do |c|
    @log.info "Found company to force fetch: #{c.company_guid}"
    force_load_companies.append(c.company_guid)
  end

  # remove duplicates and any odd blank/nil entries
  load_companies = force_load_companies.uniq.compact

  @cache_keys.each do |key,mod|
    load_companies.each do |company_guid|
      next if (key == 'ehealth_backup_reports' and !@backup_companies.include?(company_guid))

      @log.info "Setting up job for #{key} - #{company_guid}"
      @timers[key] ||= {}

      # only set a key for the company if it doesn't exist
      if (@timers[key].has_key?(company_guid))
        @log.info "Worker job already exists for #{key} - #{company_guid}"
      else
        @timers[key][company_guid] = Thread.every(@CYCLE_TIME) {
          @log.info "Starting job for #{key} - #{company_guid}"
          begin
            t = mod.bulk_load(company_guid)
            if t.blank?
              @log.warn "No value found for #{key} / #{mod} - #{company_guid}"
            end
            Cacherank.write(key, company_guid, t, :expires_in => @EXPIRE_TIME)
          rescue Exception => e
            @log.error "Error running job: #{e}"
          end
          @log.info "Finished job for #{key} - #{company_guid}"
        }
      end
    end

    # clean up stale timers for this key
    @log.info "Cleaning old timers for #{key}"
    remove_jobs = @timers[key].keys() - load_companies
    remove_jobs.each do |j|
      @log.info "Shutting down job for #{j}"
      begin
        @timers[key][j].cancel
      rescue Exception => e
        @log.error "Could not shut down timer: #{key}-#{j} - #{e}"
      end
    end
  end
end

def cancel_timers
  @log.info "Starting timer cancel cycle."
  @timers.each { |job, value|
    value.each { |guid, timer|
      @log.info "Stopping #{job} timer for #{guid}"
      timer.cancel
    }
  }
  @timers = {}
  @log.info "Finished timer cancel cycle."
end

# schedule job updates for future
job_setup_timer = Thread.every(@UPDATE_TIME) {
  @log.info "Job setup timer triggered."

  # setup new jobs
  job_setup
  @log.info "Job setup timer finished."
}

$running = true

Signal.trap("TERM") do
  @log.warn "TERM Signal Received!"
  cancel_timers
  $running = false
end


while($running) do
    # @log.info Thread.list.count.to_s
    sleep 1
  end
