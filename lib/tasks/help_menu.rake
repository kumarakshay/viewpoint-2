namespace :db do
	desc "This task would create menu and help items for default group"
	task :create_menu_help => :environment do
		begin
			Rake::Task[:environment].invoke
			Rails.logger.info "Starting to create menu and help"
			Help.destroy_all
			Menu.destroy_all
			names = ['global','devices','event_history','subscriptions','event_subscription','events','interfaces','masquerade','groups','reports']
			names.each do |name|
			  menu = Menu.new(:title => name)
			  begin
				menu.save
				help = Help.new(:title => "Help_#{name}", :body => "Help for #{name}", :menu_id => menu.id.to_s)
				help.save
				puts "Created menu '#{menu.title}' and '#{help.title}' successfully"
			  rescue Exception => e
				Rails.logger.error "Error occured while creating menu and help-  #{e.backtrace.to_s}"
			  end
			end
		rescue
			Rails.logger.error $!
			raise
		end
	end
end