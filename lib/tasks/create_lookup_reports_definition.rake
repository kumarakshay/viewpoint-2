namespace :db do
  desc "This task would create default reports"
  task :create_lookup_reports_definition => :environment do
	begin
		Rake::Task[:environment].invoke
		  Statement.destroy_all
		  hash = {
			          '1' => { name: 'device_pdf_detail', display_name: 'Health Report / Device Detail', description: 'desc',
			        				   type: '',ext: '.pdf', meta_type: 'application/pdf', file_name: 'device_', position: 1,
			        				   report_type_for_bus: 'device_pdf_report', report_filename_for_bus: 'device-report.pdf',
			        				   zenoss_instance: 'bus_zenoss_cust',
			        				   property: Property.new(is_report_for_group: true, is_report_for_device: true)},

				        '2' => { name: 'device_csv_glance', display_name: 'Raw Polled Performance Data CSV', description: 'desc',
				        				 type: '', ext: '.csv', meta_type: 'text/csv', file_name: 'performance_', position: 2,
				        				 report_type_for_bus: 'device_csv_glance', report_filename_for_bus: 'device-csv-glance.csv',
				        				 zenoss_instance: 'bus_zenoss_cust',
				        				 property: Property.new(is_report_for_device: true)},

				        '3' => { name: 'internet_usage_pdf_report', display_name: 'Network Usage', description: 'desc',
				        				 type: '', ext: '.pdf', meta_type: 'application/pdf', file_name: 'network_usage_', position: 3,
				        				 report_type_for_bus: 'internet_usage_pdf_report', report_filename_for_bus: 'internet_usage_report.pdf', 
				        				 zenoss_instance: 'bus_zenoss_net', property: Property.new()},

				        '4' => { name: 'powerbar_pdf_report', display_name: 'Powerbar', description: 'desc', type: '',
				        				 ext: '.pdf',meta_type: 'application/pdf',file_name: 'powerbar_', position: 4,
				        				 report_type_for_bus: 'powerbar_pdf_report', report_filename_for_bus: 'powerbar_report.pdf', 
				        				 zenoss_instance: 'bus_zenoss_uk',
				        				 property: Property.new(is_cabinet_and_area_report: true)},

				        '6' => { name: 'company_device_availability_report_pdf', display_name: 'Device Availability', description: 'desc',
				        				 type: 'pdf',ext: '.pdf', meta_type: 'application/pdf', file_name: 'device_availability_', position: 5,
				        				 report_type_for_bus: 'company_device_availability_report', report_filename_for_bus: 'company_device_availability.pdf', 
				        				 zenoss_instance: 'bus_zenoss_cust',
				        				 property: Property.new(pdf_and_csv_report: true)},

				        '7' => { name:'company_device_availability_report', display_name: 'Device Availability', description: 'desc',
				        			   type: 'csv', ext: '.csv',meta_type: 'text/csv', file_name: 'device_availability_', position: 5,
				        				 report_type_for_bus: 'company_device_availability_report', report_filename_for_bus: 'company_device_availability_report.csv', 
				        				 zenoss_instance: 'bus_zenoss_cust',
				        				 property: Property.new(pdf_and_csv_report: true)},

				        '8' => { name: 'company_network_availability_report_pdf', display_name: 'Network Availability', description: 'desc',
				        				 type: 'pdf', ext: '.pdf', meta_type: 'application/pdf',file_name: 'network_availability_', position:  7,
				        				 report_type_for_bus: 'company_network_availability_report', report_filename_for_bus: 'company_network_availability_report.pdf', 
				        				 zenoss_instance: 'bus_zenoss_cust',
				        				 property: Property.new(pdf_and_csv_report: true)},

				        '9' => { name: 'company_network_availability_report', display_name: 'Network Availability', description: 'desc',
				        				 type: 'csv', ext: '.csv', meta_type: 'text/csv', file_name: 'network_availability_', position: 7,
				        				 report_type_for_bus: 'company_network_availability_report', report_filename_for_bus: 'company_network_availability_report.csv', 
				        				 zenoss_instance: 'bus_zenoss_cust',
				        				 property: Property.new(pdf_and_csv_report: true)},

				        '10' => { name: 'company_power_availability_report_pdf', display_name: 'Power Availability', description: 'desc',
				        				  type: 'pdf', ext: '.pdf', meta_type: 'application/pdf', file_name: 'power_availability_', position: 8,
				        				  report_type_for_bus: 'company_power_availability_report', report_filename_for_bus: 'company_power_availability_report.pdf', 
				        				  zenoss_instance: 'bus_zenoss_cust',
				        				  property: Property.new(pdf_and_csv_report: true)},

				        '11' => { name: 'company_power_availability_report_csv', display_name: 'Power Availability', description: 'desc',
				        				  type:'csv', ext: '.csv', meta_type: 'text/csv',file_name: 'power_availability_', position: 8,
				        					report_type_for_bus: 'company_power_availability_report', report_filename_for_bus: 'company_power_availability_report.csv', 
				        					zenoss_instance: 'bus_zenoss_cust',
				        					property: Property.new(pdf_and_csv_report: true)},

				        '12' => { name: 'company_availability_report_pdf', display_name: 'Availability', description: 'desc', 
				        					type: 'pdf', ext: '.pdf', meta_type: 'application/pdf', file_name:'availability_', position: 9,
				        					report_type_for_bus: 'company_availability_report', report_filename_for_bus: 'company_availability_report.pdf', 
				        					zenoss_instance: 'bus_zenoss_cust',
				        					property: Property.new(pdf_and_csv_report: true)},

				        '13' => { name: 'company_availability_report_csv', display_name: 'Availability', description: 'desc', 
				        					type: 'csv', ext: '.csv', meta_type: 'text/csv', file_name: 'availability_', position: 9,
				        					report_type_for_bus: 'company_availability_report', report_filename_for_bus: 'company_availability_report.csv', 
				        					zenoss_instance: 'bus_zenoss_cust',
				        					property: Property.new(pdf_and_csv_report: true)},

				        '14' => { name: 'company_network_path_performance_pdf', display_name: 'Network Path Performance', description: 'desc',
				        				  type: 'pdf', ext: '.pdf', meta_type: 'application/pdf', file_name: 'network_path_performance_', position: 10,
				        					report_type_for_bus: 'network_path_performance_report', report_filename_for_bus: 'network_path_performance_report.pdf', 
				        					zenoss_instance: 'bus_zenoss_cust',
				        					property: Property.new(pdf_and_csv_report: true)},

				        '15' => { name: 'group_availability_report', display_name: 'Group Availability', description: 'desc', type: 'pdf',
				        				  ext: '.pdf', meta_type: 'application/pdf', file_name: 'group_availability_report_', position: 6,
				        				  report_type_for_bus: 'device_availability_report', report_filename_for_bus: 'group_availability_report.pdf', 
				        				  zenoss_instance: 'bus_zenoss_cust',
				        				  property: Property.new(pdf_and_csv_report: true)},

				        '16' => { name: 'company_network_path_performance_csv', display_name: 'Network Path Performance', description:'desc', 
				        				  type: 'csv', ext:'.csv', meta_type: 'text/csv', file_name: 'network_path_performance_', position: 10,
				        				  report_type_for_bus: 'network_path_performance_report', report_filename_for_bus: 'network_path_performance_report.csv', 
				        				  zenoss_instance: 'bus_zenoss_cust',
				        				  property: Property.new(pdf_and_csv_report: true)},

				        '17' => { name: 'network_interfaces_report', display_name: 'Network Interfaces', description: 'desc', type: '',
				        				  ext: '.pdf', meta_type: 'application/pdf', file_name: 'network_interfaces_', position: 11,
				        				  report_type_for_bus: 'graph_extractor', report_filename_for_bus: 'network_interfaces.pdf',
				        				  zenoss_instance: 'bus_zenoss_cust',
				        				  property: Property.new(is_report_for_group: true, is_report_for_device: true)}
		          }
		  hash.each do |report_id, statement_hash|
				report = Statement.new(statement_hash.merge!(report_id: report_id))
				begin
					report.save
					puts "Created report '#{report.name}' successfully"
			  rescue Exception => e
					Rails.logger.error "Error occured while creating custom report-  #{e.backtrace.to_s}"
			  end
			end
		rescue
			Rails.logger.error $!
			raise
		end
	end
end