class Util
  attr_accessor :start_date, :end_date, :logger, :reports, :report_reciever

  def initialize
    @start_date = start_date
    @end_date = end_date
    @logger = logger
    @reports = reports
    @report_reciever = @report_reciever
  end

  def reports
    {
      create_network_availability_report_pdf: {report_id: '8', type: 'pdf'},
      create_network_availability_report_csv: {report_id: '9', type: 'csv'}
    }
  end

  def report_reciever
    'dl.sms.automationsupport@sungardas.com'
  end

  def logger
    Logger.new("#{Rails.root}/log/monthly_bulk_reports.log")
  end

  def start_date
    (Date.today - 1.month).beginning_of_month
  end

  def end_date
    (Date.today - 1.month).end_of_month
  end
end

namespace :db do
  desc "This task would create bulk reports with all company guids in csv for service desk"
  util = Util.new
  util.reports.each do |key, value|
    task "#{key}".to_sym => :environment do
      begin
        invalid_guids = []
        unsaved_guids = []
        job_id = []
        file = "#{Rails.root}/db/data/import/service_mgt_customers.csv"
        CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
          company_guid = row.to_hash['Guid'].to_s
          unless company_guid.blank?
            company = Company.find_by_guid(company_guid)
            if company.present?
              company_name = row.to_hash['Name']
              user =  User.find_by_email(util.report_reciever)
              user_guid = user.present? ? user.guid : ''
              req = Req.new(company_guid: company_guid, report_id: value[:report_id],
                            report_type: value[:type], email: util.report_reciever,
                            start_date: util.start_date, end_date: util.end_date, user_guid: user_guid,
                            subscription: false, time_zone: 'American Samoa', delivery_time: '')
              req.custom_report_name = "#{company_name}.#{value[:type]}"
              req.generate_job_id
              req.report = req.report_name
              req.masquerade_email = util.report_reciever
              begin
                req.save
                job_id.push(req.job_id)
              rescue Exception => e
                unsaved_guids.push(company_guid)
              end
            else
              invalid_guids.push(company_guid)
            end
          end
        end
        util.logger.debug "#{Time.now} ===== find invalid guids : #{invalid_guids}" unless invalid_guids.empty?
        util.logger.debug "#{Time.now} ===== Not able to create reports for guids : #{unsaved_guids.inspect}" unless unsaved_guids.empty?
        util.logger.debug "#{Time.now} ======= Job id for all reqs : #{job_id}"
      rescue Exception => e
        util.logger.debug "#{Time.now} ===== there is some problem to genearte reports : #{e.message}"
      end
    end
  end
end