class Util
  def self.filter_params(params={})
    fil_hash = {}
    fil_hash[:guid] = params["userid"].upcase
    fil_hash[:email] = params["uid"]
    fil_hash[:portal_name] = params["portalname"]
    fil_hash[:company_name] = params["companyname"]
    fil_hash[:company_guid] = params["companyid"].upcase
    fil_hash[:first_name] = params["firstname"] if not_blank_unknown(params["firstname"] )
    fil_hash[:last_name] = params["lastname"] if not_blank_unknown (params["lastname"])
    fil_hash
  end

  def self.name_attributes(params={})
    fil_hash = {}
    fil_hash[:first_name] = params[:first_name] if not_blank_unknown(params[:first_name] )
    fil_hash[:last_name] = params[:last_name] if not_blank_unknown (params[:last_name])
    fil_hash
  end

  def self.not_blank_unknown(attribute)
    state = false
    if not attribute.blank? and not attribute == "Unknown" 
      state =  true 
    end
    state
  end
end

namespace :db do  
  desc "This task would set active value for user is true"
  task :set_active_flag => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      users = User.all
      users.each do |user|
        begin
          user.update_attribute(:active, true)
          puts "Updating #{user.first_name} active flag to  #{user.active}"
        rescue Exception => e
          Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}" 
        end
      end       
    rescue
      Rails.logger.error $!
      raise
    end     
  end

  desc "This task would give company admin rights to selected users provided in csv file"
  task :set_company_admin => :environment do
    count,failure, good , bad = 0, 0, 0,0
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      file = "#{Rails.root}/db/data/SN_AdminUsers.csv"      
      CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
        count +=1        
        params = row.to_hash
        if params["Email"].nil?
          bad += 1
        else
          begin
            if User.set_as_company_admin(params["Email"].strip)
              good +=1
            else
              failure +=1               
            end
          rescue Exception => e
            failure +=1 
            Rails.logger.error "Error occured while updating user #{params['Email']} - #{e.inspect}"
          end
        end
        print '.' if count%250==0 
      end
    rescue
      Rails.logger.error $!
      raise
    end     
    Rails.logger.info("Total records in csv file #{count}")
    Rails.logger.info("Inconsistent records    - #{bad}")
    Rails.logger.info("Records updated         - #{good}")
    Rails.logger.info("failures/not-updated    - #{failure}")      
  end

  desc "This task would import legacy users provided in text file"
  task :import_legacy_users => :environment do
    created, updated, total = 0,0,0
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      file = "#{Rails.root}/db/data/lp-users-customer.txt"      
      header =[]
      @file = IO.read(file).force_encoding("ISO-8859-1")
      @file.each_line do |line|
        #puts "reading line #{total}"
        data = line.split(/\t/)
        data.collect(&:strip!)
        if total == 0
          header = data     
        else    
          act_params = Hash[header.zip data]
          # attributes for new user
          params = Util.filter_params(act_params)
          # first name and last name attributes for exisiting user.
          name_params = Util.name_attributes(params)
          begin
            # Load user only if we have name attributes to update.            
            user = User.find_by_email(/^#{params[:email]}$/i) unless name_params.empty? 
            if user.blank?
              User.create!params
              created +=1
            else
              user.update_attributes(name_params)
              updated +=1
            end 
          rescue Exception => e
            Rails.logger.error("Error occured while creating user with attributes #{params} - #{e}")
            Rails.logger.error $!
          end
        end
        total +=1 
        #break if total == 10
        print '.' if total%250==0
      end
    rescue
      Rails.logger.error $!
      raise
    end     
    Rails.logger.info("Total records in txt file - #{total}")
    Rails.logger.info("Records created           - #{created}")
    Rails.logger.info("Records updated           - #{updated}")
  end 

  desc "This task will import fs users for hot fix"
  task :import_missing_fs_users => :environment do
    created, updated, total = 0,0,0
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      file = "#{Rails.root}/db/data/fs-lp-users-for-viewpoint-20130617.txt"      
      header =[]
      @file = IO.read(file).force_encoding("ISO-8859-1")
      @file.each_line do |line|
        data = line.split(/\t/)
        data.collect(&:strip!)
        if total == 0
          header = data     
        else    
          act_params = Hash[header.zip data]
          # attributes for new user
          params = Util.filter_params(act_params)
          # first name and last name attributes for exisiting user.
          name_params = Util.name_attributes(params)
          begin
            # Load user only if we have name attributes to update.            
            user = User.find_by_email(/^#{params[:email]}$/i) unless name_params.empty? 
            if user.blank?
              User.create!params
              created +=1
            else
              user.update_attributes(name_params)
              updated +=1
            end 
          rescue Exception => e
            Rails.logger.error("Error occured while creating user with attributes #{params} - #{e}")
            Rails.logger.error $!
          end
        end
        total +=1 
        print '.' if total%250==0
      end
    rescue
      Rails.logger.error $!
      raise
    end     
    Rails.logger.info("Total records in txt file - #{total}")
    Rails.logger.info("Records created           - #{created}")
    Rails.logger.info("Records updated           - #{updated}")
  end

  desc "This task would set active value to true for selected users"
  task :set_active_flag_for_selected => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      emails = ["maqbool.ahmed@wdfg.com",
                "Stephen.Hill@standardbank.com",
                "new.user@email.com",
                "guy.moule@fincore.com",
                "aaron.dailey@kronos.com",
                "vmessina@emffp.com",
                "sstein@emffp.com",
                "jonathan.delacruz@gwl.ca",
                "ruben.rasalingham@sungard.com",
                "Chuck.reinkemeyer@hussmann.com",
                "Tim.lenke@hussmann.com",
                "emusa@istonish.com",
                "cody.nutt@ihg.com",
                "michael.karolitzky@concorde2000.com",
                "patrick.conway@usfoods.com",
                "Dan.Bedard@gwl.ca",
                "Javier.Gurfinkiel@gwl.ca",
                "nlopes@mackenzieinvestments.com",
                "adeelmir@mackenzieinvestments.com",
                "Brian.Rempel@gwl.ca",
                "dsmith@mackenzieinvestments.com",
                "as.us.soc@sungard.com",
                "desouzae@socan.ca",
                "aarbash@globinformation.net",
                "sekhardasari@ltcsupplysource.com",
                "lino.vargas@vitas.com",
                "csingh@insidefsi.net",
                "amithleo@gmail.com",
                "franklandm@socan.ca",
                "joel.goodwin@hsn.net",
                "michael.istler@hsn.net",
                "majid.mohammed@hsn.net",
                "dw_000001@yahoo.com",
                "annemarie.osborne@sungard.com",
                "Christophe.sabattier2@sagemcom.com",
                "amit.gijare@yahoo.com",
                "ignitemarriage@gmail.com",
                "shervin.behzadi@sungard.com"
      ]
      puts 'Starting ..'
      emails.each do |usr_email|        
        user = User.first(:email => { :$regex => /^#{usr_email}$/i })
        if user
          begin
            user.update_attribute(:active, true)
            puts "Updating #{user.first_name} active flag to  #{user.active}"
          rescue Exception => e
            Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}" 
          end          
        end        
      end   
    rescue
      Rails.logger.error $!
      raise
    end     
  end



end