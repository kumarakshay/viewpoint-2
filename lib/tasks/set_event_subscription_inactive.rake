namespace :db do  
  desc "This task would set active value for event notification to false"
  task :set_event_subscription_flag => :environment do
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      user_emails = EventSubscription.fields("email").collect(&:email).uniq
      hash_map = {}
      user_emails.each do |email|
        begin
          u =  User.where(:email => email, :active => true)
          if u.blank?
            count = 0
            eventsubscriptions = EventSubscription.where(email: email)
            eventsubscriptions.each do |ev|
                ev.update_attribute(:active, false)
                count = count + 1
            end
            hash_map.merge!(email => count)
          end 
        rescue Exception => e
          Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}" 
        end
      end       
      hash_map.each do |key, value|
        puts "For User #{key} total inactive event notification count: #{value}"
      end
    rescue
      Rails.logger.error $!
      raise
    end     
  end
end
