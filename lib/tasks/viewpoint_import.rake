class Util
  
  def role_map
    @roles_hash ||= Hash[Role.all.map{|role| [role.name, role.sso_mapped_name]}]
  end
  
  def update_sso_role(user)
    sso_roles = user['roles'].inject([]){ |ar, role| ar << role_map[role] }
    url = Figaro.env.sso_v2_base_url + "/api/v2/users/#{user["guid"]}/roles"
    options = {'roles' => sso_roles}
    fire_request(:put, url, options)
  end

  def oauth_api_token
    if @api_token.blank?
      ssl = {:verify => true, :ca_file => "#{Rails.root}/config/sso-2.crt"}           
      client = OAuth2::Client.new( "#{Figaro.env.sso_v2_viewpoint_api_key}",
                                "#{Figaro.env.sso_v2_viewpoint_api_secret}", 
                                {:ssl => ssl })
      @api_token = OAuth2::AccessToken.new(client,"#{Figaro.env.sso_v2_viewpoint_api_token}")
    end
    @api_token
  end

  #This method create groups for adavptive companies from supplied data #VUPT-2817
  def create_groups_for_adaptive(data={})
    created_groups = []
    data.each do |key, val|
       company_guid = Company.find_by_snt(key).guid
       g = Group.where(:name =>"Business Essential/Advanced 99.95%").where(:company_guid => company_guid).first
       if (g.blank? and val.count > 0)
        g = Group.new
        g.company_guid = company_guid
        g.name = "Business Essential/Advanced 99.95%"
        g.asset_list = val
        g.save!
        created_groups << g.id.to_s
       end 
    end
    puts " "
    puts "Created Groups:- #{created_groups}"
  end     

  def fire_request(verb, url, body_hash={}) 
    response = oauth_api_token.request(verb,url) do |request|
      request.headers['Content-Type'] = 'application/json'
      request.body= body_hash.to_json unless body_hash.empty?
    end
    if response
      if response.status == 200
        print '.'
        #puts "#{response.inspect}"
        return true
      else
        Rails.logger.error("-------Fired request of type #{verb}\n\t url - #{url}\n\t params #{body_hash}\n\t #{response.inspect}-------")
        print 'X'
        return false
      end
    end
  end  
end

 namespace :db do  
  desc "This task would create iana time zone on mongo database"
  task :import_iana_timezones => :environment do
    puts "Starting to import timezones in IANA format"
    Rails.logger.info "Starting to import timezones in IANA format"
    count = 0
    begin
    	if APP_CONFIG["open_id_version"] == 'V2'
	    	Timezone.destroy_all 
	      Rake::Task[:environment].invoke
	      cfg = YAML.load(File.read("#{Rails.root}/db/data/import/timezone_iana.yml"))
	      cfg.each do |zone|
	      	begin
	      		Timezone.create!(:label => zone[1], :value => zone[0])
	      		count = count + 1
          rescue Exception => e
	      		Rails.logger.error "Error occured while creating timezone #{e.backtrace.to_s}"
	      	end
	      end
    	end
    rescue
      Rails.logger.error $!
      raise
    end
    Rails.logger.info "Finished importing timezones. Total records imported #{count}"
    puts "Finished importing timezones. Total records imported #{count}"
  end 

  desc "This task would create lookup for sungard companies in mongo database"

  desc "This task would map sso roles to vp roles using column sso_mapped_name"
  task :map_sso_roles_to_viewpoint_role => :environment do
    puts "Starting to map sso roles to viewpoint roles"
    Rails.logger.info "Starting to map sso roles to viewpoint roles"
     begin
      Rake::Task[:environment].invoke
      mapping = {
        'viewpoint_admin' => 'Viewpoint Admin',
        'sungard_admin' => 'Sungard Admin',
        'company_admin' => 'Company Admin',
        'infoblox_dns_customer_reporter' => 'DNS Customer Reporter',
        'infoblox_dns_customer_engineer' => 'DNS Customer Engineer',
        'switch_company' => 'Switch Company',
        'firewall_viewer' => 'Firewall Viewer'
      }
      Role.all.each do |role|
        begin
          role.sso_mapped_name = mapping[role.name]
          role.save
        rescue Exception => e
          Rails.logger.error "Error occured while updating role  #{e.backtrace.to_s}"
        end       
      end
    rescue
      Rails.logger.error $!
      raise
    end
    Rails.logger.info "Finished mapping sso roles to viewpoint roles"
    puts "Finished mapping sso roles to viewpoint roles"
  end

  desc "This task would update sso roles to vp roles using column sso_mapped_name"
  task :import_vp_user_roles_into_sso => :environment do
    puts "Starting importing vp roles to sso roles"
    Rails.logger.info "Starting importing vp roles to sso roles"
    count = 0
     begin
      Rake::Task[:environment].invoke
      users = User.collection.find({'roles.1' => {'$exists' => true }}, {fields: ['guid','roles','email']}).to_a
      puts "Updating roles of '#{users.count}' in SSO"
      util = Util.new
      users.each do |u|
        begin
          count = count + 1 if util.update_sso_role(u)
        rescue Exception => e
          Rails.logger.error "Error occured while updating role for user - #{u['email']} \nBacktrace:\n\t#{e.backtrace.join("\n\t")}"
        end
      end  
    rescue
      Rails.logger.error $!
      raise
    end
    Rails.logger.info "Finished importing vp roles to sso roles.Total records imported #{count}"
    puts "Finished importing vp roles to sso roles. Total records imported #{count}"
  end

  desc "This task would create a group based on data supplied in given spreadsheet"
  task :create_group_for_adaptive_companies => :environment do
     count = 0
     bad_assets,bad_company = [],[]
    begin
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting operation"
      # file = "#{Rails.root}/db/data/adaptive/update-adaptive-devices-for-groups.csv"
      file = "#{Rails.root}/db/data/adaptive/second-pass.csv"
      company_devices_hash = {}
      company_devices_not_exist = {}
      CSV.foreach(file, :headers => true, encoding: "ISO8859-1") do |row|
        count +=1
        params = row.to_hash
        unless params["SNT"].blank?
          begin
            company_device_list = Company.find_by_snt(params["SNT"]).asset_list
            if !company_devices_hash.has_key?(params["SNT"])
              company_devices_hash[params["SNT"]] = []
              company_devices_not_exist[params["Customer"]] = []
            end
            device = Device.where(:name => /#{params["Hostname"]}/i).first
            unless device.blank?
              if company_device_list.include?(device.asset_guid)
                  company_devices_hash[params["SNT"]] << device.asset_guid
                else
                  company_devices_not_exist[params["Customer"]] << device.asset_guid
              end
            else
              bad_assets << params["Hostname"]
            end
          rescue Exception => e
            Rails.logger.error "Error occured while iterating over the devices for company #{params['Customer']}"
          end
        else
          bad_company << params["Customer"]
        end
        print "."
      end
       #company_devices_hash.each { |k,v| print k; print " "; print v.count; print " "; }
       #Create groups from processed data
       util = Util.new
       util.create_groups_for_adaptive(company_devices_hash)
    rescue
      Rails.logger.error $!
      raise
    end
    puts " "
    puts "Total records scanned in csv file #{count}"
    puts " "
    puts "Companies not exist in db #{bad_company}"
    puts " "
    puts "Company devices not exist in self asset_list #{company_devices_not_exist}"
    puts " "
    puts "#{bad_assets.count} devices not exist in db #{bad_assets}"
  end

  desc "This task would create a country_code collection in our database"
  task :create_country_code => :environment do
    CountryCode.destroy_all
    @records =   JSON.parse(File.read("#{Rails.root}/db/data/import/country_codes.json"))
    @records.each do |record|
       c = CountryCode.new(record)
       c.save!
    end
    puts "*********************************************"
    puts "Total Countries imported #{CountryCode.count}"
    puts "*********************************************"
  end

  desc "This task would create compliance group and respective compliance version number "
  task :import_compliance_groups => :environment do
    begin
        Compliance.destroy_all
     [
        {:name => "I3 Telephony Services", :group_name =>"Voice over IP-IP PBX"},
        {:name => "Application Cloud Services", :group_name => "ACS Cisco ASA Firewalls"},
        {:name => "Application Cloud Services", :group_name => "ACS Cisco Network Switches"},
        {:name => "Application Cloud Services", :group_name => "ACS F5 Load balancer"},
        {:name => "Carrier Ethernet Services", :group_name => "Carrier Ethernet Switch - 5305"},
        {:name => "Carrier Ethernet Services", :group_name => "Carrier Ethernet Switch - 39xx,5160"},
        {:name => "Core Recovery Network", :group_name => "Recovery Routers"},
        {:name => "Core Recovery Network", :group_name => "Recovery Switches"},
        {:name => "Core Routing Network", :group_name => "Cisco P-Core"},
        {:name => "Core Routing Network", :group_name => "Juniper - PE"},
        {:name => "Core Transport Network", :group_name => "Ciena 6500"},
        {:name => "Core Transport Network", :group_name => "Ciena 3500"},
        {:name => "Core Transport Network", :group_name => "Ciena 5200"},
        {:name => "Core Transport Network", :group_name => "Cisco ONS 15454"},
        {:name => "Core Transport Network", :group_name => "BTI 7000 Series"},
        {:name => "Core Transport Network", :group_name => "Transmode TS Series"},
        {:name => "Core Transport Network", :group_name => "Transmode TM Series"},
        {:name => "Enterprise Cloud Services 1.0", :group_name => "Cisco Switches (1000v/7k)"},
        {:name => "Enterprise Cloud Services 1.0", :group_name => "ASR Routers"},
        {:name => "Managed Backup Services", :group_name => "MS - 6509 switches"},
        {:name => "Managed/Basic Internet Services", :group_name => "Cisco - ISC"},
        {:name => "Managed/Basic Internet Services", :group_name => "Juniper - ISC"},
        {:name => "Managed/Basic Internet Services", :group_name => "Foundry - ISC"},
        {:name => "MSS - Managed FW Services", :group_name => "Check Point MDS"},
        {:name => "MSS - Managed FW Services", :group_name => "Check Point Customer Firewalls"},
        {:name => "MSS - Managed FW Services", :group_name => "ASA Firewalls"},
        {:name => "MSS - Managed FW Services", :group_name => "Juniper Netscreen"},
        {:name => "MSS - Managed FW Services", :group_name => "Juniper SRX"},
        {:name => "MSS - Managed FW Services", :group_name => "R2C ASAv Firewalls"},
        {:name => "Recover2Cloud - Server Replication", :group_name => "R2C Switches (Cisco 4948e)"},
        {:name => "Recover2Cloud - Server Replication", :group_name => "R2C switch (Cisco 2960x)"},
        {:name => "Recover2Cloud - Server Replication", :group_name => "Cisco Switches (R2C)"},
        {:name => "Recover2Cloud - Server Replication", :group_name => "Juniper Routers (R2C)"},
        {:name => "Recover2Cloud Services", :group_name => "R2C Switches (Cisco 4948e)_Recover2Cloud Services"},
        {:name => "Recover2Cloud Services", :group_name => "R2C switch (Cisco 2960x)_Recover2Cloud Services"},
        {:name => "Recover2Cloud Services", :group_name => "Cisco Switches (R2C)_Recover2Cloud Services"},
        {:name => "Recover2Cloud Services", :group_name => "Juniper Routers (R2C)_ Recover2Cloud Services"},
        {:name => "Sweden Cloud", :group_name => "Cisco Switches (5k/1000v)"},
        {:name => "External DNS", :group_name => "Infoblox Grid-DNS"},
        {:name => "Internal Use Only ISE", :group_name => "Cisco ISE-Radius/TACACS"}
    ].each do |obj|
        temp =  Compliance.new(obj)
        temp.save!
      end
    rescue
      Rails.logger.error $!
      raise
    end
  end 
end
