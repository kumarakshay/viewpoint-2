def get_omi_data(url)
  options = {}
  header = {"Authorization" => AUTHORIZATION}
  options["header"] = header
  options["method"] = "GET"
  # p "******url**#{url}"
  # p "******options**#{options}"
  rest_client = HttpClient.new(url, options)
  rest_client.fetch
end

def get_prefdata_url(rtsm_cmdb_id)
  urls = []
  urls.push(BASE_URL + '/monitoring/perfdata/' + rtsm_cmdb_id + '?ds=AGENT&dsclass=GLOBAL&metric=GBL_SWAP_SPACE_UTIL&interval=300')
  urls.push(BASE_URL + '/monitoring/perfdata/' + rtsm_cmdb_id + '?ds=AGENT&dsclass=GLOBAL&metric=GBL_LOADAVG&interval=300')
  urls.push(BASE_URL + '/monitoring/perfdata/' + rtsm_cmdb_id + '?ds=AGENT&dsclass=GLOBAL&metric=GBL_MEM_PAGEIN_RATE&interval=300')
  urls
end

def get_devices(company_guid=nil)
  devices =[]
  unless company_guid.blank?
    guids = Company.where(:guid => company_guid).first.asset_list
    devices = OmiDevice.where(:asset_guid => {:$in => guids}).all
  else
    devices = OmiDevice.all
  end
  devices
end

namespace :load do
  #rake load:prefdata_for_device[BA14FE30-6693-4736-A7A7-BB58884DE800]
  desc "This task would check the omi devices and there graph data"
  task :prefdata_for_device, [:company_guid] => :environment do |t, args|
    begin
      BASE_URL = Figaro.env.omi_base_url
      AUTHORIZATION = Figaro.env.omi_authorization
      data_report = {}
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting script"
      omi_devices = get_devices(args[:company_guid])
      count = 0
      devices_without_rosetta = []
      devices_without_data = []
      devices_with_exception = []
      omi_devices.each do |d|
        if d.ci_type == "Compute"
          url = BASE_URL + '/cmdb/rosetta?asset_guid=' + d.asset_guid
          count = count+1
          rosseta_data = get_omi_data(url)
          if rosseta_data.present? && rosseta_data[0].present? && rosseta_data[0]["rtsm_cmdb_id"].present?
            rtsm_cmdb_id = rosseta_data[0]["rtsm_cmdb_id"]
            unless rtsm_cmdb_id.blank?
              url = BASE_URL + '/monitoring/perfdata/' + rtsm_cmdb_id + '?ds=AGENT&dsclass=GLOBAL&metric=GBL_CPU_TOTAL_UTIL&interval=300&starttime=1524637496&endtime=1525242296'
              begin
                records = get_omi_data(url)
                if records.present? && records.kind_of?(Array) && records[0].key?("points") && records[0]["points"].size > 0
                  p "Device passed for the pref data test."
                else
                  p devices_without_data << d.asset_guid
                  p "No Data Found for the device #{d.name} - #{d.asset_guid} "
                end
              rescue Exception => e
                devices_with_exception << d.asset_guid
                p "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                puts "#{e.message}"
              end
            end
          else
            devices_without_rosetta << d.asset_guid
            p "XXXXXXX No Rosetts Data Found for the device #{d.name} - #{d.asset_guid} XXXXXX"
            p devices_without_rosetta
          end
        end
      end
      p "*****************************************************************************************"
      p "--------total device visited(#{count})-----------------"
      p "--------devices_without_rosetta(#{devices_without_rosetta.count})-----------"
      p devices_without_rosetta
      p "--------devices_without_data(#{devices_without_data.count})--------------"
      p devices_without_data
      p "--------devices_with_exception(#{devices_with_exception.count})------------"
      p devices_with_exception
      p "*******************************************************************************************"
    rescue Exception => e
      p "@@@@@@@@@@@@@@@ EXCEPTIONS @@@@@@@@@@@@@@@@@@@@@@@@@"
      p "#{e.message}"
      p devices_with_exception
    end
  end


  #rake load:OMi_device_for_graphs[BA14FE30-6693-4736-A7A7-BB58884DE800]
  desc "This task would find the OMi device having graph data"
  task :OMi_device_for_graphs, [:company_guid] => :environment do |t, args|
    begin
      BASE_URL = Figaro.env.omi_base_url
      AUTHORIZATION = Figaro.env.omi_authorization
      data_report = {}
      Rake::Task[:environment].invoke
      Rails.logger.info "Starting script"
      p ">>>>>Starting script>>>>>>"
      omi_devices = get_devices(args[:company_guid])
      p "OMi device found for #{args[:company_guid]}: - #{omi_devices.count}"
      count = 0
      omi_devices.each do |d|
        url = BASE_URL + '/cmdb/rosetta?asset_guid=' + d.asset_guid
        count = count+1
        rosseta_data = get_omi_data(url)
        if rosseta_data.present? && rosseta_data[0].present? && rosseta_data[0]["rtsm_cmdb_id"].present?
          rtsm_cmdb_id = rosseta_data[0]["rtsm_cmdb_id"]
          unless rtsm_cmdb_id.blank?
            perfdata_urls = get_prefdata_url(rtsm_cmdb_id)
            perfdata_urls.each do |url|
              begin
                records = get_omi_data(url)
                if records.present? && records.kind_of?(Array) && records[0].key?("points") && records[0]["points"].size > 0
                  company = Company.where(:guid =>  d.company_use_guid.upcase).first
                  if company
                    p "-------------- #{company.name} ---------------------------"
                    data_report[company.name] = [d.name, d.asset_guid]
                  end
                  p "Data Found for the device #{d.name} - #{d.asset_guid}"
                  p "----------------------------------------------------"
                else
                  p "XXXXXXX No Data Found for the device #{d.name} - #{d.asset_guid} XXXXXX"
                end
              rescue Exception => e
                p "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                puts "#{e.message}"
              end
            end
          end
        else
          p "XXXXXXX No Rosetts Data Found for the device #{d.name} - #{d.asset_guid} XXXXXX"
        end
      end
      p "***************************************Total device visited #{count}********************************"
      p data_report
      p "*****************************************************************************************************"
    rescue Exception => e
      p "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
      puts "#{e.message}"
    end
  end

  desc "This task would find the CIS unique types for the devices"
  task :OMi_ci_types => :environment do
    @log ||= Logger.new("#{Rails.root}/log/esb.log", 'daily')
    original_formatter = Logger::Formatter.new
    @log.formatter = proc { |severity, datetime, progname, msg|
      original_formatter.call(severity, datetime, progname, msg)
    }
    uniq_types = []
    uri = URI.parse("https://esb.tools.sgns.net:443/cmdb/tql")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri, nil)
    request["Authorization"] = 'SGAS 6aa66ea80e28cc3652b47aa0ae312ffd:f484548e619c5e68c8452cd629faba0941feb1ea7fc5f09440572a2f8fe71583'
    request["Content-Type"] = 'application/json'
    #devices = OmiDevice.collection.find({},{fields: ['asset_guid'], batch_size: 1000})
    count = 0
    objects = OmiDevice.where(:ci_type => { :$ne => ["Compute","Network"]}).all
    puts ">>>>>>>>>>>>>>>>>>>>>>"
    puts objects.count
    puts ">>>>>>>>>>>>>>>>>>>>>>"
    objects.each do |d|
      count = count + 1
      puts count
      body = {
        tql_name: "SG_Host_Temp",
        tqlParams: {
          asset_guid: d['asset_guid']
        }
      }
      request.body = body.to_json
      response = http.request(request)
      result = JSON.parse(response.body)
      uniq_types << result['cis'].map{|x| x["type"]} rescue nil
      @log.info uniq_types.flatten.uniq
    end
    @log.info ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    @log.info uniq_types.flatten.uniq
    @log.info ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  end
end
