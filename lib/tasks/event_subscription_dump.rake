require 'csv'
namespace :event_subscription do	
	desc "This task will import csv data into worker table"
	task :import_csv_data => :environment do
		begin
			Rake::Task[:environment].invoke
			Rails.logger.info "Starting to import csv data of event notifications"
			count = 0
			Worker::RawEventSubscription.destroy_all
			path = "#{Rails.root}/db/data/subs_dump"
			files = ["#{path}/vwEventSubscription.csv","#{path}/vwAssetEventSubscriptions.csv"]
			files.each do |file|				
                          puts "Reading file - #{file}"
				CSV.foreach(file, 
					:headers => true, :header_converters => :symbol, :col_sep => "~",  :encoding =>
                                           "UTF-16LE:UTF-8") do |row|
					params = row.to_hash
					dat = Worker::RawEventSubscription.new(params)
					begin
						dat.save					
						count += 1
					rescue Exception => e					
						Rails.logger.error "Error occured while creating raw event for #{row[0]} -  #{e.backtrace.to_s}"
					end
				end
			end
			puts "Finished importing of #{count} events"
		rescue
			Rails.logger.error $!
			raise
		end			
	end	

	desc "This task will dump raw events into actual valid event notifications"
	task :import_raw_events => :environment do
		begin
			count = 0
			Rake::Task[:environment].invoke
			Rails.logger.info "Starting to pull in actual event notifications"
                        EventSubscription.destroy_all
			distinct_ids = Worker::RawEventSubscription.distinct(:idsubscriptionid)
			distinct_ids.each do|id|
				params = {}
				raw_evs = Worker::RawEventSubscription.where(:idsubscriptionid => id)				
				if raw_evs.count > 0
					assets = raw_evs.inject([]){|ar, e| ar << e.assetguid if e.assetguid } || []					
					params['rawid'] = raw_evs.first.idsubscriptionid
					params['name'] = 'Imported via CSV'
					params['company_guid'] = raw_evs.first.companyguid
					params['asset_list'] = assets
					params['email'] = raw_evs.first.emailaddress.strip unless raw_evs.first.emailaddress.blank?
					params['severity'] = raw_evs.first.severity
					params['all_asset'] = assets.empty? ? true : false
					begin					
						EventSubscription.create! params
						count += 1						
					rescue Exception => e					    
						Rails.logger.error "Error occured while creating event notification : #{e} - #{params}"
					end
				end				
			end			
			puts "Successfully imported #{count}  raw event/s"
		rescue
			Rails.logger.error $!
			raise
		end			
	end	

	desc "This task will add user_guid in event notification table"
	task :add_user_guid => :environment do
		begin
			count = 0
			Rake::Task[:environment].invoke
			Rails.logger.info "Starting to add user_guid in event notifications"
              event_subscriptions = EventSubscription.sort(:email).all
		      event_subscriptions.each do |sub|
		        begin
		          user = User.first(:email => { :$regex => /^#{sub.email}$/i })
		          if user.present? and user.guid.present?
		           sub.update_attribute(:user_guid, user.guid)
		           count +=1  
		           puts "Adding guid #{user.guid} for user #{sub.email}"
		          else
		          	sub.update_attribute(:active, false)
		          	puts "User not found having email #{sub.email}"
		          end
		        rescue Exception => e
		          Rails.logger.error "Error occured while updating user -  #{e.backtrace.to_s}" 
		        end
		      end    
		    puts "-------------------------------------------------------------"     
			puts "Successfully added user_guid in #{count}  event notifications"
		rescue
			Rails.logger.error $!
			raise
		end			
	end	
end
