Integrating otrc and VP4
========================

Deploying, using and accessing otrc components in VP4

Libraries
---------
The following libraries are requiered and manually placed into  
vendor/assets/javascript folder. As we using require.js, we need to follow AMD  
pattern library. We download Babel loader plugin for RequireJS, to ensure that    
ES6 code can be used.  

* React https://npmcdn.com/react@15.3.0/dist/react.min.js
* React With Addones https://npmcdn.com/react@15.3.0/dist/react-with-addons.js
* React Dom https://npmcdn.com/react-dom@15.3.0/dist/react-dom.js
* Axios https://cdnjs.cloudflare.com/ajax/libs/axios/0.13.1/axios.js
* Babel	https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/browser.min.js
* ES6 https://github.com/mikach/requirejs-babel/blob/master/es6.js

Installation
------------
* Take all file from ot-react-components's dist folder and paste Viewpoint project as per Rails convention
* ot-react-components.js into vendor/assets/javascripts
* ot-react-component.css into vendor/assets/stylesheets
* copy fonts and images from assets folder and place into vendor/assets/portal

Running
-------
* On initial page loading, we are loading all libraries and "ot-react-components.js".
* header.js and footer.js is containing Header and Footer related es6 code.
* All libraries need to imports in header.js file. eg: react, react-dom, axios.
* Axios is using for fetching api data.

Header file can be found at:   https://bitbucket.org/mountdiablo/viewpoint/src/7a8a8601612e4af1fd01165b838a25a3f530ad07/vendor/assets/javascripts/header.js?at=feature%2Fportal&fileviewer=file-view-default
