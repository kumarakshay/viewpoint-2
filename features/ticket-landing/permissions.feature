Feature: Ticket Landing
  In order to see ticket information
       A Viewpoint user
  Should be able to sign in through SSO
     And See ticket landing as their home page
     And Deep Links are permission sensitive

Scenario: Deep Links are wrapped in permissions
  Given I exist as user with Grant Site Access
   Then I should be allowed to grant Site Access to Other Users

Scenario: Deep Links are wrapped in permissions
     Given I exist as user with Site Access
      Then I should have access to a particular site
      And It was granted on a particular start date
      And It should have a particular end date

Scenario: Deep Links are wrapped in permissions
  Given I exist as customer user
   Then I should have access to notification matrix
    But Only admin can create or edit notification matrix

Scenario: Deep Links are wrapped in permissions
  Given I exist as customer user with Admin authorization in SN
   Then I should have access to all three

Scenario: Deep Links are wrapped in permissions
     Given I exist as customer user with Grant Site Access Authorization
      Then I should ONLY have access to Site Access Request

Scenario: Deep Links are wrapped in permissions
    Given I exist as customer without Grant Site Access Authorization
       Or Admin Authorization
     Then I would not have access to all three

You can impersonate these users to see the difference:
  Aniket Bodhankar no authorizations
  Ashok Bhatia Grant Site Access, but not Admin
  Jennifer Wang Admin access
