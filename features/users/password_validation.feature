Feature: Validate Password
  In order to get successfull signup a user password must be validated
  
Scenario: User has valid password 
  Given User with valid password
  When passwords will contain at least (1) upper case letter
   And passwords will contain at least (1) lower case letter
   And passwords will contain at least (1) number 
   And passwords will contain at least (8) characters in length
   And password can have special characters 
  Then password should be allowed and able to set 

Scenario: User has invalid password 
  Given User with invalid password
  When passwords will not contain at least (1) upper case letter
   And passwords will not contain at least (1) lower case letter
   And passwords will not contain at least (1) number 
   And passwords will not contain at least (8) characters in length
  Then password should not be allowed 
   And user is not able to set this type of password  