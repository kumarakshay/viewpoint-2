Feature: Roles
  In order to get access to protected areas of the site
  A user
  Should have roles

    Scenario: User has sungard_admin role
      Given I exist as a user
        And I have the sungard_admin role
      When I sign in with valid credentials
      Then I should see the Sungard/Viewpoint Admin link under the gear icon

    Scenario: User goes to the admin page
      Given I have the role sungard_admin
      When I access the sungard_admin page
      Then I should see links for company management

    Scenario: User has viewpoint_admin role
      Given I exist as a user
        And I have the viewpoint_admin role
      When I sign in with valid credentials
      Then I should see the Sungard/Viewpoint Admin link under the gear icon
  
    Scenario: User goes to the admin page
      Given I have the role viewpoint_admin
      When I access the viewpoint_admin page
      Then I should see link for company management
      Then I should see link for roles
      Then I should see link for help management
      Then I should see link for statements
