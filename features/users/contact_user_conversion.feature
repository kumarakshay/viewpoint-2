Feature: Convert non-portal user to portal user
  In order to convert non-portal user to portal user Admin privilege required 

Scenario: User logged in as a viewpoint/SunGard admin OR company admin
  Given I exist as a user and I have the viewpoint/sungard admin role
   When I sign in with valid credentials
   Then I should see all users
   When I logged in as a viewpoint/SunGard admin and go to non-portal users listing page for sungard company
   Then It will take me to home page with 'Unauthorized Access' message
   When I go to non-portal users listing page for non-sungard company
   Then I can see all the user information along with "convert to portal user" button
   When I click on "convert to portal user" button
   Then It should ask me for confirmation
   When I confirm "convert to portal user" action 
   Then It will take me to create portal user form with prefetched information
    And I should be able to modify any information here before saving it
   When I click on save button
   Then this information is validated
   When validation fails
   Then I can see validation errors on form 
   When Validation passes
   Then I can see success message on screen with user is registered in Viewpoint as 
   When user confirm his registration
   Then He can able to login to viewpoint using his credentials 
   When I convert non-portal user to portal user
   Then I am not able to see that user in non-portal users listing page

Feature: Convert portal user to non-portal user
  In order to convert portal user to non-portal user Admin privilege required 

Scenario: User logged in as a viewpoint, SunGard admin OR company admin
  Given I exist as a user and I have the viewpoint, sungard or company admin role
   When I sign in with valid credentials
   Then I should see the portal and non-portal users
   When I go to portal users listing page for sungard company
   Then I can see all the user information without "convert to non-portal user" button 
   When I go to portal users listing page for non-sungard company
   Then I can see all the user information along with "convert to non-portal user" button
   When I click on "convert to non-portal user" button
   Then It should ask me for confirmation
   When I confirm "convert to non-portal user" action 
   Then It will take me to create non-portal user form
    And I should be able to modify any information
   When I click on save button
   Then the form is validated
   When validation fails
   Then I can see validation errors on form 
   When Validation passes
   Then I can will see the success message 
    And His account is deleted from Viewpoint and OpenID 
   When Portal user converted from portal to non-portal
   Then I am not able to see that user in portal user listing page
    And I can see that user in non-portal users listing page

