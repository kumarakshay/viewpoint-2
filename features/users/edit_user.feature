Feature: Edit User Profile
  In order to get access to edit profile of other user 
  A user
  Should have roles higher than other user
  
Scenario: User has viewpoint_admin role 
  Given I exist as a user and I have the viewpoint_admin role
  When I sign in with valid credentials
  Then I should see the Viewpoint admin link under the gear icon

Scenario: User goes to the user management page of non sungard company 
  Given I have the role viewpoint_admin
  When I access the user_management page of any non sungard company
  Then I should see list of users for that company
  When I access the edit profile page of any user
  Then I should see user profile form and update any information
    
Scenario: User goes to the user management page of sungard company 
  Given I have the role viewpoint_admin
  When I access the user_management page of sungard company
  Then I should see list of users for sungard company
  When I access the edit profile page of any user
  Then I should see user profile form and update any information
      
Scenario: User has sungard_admin role
  Given I exist as a user and I have the sungard_admin role
  When I sign in with valid credentials
  Then I should see the Sungard admin link under the gear icon

Scenario: User goes to the user management page of non sungard company 
  Given I have the role sungard_admin
  When I access the user_management page of any company
  Then I should see list of users for that company
  When I access the edit profile page of any user
  Then I should see user profile form and update any information
    
Scenario: User goes to the user management page of sungard company 
  Given I have the role sungard_admin
  When I access the user_management page of sungard company
  Then I should see list of users for sungard company
  When I access the edit profile page of user who has role lower or equal to my role
  Then I should see user profile form and update any information
  When I access the edit profile page of user who has higher role than my role
  Then I should not able to see edit profile page and will see the message 'You are not authorized to edit this user profile!'