Feature: Firewall Log. 
  Displayed firewall log form splunk search

  Scenario: User wants to see firewall log
  	Given I exist as a company user/sungard any type of user
    When I log into the application 
    And I navigate to the logs/firewall page
    Then I should be able to see the 2 days firewall log
    And I can filter the data putting value into text field
    